<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>New Entry Requirement</h3>
        </div>
        <form id="form_applicant" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Entry Requirement Details</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" id="from_dt" name="from_dt" class="form-control datepicker" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" id="to_dt" name="to_dt" class="form-control datepicker" autocomplete="off">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" id="description" name="description" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>

                   
            </div>            
            
            
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>

        </form>
























    <form id="form_programgrade" action="" method="post">
  
   <div>
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Requirements</a>
            </li>
            <!-- <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Other Requirements</a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Proposed Program</a>
            </li> -->
        </ul>
        <div class="tab-content offers-tab-content">
          <div role="tabpanel" class="tab-pane active" id="education">
            <div class="col-12 mt-4">
                <br>

            <form id="form_requirement_detail" action="" method="post">

            <div class="form-container">
            <h4 class="form-group-title">Requirements</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Group <span class='error-text'>*</span></label>
                            <select name="id_group" id="id_group" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($groupList))
                                {
                                    foreach ($groupList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="requirement_description" name="requirement_description">
                        </div>
                    </div>

                    
                </div>

            </div>

           </form>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg" onclick="saveRequirements()">Save</button>
                </div>
            </div>


            <div class="form-container" id="view_requirement_visible" style="display: none;">
            <h4 class="form-group-title">Requirements List</h4>
                <div id="view_requirement">
                </div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="proficiency">
            <div class="col-12 mt-4">
                <br>


        <form id="form_other_requirement" action="" method="post">

            <div class="form-container">
            <h4 class="form-group-title">Other Requirements</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Group <span class='error-text'>*</span></label>
                            <select name="id_other_group" id="id_other_group" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($groupList))
                                {
                                    foreach ($groupList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Condition <span class='error-text'>*</span></label>
                            <select name="other_condition" id="other_condition" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <option value="Equal To">Equal To</option>
                                <option value="Greater Than">Greater Than</option>
                                <option value="Greater Than Or Equal To">Greater Than Or Equal To</option>
                                <option value="Smaller Than">Smaller Than</option>
                                <option value="Smaller Than Or Equal To">Smaller Than Or Equal To</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type <span class='error-text'>*</span></label>
                            <select name="other_type" id="other_type" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <option value="AGE">AGE</option>
                                <option value="Working Experience">Working Experience</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Result Value</label>
                            <input type="text" class="form-control" id="other_result_value" name="other_result_value" >
                        </div>
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg" onclick="saveOtherRequirements()">Save</button>
                </div>
            </div>

        </form>


        <div class="form-container" id="view_other_requirement_visible" style="display: none;">
            <h4 class="form-group-title">Other Requirements List</h4>
                <div id="view_other_requirement">
                </div>
        </div>

    
             
         </div> <!-- END col-12 -->  
        </div>


        <div role="tabpanel" class="tab-pane" id="employment">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Proposed Program</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">

                        <a  onclick="opendialog()"> Set Proposed Program</a>
                        </div>
                    </div>

                </div>

            </div>
                
           <!--  <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                </div>
            </div> -->

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>  
            <th>Company Name</th>
            <th>Company Address</th>
            <th>Designation</th>
            <th>Position</th>
            <th>Year Of Service</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($employmentDetails))
          {
            foreach ($employmentDetails as $record) {
          ?>
              <tr>
                <td><?php echo $record->company_name ?></td>
                <td><?php echo $record->company_address ?></td>
                <td><?php echo $record->designation ?></td>
                <td><?php echo $record->position ?></td>
                <td><?php if($record->service_year == ""){ echo "0";} else {
                    echo $record->service_year; } ?></td>
                <td class="text-center"><?php echo anchor('admission/student/delete_employment?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>


      </div>
    </div>

   </div> <!-- END row-->
   

            <!-- <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div> -->
        </form>















        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Proposed Proram</h4>
              </div>
              <div class="modal-body">
                 <h4></h4>

            <form id="form_proposed_proram" action="" method="post">


            <div class="form-container">
            <h4 class="form-group-title">Proposed Program</h4>
                <div class="row">


                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>  
                            <th class="text-center"><input type="checkbox" id="" name="">Check All</th>
                            <th>Proram Code</th>
                            <th>Program Name</th>
                            <th class="text-center">Award</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($programList))
                          {
                            foreach ($programList as $record) {
                          ?>
                              <tr>
                                <td class="text-center" value="<?php echo $record->id ?>"><input type="checkbox" id="is_submitted" name="is_submitted">
                                </td>
                                <td><?php echo $record->code ?></td>
                                <td><?php echo $record->name ?></td>
                                <td class="text-center"><?php echo $record->award_code . " - " . $record->award_name ?></td>
                              </tr>
                          <?php
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>


                </div>

            </div>

            </form>


              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-default" onclick="saveData()">Add</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

















        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    function opendialog()
    {
        // if($('#form_pr_entry').valid())
        // {
            $('#myModal').modal('show');
        // }
    }

    function saveRequirements()
    {
        // alert('sda');
        // if($('#form_requirement_detail').valid())
        // {
            // alert('sda');
            if($('#id_group').val() != '' && $('#requirement_description').val() != '')
            {

            var tempPR = {};
            tempPR['id_group'] = $("#id_group").val();
            tempPR['description'] = $("#requirement_description").val();
            // alert(tempPR['id_group']);

            $.ajax(
            {
               url: '/admission/programEntryRequirement/tempAddTempRequirements',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_requirement").html(result);
                if(result != '')
                {
                    $("#view_requirement_visible").show();
                }else
                {
                    $("#view_requirement_visible").hide();
                }
               }
            });
            }
        // }
    }



    $(document).ready(function() {
        $("#form_requirement_detail").validate({
            rules: {
                id_group: {
                    required: true
                },
                 requirement_description: {
                    required: true
                }
            },
            messages: {
                id_group: {
                    required: "<p class='error-text'>Select Group</p>",
                },
                requirement_description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function deleteTempRequirementData(id)
     {
        $.get("/admission/programEntryRequirement/deleteTempRequirementData/"+id,
        function(data, status)
        {
            if(data != '')
                {
                    $("#view_requirement_visible").show();
                }else
                {
                    $("#view_requirement_visible").hide();
                }
            $("#view_requirement").html(data);
        });
     }











     

     function saveOtherRequirements()
     {
        // alert('sda');
        // if($('#form_requirement_detail').valid())
        // {
            // alert('sda');
            var tempPR = {};
            tempPR['id_group'] = $("#id_other_group").val();
            tempPR['condition'] = $("#other_condition").val();
            tempPR['type'] = $("#other_type").val();
            tempPR['result_value'] = $("#other_result_value").val();
            // alert(tempPR['id_group']);

            $.ajax(
            {
               url: '/admission/programEntryRequirement/tempAddOtherRequirements',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_other_requirement").html(result);
                if(result != '')
                {
                    $("#view_other_requirement_visible").show();
                }else
                {
                    $("#view_other_requirement_visible").hide();
                }
               }
            });
        // }
     }

     function deleteTempOtherRequirementData(id)
     {
        $.get("/admission/programEntryRequirement/deleteTempOtherRequirementData/"+id,
        function(data, status)
        {
            if(data != '')
                {
                    $("#view_other_requirement_visible").show();
                }else
                {
                    $("#view_other_requirement_visible").hide();
                }
            $("#view_other_requirement").html(data);
        });
     }




    $(document).ready(function() {
        $("#form_applicant").validate({
            rules: {
                id_program: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 to_dt: {
                    required: true
                },
                 description: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );
</script>
<script>
    function showSibblingFields(){
            $("#sibbling").show();
    }

    function hideSibblingFields(){
            $("#sibbling").hide();
    }

    function showEmployeeFields(){
            $("#employee").show();
    }

    function hideEmployeeFields(){
            $("#employee").hide();
    }
</script>
