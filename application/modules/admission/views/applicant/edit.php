<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Applicant</h3>
        </div>
        <form id="form_applicant" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Applicant Details</h4>            
            
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($salutationList)) {
                                    foreach ($salutationList as $record) {
                                ?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php if($getApplicantDetails->salutation==$record->id)
                                            {
                                                echo "selected=selected";
                                            }
                                            ?>
                                            >
                                            <?php echo $record->name;  ?>        
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Password <span class='error-text'>*</span></label>
                            <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender">
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status</label>
                            <select class="form-control" id="martial_status" name="martial_status">
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getApplicantDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getApplicantDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span></label>
                            <select name="religion" id="religion" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($religionList))
                                {
                                    foreach ($religionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->religion)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion</label>

                            <select name="religion" id="religion" class="form-control">
                            <option value="">Select</option>
                            <option value="<?php echo $getApplicantDetails->religion;?>"
                                <?php 
                                if ($getApplicantDetails->religion == 'Islam')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Islam";  ?>
                            </option>

                            <option value="<?php echo $getApplicantDetails->religion;?>"
                                <?php 
                                if ($getApplicantDetails->religion == 'Buddhism')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Buddhism";  ?>
                            </option>

                            <option value="<?php echo $getApplicantDetails->religion;?>"
                                <?php 
                                if ($getApplicantDetails->religion == 'Christianity')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Christianity";  ?>
                            </option>

                            <option value="<?php echo $getApplicantDetails->religion;?>"
                                <?php 
                                if ($getApplicantDetails->religion == 'Hinduism')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Hinduism";  ?>
                            </option>

                            <option value="<?php echo $getApplicantDetails->religion;?>"
                                <?php 
                                if ($getApplicantDetails->religion == 'Other')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Other";  ?>
                            </option>
                            
                        </select>

                        </div>
                    </div> -->

                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Type Of Nationality</label>

                            <select name="nationality" id="nationality" class="form-control">
                                <option value="">Select</option>
                                <option value="<?php echo $getApplicantDetails->nationality;?>"
                                    <?php 
                                    if ($getApplicantDetails->nationality == 'Malaysian')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "Malaysian";  ?>
                                </option>

                                <option value="<?php echo $getApplicantDetails->nationality;?>"
                                    <?php 
                                    if ($getApplicantDetails->nationality == 'Other')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "Other";  ?>
                                </option>
                            </select>


                            </div>
                        </div>
                </div>
                
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($raceList))
                                {
                                    foreach ($raceList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_race)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label> Race </label>
                            <select name="id_race" id="id_race" class="form-control">
                                <option value="">Select</option>
                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'Bhumiputra')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "Bhumiputra";  ?>
                                </option>

                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'International')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "International";  ?>
                                </option>
                            </select>
                        </div>
                    </div> -->


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label> Intake</label>
                            <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_intake==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php if($getApplicantDetails->id_program==$record->id)
                                    { 
                                        echo "selected";
                                    } 
                                    ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-container">
                <h4 class="form-group-title">Mailing Address</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Address 1</label>
                            <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Address 2</label>
                            <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Country</label>
                            <select name="mailing_country" id="mailing_country" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing State</label>
                            <select name="mailing_state" id="mailing_state" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing City</label>
                            <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Zipcode</label>
                            <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="form-container">
                <h4 class="form-group-title">Permanent Address</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Address 1</label>
                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>">
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Address 2</label>
                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Country</label>
                            <select name="permanent_country" id="permanent_country" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent State</label>
                            <select name="permanent_state" id="permanent_state" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent City</label>
                            <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Zipcode</label>
                            <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-container">
                <h4 class="form-group-title">Other Details</h4>            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Do you have sibbling/s studying with university?</p>
                        <label class="radio-inline">
                          <input type="radio" name="sibbling_discount" id="sd1" value="Yes" onclick="showSibblingFields()" <?php if($getApplicantDetails->sibbling_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="sibbling_discount" id="sd2" value="No" onclick="hideSibblingFields()" <?php if($getApplicantDetails->sibbling_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>
            </div>
          <!--   <div class="row" id="sibbling" style="display: none;">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $getApplicantDetails->sibbling_name ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC</label>
                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $getApplicantDetails->sibbling_nric ?>">
                    </div>
                </div>
            </div> -->


            <?php
            if($getApplicantDetails->sibbling_discount=='Yes')
            {
                ?>

                <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC</label>
                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>">
                    </div>
                </div>
            </div> 

            <?php
            }
             ?> 


             
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Do you eligible for Employee discount</p>
                        <label class="radio-inline">
                          <input type="radio" name="employee_discount" id="ed1" value="Yes" onclick="showEmployeeFields()" <?php if($getApplicantDetails->employee_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="employee_discount" id="ed2" value="No" onclick="hideEmployeeFields()" <?php if($getApplicantDetails->employee_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>
            </div>
           

           <?php
            if($getApplicantDetails->employee_discount=='Yes')
            {
                ?>

                <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC</label>
                        <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>">
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation</label>
                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>">
                    </div>
                </div>
            </div> 

            <?php
            }
             ?>

            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 martial_status: {
                    required: true
                },
                 religion: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
     

  } );
  </script>
<script>
    var sd1 = $("#sd1").val();
    if(sd1 =='Yes'){
            $("#sibbling").show();
    }

    if($("#sd1").prop("checked") == true){
        $("#sibbling").show();
        }
    if($("#sd2").prop("checked") == true){
            $("#sibbling").hide();
        }
    

    var ed1 = $("#ed1").val();
    if(ed1 =='Yes'){
            $("#employee").show();
    }
    var ed2 = $("#ed2").val();
    if(ed2 =='No'){
            $("#employee").hide();
    }

    if($("#ed1").prop("checked") == true){
        $("#employee").show();
    }

    if($("#ed2").prop("checked") == true){
        $("#employee").hide();
    }

    function showSibblingFields(){
            $("#sibbling").show();
    }

    function hideSibblingFields(){
            $("#sibbling").hide();
    }

    function showEmployeeFields(){
            $("#employee").show();
    }

    function hideEmployeeFields(){
            $("#employee").hide();
    }
</script>