<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GroupSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('group_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('group_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['groupSetupList'] = $this->group_setup_model->groupSetupListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : List Group Setup';
            //print_r($subjectDetails);exit;
            $this->loadViews("group_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('group_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
            
                $duplicate_row = $this->group_setup_model->checkGroupSetupDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Group Setup Not Allowed";exit();
                }

                $result = $this->group_setup_model->addNewGroupSetup($data);
                redirect('/admission/groupSetup/list');
            }
            
            $this->global['pageTitle'] = 'Campus Management System : Add Group Setup';
            $this->loadViews("group_setup/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('group_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/groupSetup/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                $duplicate_row = $this->group_setup_model->checkGroupSetupDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Group Setup Not Allowed";exit();
                }
                
                $result = $this->group_setup_model->editGroupSetup($data,$id);
                redirect('/admission/groupSetup/list');
            }
            $data['groupSetup'] = $this->group_setup_model->getGroupSetup($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Group Setup';
            $this->loadViews("group_setup/edit", $this->global, $data, NULL);
        }
    }
}
