<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class QualificationSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('qualification_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('qualification_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['qualificationSetupList'] = $this->qualification_setup_model->qualificationSetupListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : List Qualification Setup';
            //print_r($subjectDetails);exit;
            $this->loadViews("qualification_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('qualification_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
            
                $duplicate_row = $this->qualification_setup_model->checkQualificationSetupDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Qualification Setup Not Allowed";exit();
                }

                $result = $this->qualification_setup_model->addNewQualificationSetup($data);
                redirect('/admission/qualificationSetup/list');
            }
            
            $this->global['pageTitle'] = 'Campus Management System : Add Qualification Setup';
            $this->loadViews("qualification_setup/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('qualification_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/qualificationSetup/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                $duplicate_row = $this->qualification_setup_model->checkQualificationSetupDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Qualification Setup Not Allowed";exit();
                }
                
                $result = $this->qualification_setup_model->editQualificationSetup($data,$id);
                redirect('/admission/qualificationSetup/list');
            }
            $data['qualificationSetup'] = $this->qualification_setup_model->getQualificationSetup($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Qualification Setup';
            $this->loadViews("qualification_setup/edit", $this->global, $data, NULL);
        }
    }
}
