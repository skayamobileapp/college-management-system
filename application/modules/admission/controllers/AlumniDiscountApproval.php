<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AlumniDiscountApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alumni_discount_approval_model');
        $this->load->model('setup/country_model');
        $this->load->model('setup/state_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('alumni_discount_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->alumni_discount_approval_model->intakeList();
            $data['programList'] = $this->alumni_discount_approval_model->programList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));


            $data['searchParam'] = $formData;

            $data['alumniList'] = $this->alumni_discount_approval_model->alumniList($formData);

            $this->global['pageTitle'] = 'Campus Management System : Employee Discount Approval';
            // echo "<Pre>";print_r($data['alumniList']);exit;
            $this->loadViews("alumni_discount_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('alumni_discount_approval.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/alumniDiscountApproval/list');
            }

            $data['alumniDiscountDetails'] = $this->alumni_discount_approval_model->getAlumniDiscountDetails($id);

            $id_applicant = $data['alumniDiscountDetails']->id_applicant;
            // echo "<Pre>";print_r($id_applicant);exit();


            if($this->input->post())
            {
            $id_user = $this->session->userId;
            $id_session = $this->session->session_id;


            
            $alumni_status = $this->security->xss_clean($this->input->post('alumni_status'));
            $reason = $this->security->xss_clean($this->input->post('reason'));
            $data = array(
                    'alumni_status' => $alumni_status,
                    'rejected_by' => $id_user,
                    'rejected_on' => date('Y-m-d H:i:s'),
                    'reason' => $reason
                );
                $result = $this->alumni_discount_approval_model->editAlumniDiscountDetails($data,$id);
                if($result)
                {
                    if($alumni_status == 'Approved')
                    {
                        $master_data = array('is_alumni_discount' => 1);
                    }
                    elseif($alumni_status == 'Reject')
                    {
                        $master_data = array('is_alumni_discount' => 2);
                    }
                    $updated_applicant = $this->alumni_discount_approval_model->editApplicantDetails($master_data,$id_applicant);
                }
                redirect('/admission/alumniDiscountApproval/list');
            }
            
            $data['raceList'] = $this->alumni_discount_approval_model->raceList();
            $data['religionList'] = $this->alumni_discount_approval_model->religionList();
            $data['salutationList'] = $this->alumni_discount_approval_model->salutationListByStatus('1');
            $data['degreeTypeList'] = $this->alumni_discount_approval_model->qualificationList();
            $data['countryList'] = $this->country_model->countryList();
            $data['stateList'] = $this->state_model->stateList();
            $data['programList'] = $this->alumni_discount_approval_model->programList();
            $data['intakeList'] = $this->alumni_discount_approval_model->intakeList();
            $data['branchList'] = $this->alumni_discount_approval_model->branchListByStatus();
            $data['requiremntListList'] = $this->alumni_discount_approval_model->programRequiremntListList();
            $data['partnerUniversityList'] = $this->alumni_discount_approval_model->getUniversityListByStatus('1');
            $data['schemeList'] = $this->alumni_discount_approval_model->schemeListByStatus('1');
            $data['programStructureTypeList'] = $this->alumni_discount_approval_model->programStructureTypeListByStatus('1');



            
            $data['programEntryRequirementList'] = $this->alumni_discount_approval_model->programEntryRequirementList($data['alumniDiscountDetails']->id_program);
            $data['programDetails'] = $this->alumni_discount_approval_model->getProgramDetails($data['alumniDiscountDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->alumni_discount_approval_model->getApplicantUploadedFiles($id_applicant);

            // echo "<Pre>";print_r($data['alumniDiscountDetails']);exit();
            $this->global['pageTitle'] = 'Campus Management System : Edit Employee Discount Approval';
            $this->loadViews("alumni_discount_approval/edit", $this->global, $data, NULL);
        }
    }
}

