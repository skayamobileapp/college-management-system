<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Specialization_setup_model extends CI_Model
{
    function specializationSetupList()
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function specializationSetupListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getSpecializationSetup($id)
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkSpecializationSetupDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkSpecializationSetupDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSpecializationSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('specialization_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSpecializationSetup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('specialization_setup', $data);
        return TRUE;
    }
}

