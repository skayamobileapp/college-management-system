<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Group_setup_model extends CI_Model
{
    function groupSetupList()
    {
        $this->db->select('*');
        $this->db->from('group_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function groupSetupListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('group_setup');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getGroupSetup($id)
    {
        $this->db->select('*');
        $this->db->from('group_setup');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkGroupSetupDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('group_setup');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkGroupSetupDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('group_setup');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGroupSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('group_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGroupSetup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('group_setup', $data);
        return TRUE;
    }
}

