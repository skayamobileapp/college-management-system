<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Qualification_setup_model extends CI_Model
{
    function qualificationSetupList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function qualificationSetupListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getQualificationSetup($id)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkQualificationSetupDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkQualificationSetupDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewQualificationSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('qualification_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editQualificationSetup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('qualification_setup', $data);
        return TRUE;
    }
}

