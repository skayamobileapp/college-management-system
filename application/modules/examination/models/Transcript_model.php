<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Transcript_model extends CI_Model
{
    function courseRegistrationList($applicantList)
    {
        $this->db->select(' a.*, std.nric, std.email_id, in.id as id_intake, in.name as intake, p.id as id_programme, p.code as program_code, p.name as program, std.full_name');
        $this->db->from('course_register as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(std.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(std.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(std.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(std.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(std.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
         // $this->db->where('a.is_bulk_withdraw', '0');
         // $this->db->where('a.is_exam_registered', '0');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function examCenterListSearch($search)
    {
        // $date = 
        $this->db->select('*');
        $this->db->from('exam_center');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSemesterRegistrationByStudent($id_intake,$id_programme,$id_student)
    {
        $this->db->select('Distinct(sem.code) as SemeserCode, sem.*');
        $this->db->from('course_registration as a');
        $this->db->join('semester as sem', 'a.id_semester = sem.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_programme', $id_programme);
        $this->db->where('a.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->result();
        return $result; 
    }

    function getCourseRegistrationByStudent($id_intake,$id_programme,$id_student,$id_semester)
    {
        $this->db->select('a.*, c.name as course_name, c.code as course_code, c.credit_hours, sem.name as semester_name, sem.code as semester_code, sem.start_date, sem.end_date');
        $this->db->from('course_registration as a');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('semester as sem', 'a.id_semester = sem.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_programme', $id_programme);
        $this->db->where('a.id_student', $id_student);
        $this->db->where('a.id_semester', $id_semester);

        $query = $this->db->get();
        $result = $query->result();
        return $result; 
    }


    function getGPACount($id_intake,$id_programme,$grade){
        $this->db->select('a.*');
        $this->db->from('grade_setup_details as a');
        $this->db->join('grade_setup as b', 'a.id_grade_setup = b.id');
        $this->db->where('b.id_program', $id_programme);
        $this->db->where('a.id_grade', $grade);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegistrationByStudentId($id_student)
    {
        $this->db->select('a.*, c.name as course_name, c.code as course_code, c.credit_hours, sem.name as semester_name, sem.code as semester_code, sem.start_date, sem.end_date');
        $this->db->from('course_registration as a');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('semester as sem', 'a.id_semester = sem.id');
        $this->db->where('a.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->result();
        return $result; 
    }


    function getCoursePerogramLandscape($id)
    {
        $this->db->select('a.id, b.pre_requisite, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');

        $this->db->from('courses_from_programme_landscape as a');
        $this->db->join('add_course_to_program_landscape as b', 'a.id_course_programme = b.id');
        $this->db->join('course as c', 'b.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'b.id_program_landscape = pl.id');
        $this->db->where('a.id_course_registration', $id);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegistrationList($id)
    {
        $this->db->select('*');
        $this->db->from('course_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function getCourseRegistered($id)
    {
         $this->db->select('*');
        $this->db->from('course_register');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function studentList($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addCoureRegister($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_register', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addCoureRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCoureRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_registration', $data);
        return TRUE;
    }

    function addCourseList($array, $insert_id)
    {
        foreach ($array as $row)
        {
          $data = ['id_course_programme'=>$row];
          $this->db->insert('courses_from_programme_landscape', $data);
        }
        $data = ['id_course_registration'=>$insert_id];
        $this->db->where_in('id_course_programme', $array);
        $this->db->update('courses_from_programme_landscape', $data);
        return TRUE;
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }

    function getStudentByProgNIntake($data)
    {
        $this->db->select('DISTINCT(er.id) as id, er.*');
        $this->db->from('student as er');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
         return $results;
    }

    function getStudentsByCourseLandscapeFromCourseRegistration($data)
    {
        $this->db->select('DISTINCT(a.id_student) as id_student, s.*');
        $this->db->from('course_registration as a');
        $this->db->join('student as s', 'a.id_student = s.id');
        // $this->db->where('a.id_course_registered_landscape', $data['id_course_registered_landscape']);
        $this->db->where('a.is_exam_registered !=', 0);
        $this->db->where('a.is_bulk_withdraw', 0);
        if($data['id_intake'])
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        if($data['id_programme'])
        {
            $this->db->where('a.id_programme', $data['id_programme']);
        }
        if($data['name'])
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'])
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['email_id'])
        {
            $likeCriteria = "(s.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }

         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getCourseRegisteredByProgNIntake($data)
    {
        $this->db->select('DISTINCT(er.id_course_registered_landscape) as id');
        $this->db->from('course_registration as er');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_programme', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
         $query = $this->db->get();
         $results = $query->result();  

         $details = array();
         foreach ($results as $value)
         {
            $course = $this->getCourseByLandscapeId($value->id);
            $course->id_course_registered_landscape = $value->id;
            array_push($details, $course);
         }

         return $details;
    }

    function getCourseByLandscapeId($id)
    {
        $this->db->select('c.*');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->join('course as c', 's.id_course = c.id'); 
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getProgrammeListByStudentId($id_student)
    {
        $this->db->select(' DISTINCT(a.id_programme) as id, p.code as program_code, p.name as program, std.full_name');
        $this->db->from('course_registration as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->where('a.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getIntakeListByStudentId($id_student)
    {
        $this->db->select(' DISTINCT(a.id_intake) as id, in.name as intake_name');
        $this->db->from('course_registration as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->where('a.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getCourseRegistrationByMasterId($id)
    {
         // print_r($id_intake);exit();
        $this->db->select('a.by_student, cou.id as id_course, cou.name, cou.name_in_malay, cou.code, acpl.pre_requisite, cou.credit_hours, rl.name as role');
        $this->db->from('course_registration as a');
        $this->db->join('course as cou', 'a.id_course = cou.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
        $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('users as rl', 'a.created_by = rl.id','left');
        $this->db->where('a.id_course_register', $id);
        // $this->db->where('a.is_exam_registered', '0');
        // $this->db->where('a.is_bulk_withdraw', '0');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student,$id_semester)
    {
         // print_r($id_intake);exit();
        $this->db->select('a.by_student, cou.id as id_course, cou.name, cou.name_in_malay, cou.code, acpl.pre_requisite, cou.credit_hours, rl.name as role');
        $this->db->from('course_registration as a');
        $this->db->join('course as cou', 'a.id_course = cou.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
        $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('users as rl', 'a.created_by = rl.id','left');
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->where('pl.id_intake', $id_intake);
        $this->db->where('a.id_semester', $id_semester);
        $this->db->where('a.id_programme', $id_programme);
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_student', $id_student);
        $this->db->where('a.is_exam_registered', '0');
        $this->db->where('a.is_bulk_withdraw', '0');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getCoursesIdByLandscapeIdForDetailsAdd($id)
    {
        $this->db->select('s.*');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('programme as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('intake as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function studentListByStatus()
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function semesterDetails($id)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function generateNewMainInvoiceForCourseRegistration($datas,$id_student,$id_semester)
    {

        // echo "<Pre>";print_r($datas);exit();
        
        $user_id = $this->session->userId;
        $student_data = $this->getStudent($id_student);

        $id_student = $student_data->id;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;
        $nationality = $student_data->nationality;
        $id_program_scheme = $student_data->id_program_scheme;
        $id_branch = $student_data->id_branch;


        $get_data['id_intake'] = $id_intake;
        $get_data['id_programme'] = $id_program;
        $get_data['id_programme_scheme'] = $id_program_scheme;


        // echo "<Pre>";print_r($get_data);exit();
        $programme_landscape = $this->getProgramLandscapeByData($get_data);

        $id_programme_landscape = $programme_landscape->id;


        // echo "<Pre>";print_r($id_branch);exit();





        $is_installment = 0;
        $installments = 0;

        if($id_branch != 1)
        {
            $fee_structure_training_data = $this->getFeeStructureForTrainingCenterInstallment($id_program,$id_intake,$id_program_scheme,$id_branch);


            if($fee_structure_training_data)
            {
                $is_installment = $fee_structure_training_data->is_installment;
                $installments = $fee_structure_training_data->installments;
            }
        }



        if($id_branch == 1 && $is_installment == 0 && $installments == 0)
        {

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';

                $fix_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'MYR','FIX AMOUNT');
                $subject_multiplier_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'MYR','SUBJECT MULTIPLICATION');
                $cr_hr_multiflier_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'MYR','CREDIT HOUR MULTIPLICATION');
            }
            elseif($nationality == 'Other')
            {
                $currency = 'USD';
                $fix_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'USD','FIX AMOUNT');
                $subject_multiplier_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'USD','SUBJECT MULTIPLICATION');
                $cr_hr_multiflier_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'USD','CREDIT HOUR MULTIPLICATION');
            }

            if(isset($fix_amount_data) || isset($subject_multiplier_amount_data) || isset($cr_hr_multiflier_amount_data))
            {


            
                $invoice_number = $this->generateMainInvoiceNumber();

                $invoice['invoice_number'] = $invoice_number;
                $invoice['type'] = 'Student';
                $invoice['remarks'] = 'Student Course Registration';
                $invoice['id_application'] = '';
                $invoice['id_student'] = $id_student;
                $invoice['id_program'] = $id_program;
                $invoice['id_intake'] = $id_intake;
                $invoice['currency'] = $currency;
                $invoice['invoice_total'] = 0;
                $invoice['total_amount'] = 0;
                $invoice['balance_amount'] = 0;
                $invoice['paid_amount'] = 0;
                $invoice['status'] = '1';
                $invoice['created_by'] = $user_id;
                
                // echo "<Pre>";print_r($detail_data);exit;
                $inserted_id = $this->addNewMainInvoice($invoice);



                $total = 0;


                if($fix_amount_data)
                {
                    $detail_data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fix_amount_data->id_fee_item,
                        'amount' => $fix_amount_data->amount,
                        'price' => $fix_amount_data->amount,
                        'quantity' => 1,
                        'description' => 'FIX AMOUNT',
                        'status' => '1',
                        'created_by' => $user_id
                    );

                    $this->addNewMainInvoiceDetails($detail_data);

                    $total = $total + $fix_amount_data->amount;
                }

                if($subject_multiplier_amount_data)
                {
                    $count = count($datas);

                    $amount = $count * $subject_multiplier_amount_data->amount;

                    $detail_data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $subject_multiplier_amount_data->id_fee_item,
                        'amount' => $amount,
                        'price' => $subject_multiplier_amount_data->amount,
                        'quantity' => 1,
                        // 'quantity' => $count,
                        'description' => 'SUBJECT MULTIPLICATION',
                        'status' => '1',
                        'created_by' => $user_id
                    );

                    $this->addNewMainInvoiceDetails($detail_data);
                    $total = $total + $amount;

                }


                if($cr_hr_multiflier_amount_data)
                {
                    foreach ($datas as $data)
                    {
                       
                    
                        $id_course = $data['id_course'];
                        $id_course_registered = $data['id_course_registered'];

                        $course_data = $this->getCourse($id_course);

                        $credit_hours = $course_data->credit_hours;

                        $detail_total = $credit_hours * $cr_hr_multiflier_amount_data->amount;
                   
                        $data = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_item' => $cr_hr_multiflier_amount_data->id_fee_item,
                                'amount' => $detail_total,
                                'price' => $cr_hr_multiflier_amount_data->amount,
                                'quantity' => 1,
                                'id_reference' => $id_course_registered,
                                'description' => 'CREDIT HOUR MULTIPLICATION',
                                'status' => '1',
                                'created_by' => $user_id
                            );

                        $total = $total + $detail_total;

                        $this->addNewMainInvoiceDetails($data);

                    }
                }

                $invoice_update['total_amount'] = $total;
                $invoice_update['balance_amount'] = $total;
                $invoice_update['invoice_total'] = $total;
                $invoice_update['total_discount'] = 0;
                $invoice_update['paid_amount'] = 0;
                // $invoice_update['inserted_id'] = $inserted_id;
                // echo "<Pre>";print_r($invoice_update);exit;
                $this->editMainInvoice($invoice_update,$inserted_id);

            }


        }


        // echo "<Pre>";print_r($installments);exit();

        if($id_branch > 1 && $is_installment != 0 && $installments != 0)
        {
            $currency = 'MYR';

            $fee_structure_training_data = $this->getFeeStructureInstallmentByDataNSemester($id_programme_landscape,$id_branch,$id_semester);


            if($fee_structure_training_data)
            {



                if($fee_structure_training_data)
                {
                    $currency = $fee_structure_training_data->currency;
                }

                // echo "<Pre>";print_r($fee_structure_training_data);exit();


                $amount = $fee_structure_training_data->amount;

                $invoice_number = $this->generateMainInvoiceNumber();

                $invoice['invoice_number'] = $invoice_number;
                $invoice['type'] = 'Student';
                $invoice['remarks'] = 'Student RECCURING AMOUNT INSTALLMENT';
                $invoice['id_application'] = '';
                $invoice['id_student'] = $id_student;
                $invoice['id_program'] = $id_program;
                $invoice['id_intake'] = $id_intake;
                $invoice['currency'] = $currency;
                $invoice['invoice_total'] = $amount;
                $invoice['total_amount'] = $amount;
                $invoice['balance_amount'] = $amount;
                $invoice['paid_amount'] = 0;
                $invoice['status'] = '1';
                $invoice['created_by'] = $user_id;
                
                // echo "<Pre>";print_r($detail_data);exit;
                $inserted_id = $this->addNewMainInvoice($invoice);


                if($inserted_id)
                {
                    $detail_data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure_training_data->id_fee_item,
                        'amount' => $amount,
                        'price' => $amount,
                        'id_reference' => $fee_structure_training_data->id,
                        'quantity' => 1,
                        'description' => 'RECCURING AMOUNT FOR STUDENT INSTALLMENT',
                        'status' => '1',
                        'created_by' => $user_id
                    );

                    $this->addNewMainInvoiceDetails($detail_data);
                }

            }

        }
        return TRUE;
    }


    function getProgramLandscapeByData($data)
    {
        $this->db->select('pl.*');
        $this->db->from('programme_landscape as pl');
        $this->db->where('pl.id_intake', $data['id_intake']);
         $this->db->where('pl.id_programme', $data['id_programme']);
         $this->db->where('pl.program_scheme', $data['id_programme_scheme']);
        $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }


    function getFeeStructureInstallmentByDataNSemester($id_programme_landscape,$id_training_center,$id_semester)
    {
        $this->db->select('p.*, fs.currency, fs.id_fee_item');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_structure as fs', 'p.id_fee_structure = fs.id'); 
        $this->db->where('p.id_program_landscape', $id_programme_landscape);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('p.id_semester', $id_semester);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }


    function getFeeStructure($id_programme,$id_intake,$id_program_scheme,$currency,$code)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->join('amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.currency', $currency);
        $this->db->where('p.id_training_center', 0);
        $this->db->where('fm.code', 'PER SEMESTER');
        $this->db->where('amt.code', $code);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }

    function getFeeStructureForTrainingCenterInstallment($id_programme,$id_intake,$id_program_scheme,$id_training_center)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structure = $query->row();

        return $fee_structure;
    }

    function getCourse($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $course_data = $query->row();

        return $course_data;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }
}