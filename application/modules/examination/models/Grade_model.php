<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grade_model extends CI_Model
{
    function gradeList()
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function gradeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('grade');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or description  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getGradeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGrade($data)
    {
        $this->db->trans_start();
        $this->db->insert('grade', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGradeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('grade', $data);
        return TRUE;
    }
}

