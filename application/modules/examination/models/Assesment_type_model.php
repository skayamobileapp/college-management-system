<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assesment_type_model extends CI_Model
{

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function assesmentTypeList()
    {
        $this->db->select('*');
        $this->db->from('examination_assesment_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function assesmentTypeListSearch($data)
    {
        $this->db->select('eat.*, i.name as intake');
        $this->db->from('examination_assesment_type as eat');
        $this->db->join('intake as i', 'eat.id_intake = i.id');
         if($data['id_intake'] != '')
        {
            $this->db->where('eat.id_intake', $data['id_intake']);
        }
        if($data['id_semester'] != '')
        {
            $this->db->where('eat.id_semester', $data['id_semester']);
        }
        if($data['type'] != '')
        {
            $this->db->where('eat.type', $data['type']);
        }
        if ($data['name'] != '')
        {
            $likeCriteria = "(eat.description  LIKE '%" . $data['name'] . "%' or eat.description_optional_language  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getAssesmentType($id)
    {
        $this->db->select('*');
        $this->db->from('examination_assesment_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAssesmentType($data)
    {
        $this->db->trans_start();
        $this->db->insert('examination_assesment_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAssesmentTypeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('examination_assesment_type', $data);
        return TRUE;
    }
}

