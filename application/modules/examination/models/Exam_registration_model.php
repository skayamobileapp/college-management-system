<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_registration_model extends CI_Model
{
    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function examComponentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('examination_components');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function examRegistrationListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, p.code as program_code, p.name as program_name, i.year as intake_year, i.name as intake_name, ee.from_dt as exam_date, ee.name as event_name, c.code as course_code, c.name as course_name');
        $this->db->from('examination as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('exam_event as ee', 'a.id_exam_event = ee.id');
        $this->db->join('add_course_to_program_landscape as aprl', 'a.id_course_registered_landscape = aprl.id');
        $this->db->join('course as c', 'aprl.id_course = c.id');
        if ($data['id_program'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
         $query = $this->db->get();
         $result = $query->result();  

         // $list = array();
         // foreach ($result as $value)
         // {
         //    $data = $this->getExamCenterNLocationByCenterId($value->id_exam_center);
         //    $value->exam_center = $data['exam_center'];
         //    $value->location = $data['location'];
            
         //    array_push($list, $value);
         // }
         return $result;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('programme_landscape as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getExamination($id)
    {
        $this->db->select('*');
        $this->db->from('examination');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addMarkDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMarkDistribution($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_registration', $data);
        return TRUE;
    }

    function addExamRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addExamination($data)
    {
        $this->db->trans_start();
        $this->db->insert('examination', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
        
    }

    function updateCourseRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_registration', $data);
        return TRUE;
    }

    function getTempMarkDistributionDetailsBySessionId($id_session)
    {
        $this->db->select('tctd.*, ec.name as component_name, ec.code as component_code,');
        $this->db->from('temp_exam_registration_details as tctd');
        $this->db->join('examination_components as ec', 'tctd.id_component = ec.id');
        $this->db->where('tctd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteTempMarkDistribution($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_exam_registration_details');
        return TRUE;
    }


    function deleteTempMarkDistributionBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_exam_registration_details');
        return TRUE;
    }

    function getCourseCreditHours($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();

         if($result)
         {
            return $result->credit_hours;
         }
        return 0;
    }




    function moveDetailDataFromTempToMain($id_exam_registration)
    {
        $id_session = $this->session->my_session_id;
        $details = $this->getTempMarkDistributionDetailsBySessionIdForMove($id_session);
        foreach ($details as $detail)
        {
            $detail->id_exam_registration = $id_exam_registration;
            unset($detail->id_session);
            unset($detail->id);

            $added = $this->addMarkDistributionDetails($detail);
            # code...
        }
        
        $added = $this->deleteTempMarkDistributionBySession($id_session);
    }

    function addMarkDistributionDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_registration_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getTempMarkDistributionDetailsBySessionIdForMove($id_session)
    {
        $this->db->select('tctd.*');
        $this->db->from('temp_exam_registration_details as tctd');
        $this->db->where('tctd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }

    function generateMarkDistributionNumber()
    {

        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('exam_registration');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "CT" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function getExamRegistration($id_exam_registration)
    {
        $this->db->select('tctd.*, s.full_name as student_name, s.nric, s.email_id, s.phone');
        $this->db->from('exam_registration as tctd');
        $this->db->join('student as s', 'tctd.id_student = s.id');
        $this->db->where('tctd.id_exam', $id_exam_registration);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteDetailData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('exam_registration_details');
        return TRUE;
    }

    function getProgramLandscapeCourses($data)
    {
        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];
        $id_programme_landscape = $data['id_programme_landscape'];

        $this->db->select('DISTINCT(a.id_course_registered_landscape) as id_course_registered_landscape');
        // $this->db->select('DISTINCT(a.id_course) as id_course_registered_landscape');
        $this->db->from('course_registration as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'a.id_course_registered_landscape = acpl.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.is_exam_registered', 0);
        $this->db->where('a.is_result_announced', 0);
        // $this->db->where('a.status', '1');
         $this->db->where('a.id_programme', $id_programme);
         $this->db->where('acpl.id_program_landscape', $id_programme_landscape);
        $query = $this->db->get();
        $results = $query->result();

        // echo "<Pre>";print_r($results);exit();

         $details = array();
         foreach ($results as $result)
         {
            $value = $this->getCourseDetailsByCourseToLandscapeId($result->id_course_registered_landscape);
        // echo "<Pre>";print_r($value);exit();
            if($value)
            {    
                $value->id_course_registered_landscape = $result->id_course_registered_landscape;
                array_push($details, $value);
            }
         }
        // echo "<Pre>";print_r($details);exit();
         return $details; 
    }

    function getCourseDetailsByCourseToLandscapeId($id_course_registered_landscape)
    {
        $this->db->select('a.*');
        $this->db->from('course as a');
        $this->db->join('add_course_to_program_landscape as crl', 'a.id = crl.id_course');
        $this->db->where('crl.id', $id_course_registered_landscape);
         $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }

    function getCourseForLandscape($id_course_registered)
    {
        $this->db->select('a.*, c.code, c.name, c.credit_hours, crl.pre_requisite, crl.course_type');
        $this->db->from('course_registration as a');
        $this->db->join('course as c', 'a.id_course = c.id', 'left');
        $this->db->join('add_course_to_program_landscape as crl', 'a.id_course_registered_landscape = crl.id', 'left');
        $this->db->where('a.id_course_registered_landscape', $id_course_registered);
         $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }

    function getCourse($id_course)
    {
        $this->db->select('a.*');
        $this->db->from('course as a');
        $this->db->where('a.id', $id_course);
         $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }

    function examEventListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('exam_event as a');
        $this->db->where('a.status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getStudentsByIdCourses($id_course_program_landscape)
    {
        $this->db->select('DISTINCT(a.id), a.*, i.year as intake_year, i.name as intake_name, p.code as program_code, p.name as program_name, adv.ic_no, adv.name as advisor_name, cr.id as id_course_registered');
        $this->db->from('student as a');
        $this->db->join('course_registration as cr', 'a.id = cr.id_student', 'left');
        $this->db->join('course as c', 'cr.id_course = c.id', 'left');
        $this->db->join('staff as adv', 'a.id_advisor = adv.id', 'left');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->where('cr.id_course_registered_landscape', $id_course_program_landscape);
        $this->db->where('cr.is_exam_registered', 0);
        $this->db->where('cr.is_result_announced', 0);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegistrationById($id)
    {
        $this->db->select('a.*');
        $this->db->from('course_registration as a');
        $this->db->where('a.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }

    function searchStudent($data)
    {
        $this->db->select('DISTINCT(a.id), a.*, i.year as intake_year, i.name as intake_name, p.code as program_code, p.name as program_name, adv.ic_no, adv.name as advisor_name, cr.id as id_course_registered');
        $this->db->from('student as a');
        $this->db->join('course_registration as cr', 'a.id = cr.id_student', 'left');
        $this->db->join('course as c', 'cr.id_course = c.id', 'left');
        $this->db->join('staff as adv', 'a.id_advisor = adv.id', 'left');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->where('cr.id_course_registered_landscape', $data['id_course_registered_landscape']);
        if($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        if($data['id_intake'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
        if($data['name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('cr.is_exam_registered', 0);
        $this->db->where('cr.is_result_announced', 0);
        $this->db->where('cr.is_bulk_withdraw', 0);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function deleteExamRegistration($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('exam_registration');
        return TRUE;
    }

    function getExamRegistrationData($id)
    {
        $this->db->select('a.*');
        $this->db->from('exam_registration as a');
        $this->db->where('a.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }

    function getLandscapeListByProgramIdNIntakeId($data)
    {
        $this->db->select('a.*');
        $this->db->from('programme_landscape as a');
        $this->db->where('a.id_programme', $data['id_programme']);
        $this->db->where('a.id_intake', $data['id_intake']);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function programmeLandscapeListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('programme_landscape as a');
        $this->db->where('a.status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }
}