<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Exam Calender</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Calender Details</h4>

            <div class="row">
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $examCalender->id_semester)
                                    { echo "selected"; }
                                ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity <span class='error-text'>*</span></label>
                        <select name="activity" id="activity" class="form-control">
                            <option value="">Select</option>
                            <option value="Update Exam Center"
                             <?php 
                                if('Update Exam Center' == $examCalender->activity)
                                    { echo "selected"; }
                                ?>
                            >Update Exam Center</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="false" value="<?php echo date('d-m-Y', strtotime($examCalender->start_date)) ?>">
                    </div>
                </div>

                
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="false" value="<?php echo date('d-m-Y', strtotime($examCalender->end_date)) ?>">
                    </div>
                </div>

                

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );




    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_semester: {
                    required: true
                },
                 activity: {
                    required: true
                },
                 start_date: {
                    required: true
                },
                 end_date: {
                    required: true
                }
            },
            messages: {
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                activity: {
                    required: "<p class='error-text'>Select Activity</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>