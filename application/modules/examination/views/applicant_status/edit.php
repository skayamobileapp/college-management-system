<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Applicant Status</h3>
        </div>
        <form id="form_category" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Applicant Status Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Applicant Status <span class='error-text'>*</span></label><br>
                        <input type="radio" name="name" value="Draft" <?php if($applicantStatusDetails->name == 'Draft'){ echo "checked";} ?>>Draft
                        <input type="radio" name="name" value="Pending" <?php if($applicantStatusDetails->name == 'Pending'){ echo "checked";} ?>>Pending
                    </div>
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_programgrade").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Status required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>