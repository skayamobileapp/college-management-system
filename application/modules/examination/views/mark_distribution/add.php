<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Mark Distribution</h3>
        </div>
    <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Institution Details</h4>


            <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control selitemIcon" onchange="getIntakeByProgramme(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <span id="view_intake">
                              <select class="form-control" id='id_intake' name='id_intake'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Programme Landscape <span class='error-text'>*</span></label>
                            <span id="view_programme_landscape">
                              <select class="form-control" id='id_programme_landscape' name='id_programme_landscape'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label> Intake <span class='error-text'>*</span></label>
                            <span id="view_intake"></span>
                        </div>
                    </div> -->


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label> Course <span class='error-text'>*</span></label>
                            <span id="view_course"></span>
                        </div>
                    </div> -->

            </div>


            <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Course <span class='error-text'>*</span></label>
                            <span id="view_course">
                              <select class="form-control" id='id_course_registered' name='id_course_registered'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>

            </div>

        </div>

        <div class="form-container" id="display_course_details" style="display: none">
            <h4 class="form-group-title">Course Registration Details</h4>

            <div class="row">

                <div id="view_course_details">
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


    </form>


    <form id="form_detail" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Add Distribution Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Exam Components <span class='error-text'>*</span></label>
                        <select name="id_component" id="id_component" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($componentList))
                                foreach ($componentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="max_marks" id="max_marks" autocomplete="off">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Attendance Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="attendance_status" id="attendance_status" value="1" checked="checked"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="attendance_status" id="attendance_status" value="0"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>


                
            </div>


            <div class="row">



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Pass Compulsary <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass_compulsary" id="is_pass_compulsary" value="1" checked="checked"><span class="check-radio" onclick="showPassMarks()"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass_compulsary" id="is_pass_compulsary" value="0" onclick="hidePassMarks()"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>


                <div class="col-sm-4" id="view_pass_marks">
                    <div class="form-group">
                        <label>Pass Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="pass_marks" id="pass_marks" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


            </div>

            <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="saveDetailData()">Add</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

        </div>

    </form>



        <div id="view_temp_details">
        </div>





        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">

    function showPassMarks()
     {
        $("#view_pass_marks").show();
     }

    function hidePassMarks()
    {
        $("#view_pass_marks").hide();
    }
    
    $('select').select2();

    function getIntakeByProgramme(id)
     {
        if(id != '')
        {
            $.get("/examination/markDistribution/getIntakeByProgramme/"+id, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });
        }
     }

    function getLandscapeListByProgramIdNIntakeId()
    {
        var data = {};

        data['id_intake'] = $("#id_intake").val();
        data['id_programme'] = $("#id_program").val();
        // alert(id_student);
        if ($("#id_program").val() != '' && $("#id_intake").val() != '')
        {
            $.ajax(
            {
               url: '/examination/markDistribution/getLandscapeListByProgramIdNIntakeId',
                type: 'POST',
               data:
               {
                'data': data,
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_programme_landscape").html(result);
               }
            }); 
        }      
    }

    function getCoursesByProgramIdNIntakeId()
    {
        var tempData = {};

        tempData['id_intake'] = $("#id_intake").val();
        tempData['id_programme'] = $("#id_program").val();
        tempData['id_programme_landscape'] = $("#id_programme_landscape").val();
        // alert(id_student);
        if ($("#id_program").val() != '' && $("#id_intake").val() != '' && $("id_programme_landscape").val() != '')
        {
            $.ajax(
            {
               url: '/examination/markDistribution/getCoursesByProgramIdNIntakeId',
                type: 'POST',
               data:
               {
                'data': tempData,
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_course").html(result);
               }
            }); 
        }       
    }

    function getCourseForLandscape()
    {
        var id_course_registered = $("#id_course_registered").val();
        if (id_course_registered != '')
        {
            $.ajax(
            {
               url: '/examination/markDistribution/getCourseForLandscape',
                type: 'POST',
               data:
               {
                id_course_registered: id_course_registered
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                
                    $("#view_course_details").html(result);
                    $("#display_course_details").show();

                
               }
            }); 
        }       

    }


    function saveDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['id_component'] = $("#id_component").val();
        tempPR['is_pass_compulsary'] = $("#is_pass_compulsary").val();
        tempPR['pass_marks'] = $("#pass_marks").val();
        tempPR['max_marks'] = $("#max_marks").val();
        tempPR['attendance_status'] = $("#attendance_status").val();

            $.ajax(
            {
               url: '/examination/markDistribution/saveTempDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                $("#view_temp_details").html(result);

                // location.reload();
                // window.location.reload();

               }
            });
        }
    }

    function deleteTempData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/examination/markDistribution/deleteTempData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_temp_details").html(result);
                    // window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }

    function getStudentById(id)
    {
        if(id != '')
        {

        $.ajax(
            {
               url: '/registration/creditTransfer/getStudentById/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {

                    $("#view_student_data").show();
                    $("#view_student_data").html(result);
                    // window.location.reload();
                    // alert(result);
                    // window.location.reload();
                }
               }
            });
        }
    }

    function validateDetailsData()
    {
        if($('#form_main').valid())
        {
            console.log($("#view_temp_details").html());
            var addedProgam = $("#view_temp_details").html();
            // alert(addedProgam);
            if(addedProgam=='')
            {
                alert("Add Credit Transfer Details");
            }
            else
            {
                $('#form_main').submit();
            }
        }    
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_course_registered: {
                    required: true
                },
                id_programme_landscape: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_course_registered: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_programme_landscape: {
                    required: "<p class='error-text'>Select Programme Landscape</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_component: {
                    required: true
                },
                is_pass_compulsary: {
                    required: true
                },
                pass_marks: {
                    required: true
                },
                 max_marks: {
                    required: true
                },
                 attendance_status: {
                    required: true
                }
            },
            messages: {
                id_component: {
                    required: "<p class='error-text'>Subject Component</p>",
                },
                is_pass_compulsary: {
                    required: "<p class='error-text'>Select Pass Compulsary</p>",
                },
                pass_marks: {
                    required: "<p class='error-text'>Pass Marks Required</p>",
                },
                max_marks: {
                    required: "<p class='error-text'>Ma. Marks Required</p>",
                },
                attendance_status: {
                    required: "<p class='error-text'>Select Attendence Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $('select').select2();

$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );


</script>