<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Student IPS Progress</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>
        
    
   <div>
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#tab_1" class="nav-link border rounded text-center"
                    aria-controls="tab_1" aria-selected="true"
                    role="tab" data-toggle="tab">Student Progress Details</a>
            </li>
            <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center"
                    aria-controls="tab_2" role="tab" data-toggle="tab">Progress History Details</a>
            </li>
        </ul>


        <div class="tab-content offers-tab-content">


        <div role="tabpanel" class="tab-pane active" id="tab_1">


            <div class="col-12 mt-4">
            

                <h3 class='sub-title'>Student Profile</h3>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Learning Mode :</dt>
                                <dd><?php echo $getStudentData->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $getStudentData->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Organisation :</dt>
                                <dd><?php
                                if($getStudentData->id_university != 1 && $getStudentData->id_university != 0)
                                {
                                 echo $getStudentData->partner_university_code , " - " . $getStudentData->partner_university_name;
                                 }
                                else
                                {
                                    echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                                }
                                  ?></dd>
                            </dl> 
                            <dl>
                                <dt>Research Supervisor :</dt>
                                <dd><?php 
                                if($getStudentData->supervisor_type == 0)
                                {
                                    echo 'External';
                                }elseif($getStudentData->supervisor_type == 1)
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->supervisor_name ; ?></dd>
                            </dl>
                            <dl>
                                <dt>Current IPS Duration :</dt>
                                <dd><?php echo $getStudentData->phd_duration; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->programme_code . " - " . $getStudentData->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $getStudentData->scheme_code . " - " .  $getStudentData->scheme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Passport Number :</dt>
                                <dd><?php echo $getStudentData->passport_number; ?></dd>
                            </dl>
                            <dl>
                                <dt>Branch :</dt>
                                <dd><?php
                                
                                    echo $getStudentData->branch_code . " - " . $getStudentData->branch_name;
                                  ?></dd>
                            </dl> 
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo 
                                // $getStudentData->qualification_code . " - " .
                                  $getStudentData->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Current Deiverable :</dt>
                                <dd><?php echo $getStudentData->current_deliverable; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
            <br>



            <form id="form_profile" action="" method="post"> 

                <div class="form-container">
                <h4 class="form-group-title">Student Progress Details</h4>





                    <div class="row">

                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent State <span class='error-text'>*</span> </label>
                                <select name="permanent_state" id="permanent_state" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($stateList))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($student->permanent_state==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div> -->



                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Current Duration <span class='error-text'>*</span> </label>
                                <input type="text" class="form-control" id="phd_duration" name="phd_duration" value="<?php echo $getStudentData->phd_duration; ?>" readonly>
                            </div>
                        </div>
                       

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Curret Deliverable <span class='error-text'>*</span> </label>
                                <input type="text" class="form-control" id="current_deliverable" name="current_deliverable" value="<?php echo $getStudentData->current_deliverable; ?>" readonly>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="Satisfied" checked="checked"><span class="check-radio"></span> Satisfied
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="Un-Satisfied"><span class="check-radio"></span> Un-Satisfied
                                </label>                              
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Upload Employment Letter <span class='error-text'>*</span> </label>
                                <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                            </div>
                        </div>
                    </div> -->
                </div>
                    
                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="3">Save</button>
                    </div>
                </div>

            </form>








                <!-- <div class="form-container">
                        <h4 class="form-group-title">English Proficiency Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Test</th>
                            <th>Date</th>
                            <th>Score</th>
                            <th>File</th>
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($proficiencyDetails))
                          {
                            $i=1;
                            foreach ($proficiencyDetails as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->test ?></td>
                                <td><?php echo $record->date ?></td>
                                <td><?php echo $record->score ?></td>
                                <td><?php echo $record->file ?></td>
                                <td class="text-center"><?php echo anchor('admission/student/delete_english?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div> -->
             
            </div> <!-- END col-12 -->  

        </div>


        <div role="tabpanel" class="tab-pane" id="tab_2">
            <div class="col-12 mt-4">
                <br>

            

            <div class="form-container">
                    <h4 class="form-group-title">Progress History Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>PHD Duration</th>
                        <th>Deliverable</th>
                        <th>By</th>
                        <th>Date</th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getPostgraduateStudentProgressByIdStudent))
                      {
                        $i=1;
                        foreach ($getPostgraduateStudentProgressByIdStudent as $record) {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->phd_duration ?></td>
                            <td><?php echo $record->current_deliverable ?></td>
                            <td><?php echo $record->created_by ?></td>
                            <td><?php if($record->created_dt_tm)
                            {
                                echo date('d-m-Y',strtotime($record->created_dt_tm));
                            } 
                             ?></td>
                            <td class="text-center"><?php echo $record->status ?></td>
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        





      </div>
    </div>

   </div> <!-- END row-->
   

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    
    $('select').select2();

    $( function()
    {
        $( ".datepicker" ).datepicker(
        {
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
        });
  });

  $(document).ready(function() {
        $("#form_profile").validate({
            rules: {
                phd_duration: {
                    required: true
                },
                current_deliverable: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                phd_duration: {
                    required: "<p class='error-text'>PHD Duration Required</p>",
                },
                current_deliverable: {
                    required: "<p class='error-text'>Current Deliverable Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
  </script>