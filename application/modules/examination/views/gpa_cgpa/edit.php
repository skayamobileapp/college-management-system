<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit GPA / CGPA</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">GPA / CGPA Details</h4>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Setup By <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="setup_by" id="setup_by" value="1" <?php if($gpaCgpa->setup_by=='Award') {
                                 echo "checked=checked";
                              };?>   onclick="showAward()"><span class="check-radio"></span> Award
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="setup_by" id="setup_by" value="0" <?php if($gpaCgpa->setup_by=='Program') {
                                 echo "checked=checked";
                              };?> onclick="showProgram()">
                              <span class="check-radio"></span> Program
                            </label>                              
                        </div>                         
                </div>  


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $gpaCgpa->id_intake)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $gpaCgpa->id_semester)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Academic Status <span class='error-text'>*</span></label>
                        <select name="academic_status" id="academic_status" class="form-control" >
                            <option value="">Select</option>
                            <option value="GPA"
                            <?php 
                                if('GPA' == $gpaCgpa->academic_status)
                                {
                                    echo "selected=selected";
                                } ?>
                            >GPA</option>
                            <option value="CGPA"
                            <?php 
                                if('CGPA' == $gpaCgpa->academic_status)
                                {
                                    echo "selected=selected";
                                } ?>
                            >CGPA</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id="view_award">
                    <div class="form-group">
                        <label>Award <span class='error-text'>*</span></label>
                        <select name="id_award" id="id_award" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($awardList))
                            {
                                foreach ($awardList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $gpaCgpa->id_award)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id="view_program" style="display: none">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $gpaCgpa->id_program)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($gpaCgpa->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($gpaCgpa->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>           
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        
        </form>




        <form id="form_detail" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Academic Setup Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Minimum Grade Point <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="min_grade_point" id="min_grade_point" autocomplete="off" value="" min="1" max="100">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Maximum Grade Point <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="max_grade_point" id="max_grade_point" autocomplete="off" min="1" max="100">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="description" id="description" autocomplete="off" >
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status Description Optional language <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="status_optional_language" id="status_optional_language" autocomplete="off" >
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Probation <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="probation" id="probation" value="1" checked="checked"><span class="check-radio"></span> Yes
                            </label>      
                            <label class="radio-inline">
                              <input type="radio" name="probation" id="probation" value="0" ><span class="check-radio"></span> No
                            </label>                        
                        </div>                         
                </div>

                
                
                
                <!-- 
            </div>


            <div class="row"> -->


            </div>

            <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="saveDetailData()">Add</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

        </div>

    </form>



    
     <?php

                    if(!empty($gpaCgpaDetails))
                    {
                        ?>
                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Course Details</h4>

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Min. Grade Point</th>
                                        <th>Max. Grade Point</th>
                                        <th>Status Description</th>
                                        <th>Description Optional Language</th>
                                        <th>Probation</th>
                                        <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($gpaCgpaDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $gpaCgpaDetails[$i]->min_grade_point; ?></td>
                                        <td><?php echo $gpaCgpaDetails[$i]->max_grade_point; ?></td>
                                        <td><?php echo $gpaCgpaDetails[$i]->description;?></td>
                                        <td><?php echo $gpaCgpaDetails[$i]->status_optional_language; ?></td>
                                        <td><?php
                                         if($gpaCgpaDetails[$i]->probation ==1)
                                         {
                                            echo 'Yes';
                                         }else
                                         {
                                            echo 'No';
                                         }
                                          ?></td>
                                    
                                        <td class="text-center">
                                        <a onclick="deleteDetailData(<?php echo $gpaCgpaDetails[$i]->id; ?>)">Delete</a>
                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


     function showAward()
     {
        $("#view_award").show();
        $("#view_program").hide();
     }

    function showProgram()
    {
        $("#view_award").hide();
        $("#view_program").show();
    }



    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                setup_by: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                academic_status: {
                    required: true
                },
                 id_award: {
                    required: true
                },
                id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                }
            },
            messages: {
                setup_by: {
                    required: "<p class='error-text'>Select Setup By</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                academic_status: {
                    required: "<p class='error-text'>Select Academic Status</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     function saveDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['min_grade_point'] = $("#min_grade_point").val();
        tempPR['max_grade_point'] = $("#max_grade_point").val();
        tempPR['description'] = $("#description").val();
        tempPR['status_optional_language'] = $("#status_optional_language").val();
        tempPR['probation'] = $("#probation").val();
        tempPR['id_gpa_cgpa'] = <?php echo $gpaCgpa->id ?>;

            $.ajax(
            {
               url: '/examination/gpaCgpaSetup/saveDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();

               }
            });
        }
    }

    function deleteDetailData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/examination/gpaCgpaSetup/deleteDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }


     $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                min_grade_point: {
                    required: true
                },
                max_grade_point: {
                    required: true
                },
                description: {
                    required: true
                },
                 probation: {
                    required: true
                }
            },
            messages: {
                min_grade_point: {
                    required: "<p class='error-text'>Min. Grade Points Required</p>",
                },
                max_grade_point: {
                    required: "<p class='error-text'>Max. Grade Points Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                probation: {
                    required: "<p class='error-text'>Select Probation</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

     $('select').select2();

</script>






