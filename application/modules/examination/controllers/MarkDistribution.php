<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MarkDistribution extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mark_distribution_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('mark_distribution.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['markDistributionList'] = $this->mark_distribution_model->markDistributionListSearch($formData);
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');


                // echo "<Pre>";print_r($data['markDistributionList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Mark Distribution';
            $this->loadViews("mark_distribution/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('mark_distribution.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();

                $id_programme_landscape = $this->security->xss_clean($this->input->post('id_programme_landscape'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_course_registered = $this->security->xss_clean($this->input->post('id_course_registered'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));


                // $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                // $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();

                $generated_number = $this->mark_distribution_model->generateMarkDistributionNumber();
                
                $data = array(
                   'id_program' => $id_program,
                   'id_programme_landscape' => $id_programme_landscape,
                   'id_intake' => $id_intake,
                   'id_course_registered' => $id_course_registered,
                   'id_course' => $id_course,
                   'status' => 0,
                   'created_by' => $user_id
                );

                $result = $this->mark_distribution_model->addMarkDistribution($data);
                if($result)
                {
                    $result = $this->mark_distribution_model->moveDetailDataFromTempToMain($result);
                }
                redirect('/examination/markDistribution/list');
            }
            else
            {
                    $result = $this->mark_distribution_model->deleteTempMarkDistributionBySession($id_session);
            }

            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['componentList'] = $this->mark_distribution_model->examComponentListByStatus('1');

                // echo "<Pre>";print_r($time_in_12_hour_format);exit();

            $this->global['pageTitle'] = 'Campus Management System : Add Mark Distribution';
            $this->loadViews("mark_distribution/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('mark_distribution.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/markDistribution/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();
                
                $data = array(
                   'name' => $name,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'status' => $status,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->mark_distribution_model->editMarkDistribution($data,$id);
                redirect('/examination/markDistribution/list');
            }

            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['componentList'] = $this->mark_distribution_model->examComponentListByStatus('1');
            $data['programmeLandscapeList'] = $this->mark_distribution_model->programmeLandscapeListByStatus('1');

            $data['markDistribution'] = $this->mark_distribution_model->getMarkDistribution($id);
            $data['markDistributionDetails'] = $this->mark_distribution_model->getMarkDistributionDetailsByIdMarkDistribution($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Mark Distribution';
            $this->loadViews("mark_distribution/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('mark_distribution.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['markDistributionList'] = $this->mark_distribution_model->markDistributionListSearch($formData);
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');

                // echo "<Pre>";print_r($data['markDistributionList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Mark Distribution';
            $this->loadViews("mark_distribution/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('mark_distribution.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/markDistribution/approvallist');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                
                $data = array(
                   'status' => $status,
                    'reason' => $reason,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->mark_distribution_model->editMarkDistribution($data,$id);
                redirect('/examination/markDistribution/approvalList');
            }

            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['componentList'] = $this->mark_distribution_model->examComponentListByStatus('1');
            $data['programmeLandscapeList'] = $this->mark_distribution_model->programmeLandscapeListByStatus('1');



            $data['markDistribution'] = $this->mark_distribution_model->getMarkDistribution($id);
            $data['markDistributionDetails'] = $this->mark_distribution_model->getMarkDistributionDetailsByIdMarkDistribution($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Mark Distribution';
            $this->loadViews("mark_distribution/view", $this->global, $data, NULL);
        }
    }

    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->mark_distribution_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getLandscapeListByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getLandscapeListByProgramIdNIntakeId()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));
         // echo "<Pre>"; print_r($data);exit();
        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];

        $temp_details = $this->mark_distribution_model->getLandscapeListByProgramIdNIntakeId($data);

        // echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_programme_landscape' id='id_programme_landscape' class='form-control' onchange='getCoursesByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $name = $temp_details[$i]->name;
            $program_landscape_type = $temp_details[$i]->program_landscape_type;
            // $code = $temp_details[$i]->code;

            $table.="<option value=".$id.">".$program_landscape_type . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getCoursesByProgramIdNIntakeId()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));
         // echo "<Pre>"; print_r($data);exit();
        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->mark_distribution_model->getProgramLandscapeCourses($data);

        // echo "<Pre>"; print_r($id_programme);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_course_registered' id='id_course_registered' class='form-control' onchange='getCourseForLandscape()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $id_course_registered = $temp_details[$i]->id_course_registered;
            $name = $temp_details[$i]->name;
            $code = $temp_details[$i]->code;

            $table.="<option value=".$id_course_registered.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getCourseForLandscape()
    {
        $id_course_registered = $this->security->xss_clean($this->input->post('id_course_registered'));
        $course_landscpae = $this->mark_distribution_model->getCourseForLandscape($id_course_registered);
        
        // echo "<Pre>"; print_r($course_landscpae);exit();

        if(!empty($course_landscpae))
        {

            $id_course = $course_landscpae->id_course;
            $pre_requisite = $course_landscpae->pre_requisite;
           $course_type = $course_landscpae->course_type;
           $name = $course_landscpae->name;
           $code = $course_landscpae->code;



        $table  = "



             <h4 class='sub-title'>Course Registration Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <input type='hidden' class='form-control' name='id_course' id='id_course' value='$id_course'>
                                <dt>Course Name :</dt>
                                <dd>$name</dd>
                            </dl>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd>$code</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Pre-Requisite :</dt>
                                <dd>
                                    $pre_requisite
                                </dd>
                            </dl>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd>$course_type</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";
         }
        else
        {
            echo "<h4 class='sub-title'>No Data Found</h4>";exit;
        }

        print_r($table);exit();
    }











    function saveTempDetailData()
    {
        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->mark_distribution_model->saveTempDetailData($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->mark_distribution_model->getTempMarkDistributionDetailsBySessionId($id_session);

        // echo "<Pre>";print_r($temp_details);exit;



        if(!empty($temp_details))
        {
            
        $table ="
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Exam Components</th>
                    <th>Pass Compulsary</th>
                    <th>Pass Marks</th>
                    <th>Max. Marks</th>
                    <th>Attendance Status</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $is_pass_compulsary = $temp_details[$i]->is_pass_compulsary;
                        $pass_marks = $temp_details[$i]->pass_marks;
                        $max_marks = $temp_details[$i]->max_marks;
                        $attendance_status = $temp_details[$i]->attendance_status;
                        $component_code = $temp_details[$i]->component_code;
                        $component_name = $temp_details[$i]->component_name;

                        if($is_pass_compulsary == 1)
                        {
                            $is_pass_compulsary = 'Yes';
                        }else
                        {
                            $is_pass_compulsary = 'No';
                        }

                        if($attendance_status == 1)
                        {
                            $attendance_status = 'Yes';
                        }else
                        {
                            $attendance_status = 'No';
                        }


                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$component_code - $component_name</td>
                            <td>$is_pass_compulsary</td>
                            <td>$pass_marks</td>
                            <td>$max_marks</td>
                            <td>$attendance_status</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table = "";

        }


        return $table;
    }

    function deleteTempData($id)
    {
        $id_session = $this->session->my_session_id;

        $deleted = $this->mark_distribution_model->deleteTempMarkDistribution($id);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    


    function getCentersByLocatioin($id_location)
    {
            $results = $this->mark_distribution_model->getCentersByLocatioin($id_location);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function saveDetailData()
    {

        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->mark_distribution_model->addMarkDistributionDetails($tempData);

        // $data = $this->displayTempData();

        echo "success";exit();
    }

    function deleteDetailData($id)
    {
        $deleted = $this->mark_distribution_model->deleteDetailData($id);
        echo 'success';exit;
    }
}
