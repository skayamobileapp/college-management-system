<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BudgetAllocation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('budget_allocation_model');
        $this->load->model('budget_amount_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('budget_allocation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             $data['financialYearList'] = $this->budget_amount_model->financialYearListByStatus('1');
            $data['departmentList'] = $this->budget_amount_model->departmentListByStatus('1');
            $data['budgetYearList'] = $this->budget_amount_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->budget_amount_model->departmentCodeListByStatus('1');

            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['budgetAmountList'] = $this->budget_allocation_model->budgetAmountListSearch($formData);

            // echo "<Pre>"; print_r($data['budgetAmountList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Budget Allocation';
            $this->loadViews("budget_allocation/list", $this->global, $data, NULL);
        }
    }
    
    function add($id)
    {
        if ($this->checkAccess('budget_allocation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetAllocation/list');
            }
            else
            {
            	$id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                if($this->input->post())
                {
                    $results = $this->budget_allocation_model->deleteBudgetAllocationByMaster($id);

                    $result = $this->budget_allocation_model->addNewBudgetAllocation($id_session,$id);

                    if($result)
                    {
                        $data['is_detail_added'] = 1;
                        $result_updated = $this->budget_allocation_model->updateBudgetAmount($data,$id);
                    }

                
                    // $check_duplication = $this->budget_allocation_model->checkDuplicateBudgetAllocation($data);
                    // if($check_duplication)
                    // {
                    // 	echo "Duplicate Budget Allocated For The Department On Same Budget Year";exit();
                    // }

                    redirect('/budget/budgetAllocation/list');
                }
                else
                {
                    $result = $this->budget_allocation_model->deleteTempDetailsBySession($id_session);
                    $move_detail_to_temp = $this->budget_allocation_model->moveDetailToTemp($id);
                    
                }

                $data['financialYearList'] = $this->budget_allocation_model->financialYearListByStatus('1');
                $data['departmentList'] = $this->budget_allocation_model->departmentListByStatus('1');
                $data['budgetYearList'] = $this->budget_allocation_model->budgetYearListByStatus('1');

                $data['fundCodeList'] = $this->budget_allocation_model->fundCodeListByStatus('1');
                $data['departmentCodeList'] = $this->budget_allocation_model->departmentCodeListByStatus('1');
                $data['activityCodeList'] = $this->budget_allocation_model->activityCodeListByStatus('1');
                $data['accountCodeList'] = $this->budget_allocation_model->accountCodeListByStatus('1');

                $data['budgetAmount'] = $this->budget_amount_model->getBudgetAmount($id);
                $data['budgetAllocationList'] = $this->budget_allocation_model->getBudgetAllocationByBudgetAmount($id);

                $data['id_budget_amount'] = $id;
                $data['department_code'] = $data['budgetAmount']->department_code;
            // echo "<Pre>"; print_r($data['budgetAmount']);exit;
                // echo "<Pre>";print_r($data);exit();
                $this->global['pageTitle'] = 'FIMS : Add Budget Allocation';
                $this->loadViews("budget_allocation/add", $this->global, $data, NULL);
            }
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('budget_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/budget/budgetAllocation/list');
            }
            if($this->input->post())
            {            
                // $data = array(
                //     'status' => 1
                // );
                
                // $result = $this->budget_allocation_model->editBudgetAllocationByMaster($data,$id);
                $result = $this->budget_allocation_model->deleteBudgetAllocationByMaster($id);
                $result = $this->budget_allocation_model->addNewBudgetAllocation($id_session,$id);
                redirect('/budget/budgetAllocation/list');
            }
            else
            {
                $result = $this->budget_allocation_model->deleteTempDetailsBySession($id_session);
                $move_detail_to_temp = $this->budget_allocation_model->moveDetailToTemp($id);
            }

            $data['fundCodeList'] = $this->budget_allocation_model->fundCodeListByStatus('1');
            $data['departmentCodeList'] = $this->budget_allocation_model->departmentCodeListByStatus('1');
            $data['activityCodeList'] = $this->budget_allocation_model->activityCodeListByStatus('1');
            $data['accountCodeList'] = $this->budget_allocation_model->accountCodeListByStatus('1');

            $data['budgetAmount'] = $this->budget_amount_model->getBudgetAmount($id);
            // $data['budgetAllocationList'] = $this->budget_allocation_model->getBudgetAllocationByBudgetAmount($id); 
            $data['budgetAllocationList'] = $this->budget_allocation_model->getTempBudgetAllocationDetailsBySession($id_session); 

            $data['id_budget_amount'] = $id;
            // echo "<Pre>"; print_r($data['budgetAmount']);exit;
            $this->global['pageTitle'] = 'FIMS : View Budget Allocation';
            $this->loadViews("budget_allocation/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
         if ($this->checkAccess('budget_allocation.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             $data['financialYearList'] = $this->budget_amount_model->financialYearListByStatus('1');
            $data['departmentList'] = $this->budget_amount_model->departmentListByStatus('1');
            $data['budgetYearList'] = $this->budget_amount_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->budget_allocation_model->departmentCodeListByStatus('1');

            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['budgetAmountList'] = $this->budget_allocation_model->budgetAmountListSearch($formData);

            // echo "<Pre>"; print_r($data['budgetAllocationList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Budget Allocation';
            $this->loadViews("budget_allocation/approval_list", $this->global, $data, NULL);
        }
    }

    function approve($id = NULL)
    {
        if ($this->checkAccess('budget_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetAllocation/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));

            
                $data = array(
                    'allocation_status' => $status,
                    'allocation_reason' => $reason
                );
                
                $result = $this->budget_amount_model->editBudgetAmount($data,$id);
                redirect('/budget/budgetAllocation/approvalList');
            }
            $data['budgetAmount'] = $this->budget_amount_model->getBudgetAmount($id);
            $data['budgetAllocationList'] = $this->budget_allocation_model->getBudgetAllocationByBudgetAmount($id); 
            // echo "<Pre>"; print_r($data['budgetAllocationList']);exit;
            $this->global['pageTitle'] = 'FIMS : View Budget Allocation';
            $this->loadViews("budget_allocation/approve", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('budget_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetAllocation/list');
            }
            if($this->input->post())
            {
                redirect('/budget/budgetAllocation/list');
            }
            $data['budgetAmount'] = $this->budget_amount_model->getBudgetAmount($id);
            $data['budgetAllocationList'] = $this->budget_allocation_model->getBudgetAllocationByBudgetAmount($id); 
            // echo "<Pre>"; print_r($data['budgetAllocationList']);exit;
            $this->global['pageTitle'] = 'FIMS : View Budget Allocation';
            $this->loadViews("budget_allocation/view", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $user_id = $this->session->userId;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['balance_amount'] = $tempData['allocated_amount'];
        $tempData['increment_amount'] = 0;
        $tempData['decrement_amount'] = 0;
        $tempData['used_amount'] = 0;
        $tempData['id_session'] = $id_session;
        $tempData['status'] = 1;
        $tempData['created_by'] = $user_id;
        // echo "<Pre>";print_r($tempData);exit;

         $duplicate_data = $this->budget_allocation_model->checkTempDuplicateBudgetAllocation($tempData);
        // echo "<Pre>";print_r($duplicate_data);exit;
        // if($duplicate_data)
        // {
        //     echo "";exit();
        // }

        $inserted_id = $this->budget_allocation_model->addTempBudgetAllocationDetails($tempData);
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function viewData()
    {
        $id_budget_amount = $this->security->xss_clean($this->input->post('id_budget_amount'));
        // echo "<Pre>";print_r($id_budget_amount);exit;
        $details = $this->budget_allocation_model->getBudgetAllocationByBudgetAmount($id_budget_amount); 
        if(!empty($details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fund Code</th>
                    <th>Department Code</th>
                    <th>Activity Code</th>
                    <th>Account Code</th>
                    <th>Balance Amount</th>
                    <th>Allocated Amount</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
        // echo "<Pre>";print_r($details);exit;
                        $id = $details[$i]->id;
                        $fund_code = $details[$i]->fund_code;
                        $department_code = $details[$i]->department_code;
                        $activity_code = $details[$i]->activity_code;
                        $account_code = $details[$i]->account_code;
                        $used_amount = $details[$i]->used_amount;
                        $used_amount = $details[$i]->increment_amount;
                        $used_amount = $details[$i]->decrement_amount;
                        $balance_amount = $details[$i]->balance_amount;
                        $allocated_amount = $details[$i]->allocated_amount;
                        $increment_amount = $details[$i]->increment_amount;
                        $decrement_amount = $details[$i]->decrement_amount;

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$fund_code</td>
                            <td>$department_code</td>
                            <td>$activity_code</td>
                            <td>$account_code</td>
                            <td>$balance_amount</td>
                            <td>$allocated_amount</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        // <span onclick='deleteTempData($id)'>Delete</a>
                        $total_detail = $total_detail + $allocated_amount;
                    }

                    $total_detail = number_format($total_detail, 2, '.', '');

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='5'></td>
                            <td bgcolor='' ><b> Total : </b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        print_r($table);exit();
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->budget_allocation_model->getTempBudgetAllocationDetailsBySession($id_session); 
        if(!empty($details))
        {
            
        // echo "<Pre>";print_r($details);exit;

            //  <th>Increment Amount</th>
            //  <th>Decrement Amount</th>
            // <th>Used Amount</th>
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fund Code</th>
                    <th>Department Code</th>
                    <th>Activity Code</th>
                    <th>Account Code</th>
                    <th>Balance Amount</th>
                    <th>Allocated Amount</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $fund_code = $details[$i]->fund_code;
                        $department_code = $details[$i]->department_code;
                        $activity_code = $details[$i]->activity_code;
                        $account_code = $details[$i]->account_code;
                        $allocated_amount = $details[$i]->allocated_amount;
                        $used_amount = $details[$i]->used_amount;
                        $balance_amount = $details[$i]->balance_amount;
                        $increment_amount = $details[$i]->increment_amount;
                        $decrement_amount = $details[$i]->decrement_amount;

                        $j=$i+1;

                         // <td>$increment_amount</td>
                         //    <td>$decrement_amount</td>
                         //    <td>$used_amount</td>


                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$fund_code</td>
                            <td>$department_code</td>
                            <td>$activity_code</td>
                            <td>$account_code</td>
                            <td>$balance_amount</td>
                            <td>$allocated_amount</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>    
                            <td>
                        </tr>";
                        // <span onclick='deleteTempData($id)'>Delete</a>
                        $total_detail = $total_detail + $allocated_amount;
                    }

                    $total_detail = number_format($total_detail, 2, '.', '');

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='5'></td>
                            <td bgcolor='' ><b> Total : </b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function deleteBudgetAllocation($id_allocation)
    {
        $inserted_id = $this->budget_allocation_model->deleteData($id_allocation);

        return TRUE;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";print_r($id);exit;
        $inserted_id = $this->budget_allocation_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function addAllocationData()
    {
        $user_id = $this->session->userId;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['status'] = 0;
        $tempData['created_by'] = $user_id;
        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->budget_allocation_model->addBudgetAllocation($tempData);
    }

    function tempDeleteEdit($id)
    {
        // echo "<Pre>";print_r($id);exit;
        $inserted_id = $this->budget_allocation_model->deleteTempData($id);
        
        echo "";exit(); 
    } 
}
