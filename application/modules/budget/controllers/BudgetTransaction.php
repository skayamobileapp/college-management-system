<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BudgetTransaction extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('budget_transaction_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('budget_transaction.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['financialYearList'] = $this->budget_transaction_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->budget_transaction_model->budgetYearListByStatus('1');
            $data['departmentList'] = $this->budget_transaction_model->departmentListByStatus('1');

            $data['fundCodeList'] = $this->budget_transaction_model->fundCodeListByStatus('1');
            $data['departmentCodeList'] = $this->budget_transaction_model->departmentCodeListByStatus('1');
            $data['activityCodeList'] = $this->budget_transaction_model->activityCodeListByStatus('1');
            $data['accountCodeList'] = $this->budget_transaction_model->accountCodeListByStatus('1');

            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;


            $data['budgetTransactionList'] = $this->budget_transaction_model->budgetTransactionListSearch($formData);

            // echo "<Pre>"; print_r($data['searchParam']);exit;
            $this->global['pageTitle'] = 'FIMS : List Budget Transaction';
            $this->loadViews("budget_transaction/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('budget_transaction.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $id_budget_year = $this->security->xss_clean($this->input->post('id_dt_budget_year'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_dt_financial_year'));
                $department_code = $this->security->xss_clean($this->input->post('id_dt_department'));
                $amount = $this->security->xss_clean($this->input->post('entered_amount'));
                $id_budget_allocation = $this->security->xss_clean($this->input->post('id_budget_allocation'));

                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

                $id_budget_amount = $this->budget_transaction_model->getBudgetAmount($id_budget_year,$department_code,$id_financial_year);


                $data = array(
                        'id_budget_allocation' => $id_budget_allocation,             
                        'id_budget_amount' => $id_budget_amount->id,             
                        'id_budget_year' => $id_budget_year,                 
                        'id_financial_year' => $id_financial_year,                 
                        'department_code' => $department_code,                 
                        'amount' => $amount,
                        'status' => '0',
                        'created_by' => $user_id
                        );
               // echo "<Pre>"; print_r($data);exit;
                $id_budget_transaction = $this->budget_transaction_model->addNewBudgetTransaction($data);

                if($id_budget_transaction)
                {

                $updated_master = $this->budget_transaction_model->moveBudgetTransactionDetailsTempTDetail($id_budget_transaction);
                }


                redirect('/budget/budgetTransaction/list');
            }
            else
            {
                $temp_deleted = $this->budget_transaction_model->deleteTempBudgetTransactionDetailsBySession($id_session);
            }

            $data['financialYearList'] = $this->budget_transaction_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->budget_transaction_model->budgetYearListByStatus('1');
            $data['departmentList'] = $this->budget_transaction_model->departmentListByStatus('1');

            $data['fundCodeList'] = $this->budget_transaction_model->fundCodeListByStatus('1');
            $data['activityCodeList'] = $this->budget_transaction_model->activityCodeListByStatus('1');
            $data['accountCodeList'] = $this->budget_transaction_model->accountCodeListByStatus('1');
            // $data['departmentCodeList'] = $this->budget_transaction_model->departmentCodeForTransfer('');
            $data['departmentCodeList'] = $this->budget_transaction_model->departmentCodeListByStatus('1');
            
            $this->global['pageTitle'] = 'FIMS : Add Budget Transaction';
            $this->loadViews("budget_transaction/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('budget_transaction.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetTransaction/list');
            }
            if($this->input->post())
            {
                redirect('/budget/budgetTransaction/list');
            }
            $data['budgetTransaction'] = $this->budget_transaction_model->getBudgetTransaction($id);
            $data['budgetTransactionDetailsList'] = $this->budget_transaction_model->getBudgetTransactionDetails($id);

            $id_budget_allocation = $data['budgetTransaction']->id_budget_allocation;
            $data['budgetAllocation'] = $this->budget_transaction_model->getBudgetAllocation($id_budget_allocation);
            // echo "<Pre>"; print_r($data['budgetAllocation']);exit;
            $this->global['pageTitle'] = 'FIMS : View Budget Transaction';
            $this->loadViews("budget_transaction/edit", $this->global, $data, NULL);
        }
    }

     function approvalList()
    {

        if ($this->checkAccess('budget_transaction.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['financialYearList'] = $this->budget_transaction_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->budget_transaction_model->budgetYearListByStatus('1');
            $data['departmentList'] = $this->budget_transaction_model->departmentListByStatus('1');

            $data['fundCodeList'] = $this->budget_transaction_model->fundCodeListByStatus('1');
            $data['departmentCodeList'] = $this->budget_transaction_model->departmentCodeListByStatus('1');
            $data['activityCodeList'] = $this->budget_transaction_model->activityCodeListByStatus('1');
            $data['accountCodeList'] = $this->budget_transaction_model->accountCodeListByStatus('1');

            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;


            $data['budgetTransactionList'] = $this->budget_transaction_model->budgetTransactionListSearch($formData);

            // echo "<Pre>"; print_r($data['budgetTransactionList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Budget Transaction';
            $this->loadViews("budget_transaction/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('budget_transaction.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetTransaction/list');
            }
            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'reason' => $reason,
                    'status' => $status
                );
                
                $result = $this->budget_transaction_model->updateBudgetTransaction($data,$id);
                if($status == 1)
                {
                    if($result)
                    {
                        $updated = $this->budget_transaction_model->updateBudgetAllocationAmount($id);
                    }
                }
                redirect('/budget/budgetTransaction/approvalList');
            }
            $data['budgetTransaction'] = $this->budget_transaction_model->getBudgetTransaction($id);
            $data['budgetTransactionDetailsList'] = $this->budget_transaction_model->getBudgetTransactionDetails($id);
            $id_budget_allocation = $data['budgetTransaction']->id_budget_allocation;
            $data['budgetAllocation'] = $this->budget_transaction_model->getBudgetAllocation($id_budget_allocation);
            // echo "<Pre>"; print_r($data['budgetTransactionDetails']);exit;
            $this->global['pageTitle'] = 'FIMS : View Budget Transaction';
            $this->loadViews("budget_transaction/view", $this->global, $data, NULL);
        }
    }

    function getDebitBudget()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->budget_transaction_model->getDebitBudget($data);

        if($budget_data == '')
        {
            echo "";exit;
        }

        $id_budget_amount = $budget_data->id;
        
        // echo "<Pre>";print_r($id_budget_amount);exit;

        $results = $this->budget_transaction_model->getBudgetAllocationByBudgetAmount($id_budget_amount);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
        ";

        $table.="
        <select name='dt_fund_code' id='dt_fund_code' class='form-control' style='width: 420px' onchange='getActivityDebitCodes()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {


        // $id = $results[$i]->id_procurement_category;
        // $id = $results[$i]->id;
        $fund_code = $results[$i]->fund_code;
        $fund_data = $this->budget_transaction_model->getFundNameByCode($fund_code);
        // echo "<Pre>"; print_r($fund_data);exit;
        $fund_name = $fund_data->fund_name;

        $table.="<option value=".$fund_code.">".$fund_code . " - " . $fund_name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }


    function getActivityDebitCodes()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->budget_transaction_model->getDebitBudget($data);

        if($budget_data == '')
        {
            echo "";exit;
        }

        $id_budget_amount = $budget_data->id;
        
        // echo "<Pre>";print_r($id_budget_amount);exit;

        $results = $this->budget_transaction_model->getBudgetAllocationByBudgetAmounNFund($id_budget_amount,$data['dt_fund_code']);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
        ";

        $table.="
        <select name='dt_activity_code' id='dt_activity_code' class='form-control' style='width: 420px' onchange='getAccountDebitCodes()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        // $id = $results[$i]->id;
        // $activity_name = $results[$i]->activity_name;
        $activity_code = $results[$i]->activity_code;
        $data = $this->budget_transaction_model->getActivityNameByCode($activity_code);
        // echo "<Pre>"; print_r($fund_data);exit;
        $activity_name = $data->activity_name;

        $table.="<option value=".$activity_code.">".$activity_code . " - " . $activity_name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }

    function getAccountDebitCodes()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->budget_transaction_model->getDebitBudget($data);

        if($budget_data == '')
        {
            echo "";exit;
        }

        $id_budget_amount = $budget_data->id;
        
        // echo "<Pre>";print_r($data);exit;

        $results = $this->budget_transaction_model->getBudgetAllocationByBudgetAmounNFundNActivity($id_budget_amount,$data['dt_fund_code'],$data['dt_activity_code']);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
        ";

        $table.="
        <select name='dt_account_code' id='dt_account_code' class='form-control' style='width: 420px' onchange='getBudgetAllocationByCodes()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        // $id = $results[$i]->id;
        $account_code = $results[$i]->account_code;
        $data = $this->budget_transaction_model->getAccountNameByCode($account_code);
        // echo "<Pre>"; print_r($fund_data);exit;

        $account_name = $data->account_name;

        $table.="<option value=".$account_code.">".$account_code . " - " . $account_name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }

    function getBudgetAllocationByCodes()
    {

        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->budget_transaction_model->getDebitBudget($data);

        if($budget_data == '')
        {
            echo "";exit;
        }


        if($budget_data != '')
        {
            $id_budget_amount = $budget_data->id;
            $amount = $budget_data->amount;

            $data['dt_id_budget_amount'] = $id_budget_amount;

             $budget_allocation = $this->budget_transaction_model->getBudgetAllocationByCodes($data);

             // echo "<Pre>"; print_r($budget_allocation);exit;

             $id = $budget_allocation->id;
             $allocated_amount = $budget_allocation->allocated_amount;
             $balance_amount = $budget_allocation->balance_amount;
             $used_amount = $budget_allocation->used_amount;
             $increment_amount = $budget_allocation->increment_amount;
             $decrement_amount = $budget_allocation->decrement_amount;
            
            $table = "

                 <div class='row'>
                    
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Allocated Amount <span class='error-text'>*</span></label>
                            <input type='number' class='form-control' name='allocated_amount' id='allocated_amount' value='$allocated_amount' readonly>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Used Amount <span class='error-text'>*</span></label>
                            <input type='number' class='form-control' name='used_amount' id='used_amount' value='$used_amount' readonly>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Balance Amount <span class='error-text'>*</span></label>
                            <input type='number' class='form-control' name='balance_amount' id='balance_amount' value='$balance_amount' readonly>
                        </div>
                    </div>

                </div>


                <div class='row'>
                    
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Enter Transaction Amount <span class='error-text'>*</span></label>
                            <input type='number' class='form-control' name='entered_amount' id='entered_amount'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label> <span class='error-text'>*</span></label>
                            <input type='hidden' class='form-control' name='id_budget_allocation' id='id_budget_allocation' value='$id'>
                        </div>
                    </div>
                </div> ";

        }
        else
        {
            $table="";
        }

       echo $table;exit();

    }


    function checkDepartmentAvailable()
    {
         $data = $this->security->xss_clean($this->input->post('data'));
         // echo "<Pre>";print_r($tempData);exit();
         $department_data = $this->budget_transaction_model->checkDepartmentAvailable($data);

         if($department_data)
         {
            echo "<div class='row'>
             <div class='col-sm-4'>
                        <div class='form-group'>
                            <label> <span class='error-text'>*</span></label>
                            <input type='hidden' class='form-control' name='id_budget_allocation' id='id_budget_allocation' value='$id'>
                        </div>
                    </div>
            </div>";exit();
         }
         else
         {
            echo "";exit();
         }

    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $user_id = $this->session->userId;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['balance_amount'] = $tempData['allocated_amount'];
        $tempData['increment_amount'] = 0;
        $tempData['decrement_amount'] = 0;
        $tempData['used_amount'] = 0;
        $tempData['id_session'] = $id_session;
        $tempData['status'] = 1;
        $tempData['created_by'] = $user_id;
        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->budget_transaction_model->addTempBudgetTransactionDetails($tempData);
        $data = $this->displaytempdata();
        
        echo $data;

    }

    function tempDelete($id)
    {
        // echo "<Pre>";print_r($id);exit;
        $inserted_id = $this->budget_transaction_model->deleteTempBudgetTransactionDetailsById($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 


    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->budget_transaction_model->getTempBudgetTransactionDetailsBySession($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($details))
        {
            
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>GL CODE</th>
                    <th>Used Amount</th>
                    <th>Balance Amount</th>
                    <th>Allocated Amount</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $fund_code = $details[$i]->fund_code;
                        $department_code = $details[$i]->department_code;
                        $activity_code = $details[$i]->activity_code;
                        $account_code = $details[$i]->account_code;
                        $allocated_amount = $details[$i]->allocated_amount;
                        $used_amount = $details[$i]->used_amount;
                        $balance_amount = $details[$i]->balance_amount;
                        $increment_amount = $details[$i]->increment_amount;
                        $decrement_amount = $details[$i]->decrement_amount;

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$fund_code - $department_code - $activity_code - $account_code</td>
                            <td>$used_amount</td>
                            <td>$balance_amount</td>
                            <td>$allocated_amount</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTemp($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_detail = $total_detail + $allocated_amount;
                                // <span onclick='deleteTemp($id)'>Delete</a>
                    }

                    $total_detail = number_format($total_detail, 2, '.', '');

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='3'></td>
                            <td bgcolor='' ><b> Total : </b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }


    function budgetTransactionReportByDepartment()
    {
        if ($this->checkAccess('budget_transaction_by_department.report') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['financialYearList'] = $this->budget_transaction_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->budget_transaction_model->budgetYearListByStatus('1');
            $data['departmentList'] = $this->budget_transaction_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->budget_transaction_model->departmentCodeListByStatus('1');

            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'reason' => $reason,
                    'status' => $status
                );
                
                $result = $this->budget_transaction_model->updateBudgetTransaction($data,$id);
                if($status == 1)
                {
                    if($result)
                    {
                        $updated = $this->budget_transaction_model->updateBudgetAllocationAmount($id);
                    }
                }
                redirect('/budget/budgetTransaction/approvalList');
            }

            $this->global['pageTitle'] = 'FIMS : List Budget Transaction';
            $this->loadViews("reports/transaction_by_department", $this->global, $data, NULL);
        }
    }

    function getBudgetTransactionReportByDepartment()
    {

        $user_id = $this->session->userId;
        $data = $this->security->xss_clean($this->input->post('data'));

        $report_data = $this->budget_transaction_model->getBudgetTransactionReport($data);
        if(!empty($report_data))
        {
            $data = $this->displayBudgetTransactionReport($report_data);
        }
        else
        {
            $data = "
            <div align='center' class='row'>
            <h3>No Data Found</h3>
            </div>
            ";
        }
        // echo "<Pre>";print_r($report_data);exit;

        
        echo $data;
    }

    function displayBudgetTransactionReport($details)
    {
        if(!empty($details))
        {

        // echo "<Pre>";print_r($details);exit;
            
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fund Code</th>
                    <th>Department Code</th>
                    <th>Activity Code</th>
                    <th>Account Code</th>
                    <th>Allocated Amount</th>
                    <th>Increment Amount</th>
                    <th>Decrement Amount</th>
                    <th>Used Amount</th>
                    <th>Balance Amount</th>
                    </tr>
                </thead>";
                    $total_balance = 0;
                    $total_increment = 0;
                    $total_decrement = 0;
                    $total_allocated = 0;
                    $total_used = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $fund_code = $details[$i]->fund_code;
                        $department_code = $details[$i]->department_code;
                        $activity_code = $details[$i]->activity_code;
                        $account_code = $details[$i]->account_code;
                        $allocated_amount = $details[$i]->allocated_amount;
                        $used_amount = $details[$i]->used_amount;
                        $balance_amount = $details[$i]->balance_amount;
                        $increment_amount = $details[$i]->increment_amount;
                        $decrement_amount = $details[$i]->decrement_amount;

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$fund_code</td>
                            <td>$department_code</td>
                            <td>$activity_code</td>
                            <td>$account_code</td>
                            <td>$allocated_amount</td>
                            <td>$increment_amount</td>
                            <td>$decrement_amount</td>
                            <td>$used_amount</td>
                            <td>$balance_amount</td>
                        </tr>";
                        $total_balance = $total_balance + $balance_amount;
                        $total_increment = $total_increment + $increment_amount;
                        $total_decrement = $total_decrement + $decrement_amount;
                        $total_allocated = $total_allocated + $allocated_amount;
                        $total_used = $total_used + $used_amount;
                    }

                    $total_balance = number_format($total_balance, 2, '.', '');
                    $total_increment = number_format($total_increment, 2, '.', '');
                    $total_decrement = number_format($total_decrement, 2, '.', '');
                    $total_allocated = number_format($total_allocated, 2, '.', '');
                    $total_used = number_format($total_used, 2, '.', '');

                     $table .= "

                    <tr>
                            <td colspan='4'></td>
                            <td ><b> Total : </b></td>
                            <td ><b>$total_allocated</b></td>
                            <td ><b>$total_increment</b></td>
                            <td ><b>$total_decrement</b></td>
                            <td ><b>$total_used</b></td>
                            <td ><input type='hidden' name='total_detail' id='total_detail' value='$total_balance' /><b>$total_balance</b></td>
                        </tr>
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_balance='';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_balance' />
            ";
        }
        return $table;
    }
}
