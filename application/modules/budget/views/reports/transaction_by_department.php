<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Budget Transaction By Department Report</h3>
        </div>


        <form id="form_subject" action="" method="post">



        <h3>From Budget GL</h3>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debit Budget Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debit Financial Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debit Department Code<span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   

            </div>

              <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateData()">Submit</button>
                    <!-- <a href="list" class="btn btn-link">Back</a> -->
                </div>
            </div>




             <div id="view_report_data_span" style="display: none">

            </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>



<script>

    function validateData()
    {
        if($('#form_subject').valid())
        {

            var id_financial_year = $("#id_financial_year").val();
            var id_budget_year = $("#id_budget_year").val();
            var department_code = $("#department_code").val();

            var tempPR = {};
            tempPR['id_financial_year'] = id_financial_year;
            tempPR['id_budget_year'] = id_budget_year;
            tempPR['department_code'] = department_code;

            // alert(id_financial_year);
                $.ajax(
                {
                   url: '/budget/budgetTransaction/getBudgetTransactionReportByDepartment',
                   type: 'POST',
                   data:
                   {
                    data: tempPR
                   },
                   error: function()
                   {
                    alert('Something Wrong');
                   },
                   success: function(result)
                   {
                    if(result == '')
                    {
                        alert('No Debit Budget GL Found For Entered Data');
                    }
                    else
                    {
                        $("#view_report_data_span").html(result);
                        $("#view_report_data_span").show();

                    }
                    // $('#myModal').modal('hide');
                    // var ta = $("#total_detail").val();
                    // alert(ta);
                    // $("#amount").val(ta);
                   }
                });
        }    
    }


    $('select').select2();


    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                id_financial_year:
                {
                    required: true
                },
                id_budget_year:
                {
                    required: true
                },
                department_code:
                {
                    required: true
                }
            },
            messages:
            {
                id_financial_year:
                {
                    required: "<p class='error-text'>Select Debit Financial Year</p>",
                },
                id_budget_year:
                {
                    required: "<p class='error-text'>Select Debit Budget Year</p>",
                },
                department_code:
                {
                    required: "<p class='error-text'>Select Debit Department</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>