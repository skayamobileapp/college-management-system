<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_amount_model extends CI_Model
{

    function budgetAmountListSearch($data)
    {
        $this->db->select('ba.*, fy.name as financial_year, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        if ($data['id_budget_year']!='')
        {
            $this->db->where('ba.id_budget_year', $data['id_budget_year']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('ba.id_financial_year', $data['id_financial_year']);
        }
        if ($data['department_code']!='')
        {
            $this->db->where('ba.department_code', $data['department_code']);
        }
        if ($data['status']!='')
        {
            $this->db->where('ba.status', $data['status']);
            if($data['status'] == '0')
            {
                $this->db->where('ba.is_detail_added', '0');
                $this->db->where('ba.allocation_status', '0');
            }
        }
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function departmentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function fundCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    


    function departmentCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();

    }    

    function activityCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function accountCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function addNewBudgetAmount($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_amount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function getBudgetAmount($id)
    {
        $this->db->select('ba.*, fy.name as financial_year, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        $this->db->where('ba.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkDuplicateBudgetAmount($data)
    {
        $this->db->select('ba.*, fy.name as financial_year, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id_budget_year', $data['id_budget_year']);
        $this->db->where('ba.department_code', $data['department_code']);
        $this->db->where('ba.id_financial_year', $data['id_financial_year']);
        $this->db->where('ba.status !=','2');
        $query = $this->db->get();
        return $query->row();
    }

    function editBudgetAmount($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_amount', $data);
        return TRUE;
    }
}
