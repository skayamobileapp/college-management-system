<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EmergencyFundAllocation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('emergency_fund_allocation_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('emergency_fund_allocation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->emergency_fund_allocation_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->emergency_fund_allocation_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->emergency_fund_allocation_model->getDepartmentCodeList();



           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['emergencyFundAllocationList'] = $this->emergency_fund_allocation_model->emergencyFundAllocationListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : List Emergency Fund Allocation';
            $this->loadViews("emergency_fund_allocation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('emergency_fund_allocation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $contribution_amount = $this->security->xss_clean($this->input->post('contribution_amount'));
                $used_amount = $this->security->xss_clean($this->input->post('used_amount'));
                $balance_amount = $this->security->xss_clean($this->input->post('balance_amount'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $description = $this->security->xss_clean($this->input->post('description'));


                $data = array(
					'type' => $type,
					'id_financial_year' => $id_financial_year,
					'id_budget_year' => $id_budget_year,
					'fund_code' => $fund_code,
					'department_code' => $department_code,
					'activity_code' => $activity_code,
					'account_code' => $account_code,
                    'contribution_amount' => $contribution_amount,
					'balance_amount' => $contribution_amount,
					'used_amount' => 0,
					'id_staff' => $id_staff,
					'id_student' => $id_student,
                    'description' => $description,
                    'status' => 0,
                    'created_by' => $user_id
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->emergency_fund_allocation_model->addNewEmergencyFundAllocation($data);
                redirect('/ap/emergencyFundAllocation/list');
            }

            $data['financialYearList'] = $this->emergency_fund_allocation_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->emergency_fund_allocation_model->budgetYearListByStatus('1');
            $data['fundCodeList'] = $this->emergency_fund_allocation_model->getFundCodeList();
            $data['departmentCodeList'] = $this->emergency_fund_allocation_model->getDepartmentCodeList();
            $data['activityCodeList'] = $this->emergency_fund_allocation_model->getActivityCodeList();
            $data['accountCodeList'] = $this->emergency_fund_allocation_model->getAccountCodeList();



            $this->global['pageTitle'] = 'FIMS : Add Emergency Fund Allocation';
            $this->loadViews("emergency_fund_allocation/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('emergency_fund_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/emergencyFundAllocation/list');
            }
            if($this->input->post())
            {

                redirect('/ap/emergencyFundAllocation/list');
            }

            // $data['financialYearList'] = $this->emergency_fund_allocation_model->financialYearListByStatus('1');
            // $data['budgetYearList'] = $this->emergency_fund_allocation_model->budgetYearListByStatus('1');
            // $data['fundCodeList'] = $this->emergency_fund_allocation_model->getFundCodeList();
            // $data['departmentCodeList'] = $this->emergency_fund_allocation_model->getDepartmentCodeList();
            // $data['activityCodeList'] = $this->emergency_fund_allocation_model->getActivityCodeList();
            // $data['accountCodeList'] = $this->emergency_fund_allocation_model->getAccountCodeList();
            $data['staffList'] = $this->emergency_fund_allocation_model->getStaffListByStatus('1');
            $data['studentList'] = $this->emergency_fund_allocation_model->getStudentListByStatus('Approved');

            $data['emergencyFundAllocation'] = $this->emergency_fund_allocation_model->getEmergencyFundAllocation($id);

            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'FIMS : View Emergency Fund Allocation';
            $this->loadViews("emergency_fund_allocation/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('emergency_fund_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/emergencyFundAllocation/approvalList');
            }
            if($this->input->post())
            {

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
            // echo "<Pre>"; print_r($data);exit;

                
                 $result = $this->emergency_fund_allocation_model->updateEmergencyFundAllocation($data,$id);

                redirect('/ap/emergencyFundAllocation/approvalList');
            }

            $data['staffList'] = $this->emergency_fund_allocation_model->getStaffListByStatus('1');
            $data['studentList'] = $this->emergency_fund_allocation_model->getStudentListByStatus('Approved');

            $data['emergencyFundAllocation'] = $this->emergency_fund_allocation_model->getEmergencyFundAllocation($id);

            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'FIMS : Approve Emergency Fund Allocation';
            $this->loadViews("emergency_fund_allocation/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('emergency_fund_allocation.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->emergency_fund_allocation_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->emergency_fund_allocation_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->emergency_fund_allocation_model->getDepartmentCodeList();



           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['emergencyFundAllocationList'] = $this->emergency_fund_allocation_model->emergencyFundAllocationListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Approval List Emergency Fund Allocation';
            $this->loadViews("emergency_fund_allocation/approval_list", $this->global, $data, NULL);
        }
    }


    function getEFADataByType()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];
        
        switch ($type)
        {
            case 'Staff':

                $table = $this->getStaffList();

                break;

            case 'Student':

                $table = $this->getStudentList();
                
                break;


            default:
                # code...
                break;
        }
        echo $table;        
    }

    function getStaffList()
    {
         $data = $this->emergency_fund_allocation_model->getStaffListByStatus('1');
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Staff <span class='error-text'>*</span></label>
                <select name='id_staff' id='id_staff' class='form-control'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $name = $data[$i]->name;
                $ic_no = $data[$i]->ic_no;

                $table.="<option value=".$id.">".$name. " - " . $ic_no . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentList()
    {
         $data = $this->emergency_fund_allocation_model->getStudentListByStatus('Approved');
                // echo "<Pre>";print_r($data);exit();
        
        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control' >";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }
}
