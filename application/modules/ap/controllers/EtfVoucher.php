<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EtfVoucher extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('etf_voucher_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('etf_voucher.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['bankList'] = $this->etf_voucher_model->getBankList();
            // $data['investmentInstitutionList'] = $this->etf_voucher_model->investmentInstitutionListByStatus('1');

            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';

 
            $data['searchParam'] = $formData;

            $data['etfVoucherList'] = $this->etf_voucher_model->getEtfVoucherListSearch($formData);

            // echo "<Pre>";print_r($data['etfVoucherList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Etf Voucher';
            $this->loadViews("etf_voucher/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('etf_voucher.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $formData = $this->input->post();


                $generated_number = $this->etf_voucher_model->generateEtfVoucherNumber();

                $payment_mode = $this->security->xss_clean($this->input->post('payment_mode'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $letter_reference_number = $this->security->xss_clean($this->input->post('letter_reference_number'));
                $batch_id = $this->security->xss_clean($this->input->post('batch_id'));


                $data = array(
                    'payment_mode'=> $payment_mode,
                    'id_bank'=> $id_bank,
                    'description'=> $description,
                    'letter_reference_number'=> $letter_reference_number,
                    'batch_id'=> $batch_id,
                    'reference_number'=> $generated_number,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
             $inserted_id = $this->etf_voucher_model->addEtfVoucher($data);

             if($inserted_id)
                {
                    // $referal_update['ap_status'] = 'V0';
                    $referal_update['is_sab'] = $inserted_id;

                // echo "<Pre>"; print_r($formData);exit;

                $total_bill_amount = 0;
                 for($i=0;$i<count($formData['id_pv']);$i++)
                 {
                    $id_pv = $formData['id_pv'][$i];
                    // $total_amount = $formData['total_amount'][$i];

                    if($id_pv > 0)
                    {
                        $detail_data = $this->etf_voucher_model->getPaymentVoucher($id_pv);


                             $detailsData = array(
                                'id_etf_voucher'=>$inserted_id,
                                'id_referal'=>$id_pv,
                                'amount'=>$detail_data->total_amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );

                        $total_bill_amount = $total_bill_amount + $detail_data->total_amount;

                        $inserted_detail_id = $this->etf_voucher_model->addEtfVoucherDetail($detailsData);
                        if($inserted_detail_id)
                        {
                            $updated_referal = $this->etf_voucher_model->updatePaymentVoucher($referal_update,$id_pv);
                        }
                    }

                }
            }
            if($inserted_id)
            {
                $bill_data['total_amount'] = $total_bill_amount;
                $updated_bill_amount = $this->etf_voucher_model->updateEtfVoucher($bill_data,$inserted_id);
            }
                    // echo "<Pre>"; print_r($inserted_detail_id);exit;
                redirect('/ap/etfVoucher/list');
            }

            $data['bankList'] = $this->etf_voucher_model->getBankList();
            

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Etf Voucher';
            $this->loadViews("etf_voucher/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('etf_voucher.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/etfVoucher/list');
            }
            
            $data['etfVoucher'] = $this->etf_voucher_model->getEtfVoucher($id);
            $data['etfVoucherDetails'] = $this->etf_voucher_model->getEtfVoucherDetails($id);
            // echo "<Pre>";print_r($data);exit();

            

            $this->global['pageTitle'] = 'FIMS : View Etf Voucher';
            $this->loadViews("etf_voucher/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {

        if ($this->checkAccess('etf_voucher.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/etfVoucher/approvalList');
            }

            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                // $type = $this->security->xss_clean($this->input->post('type'));
                // $id_from = $this->security->xss_clean($this->input->post('id_from'));



                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->etf_voucher_model->updateEtfVoucher($data,$id);
                redirect('/ap/etfVoucher/approvalList');

            }
            
           $data['etfVoucher'] = $this->etf_voucher_model->getEtfVoucher($id);
            $data['etfVoucherDetails'] = $this->etf_voucher_model->getEtfVoucherDetails($id);
            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'FIMS : Approve Etf Voucher';
            $this->loadViews("etf_voucher/view", $this->global, $data, NULL);
        }
    }



     function approvalList()
    {

        if ($this->checkAccess('etf_voucher.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             $data['bankList'] = $this->etf_voucher_model->getBankList();
            // $data['investmentInstitutionList'] = $this->etf_voucher_model->investmentInstitutionListByStatus('1');

            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';

 
            $data['searchParam'] = $formData;

            $data['etfVoucherList'] = $this->etf_voucher_model->getEtfVoucherListSearch($formData);


            // echo "<Pre>";print_r($data['etfVoucherList']);exit;
            $this->global['pageTitle'] = 'FIMS : Approval List Etf Voucher';
            $this->loadViews("etf_voucher/approval_list", $this->global, $data, NULL);
        }
    }


    function searchPV()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // if($tempData['from_date'])
        // {
        //     $tempData['from_date'] = date('Y-m-d',strtotime($tempData['from_date']));
        // }
        // if($tempData['to_date'])
        // {
        //     $tempData['to_date'] = date('Y-m-d',strtotime($tempData['to_date']));
        // }

        // echo "<Pre>";print_r($tempData);exit();

        $voucher_data = $this->etf_voucher_model->searchPaymentVoucher($tempData);
        // echo "<Pre>";print_r($voucher_data);exit();

        if(!empty($voucher_data))
         {
             $table = "
        <h4>Payment Vouchers For Etf Voucher</h4>
        <div class='custom-table'>
        <table class='table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Voucher Number</th>
                    <th>Type </th>
                    <th>Entry</th>
                    <th>Account Number</th>
                    <th>Payment Mode</th>
                    <th>Payment Date</th>
                    <th>Payee Name</th>
                    <th>Bank</th>
                    <th>Amount</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                    </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($voucher_data);$i++)
                    {
                        $id = $voucher_data[$i]->id;
                        $type = $voucher_data[$i]->type;
                        $entry = $voucher_data[$i]->entry;
                        $reference_number = $voucher_data[$i]->reference_number;
                        $payment_mode = $voucher_data[$i]->payment_mode;
                        $payee_name = $voucher_data[$i]->payee_name;
                        $payment_reference_number = $voucher_data[$i]->payment_reference_number;
                        $payment_date = $voucher_data[$i]->payment_date;
                        $bank_name = $voucher_data[$i]->bank_name;
                        $bank_code = $voucher_data[$i]->bank_code;
                        $total_amount = $voucher_data[$i]->total_amount;
                        $account_number = $voucher_data[$i]->account_number;



                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                            </td>
                            <td>$reference_number</td>
                            <td>$type</td>
                            <td>$entry</td>
                            <td>$account_number</td>
                            <td>$payment_mode</td>
                            <td>$payment_date</td>
                            <td>$payee_name</td>
                            <td>$bank_code - $bank_name</td>
                            <td>$total_amount</td>
                            
                            <td class='text-center'>

                            <input type='checkbox' id='id_pv[]' name='id_pv[]' class='check' value='".$id."'>
                        </tr>
                        </tbody>";
                    }

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
                </div>";

         

         }
         else
         {
               $table = "
        <h4>No Payment Voucher Availble For Selected Search</h4>";

        }
        

        echo $table;exit();
    }
}
