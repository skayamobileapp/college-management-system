<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PettyCashEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('petty_cash_entry_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('petty_cash_entry.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->petty_cash_entry_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->petty_cash_entry_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->petty_cash_entry_model->getDepartmentCodeList();

           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['pettyCashEntryList'] = $this->petty_cash_entry_model->pettyCashEntryListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data['pettyCashEntryList']);exit();


            $this->global['pageTitle'] = 'FIMS : List Petty Cash Entry';
            $this->loadViews("petty_cash_entry/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('petty_cash_entry.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $generated_number = $this->petty_cash_entry_model->generatePettyCashEntryNumber();

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $id_petty_cash_allocation = $this->security->xss_clean($this->input->post('id_petty_cash_allocation'));
                $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));
                $transation_type = $this->security->xss_clean($this->input->post('transation_type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $description = $this->security->xss_clean($this->input->post('description'));


                $petty_cash_allocation_used_amount = $this->security->xss_clean($this->input->post('petty_cash_allocation_used_amount'));
                $petty_cash_allocation_balance_amount = $this->security->xss_clean($this->input->post('petty_cash_allocation_balance_amount'));



                $data = array(
					'id_financial_year' => $id_financial_year,
                    'id_budget_year' => $id_budget_year,
					'id_petty_cash_allocation' => $id_petty_cash_allocation,
					'fund_code' => $fund_code,
					'department_code' => $department_code,
					'activity_code' => $activity_code,
					'account_code' => $account_code,
                    'reference_number' => $generated_number,
					'paid_amount' => $paid_amount,
					'id_staff' => $id_staff,
                    'description' => $description,
                    'status' => 0,
                    'created_by' => $user_id
                );
                // echo "<Pre>"; print_r($data);exit;

                $inserted_id = $this->petty_cash_entry_model->addNewPettyCashEntry($data);
                if($inserted_id)
                {


                    $update_allocation['used_amount'] = $petty_cash_allocation_used_amount + $paid_amount;
                    $update_allocation['balance_amount'] = $petty_cash_allocation_balance_amount - $paid_amount;

                    $inserted_id = $this->petty_cash_entry_model->updatePettyCashAllocation($update_allocation,$id_petty_cash_allocation);
                }
                redirect('/ap/pettyCashEntry/list');
            }

            $data['financialYearList'] = $this->petty_cash_entry_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->petty_cash_entry_model->budgetYearListByStatus('1');
            $data['fundCodeList'] = $this->petty_cash_entry_model->getFundCodeList();
            $data['departmentCodeList'] = $this->petty_cash_entry_model->getDepartmentCodeList();
            $data['activityCodeList'] = $this->petty_cash_entry_model->getActivityCodeList();
            $data['accountCodeList'] = $this->petty_cash_entry_model->getAccountCodeList();
            $data['pettyCashAllocationList'] = $this->petty_cash_entry_model->pettyCashAllocationListByStatus('1');
            $data['staffList'] = $this->petty_cash_entry_model->getStaffListByStatus('1');



            $this->global['pageTitle'] = 'FIMS : Add Petty Cash Entry';
            $this->loadViews("petty_cash_entry/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('petty_cash_entry.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/pettyCashEntry/list');
            }
            if($this->input->post())
            {

                redirect('/ap/pettyCashEntry/list');
            }

            $data['staffList'] = $this->petty_cash_entry_model->getStaffListByStatus('1');
            $data['studentList'] = $this->petty_cash_entry_model->getStudentListByStatus('Approved');

            $data['pettyCashEntry'] = $this->petty_cash_entry_model->getPettyCashEntry($id);

            // echo "<Pre>"; print_r($data['pettyCashEntry']);exit;


            $this->global['pageTitle'] = 'FIMS : View Petty Cash Entry';
            $this->loadViews("petty_cash_entry/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('petty_cash_entry.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/pettyCashEntry/list');
            }
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $id_petty_cash_allocation = $this->security->xss_clean($this->input->post('id_petty_cash_allocation'));
                $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
            // echo "<Pre>"; print_r($data);exit;

                
                 $result = $this->petty_cash_entry_model->updatePettyCashEntry($data,$id);
                 if($status == '2')
                 {
                    if($result)
                    {
                    
                        $allocation_data = $this->petty_cash_entry_model->getPettyCashAllocationById($id_petty_cash_allocation);


                        $updated_allocation['used_amount'] = $allocation_data->used_amount - $paid_amount;
                        $updated_allocation['balance_amount'] = $allocation_data->balance_amount + $paid_amount;

                    // echo "<Pre>"; print_r($updated_allocation);exit;
                        $result = $this->petty_cash_entry_model->updatePettyCashAllocation($updated_allocation,$id_petty_cash_allocation);
                    }
                 }

                redirect('/ap/pettyCashEntry/approvalList');
            }

            $data['staffList'] = $this->petty_cash_entry_model->getStaffListByStatus('1');
            $data['studentList'] = $this->petty_cash_entry_model->getStudentListByStatus('Approved');

            $data['pettyCashEntry'] = $this->petty_cash_entry_model->getPettyCashEntry($id);

            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'FIMS : Approve Petty Cash Entry';
            $this->loadViews("petty_cash_entry/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('petty_cash_entry.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->petty_cash_entry_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->petty_cash_entry_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->petty_cash_entry_model->getDepartmentCodeList();



           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['pettyCashEntryList'] = $this->petty_cash_entry_model->pettyCashEntryListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Approval List Petty Cash Allocation';
            $this->loadViews("petty_cash_entry/approval_list", $this->global, $data, NULL);
        }
    }


    function getPCAllocation()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $data = $this->petty_cash_entry_model->getPettyCashAllocation($tempData['id_petty_cash_allocation']);
        
        // echo "<Pre>";print_r($data);exit();
        $table = "

        

            <div class='row'>

                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Petty Cash Allocation Reference Number<span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='petty_cash_allocation' name='petty_cash_allocation' value='$data->reference_number' readonly>

                        <input type='hidden' class='form-control' id='petty_cash_allocation_used_amount' name='petty_cash_allocation_used_amount' value='$data->used_amount'>
                    </div>
                </div>



                  <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Balance Amount <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='petty_cash_allocation_balance_amount' name='petty_cash_allocation_balance_amount' value='$data->balance_amount' readonly>
                    </div>
                </div>
            </div>

            ";
        
        echo $table;        
    }

    function getStaffList()
    {
         
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Staff <span class='error-text'>*</span></label>
                <select name='id_staff' id='id_staff' class='form-control' onchange='getAllocationData()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $name = $data[$i]->name;
                $ic_no = $data[$i]->ic_no;

                $table.="<option value=".$id.">".$name. " - " . $ic_no . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }
}
