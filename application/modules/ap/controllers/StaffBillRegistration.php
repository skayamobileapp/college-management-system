<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StaffBillRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_bill_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('staff_bill_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['financialYearList'] = $this->staff_bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->staff_bill_registration_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->staff_bill_registration_model->getDepartmentCodeList();

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['billRegistrationList'] = $this->staff_bill_registration_model->getBillRegistrationListSearch($formData);

            // echo "<Pre>";print_r($data['billRegistrationList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Staff Bill Registration';
            $this->loadViews("staff_bill_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('staff_bill_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;



                $generated_number = $this->staff_bill_registration_model->generateStaffBillRegistrationNumber();

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $customer_name = $this->security->xss_clean($this->input->post('customer_name'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $bank_acc_no = $this->security->xss_clean($this->input->post('bank_acc_no'));

                if($type == 'Staff Claim')
                {
                    $customer_name = '';
                }
                elseif($type == 'General Claim')
                {
                    $id_staff = '';
                }

                $data = array(
                    'type'=> $type,
                    'reference_number'=> $generated_number,
                    'id_financial_year'=> $id_financial_year,
                    'id_budget_year'=> $id_budget_year,
                    'department_code'=> $department_code,
                    'bank_acc_no'=> $bank_acc_no,
                    'date_time'=> $date_time,
                    'total_amount'=> $amount,
                    'description'=> $description,
                    'customer_name'=> $customer_name,
                    'id_staff'=> $id_staff,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
             $inserted_id = $this->staff_bill_registration_model->addStaffBillRegistration($data);
                // echo "<Pre>"; print_r($inserted_id);exit;

             if($inserted_id)
                {       
                    $inserted_detail_id = $this->staff_bill_registration_model->moveTempBillRegistrationDetail($inserted_id);
                }
                redirect('/ap/staffBillRegistration/list');
            }
            else
            {
                $deleted_temp = $this->staff_bill_registration_model->deleteTempDetailsBySession();
            }

            $data['financialYearList'] = $this->staff_bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->staff_bill_registration_model->budgetYearListByStatus('1');
            $data['departmentList'] = $this->staff_bill_registration_model->departmentListByStatus('1');
            $data['fundCodeList'] = $this->staff_bill_registration_model->getFundCodeList();
            $data['departmentCodeList'] = $this->staff_bill_registration_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->staff_bill_registration_model->getAccountCodeList();
            $data['activityCodeList'] = $this->staff_bill_registration_model->getActivityCodeList();
            $data['prCategoryList'] = $this->staff_bill_registration_model->prCategoryList();
            $data['taxList'] = $this->staff_bill_registration_model->getTaxList();
            $data['staffList'] = $this->staff_bill_registration_model->getStaffListByStatus('1');
            // $data['bankList'] = $this->staff_bill_registration_model->getBankList();
            

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Staff Bill Registration';
            $this->loadViews("staff_bill_registration/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('staff_bill_registration.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/staffBillRegistration/list');
            }
            
            
            $data['billRegistration'] = $this->staff_bill_registration_model->getBillRegistration($id);
            $data['billRegistrationDetails'] = $this->staff_bill_registration_model->getBillRegistrationDetails($id);
            $data['staffList'] = $this->staff_bill_registration_model->getStaffListByStatus('1');

            // $get['type'] = $data['billRegistration']->type;
            // $get['id'] = $data['billRegistration']->id_from;
            // $data['billData'] = $this->staff_bill_registration_model->getBillData($get);
            // echo "<Pre>";print_r($data);exit();

            

            $this->global['pageTitle'] = 'FIMS : View Staff Bill Registration';
            $this->loadViews("staff_bill_registration/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {

        if ($this->checkAccess('staff_bill_registration.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/billRegistration/approvalList');
            }

            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->staff_bill_registration_model->updateStaffBillRegistration($data,$id);

                redirect('/ap/staffBillRegistration/approvalList');

            }
            
            $data['billRegistration'] = $this->staff_bill_registration_model->getBillRegistration($id);
            $data['billRegistrationDetails'] = $this->staff_bill_registration_model->getBillRegistrationDetails($id);
            $data['staffList'] = $this->staff_bill_registration_model->getStaffListByStatus('1');
            // echo "<Pre>";print_r($data);exit();

            

            $this->global['pageTitle'] = 'FIMS : Approve Staff Bill Registration';
            $this->loadViews("staff_bill_registration/view", $this->global, $data, NULL);
        }
    }



     function approvalList()
    {

        if ($this->checkAccess('staff_bill_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
           $data['financialYearList'] = $this->staff_bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->staff_bill_registration_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->staff_bill_registration_model->getDepartmentCodeList();

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['billRegistrationList'] = $this->staff_bill_registration_model->getBillRegistrationListSearch($formData);

            // echo "<Pre>";print_r($data['billRegistrationList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Investment Application';
            $this->loadViews("staff_bill_registration/approval_list", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit();
        $inserted_id = $this->staff_bill_registration_model->addTempDetails($tempData);
        
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        
        $details = $this->staff_bill_registration_model->getTempStaffBRDetails(); 
        if(!empty($details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Debit GL Code</th>
                    <th>Credit GL Code</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>Item</th>
                    <th>Tax</th>
                    <th>Quantity</th>
                    <th>Tax Price</th>
                    <th>Amount</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $dt_account = $details[$i]->dt_account;
                        $dt_activity = $details[$i]->dt_activity;
                        $dt_department = $details[$i]->dt_department;
                        $dt_fund = $details[$i]->dt_fund;
                        $cr_account = $details[$i]->cr_account;
                        $cr_activity = $details[$i]->cr_activity;
                        $cr_department = $details[$i]->cr_department;
                        $cr_fund = $details[$i]->cr_fund;
                        $category_name = $details[$i]->category_name;
                        $sub_category_name = $details[$i]->sub_category_name;
                        $item_name = $details[$i]->item_name;
                        $tax_code = $details[$i]->tax_code;
                        $tax_name = $details[$i]->tax_name;
                        $quantity = $details[$i]->quantity;
                        $price = $details[$i]->price;
                        $tax_price = $details[$i]->tax_price;
                        $total_final = $details[$i]->total_final;
                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$dt_fund - $dt_department - $dt_activity - $dt_account</td>
                            <td>$cr_fund - $cr_department - $cr_activity - $cr_account</td>
                            <td>$category_name</td>
                            <td>$sub_category_name</td>
                            <td>$item_name</td>
                            <td>$tax_code - $tax_name</td>
                            <td>$quantity</td>
                            <td>$tax_price</td>
                            <td>$price</td>
                            <td>$total_final</td>
                            
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_detail = $total_detail + $total_final;
                    }

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='9'></td>
                            <td bgcolor=''><b> Total : </b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";print_r($type);exit;
        $inserted_id = $this->staff_bill_registration_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 
}
