<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Petty Cash Entry</h3>
            </div>


        <div class="form-container">
            <h4 class="form-group-title">Petty Cash Entry Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Petty Cash Allocation <span class='error-text'>*</span></label>
                        <select name="id_petty_cash_allocation" id="id_petty_cash_allocation" class="form-control" onchange="getPCAllocation()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($pettyCashAllocationList))
                            {
                                foreach ($pettyCashAllocationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->reference_number;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <select name="fund_code" id="fund_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <select name="activity_code" id="activity_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <select name="id_staff" id="id_staff" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->ic_no . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Paid Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="paid_amount" name="paid_amount">
                    </div>
                </div>

                
            </div>




            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

            </div>

        </div>



        <div class="form-container" style="display: none;" id="view_pca_display">
            <h4 class="form-group-title">Petty Cash Allocation Details</h4>

            <div id="view_pca_allocation">

            </div>


        </div>




            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>

            <br>

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

</form>
<script>


    function getPCAllocation()
    {

        var tempPR = {};
        tempPR['id_petty_cash_allocation'] = $("#id_petty_cash_allocation").val();
        if(tempPR['id_petty_cash_allocation'] != '')
        {
            $.ajax(
            {
               url: '/ap/pettyCashEntry/getPCAllocation',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_pca_display").show();
                $("#view_pca_allocation").html(result);
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_petty_cash_allocation: {
                    required: true
                },
                 id_financial_year: {
                    required: true
                }
                ,
                 id_budget_year: {
                    required: true
                },
                 fund_code: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 activity_code: {
                    required: true
                },
                 account_code: {
                    required: true
                },
                 paid_amount: {
                    required: true
                },
                id_staff: 
                {
                    required: true
                },
                description: 
                {
                    required: true
                }
            },
            messages: {
                id_petty_cash_allocation: {
                    required: "<p class='error-text'>Select Petty Cash Allocation</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                fund_code: {
                    required: "<p class='error-text'>Select Fund Year</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                activity_code: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                paid_amount: {
                    required: "<p class='error-text'>Paid Amount Required</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Transaction Type</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>