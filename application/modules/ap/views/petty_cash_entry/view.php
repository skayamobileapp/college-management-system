<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Reimburse Petty Cash Entry</h3>
            </div>



    <div class="form-container">
            <h4 class="form-group-title">Petty Cash Entry Details</h4>



           <div class="row">



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->financial_year;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->budget_year;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->fund_code;?>" readonly="readonly">
                    </div>
                </div>                    

               

            </div>





            <div class="row">


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->department_code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->activity_code;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->account_code;?>" readonly="readonly">
                    </div>
                </div>



            </div>


            <div class="row">



              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo $pettyCashEntry->ic_no . " - ". $pettyCashEntry->staff_name;?>" readonly="readonly">
                    </div>
                </div>               



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Paid Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="paid_amount" name="paid_amount" value="<?php echo $pettyCashEntry->paid_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $pettyCashEntry->description;?>" readonly="readonly">
                    </div>
                </div>



            </div>

            <h4>Petty Cash Allocation</h4>

             <div class="row">


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Petty Cash Allocation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="petty_cash_allocation" name="petty_cash_allocation" value="<?php echo $pettyCashEntry->petty_cash_allocation;?>" readonly="readonly">
                    </div>
                </div>



            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php
                        if($pettyCashEntry->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($pettyCashEntry->status == '1')
                        {
                            echo "Reimbursed";
                        }
                        elseif($pettyCashEntry->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
        
            </div>
            


            <div class="row">

                <input type="hidden" class="form-control" id="id_petty_cash_allocation" name="id_petty_cash_allocation" value="<?php echo $pettyCashEntry->id_petty_cash_allocation;?>">

            <?php
            if($pettyCashEntry->status == '0')
            {
             ?> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Reimburse <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Reimburse
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

            <?php
                }
             ?> 

            </div>



            <div class="row">

                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>
            </div>




        </div>



            <div class="button-block clearfix">
                <div class="bttn-group">

                 <?php
            if($pettyCashEntry->status == '0')
            {
             ?> 
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

             <?php
                }
             ?> 
                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
    
    $('select').select2();

</script>