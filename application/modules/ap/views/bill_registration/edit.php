<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Bill Registration</h3>
            </div>

        <div class="form-container">
          <h4 class="form-group-title">Bill Registration Entry</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->type;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $billRegistration->date_time;?>" readonly="readonly">
                    </div>
                </div>

                 <?php

            if($billRegistration->type == 'Registered Vendor')
            {
             ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Registration Number<span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo $billRegistration->reference_number;?>" readonly="readonly">
                    </div>
                </div>

            <?php
            }

            if($billRegistration->type == 'Emergency Fund')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Bill Registered For <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $billRegistration->ef_for; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            if($billRegistration->type == 'Petty Cash')
                    {

                ?>

                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Staff <span class='error-text'>*</span></label>
                      <select name="id_staff" id="id_staff" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($staffList)) {
                            foreach ($staffList as $record)
                            {
                              $selected = '';
                              if ($record->id == $billRegistration->id_staff) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->ic_no . " - " . $record->salutation . ". " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>
                

                <?php
                }
            ?>   

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$billRegistration->id_financial_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$billRegistration->id_budget_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <?php

                if($billRegistration->ef_for == 'Staff')
                    {

                ?>

                <div class='row'>


                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Staff <span class='error-text'>*</span></label>
                      <select name="ef_for_id" id="ef_for_id" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($staffList)) {
                            foreach ($staffList as $record)
                            {
                              $selected = '';
                              if ($record->id == $billRegistration->ef_for_id) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->ic_no . " - " . $record->salutation . ". " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>
                
                </div>

                <?php
                }
                elseif($billRegistration->ef_for == 'Student')
                {
                    ?>

                    <div class='row'>

                    <div class="col-sm-4">
                    <div class="form-group">
                      <label>Student <span class='error-text'>*</span></label>
                      <select name="ef_for_id" id="ef_for_id" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($studentList)) {
                            foreach ($studentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $billRegistration->ef_for_id) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->nric . " - " . $record->full_name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>
                
                </div>

                    

                    <?php
                }

                ?>



              
            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->contact_person_one;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 2 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->contact_person_two;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 3 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->contact_person_three;?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->email;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Ban Account Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->bank_acc_no;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->bank_code . " - " . $billRegistration->bank_name;?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Descripion <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo $billRegistration->description;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo $billRegistration->total_amount;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($billRegistration->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($billRegistration->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($billRegistration->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            

            </div>

             <div class="row">


             <?php
            if($billRegistration->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $billRegistration->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


            </div>





            <?php
            switch ($billRegistration->type)
            {
                case 'Registered Vendor':

                ?>

                <br>
                 <h3>GRN Details For Bill Registration</h3>


                 <div class='row'>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Po Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='po_number' name='po_number' value="<?php echo $billData->po_number ?>" readonly>
                        </div>
                    </div>



                      <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='pr_number' name='pr_number' value='<?php echo $billData->pr_number ?>' readonly>
                        </div>
                    </div>



                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>GRN Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='grn_number' name='grn_number' value='<?php echo $billData->reference_number ?>' readonly>
                        </div>
                    </div>
                   

                </div>


                <div class='row'>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>GRN Date Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='grn_date' name='grn_date' value='<?php echo $billData->date_time ?>' readonly>
                        </div>
                    </div>



                      <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Expire Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='grn_expire_date' name='grn_expire_date' value='<?php echo $billData->expire_date ?>' readonly>
                        </div>
                    </div>



                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Total Amount <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='total_amount' name='total_amount' value='<?php echo $billData->total_amount ?>' readonly>
                        </div>
                    </div>
                   

                </div>


                <div class='row'>

                      <div class='col-sm-4'>
                            <div class='form-group'>
                                <label>GRN Rating <span class='error-text'>*</span></label>
                                <input type='text' class='form-control' id='contact_person_three' name='contact_person_three' value='<?php echo $billData->rating ?>' readonly>
                            </div>
                        </div>

                </div>



                <?php

                break;

                case 'Emergency Fund':
                    
                    break;

                case 'Petty Cash':
                    
                    break;

                default:
                    # code...
                    break;
            }

             ?>


           </div>






            <br>
            <h3>Bill Registration Details</h3>

          <div class="form-container">
          <h4 class="form-group-title">Bill Registration Details</h4>




            <?php

             if($billRegistration->type == 'Registered Vendor')
            {
             ?>


            <div class="custom-table">
            <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Debit Gl Code</th>
                    <th>Credit GL Code</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($billRegistrationDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $billRegistrationDetails[$i]->dt_fund . " - " . $billRegistrationDetails[$i]->dt_department . " - " . $billRegistrationDetails[$i]->dt_activity  . " - " . $billRegistrationDetails[$i]->dt_account;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->cr_fund . " - " . $billRegistrationDetails[$i]->cr_department . " - " . $billRegistrationDetails[$i]->cr_activity  . " - " . $billRegistrationDetails[$i]->cr_account;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->quantity;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->price;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->total_final;?></td>
                     </tr>
                  <?php 
                  // $total = $total + $poDetails[$i]->total_final;
                }
                // $total = number_format($total, 2, '.', ',');
                ?>

                <!-- <tr>
                    <td bgcolor="" colspan="9"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr> -->

            </tbody>
        </table>
        </div>

            <?php
            }
            


            if($billRegistration->type == 'Emergency Fund')
            {
             ?>


                  <div class="custom-table">
        <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Description</th>
                    <th>Debit Gl Code</th>
                    <th>Credit GL Code</th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($billData);$i++)
                    { 
                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $billData[$i]->reference_number;?></td>
                    <td><?php echo $billData[$i]->emegency_fund_entry_description;?></td>
                    <td><?php echo $billData[$i]->dt_fund . " - " . $billData[$i]->dt_department . " - " . $billData[$i]->dt_activity  . " - " . $billData[$i]->dt_account;?></td>
                    <td><?php echo $billData[$i]->cr_fund . " - " . $billData[$i]->cr_department . " - " . $billData[$i]->cr_activity  . " - " . $billData[$i]->cr_account;?></td>
                    <td><?php echo $billData[$i]->total_final;?></td>
                     </tr>
                  <?php 
                }
                ?>

            </tbody>
        </table>
        </div>

            <?php
            }

            if($billRegistration->type == 'Petty Cash')
            {
             ?>


                  <div class="custom-table">
        <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Description</th>
                    <th>Debit Gl Code</th>
                    <th>Credit GL Code</th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($billData);$i++)
                    { 
                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $billData[$i]->reference_number;?></td>
                    <td><?php echo $billData[$i]->petty_cash_entry_description;?></td>
                    <td><?php echo $billData[$i]->dt_fund . " - " . $billData[$i]->dt_department . " - " . $billData[$i]->dt_activity  . " - " . $billData[$i]->dt_account;?></td>
                    <td><?php echo $billData[$i]->cr_fund . " - " . $billData[$i]->cr_department . " - " . $billData[$i]->cr_activity  . " - " . $billData[$i]->cr_account;?></td>
                    <td><?php echo $billData[$i]->total_final;?></td>
                     </tr>
                  <?php 
                }
                ?>

            </tbody>
        </table>
        </div>

            <?php
            }
            ?>   




      </div>
      

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>