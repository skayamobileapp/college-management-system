<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Bill Registration</h3>
            </div>

          <div class="form-container">
          <h4 class="form-group-title">Bill Registration Entry</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Type <span class='error-text'>*</span></label>
                        <select name='type' id='type' class='form-control' onchange="getBillDataByType()">
                            <option value=''>Select</option>
                            <option value='Emergency Fund'>Emergency Fund</option>
                            <option value='Registered Vendor'>Registered Vendor - GRN</option>
                            <!-- <option value='Petty Cash'>Staff Claim - Petty Cash</option> -->
                            <!-- <option value='Cash Application'>Cash Application</option> -->
                        </select>
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Registration Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control" onchange="getBillDataByType()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control" onchange="getBillDataByType()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                

                <div id="view_bill_dropdown">
                </div>

              

                <div id="view_bill_dropdown_for_pc_n_ef">
                </div>

             </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_person_one" name="contact_person_one">
                    </div>
                </div>



                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 2 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_person_two" name="contact_person_two">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 3 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_person_three" name="contact_person_three">
                    </div>
                </div>
               
            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Account Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="bank_acc_no" name="bank_acc_no">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Bank <span class='error-text'>*</span></label>
                        <select name="id_bank" id="id_bank" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bankList))
                            {
                                foreach ($bankList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

            </div>

          </div>

             <div class="row">
                <div id="view_bill_data"></div>
            </div>




            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>

            <br>

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

</form>
<script>


    function getBillDataByType()
    {

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_financial_year'] = $("#id_financial_year").val();
        tempPR['id_budget_year'] = $("#id_budget_year").val();
        if(tempPR['type'] != '' && tempPR['id_financial_year'] != '' && tempPR['id_budget_year'] != '')
        {
            $.ajax(
            {
               url: '/ap/billRegistration/getBillDataByType',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(tempPR['type'] == 'Registered Vendor')
                {
                  $("#view_bill_dropdown_for_pc_n_ef").hide();
                }
                $("#view_bill_dropdown").html(result);
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#total_amount").val(ta);
               }
            });
        }
    }

    function getEmergencyFundByData()
    {
      var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_financial_year'] = $("#id_financial_year").val();
        tempPR['id_budget_year'] = $("#id_budget_year").val();
        tempPR['ef_for'] = $("#ef_for").val();
        if(tempPR['type'] != '' && tempPR['id_financial_year'] != '' && tempPR['id_budget_year'] != '' && tempPR['ef_for'] != '')
        {
            $.ajax(
            {
               url: '/ap/billRegistration/getEmergencyFundByData',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                  alert('No Emergency Fund Available For '+tempPR['ef_for']);
                  $("#view_bill_dropdown_for_pc_n_ef").html('');
                  $("#ef_for").val('');


                }else
                {
                  $("#view_bill_dropdown_for_pc_n_ef").html(result);
                }
              }
            });
        }
    }

    function getEmergencyFundsByData()
    {

      var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['ef_for'] = $("#ef_for").val();
        tempPR['ef_for_id'] = $("#ef_for_id").val();
        if(tempPR['type'] != '' && tempPR['ef_for'] != '' && tempPR['ef_for_id'] != '')
        {
            $.ajax(
            {
               url: '/ap/billRegistration/getEmergencyFundsByData',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_bill_data").html(result);
               }
            });
        }
    }

    function getPCByStaff()
    {
      var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_staff'] = $("#id_staff").val();
        if(tempPR['type'] != '' && tempPR['id_staff'] != '')
        {
            $.ajax(
            {
               url: '/ap/billRegistration/getPCByStaff',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_bill_data").html(result);
               }
            });
        }
    }

    function getFromGRN()
    {

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_from'] = $("#id_from").val();
        if(tempPR['type'] != '' && tempPR['id_from'] != '')
        {
            $.ajax(
            {
               url: '/ap/billRegistration/getFromGRN',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_bill_data").html(result);
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#total_amount").val(ta);
               }
            });
        }
    }


    function deleteTempData(id) {
         $.ajax(
            {
               url: '/investment/investmentApplication/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
                    var ta = $("#total_detail").val();
                    // alert(ta);
                    $("#total_amount").val(ta);
               }
            });
    }


    function getCategory(type)
     {
        // alert(type);
         $.get("/procurement/prEntry/getCategory/"+type, function(data, status)
         {
            // alert(data);
            $("#view_category").html(data);
            $("#view_sub_category").html('');
            $("#view_item").html('');
            // $("#view_student_details").show();
        });
     }

    

    


    function getTempData(id) {
        $.ajax(
            {
               url: '/procurement/prEntry/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#dt_activity").val(result['dt_activity']);
                    $("#dt_account").val(result['dt_account']);
                    $("#cr_fund").val(result['cr_fund']);
                    $("#cr_department").val(result['cr_department']);
                    $("#cr_activity").val(result['cr_activity']);
                    $("#cr_account").val(result['cr_account']);
                    $("#type").val(result['type']);
                    $("#id_category").val(result['id_category']);
                    $("#id_sub_category").val(result['id_sub_category']);
                    $("#id_item").val(result['id_item']);
                    $("#quantity").val(result['quantity']);
                    $("#price").val(result['price']);
                    $("#id_tax").val(result['id_tax']);
                    $("#tax_price").val(result['tax_price']);
                    $("#total_final").val(result['total_final']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                type: {
                    required: true
                },
                 contact_person_one: {
                    required: true
                }
                ,
                 contact_person_two: {
                    required: true
                },
                 contact_person_three: {
                    required: true
                },
                 email: {
                    required: true
                },
                 bank_acc_no: {
                    required: true
                },
                 id_bank: {
                    required: true
                },
                 description: {
                    required: true
                },
                id_from: 
                {
                    required: true
                },
                ef_for:
                {
                  required: true
                },
                ef_for_id:
                {
                  required: true
                },
                id_staff:
                {
                  required: true
                },
                id_financial_year:
                {
                  required: true
                },
                id_budget_year:
                {
                  required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Bill Type</p>",
                },
                contact_person_one: {
                    required: "<p class='error-text'>Contact Person 1 Required</p>",
                },
                contact_person_two: {
                    required: "<p class='error-text'>Contact Person 2 Required</p>",
                },
                contact_person_three: {
                    required: "<p class='error-text'>Contact Person 3 Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                bank_acc_no: {
                    required: "<p class='error-text'>Bank Account No Required</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Total Amount Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                id_from: {
                    required: "<p class='error-text'>Select Bill For Registration</p>",
                },
                ef_for: {
                    required: "<p class='error-text'>Select Emergency Fund For Bill</p>",
                },
                ef_for_id:
                { 
                    required: "<p class='error-text'>Select Emergency Fund For User</p>",
                },
                id_staff:
                { 
                    required: "<p class='error-text'>Select Staff For Bill</p>",
                },
                id_financial_year:
                { 
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year:
                { 
                    required: "<p class='error-text'>Select Budget Year</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );

  $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });


</script>