<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Payment Voucher</h3>
            </div>


        <div class="form-container">
        <h4 class="form-group-title">Payment Voucher Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Voucher Type <span class='error-text'>*</span></label>
                        <select name='type' id='type' class='form-control' onchange="getVoucherDataByType()">
                            <option value=''>Select</option>
                            <option value='Investment'>Investment</option>
                            <option value='Bill Registration'>Bill Registration</option>
                            <option value='Petty Cash'>Petty Cash</option>
                        </select>
                    </div>
                </div>

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control" onchange="getVoucherDataByType()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control" onchange="getVoucherDataByType()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Entry Type <span class='error-text'>*</span></label>
                        <select name='entry' id='entry' class='form-control' >
                            <option value=''>Select</option>
                            <option value='Single Entry'>Single Entry</option>
                            <option value='Multiple Entry'>Multiple Entry</option>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="payment_reference_number" name="payment_reference_number" autocomplete="off">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="account_number" name="account_number">
                    </div>
                </div>

            </div>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Bank <span class='error-text'>*</span></label>
                        <select name="id_bank" id="id_bank" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bankList))
                            {
                                foreach ($bankList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payee Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="payee_name" name="payee_name" autocomplete="off">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Mode <span class='error-text'>*</span></label>
                        <select name='payment_mode' id='payment_mode' class='form-control' >
                            <option value=''>Select</option>
                            <option value='Cash'>Cash</option>
                            <option value='Checq'>Checq</option>
                        </select>
                    </div>
                </div>

             </div>


             <div class="row">
                 
                <div id="view_petty_cash_dropdown">
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Voucher Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="payment_date" name="payment_date" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                


             </div>



             <div class="row">
               
            </div>


        </div>


         <div class="row">
            <div id="view_bill_data"></div>
        </div>





        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

            <br>

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

</form>
<script>


    function getVoucherDataByType()
    {

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_financial_year'] = $("#id_financial_year").val();
        tempPR['id_budget_year'] = $("#id_budget_year").val();
        if(tempPR['type'] != '' && tempPR['id_financial_year'] != '' && tempPR['id_budget_year'] != '')
        {

            $.ajax(
            {
               url: '/ap/paymentVoucher/getVoucherDataByType',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(tempPR['type'] == 'Petty Cash')
                {
                    $("#view_petty_cash_dropdown").html(result);
                    $("#view_bill_data").hide();
                    $("#view_bill_data").val('');
                }else
                {
                    $("#view_bill_data").html(result);
                }

                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#total_amount").val(ta);
               }
            });
        }
    }

    function getPCByStaff()
    {
      var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_staff'] = $("#id_staff").val();
        if(tempPR['type'] != '' && tempPR['id_staff'] != '')
        {
            $.ajax(
            {
               url: '/ap/billRegistration/getPCByStaff',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_bill_data").show();
                $("#view_bill_data").html(result);
               }
            });
        }
    }


    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                type: {
                    required: true
                },
                 entry: {
                    required: true
                },
                 description: {
                    required: true
                },
                 payment_reference_number: {
                    required: true
                },
                 account_number: {
                    required: true
                },
                 id_bank: {
                    required: true
                },
                 payee_name: {
                    required: true
                },
                payment_mode: 
                {
                    required: true
                },
                id_staff:
                {
                    required: true
                },
                id_financial_year: 
                {
                    required: true
                },
                id_budget_year:
                {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Voucher Type</p>",
                },
                entry: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                payment_reference_number: {
                    required: "<p class='error-text'>Payment Reference Number Required</p>",
                },
                account_number: {
                    required: "<p class='error-text'>Bank Account No Required</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                payee_name: {
                    required: "<p class='error-text'>Payee Name Required</p>",
                },
                payment_mode: {
                    required: "<p class='error-text'>Select Payment Mode</p>",
                },
                 id_staff: {
                    required: "<p class='error-text'>Select Staff For Petty Cash Entry</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                 id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );

  $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });


</script>