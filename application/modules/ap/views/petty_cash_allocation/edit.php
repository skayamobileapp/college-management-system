<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Petty Cash Allocation</h3>
            </div>

    <div class="form-container">
            <h4 class="form-group-title">Emergency Fund Allocation Details</h4>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="financial_name" name="financial_name" value="<?php echo $pettyCashAllocation->financial_name;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->budget_year;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="department_code" name="department_code" value="<?php echo $pettyCashAllocation->department_code;?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="financial_name" name="financial_name" value="<?php echo $pettyCashAllocation->fund_code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->activity_code;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="department_code" name="department_code" value="<?php echo $pettyCashAllocation->account_code;?>" readonly="readonly">
                    </div>
                </div>

            </div>


           <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Allocation Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="financial_name" name="financial_name" value="<?php echo $pettyCashAllocation->amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->min_amount;?>" readonly="readonly">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->max_amount;?>" readonly="readonly">
                    </div>
                </div>

            </div>




            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Reimbursement Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="min_reimbursement_amount" name="min_reimbursement_amount" value="<?php echo $pettyCashAllocation->min_reimbursement_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Reimbursement Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_reimbursement_amount" name="max_reimbursement_amount" value="<?php echo $pettyCashAllocation->max_reimbursement_amount;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Reimbursement Percentage <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="min_reimbursement_percentage" name="min_reimbursement_percentage" value="<?php echo $pettyCashAllocation->min_reimbursement_percentage;?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Reimbursement Percentage <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_reimbursement_percentage" name="max_reimbursement_percentage" value="<?php echo $pettyCashAllocation->max_reimbursement_percentage;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number" value="<?php echo $pettyCashAllocation->reference_number;?>" readonly="readonly">
                    </div>
                </div>


            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php
                        if($pettyCashAllocation->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($pettyCashAllocation->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($pettyCashAllocation->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            


             <?php
            if($pettyCashAllocation->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $pettyCashAllocation->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script type="text/javascript">
     $('select').select2();
</script>