<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Petty Cash Allocation</h3>
            </div>

    <div class="form-container">
            <h4 class="form-group-title">Emergency Fund Allocation Details</h4>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="financial_name" name="financial_name" value="<?php echo $pettyCashAllocation->financial_name;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->budget_year;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="department_code" name="department_code" value="<?php echo $pettyCashAllocation->department_code;?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="financial_name" name="financial_name" value="<?php echo $pettyCashAllocation->fund_code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->activity_code;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="department_code" name="department_code" value="<?php echo $pettyCashAllocation->account_code;?>" readonly="readonly">
                    </div>
                </div>

            </div>


           <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Allocation Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="financial_name" name="financial_name" value="<?php echo $pettyCashAllocation->amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->min_amount;?>" readonly="readonly">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget_year" name="budget_year" value="<?php echo $pettyCashAllocation->max_amount;?>" readonly="readonly">
                    </div>
                </div>

            </div>




            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Reimbursement Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="min_reimbursement_amount" name="min_reimbursement_amount" value="<?php echo $pettyCashAllocation->min_reimbursement_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Reimbursement Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_reimbursement_amount" name="max_reimbursement_amount" value="<?php echo $pettyCashAllocation->max_reimbursement_amount;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Reimbursement Percentage <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="min_reimbursement_percentage" name="min_reimbursement_percentage" value="<?php echo $pettyCashAllocation->min_reimbursement_percentage;?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Reimbursement Percentage <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_reimbursement_percentage" name="max_reimbursement_percentage" value="<?php echo $pettyCashAllocation->max_reimbursement_percentage;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number" value="<?php echo $pettyCashAllocation->reference_number;?>" readonly="readonly">
                    </div>
                </div>


            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php
                        if($pettyCashAllocation->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($pettyCashAllocation->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($pettyCashAllocation->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            </div>
            


         <?php
            if($pettyCashAllocation->status == '0')
            {
             ?> 


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

            </div>

            <?php
                }
             ?> 



            <div class="row">

                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>
            </div>

        </div>



            <div class="button-block clearfix">
                <div class="bttn-group">

                <?php
            if($pettyCashAllocation->status == '0')
            {
             ?> 
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

            <?php
                }
             ?> 
                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
    
    $('select').select2();

</script>