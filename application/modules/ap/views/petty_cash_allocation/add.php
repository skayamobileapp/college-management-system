<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Emergency Fund Allocation</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Emergency Fund Allocation</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
               
            </div>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <select name="fund_code" id="fund_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <select name="activity_code" id="activity_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Allocation Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="amount" name="amount">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_amount" name="min_amount">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_amount" name="max_amount">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Reimbursement Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_reimbursement_amount" name="min_reimbursement_amount">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Reimbursement Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_reimbursement_amount" name="max_reimbursement_amount">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Reimbursement Percentage <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_reimbursement_percentage" name="min_reimbursement_percentage">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Reimbursement Percentage <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_reimbursement_percentage" name="max_reimbursement_percentage">
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

</form>
<script>


    function getEFADataByType()
    {

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        if(tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/ap/emergencyFundAllocation/getEFADataByType',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_efa_dropdown").html(result);
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                 id_financial_year: {
                    required: true
                },
                 id_budget_year: {
                    required: true
                },
                activity_code: {
                    required: true
                },
                 account_code: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 fund_code: {
                    required: true
                },
                 min_amount: {
                    required: true
                },
                 max_amount: {
                    required: true
                },
                 min_reimbursement_amount: {
                    required: true
                },
                max_reimbursement_amount: 
                {
                    required: true
                },
                min_reimbursement_percentage: {
                    required: true
                },
                max_reimbursement_percentage: 
                {
                    required: true
                },
                amount: 
                {
                    required: true
                }
            },
            messages: {
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                activity_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                fund_code: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                min_amount: {
                    required: "<p class='error-text'>Min. Amount Required</p>",
                },
                max_amount: {
                    required: "<p class='error-text'>Max. Amount Required</p>",
                },
                min_reimbursement_amount: {
                    required: "<p class='error-text'>Min. Reimbursement Amount Required</p>",
                },
                max_reimbursement_amount: {
                    required: "<p class='error-text'>Max. Reimbursement Amount Required</p>",
                },
                min_reimbursement_percentage: {
                    required: "<p class='error-text'>Min. Reimbursement Percentage Required</p>",
                },
                max_reimbursement_percentage: {
                    required: "<p class='error-text'>Max. Reimbursement Percentage Required</p>",
                },
                amount: {
                    required: "<p class='error-text'>Allocation Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>