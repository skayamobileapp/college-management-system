<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add ETF Voucher</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">ETF Voucher Entry</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Mode <span class='error-text'>*</span></label>
                        <select name='payment_mode' id='payment_mode' class='form-control' >
                            <option value=''>Select</option>
                            <option value='Cash'>Cash</option>
                            <option value='Check'>Checq</option>
                        </select>
                    </div>
                </div>

                

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="payment_date" name="payment_date" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Bank <span class='error-text'>*</span></label>
                        <select name="id_bank" id="id_bank" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bankList))
                            {
                                foreach ($bankList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
  
            </div>



            <div class="row">

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Letter Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="letter_reference_number" name="letter_reference_number">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Batch Id <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="batch_id" name="batch_id">
                    </div>
                </div>
              
            </div>

        </div>

        <div class="form-container">
            <h4 class="form-group-title">Search Payment Voucher</h4>

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Reference Number</label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Voucher Type <span class='error-text'>*</span></label>
                        <select name='voucher_type' id='voucher_type' class='form-control' >
                            <option value=''>Select</option>
                            <option value='Investment'>Investment</option>
                            <option value='Bill Registration'>Bill Registration</option>
                            <option value='Petty Cash'>Petty Cash</option>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>From Date</label>
                        <input type="text" class="form-control datepicker" id="from_date" name="from_date">
                    </div>
               </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>To Date</label>
                        <input type="text" class="form-control datepicker" id="to_date" name="to_date">
                    </div>
               </div>
                
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchPV()">Search</button>
              </div>
        </div>

        <div  style="display: none" class="form-container" id="view_pv_details">
            <h4 class="form-group-title">Payment Vouchers</h4>

            <div class="row">
               <span id='view_pv'></span>
            </div>

        </div>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>


<script>

     function searchPV()
    {
                    // alert('No Vouchers Available For Selected Search')

        var tempPR = {};
        tempPR['reference_number'] = $("#reference_number").val();
        tempPR['voucher_type'] = $("#voucher_type").val();
        tempPR['from_date'] = $("#from_date").val();
        tempPR['to_date'] = $("#to_date").val();
        // alert(tempPR);
            $.ajax(
            {
               url: '/ap/etfVoucher/searchPV',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert('assd');
                if(result)
                {
                    $("#view_pv_details").show();
                    $("#view_pv").html(result);
                }else
                {
                    alert('No Payment Vouchers Available For Selected Search')
                }
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }



    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                payment_mode: {
                    required: true
                },
                 payment_date: {
                    required: true
                },
                 id_bank: {
                    required: true
                },
                 description: {
                    required: true
                },
                 letter_reference_number: {
                    required: true
                },
                 batch_id: {
                    required: true
                }
            },
            messages: {
                payment_mode: {
                    required: "<p class='error-text'>Select Payment Mode</p>",
                },
                payment_date: {
                    required: "<p class='error-text'>Select Payment Date</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                letter_reference_number: {
                    required: "<p class='error-text'>Letter Reference Number Required</p>",
                },
                batch_id: {
                    required: "<p class='error-text'>Batch Id Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );

  $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });


</script>