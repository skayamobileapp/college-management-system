<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_voucher_model extends CI_Model
{

    function getBankList()
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    function getPaymentVoucherListSearch($data)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code, fy.name as financial_year, bty.name as budget_year');
        $this->db->from('payment_voucher as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        $this->db->join('financial_year as fy','ina.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','ina.id_budget_year = bty.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['name'] . "%' or ina.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_financial_year'] !='')
        {
            $this->db->where('ina.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year'] !='')
        {
            $this->db->where('ina.id_budget_year', $data['id_budget_year']);
        }
        if ($data['id_bank'] !='')
        {
            $this->db->where('ina.id_bank', $data['id_bank']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ina.status', $data['status']);
        }
        if ($data['cancel'] !='')
        {
            $this->db->where('ina.is_sab', '0');
        }
        $this->db->order_by("ina.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($data);exit();     
         return $result;
    }

    function generatePaymentVoucherNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('pv.*');
            $this->db->from('payment_voucher as pv');
            $this->db->order_by("pv.id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

            $count= $result + 1;
            $generated_number = "PV" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addPaymentVoucher($data)
    {
        $this->db->trans_start();
        $this->db->insert('payment_voucher', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getInvestmentRegistrationForVoucher()
    {
        $this->db->select('invr.*');
        $this->db->from('investment_registration as invr');
        $this->db->where('invr.is_payment_voucher', '0');
        $this->db->where('invr.ap_status', '0');
        $this->db->where('invr.is_reinvested', '0');
        $this->db->where('invr.investment_status', '1');
        $this->db->order_by("invr.id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getBillRegistrationForVoucher($data)
    {
        $this->db->select('br.*');
        $this->db->from('bill_registration as br');
        $this->db->where('br.is_payment_voucher', '0');
        $this->db->where('br.ap_status', '0');
        $this->db->where('br.status', '1');
        $this->db->where('br.id_financial_year', $data['id_financial_year']);
        $this->db->where('br.id_budget_year', $data['id_budget_year']);
        $this->db->order_by("br.id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffById($id)
    {
        $this->db->select('ao.ic_no, ao.name');
        $this->db->from('staff as ao');
        $this->db->where('ao.id', $id);
        $query = $this->db->get();
        $staff = $query->row();

        return $staff->ic_no . " - " . $staff->name;
    }

    function getStudentById($id)
    {
        $this->db->select('ao.nric, ao.full_name');
        $this->db->from('student as ao');
        $this->db->where('ao.id', $id);
        $query = $this->db->get();
        $student = $query->row();

        return $student->nric . " - " . $student->full_name;
    }


     function updatePettyCash($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('petty_cash_entry', $data);
        return TRUE;
    }

    function updateInvestmentRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('investment_registration', $data);
        return TRUE;
    }

    function updateBillRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('bill_registration', $data);
        return TRUE;
    }

    function addPaymentVoucherDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('payment_voucher_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updatePaymentVoucher($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('payment_voucher', $data);
        return TRUE;
    }

    function getPaymentVoucher($id)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('payment_voucher as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        $this->db->where('ina.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function getPaymentVoucherDetailsByInvestment($id)
    {
        $this->db->select('ina.*, bnk.*');
        $this->db->from('payment_voucher_details as ina');
        $this->db->join('investment_registration as bnk','ina.id_referal = bnk.id');
        $this->db->where('ina.id_payment_voucher', $id);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getPaymentVoucherDetailsByBillRegistration($id)
    {
        $this->db->select('ina.*, bnk.*');
        $this->db->from('payment_voucher_details as ina');
        $this->db->join('bill_registration as bnk','ina.id_referal = bnk.id');
        $this->db->where('ina.id_payment_voucher', $id);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getPaymentVoucherDetailsByPettyCash($id)
    {
        $this->db->select('ina.*, bnk.*');
        $this->db->from('payment_voucher_details as ina');
        $this->db->join('petty_cash_entry as bnk','ina.id_referal = bnk.id');
        $this->db->where('ina.id_payment_voucher', $id);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getPCStaffDataForBillRegistration($data)
    {
        $this->db->select('DISTINCT(pc.id_staff) as id_staff, sta.ic_no as code, sta.name as full_name');
        $this->db->from('petty_cash_entry as pc');
        $this->db->join('staff as sta','pc.id_staff = sta.id');
        $this->db->where('pc.is_bill_registered', '0');
        $this->db->where('pc.ap_status', '0');
        $this->db->where('pc.status', '1');
        $this->db->where('pc.id_financial_year', $data['id_financial_year']);
        $this->db->where('pc.id_budget_year', $data['id_budget_year']);
        $query = $this->db->get();
        return $query->result();
    }

     function getPettyCashByEntryIdForBillRegistration($id)
    {

       $this->db->select('ef.id, ef.description, ef.reference_number, ef.id_budget_year, ef.id_financial_year, ef.fund_code as cr_fund, ef.department_code as cr_department, ef.activity_code as cr_activity, ef.account_code as cr_account, ef.paid_amount, efa.fund_code as dt_fund, efa.department_code as dt_department, efa.activity_code as dt_activity, efa.account_code as dt_account, efa.amount');
        $this->db->from('petty_cash_entry as ef');
        $this->db->join('petty_cash_allocation as efa','ef.id_petty_cash_allocation = efa.id');
        $this->db->join('staff as sta','ef.id_staff = sta.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id", $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getBillRegistrationById($id)
    {
        $this->db->select('br.*');
        $this->db->from('bill_registration as br');
        $this->db->where('br.is_payment_voucher', '0');
        $this->db->where('br.ap_status', '0');
        $this->db->where('br.status', '1');
        $this->db->where("br.id", $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInvestmentRegistrationById($id)
    {
        $this->db->select('invr.*');
        $this->db->from('investment_registration as invr');
        $this->db->where('invr.is_payment_voucher', '0');
        $this->db->where('invr.ap_status', '0');
        $this->db->where('invr.is_reinvested', '0');
        $this->db->where('invr.investment_status', '1');
        $this->db->where("invr.id", $id);
        $query = $this->db->get();
        return $query->row();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getPaymentVoucherDetailsByMasterId($id)
    {
        $this->db->select('*');
        $this->db->from('payment_voucher_details');
         $this->db->where('id_payment_voucher', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getBillRegistrationDetailsByMasterId($id_bill)
    {
        $this->db->select('*');
        $this->db->from('bill_registration_details');
         $this->db->where('id_bill_registration', $id_bill);
        $query = $this->db->get();
        return $query->result();
    }

    function getPaymentVoucherDetailsByMasterIdForLedgerDetails($id_payment_voucher)
    {
         $this->db->select('dt_account as account_code, dt_activity as activity_code, dt_department as department_code, dt_fund as fund_code, amount as debit_amount, journal_type');
        $this->db->from('payment_voucher_details');
         $this->db->where('id_payment_voucher', $id_payment_voucher);
        $query = $this->db->get();
        return $query->result();
    }

    function getBankByIdForLedgerDetails($id_bank)
    {
        $this->db->select('cr_account as account_code, cr_activity as activity_code, cr_department as department_code, cr_fund as fund_code');
        $this->db->from('bank_registration');
         $this->db->where('id', $id_bank);
        $query = $this->db->get();
        return $query->row();
    }

    function generateLedgerNumber()
    {
        $year = date('y');
        $Year = date('Y');

            $this->db->select('j.*');
            $this->db->from('ledger as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "LD" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


    function addNewLedger($data)
    {
        $this->db->trans_start();
        $this->db->insert('ledger', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewLedgerDetails($data)
    {
         $this->db->trans_start();
        $this->db->insert('ledger_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewLedgerDetailsFiltered($datas,$inserted_ledger_id)
    {
        foreach ($datas as $data)
        {
            // if(isset($data->debit_amount))
            // {
            //     $data->journal_type = 'Debit';
            // }
            // elseif(isset($data->credit_amount))
            // {
            //     $data->journal_type = 'Credit';
            // }

            $data->id_ledger = $inserted_ledger_id;
                // echo "<Pre>";print_r($data);exit;
            $inserted_detail_id = $this->addNewLedgerDetails($data);
        }
        return $inserted_detail_id;
       }
}
