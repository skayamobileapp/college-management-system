<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Bill_registration_model extends CI_Model
{

    function getBankList()
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    function getBillRegistrationListSearch($data)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code, fy.name as financial_year, bty.name as budget_year');
        $this->db->from('bill_registration as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        $this->db->join('financial_year as fy','ina.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','ina.id_budget_year = bty.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['name'] . "%' or ina.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_bank'] !='')
        {
            $this->db->where('ina.id_bank', $data['id_bank']);
        }
        if ($data['id_financial_year'] !='')
        {
            $this->db->where('ina.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year'] !='')
        {
            $this->db->where('ina.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ina.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($data);exit();     
         return $result;
    }

    function generateBillRegistrationNumber()
    {
        $grn = "Registered Vendor";
        $ef = "Emergency Fund";

        $year = date('y');
        $Year = date('Y');
            $this->db->select('br.*');
            $this->db->from('bill_registration as br');
            $this->db->where('br.type', $grn);
            $this->db->or_where('br.type', $ef);
            $query = $this->db->get();
            $result = $query->num_rows();

           // echo "<Pre>";print_r($result);exit();
     
            $count= $result + 1;
            $generated_number = "BR" .(sprintf("%'06d", $count)). "/" . $Year;
           return $generated_number;
    }

    function addBillRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('bill_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getRegisteredVendorDataForBillRegistration($data)
    {
        $this->db->select('grn.*');
        $this->db->from('grn as grn');
        $this->db->where('grn.is_bill_registered', '0');
        $this->db->where('grn.ap_status', '0');
        $this->db->where('grn.status', '1');
        $this->db->where('grn.id_financial_year', $data['id_financial_year']);
        $this->db->where('grn.id_budget_year', $data['id_budget_year']);
        $this->db->order_by("grn.id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getEFDataForBillRegistration()
    {
        $this->db->select('ef.*');
        $this->db->from('emergency_fund_entry as ef');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->order_by("ef.id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getPCDataForBillRegistration()
    {
        $this->db->select('pc.*');
        $this->db->from('petty_cash_entry as pc');
        $this->db->where('pc.is_bill_registered', '0');
        $this->db->where('pc.ap_status', '0');
        $this->db->order_by("pc.id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function getGrn($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, apo.po_number, pr.pr_number, bty.name as budget_year');
        $this->db->from('grn as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('po_entry as apo','po.id_po = apo.id');
        $this->db->join('pr_entry as pr','apo.id = pr.is_po');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        $this->db->where('po.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function getAssetOrderByGRNId($id)
    {
        $this->db->select('DISTINCT(ao.id) as id, ao.*, po.po_number, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code');
        $this->db->from('asset_order as ao');
        $this->db->join('po_entry as po','ao.id_po = po.id');
        $this->db->join('po_detail as pod','ao.id_po_detail = pod.id');
        $this->db->join('asset_category as pc', 'ao.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'ao.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'ao.id_item = pi.id');
        $this->db->where('ao.id_grn', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getAssetOrderById($id)
    {
        $this->db->select('ao.*');
        $this->db->from('asset_order as ao');
        $this->db->where('ao.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addBillRegistrationDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('bill_registration_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getBillRegistration($id)    
    {

        $this->db->select('ao.*, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('bill_registration as ao');
        $this->db->join('bank_registration as bnk','ao.id_bank = bnk.id');
        $this->db->where('ao.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getBillRegistrationDetails($id)
    {

        $this->db->select('ao.*');
        $this->db->from('bill_registration_details as ao');
        $this->db->where('ao.id_bill_registration', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getBillData($data)
    {
        $type = $data['type'];
        $id = $data['id'];

        switch ($type)
        {
            case 'Registered Vendor':
                $detail_data = $this->getGrn($id);
                break;

            case 'Emergency Fund':
                $detail_data = $this->getEFDetailsByBillId($id);
                break;

            case 'Petty Cash':
                $detail_data = $this->getPCDetailsByBillId($id);
                break;

            default:
                # code...
                break;
        }

        return $detail_data;
    }

    function updateBillRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('bill_registration', $data);
        return TRUE;
    }

    function getEFStaffDataForBillRegistration($data)
    {
        $this->db->select('DISTINCT(ef.id_staff) as ef_for_id, sta.ic_no as code, sta.name as full_name');
        $this->db->from('emergency_fund_entry as ef');
        $this->db->join('staff as sta','ef.id_staff = sta.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id_financial_year", $data['id_financial_year']);
        $this->db->where("ef.id_budget_year", $data['id_budget_year']);
        $this->db->where("ef.type", "Staff");
        $query = $this->db->get();
        return $query->result();
    }



    function getEFStudentDataForBillRegistration($data)
    {
        $this->db->select('DISTINCT(ef.id_student) as ef_for_id, stu.full_name, stu.nric as code');
        $this->db->from('emergency_fund_entry as ef');
        $this->db->join('student as stu','ef.id_student = stu.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id_financial_year", $data['id_financial_year']);
        $this->db->where("ef.id_budget_year", $data['id_budget_year']);
        $this->db->where("ef.type", "Student");
        $query = $this->db->get();
        return $query->result();
    }

    function getEmergencyFundsByStaffData($data)
    {
        $this->db->select('ef.id, ef.description, ef.reference_number, ef.id_budget_year, ef.id_financial_year, ef.fund_code as cr_fund, ef.department_code as cr_department, ef.activity_code as cr_activity, ef.account_code as cr_account, ef.requested_amount, efa.fund_code as dt_fund, efa.department_code as dt_department, efa.activity_code as dt_activity, efa.account_code as dt_account, efa.contribution_amount');
        $this->db->from('emergency_fund_entry as ef');
        $this->db->join('emergency_fund_allocation as efa','ef.id_emergency_fund_allocation = efa.id');
        $this->db->join('staff as sta','ef.id_staff = sta.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where("ef.type", "Staff");
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id_staff", $data['ef_for_id']);
        $query = $this->db->get();
        return $query->result();
    }

    function getEmergencyFundsByStudentData($data)
    {
        $this->db->select('ef.id, ef.description, ef.reference_number, ef.id_budget_year, ef.id_financial_year, ef.fund_code as cr_fund, ef.department_code as cr_department, ef.activity_code as cr_activity, ef.account_code as cr_account, ef.requested_amount, efa.fund_code as dt_fund, efa.department_code as dt_department, efa.activity_code as dt_activity, efa.account_code as dt_account, efa.contribution_amount');
        $this->db->from('emergency_fund_entry as ef');
        $this->db->join('student as stu','ef.id_student = stu.id');
        $this->db->join('emergency_fund_allocation as efa','ef.id_emergency_fund_allocation = efa.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where("ef.type", "Student");
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id_student", $data['ef_for_id']);
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
         $this->db->where('applicant_status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getEFDetailsByBillId($id_bill)
    {
        $this->db->select('ao.*, efe.description as emegency_fund_entry_description, efe.reference_number');
        $this->db->from('bill_registration_details as ao');
        $this->db->join('emergency_fund_entry as efe','ao.id_referal = efe.id');
        $this->db->where('ao.id_bill_registration', $id_bill);
        $query = $this->db->get();
        return $query->result();
    }

    function getPCStaffDataForBillRegistration()
    {
        $this->db->select('DISTINCT(pc.id_staff) as id_staff, sta.ic_no as code, sta.name as full_name');
        $this->db->from('petty_cash_entry as pc');
        $this->db->join('staff as sta','pc.id_staff = sta.id');
        $this->db->where('pc.is_bill_registered', '0');
        $this->db->where('pc.ap_status', '0');
        $this->db->where('pc.status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getPCByStaff($data)
    {

       $this->db->select('ef.id, ef.description, ef.reference_number, ef.id_budget_year, ef.id_financial_year, ef.fund_code as cr_fund, ef.department_code as cr_department, ef.activity_code as cr_activity, ef.account_code as cr_account, ef.paid_amount, efa.fund_code as dt_fund, efa.department_code as dt_department, efa.activity_code as dt_activity, efa.account_code as dt_account, efa.amount');
        $this->db->from('petty_cash_entry as ef');
        $this->db->join('petty_cash_allocation as efa','ef.id_petty_cash_allocation = efa.id');
        $this->db->join('staff as sta','ef.id_staff = sta.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id_staff", $data['id_staff']);
        $query = $this->db->get();
        return $query->result();
    }

    function getPCDetailsByBillId($id_bill)
    {
        $this->db->select('ao.*, efe.description as petty_cash_entry_description, efe.reference_number');
        $this->db->from('bill_registration_details as ao');
        $this->db->join('petty_cash_entry as efe','ao.id_referal = efe.id');
        $this->db->where('ao.id_bill_registration', $id_bill);
        $query = $this->db->get();
        return $query->result();
    }

    function updateGRNDetailsForBill($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('grn', $data);
        return TRUE;
    }

    function updateEFEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('emergency_fund_entry', $data);
        return TRUE;
    }

    function updatePCEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('petty_cash_entry', $data);
        return TRUE;
    }

    function getEmergencyFundsByEntryIdForBillRegistration($id)
    {
        $this->db->select('ef.id, ef.description, ef.reference_number, ef.id_budget_year, ef.id_financial_year, ef.fund_code as cr_fund, ef.department_code as cr_department, ef.activity_code as cr_activity, ef.account_code as cr_account, ef.requested_amount, efa.fund_code as dt_fund, efa.department_code as dt_department, efa.activity_code as dt_activity, efa.account_code as dt_account, efa.contribution_amount');
        $this->db->from('emergency_fund_entry as ef');
        $this->db->join('emergency_fund_allocation as efa','ef.id_emergency_fund_allocation = efa.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id", $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getPettyCashByEntryIdForBillRegistration($id)
    {

       $this->db->select('ef.id, ef.description, ef.reference_number, ef.id_budget_year, ef.id_financial_year, ef.fund_code as cr_fund, ef.department_code as cr_department, ef.activity_code as cr_activity, ef.account_code as cr_account, ef.paid_amount, efa.fund_code as dt_fund, efa.department_code as dt_department, efa.activity_code as dt_activity, efa.account_code as dt_account, efa.amount');
        $this->db->from('petty_cash_entry as ef');
        $this->db->join('petty_cash_allocation as efa','ef.id_petty_cash_allocation = efa.id');
        $this->db->join('staff as sta','ef.id_staff = sta.id');
        $this->db->where('ef.is_bill_registered', '0');
        $this->db->where('ef.ap_status', '0');
        $this->db->where('ef.status', '1');
        $this->db->where("ef.id", $id);
        $query = $this->db->get();
        return $query->row();
    }
}
