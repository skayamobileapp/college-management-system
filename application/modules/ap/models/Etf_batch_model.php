<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Etf_batch_model extends CI_Model
{

    function getEtfBatchListSearch($data)
    {
        $this->db->select('ina.*');
        $this->db->from('etf_batch as ina');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['name'] . "%' or ina.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ina.status', $data['status']);
        }
        $this->db->order_by("ina.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($data);exit();     
         return $result;
    }

    function generateEtfBatchNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('pv.*');
            $this->db->from('etf_batch as pv');
            $this->db->order_by("pv.id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

            $count= $result + 1;
            $generated_number = "BATCH" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addEtfBatch($data)
    {
        $this->db->trans_start();
        $this->db->insert('etf_batch', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addEtfBatchDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('etf_batch_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateEtfVoucher($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('etf_voucher', $data);
        return TRUE;
    }

    function updateEtfBatch($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('etf_batch', $data);
        return TRUE;
    }

    function searchEtfVoucher($data)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('etf_voucher as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        if ($data['reference_number']!='')
        {
            $likeCriteria = "(ina.batch_id  LIKE '%" . $data['reference_number'] . "%' or ina.reference_number  LIKE '%" . $data['reference_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['from_date'] !='')
        {
            $likeCriteria = "(date(ina.payment_date)  >= '" . date('Y-m-d',strtotime($data['from_date'])) . "')";
            $this->db->where($likeCriteria);
        }
        if ($data['to_date'] !='')
        {
            $likeCriteria = "(date(ina.payment_date)  <= '" . date('Y-m-d',strtotime($data['to_date'])) . "')";
            $this->db->where($likeCriteria);

        }
        $this->db->where('ina.is_sab_group', '0');
        $this->db->where('ina.status', '1');
        $this->db->order_by("ina.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         return $result;

    }

    function getEtfBatch($id)
    {
        $this->db->select('ina.*');
        $this->db->from('etf_batch as ina');
        $this->db->where('ina.id', $id);
        $query = $this->db->get();
        return $query->row();

    }

    function getEtfBatchDetails($id)
    {
        $this->db->select('ina.*, bnk.*');
        $this->db->from('etf_batch_details as ina');
        $this->db->join('etf_voucher as bnk','ina.id_referal = bnk.id');
        $this->db->where('ina.id_etf_batch', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getEtfVoucher($id)
    {
        $this->db->select('ina.*');
        $this->db->from('etf_voucher as ina');
        $this->db->where('ina.id', $id);
        $query = $this->db->get();
        return $query->row();

    }
}
