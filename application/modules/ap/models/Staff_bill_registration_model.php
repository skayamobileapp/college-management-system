<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Staff_bill_registration_model extends CI_Model
{

    function getBillRegistrationListSearch($data)
    {
        $staff = "Staff Claim";
        $general = "General Claim";

        $this->db->select('ina.*, bty.name as budget_year, fy.name as financial_year, d.name as department_name, d.code as department_code');
        $this->db->from('bill_registration as ina');
        $this->db->join('financial_year as fy','ina.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','ina.id_budget_year = bty.id');
        $this->db->join('department_code as d','ina.department_code = d.code');
        $likeCriteria = "(ina.type  = '" . $staff . "' or ina.type = '" . $general . "')";
            $this->db->where($likeCriteria);
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['name'] . "%' or ina.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type']!='')
        {
            $this->db->where('ina.type', $data['type']);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('ina.department_code', $data['department_code']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('ina.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('ina.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ina.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($data);exit();     
         return $result;
    }



    function getStaffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function departmentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
         $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();

    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function prCategoryList()
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getTaxList()
    {
        $this->db->select('*');
        $this->db->from('tax');
         $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_bill_registration_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addBillRegistrationDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('bill_registration_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempStaffBRDetails()
    {
            // echo "<Pre>";print_r($type);exit();
        $id_session = $this->session->my_session_id;


        $this->db->select('tpe.*, pc.description as category_name, pc.code as category_code,  psc.code as sub_category_code,  psc.description as sub_category_name, pi.code as item_code,  pi.description as item_name, tx.code as tax_code, tx.name as tax_name, tx.percentage as tax_percentage');
        $this->db->from('temp_bill_registration_details as tpe');
        $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id');
        $this->db->join('tax as tx', 'tpe.id_tax = tx.id');
        $this->db->where('tpe.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function generateStaffBillRegistrationNumber()
    {
        $staff = "Staff Claim";
        $general = "General Claim";
        $year = date('y');
        $Year = date('Y');
            $this->db->select('br.*');
            $this->db->from('bill_registration as br');
            $this->db->where('br.type', $staff);
            $this->db->or_where('br.type', $general);
            $query = $this->db->get();
            $result = $query->num_rows();

           // echo "<Pre>";print_r($result);exit();

     
            $count= $result + 1;
            $generated_number = "SBR" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addStaffBillRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('bill_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function moveTempBillRegistrationDetail($id_staff_bill_registration)
    {
        $temp_details = $this->getTempStaffBRDetailsBySessionId();
        foreach ($temp_details as $key => $temp_detail)
        {
            $temp_detail->id_bill_registration = $id_staff_bill_registration;
            unset($temp_detail->id_session);
            unset($temp_detail->id);

            $insert_detail_id = $this->addBillRegistrationDetails($temp_detail);
        }

        $deleted_temp = $this->deleteTempDetailsBySession();

    }

    function getTempStaffBRDetailsBySessionId()
    {
        $id_session = $this->session->my_session_id;

        $this->db->select('tpe.*');
        $this->db->from('temp_bill_registration_details as tpe');
        $this->db->where('tpe.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getBillRegistration($id)    
    {

        $this->db->select('ina.*, bty.name as budget_year, fy.name as financial_year, d.name as department_name, d.code as department_code');
        $this->db->from('bill_registration as ina');
        $this->db->join('financial_year as fy','ina.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','ina.id_budget_year = bty.id');
        $this->db->join('department_code as d','ina.department_code = d.code');
        $this->db->where('ina.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getBillRegistrationDetails($id)
    {
        $this->db->select('tpe.*, pc.description as category_name, pc.code as category_code,  psc.code as sub_category_code,  psc.description as sub_category_name, pi.code as item_code,  pi.description as item_name, tx.code as tax_code, tx.name as tax_name, tx.percentage as tax_percentage');
        $this->db->from('bill_registration_details as tpe');
        $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id');
        $this->db->join('tax as tx', 'tpe.id_tax = tx.id');
        $this->db->where('tpe.id_bill_registration', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function updateStaffBillRegistration($data,$id_staff_bill_registration)
    {
        $this->db->where('id', $id_staff_bill_registration);
        $this->db->update('bill_registration', $data);
        return TRUE;
    }


    function deleteTempDetailsBySession()
    {
        $id_session = $this->session->my_session_id;

        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_bill_registration_details');
        return TRUE;
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_bill_registration_details');
        return TRUE;
    }
}
