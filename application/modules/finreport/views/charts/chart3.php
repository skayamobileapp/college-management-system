<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">


<div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Cash Flow</h3>
    </div>

     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
Cash Flow            </a>
          </h4>
        </div>


      
        <form id="form_award" action="" method="post">

        <div class="form-container">

      <div class="row">

                <div class="col-sm-12">
                    <table class="table" border="1">
                        <thead>
                            <tr>
                              <th style='text-align:center;width:30%'>Particulars</th>
                              <th style='text-align:center;width:20%'>Amount(MYR)</th>
                            </tr>
                            <tr>
                              <th colspan="2">Cash Flow From Operations</th>
                            </tr>
                          
                          
                        </thead>
                        <tbody>
                            <tr>
                                <td>Net Income</td>
                                <td style="text-align: right;">60,000.00</td>
                               
                            </tr>
                             <tr>
                                <td>Increase in Accounts Payable</td>
                                <td style="text-align: right;">20,000.00</td>
                               
                            </tr>
                             <tr>
                                <td>Increase in Accounts Receivable</td>
                                <td style="text-align: right;">(10,000.00)</td>
                               
                            </tr>
                           
                            <tr>
                                <td>Increase in Inventory</td>
                                <td style="text-align: right;">(20,000.00)</td>
                               
                            </tr>
                          <tr>
                              <td><b>Net operation from Cash</b></td>
                                <td style="text-align: right;"><b>50,000.00</b></td>

                            </tr>
                             <tr>
                              <th colspan="2">Cash Flow From Investing</th>
                            </tr>
                             <tr>
                                <td>Purchase of Equipment</td>
                                <td style="text-align: right;">(10,000.00)</td>
                               
                            </tr>
                              <tr>
                              <th colspan="2">Cash Flow From Finance</th>
                            </tr>
                             <tr>
                                <td>Notes Payable</td>
                                <td style="text-align: right;">7,500.00</td>
                               
                            </tr>

                            <tr>
                                <td><b>Cash Flow For the Month Ended April 2022</b></td>
                                <td style="text-align: right;"><b>47,500.00</b></td>
                               
                            </tr>
                           

                        </table>
                      </div>
                    </div>
                      

        </div>
    </form>
</div>
</div>
