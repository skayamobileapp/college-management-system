<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
	.highcharts-figure, .highcharts-data-table table {
    min-width: 360px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Students Count based on Demographic </h3>
        </div>
        <form id="form_award" action="" method="post">

        <div class="form-container">
            <h3>Students Count based on Gender Wise </h3>

			<figure class="highcharts-figure">
			    <div id="container"></div>
			    <p class="highcharts-description">
			    </p>
			</figure>


                        <h3>Students Count based on Age Group </h3>

            <figure class="highcharts-figure">
                <div id="container2"></div>
                <p class="highcharts-description">
                </p>
            </figure>


                        <h3>Students Count based on Race </h3>

                        <figure class="highcharts-figure">
                <div id="container3"></div>
                <p class="highcharts-description">
                </p>
            </figure>
		</div>
	</form>
</div>
</div>
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Student Status by Intake Jan2022'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Status',
        colorByPoint: true,
        data: [{
            name: 'Male',
            y: 48
        }, {
            name: 'Female',
            y: 52
        }]
    }]
});
</script>
<script type="text/javascript">
Highcharts.chart('container2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Student Status by Intake Jan2022'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Status',
        colorByPoint: true,
        data: [{
            name: '21 - 23 Years',
            y: 26.41
        }, {
            name: '24 - 26 Years',
            y: 11.84
        }, {
            name: '26 - 28 Years',
            y: 62.85
        }, {
            name: '28 - 32 Years',
            y: 1.5
        }, {
            name: '32+ Years',
            y: 1.0
        }]
    }]
});
</script>
<script type="text/javascript">
Highcharts.chart('container3', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Student Status by Intake Jan2022'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Status',
        colorByPoint: true,
        data: [{
            name: 'Malay',
            y: 46.41
        }, {
            name: 'Indian',
            y: 11.84
        }, {
            name: 'Chinese',
            y: 12.85
        }, {
            name: 'Lain - Lain',
            y: 1.5
        }]
    }]
});
</script>