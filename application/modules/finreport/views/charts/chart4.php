<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">


<div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Asset Report</h3>
    </div>

     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
Asset Report            </a>
          </h4>
        </div>


      
        <form id="form_award" action="" method="post">

        <div class="form-container">

      <div class="row">

                <div class="col-sm-12">
                    <table class="table" border="1">
                        <thead>
                            <tr>
                              <th>Description of Asset</th>
                              <th>Purchase Date</th>
                              <th>Cost(MYR)</th>
                              <th>Life</th>
                              <th>Current Year Depreciation</th>
                              <th>Cumlative Depreciation Value</th>
                              <th>Net Book Value(MYR)</th>
                            </tr>
                            
                              <tr>
                              <td>MAC Book Air</td>
                              <td>01-01-2022</td>
                              <td>2,590.00</td>
                              <td>5</td>
                              <td>300.00</td>
                              <td>300.00</td>
                              <td>2,290.00</td>
                            </tr>
                              <tr>
                              <td>Truck</td>
                              <td>01-01-2019</td>
                              <td>15,000.00</td>
                              <td>8</td>
                              <td>500.00</td>
                              <td>1500.00</td>
                              <td>12,500.00</td>
                            </tr>
                          
                          
                        </thead>

                        </table>
                      </div>
                    </div>
                      

        </div>
    </form>
</div>
</div>
