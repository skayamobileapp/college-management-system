<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">


<div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Profit and Loss Report</h3>
    </div>

     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
Profit and Loss Report            </a>
          </h4>
        </div>


      
        <form id="form_award" action="" method="post">

        <div class="form-container">

      <div class="row">

                <div class="col-sm-12">
                    <table class="table" border="1">
                        <thead>
                            <tr>
                              <th style='text-align:center;width:30%'>Particulars</th>
                              <th style='text-align:center;width:20%'>Amount(MYR)</th>
                              <th style='text-align:center;width:30%'>Amount(MYR)</th>
                            </tr>
                            <tr>
                              <th colspan="3">Revenue</th>
                            </tr>
                          
                          
                        </thead>
                        <tbody>
                            <tr>
                                <td>Application Sales</td>
                                <td style="text-align: right;">100,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Scrap Sales</td>
                                <td style="text-align: right;">20,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Interest Income</td>
                                <td style="text-align: right;">15,000.00</td>
                                <td></td>
                               
                            </tr>
                           
                           
                             <tr>
                                <td><b>Total Revenue (A)</b></td>
                                <td>-</td>
                                <td style="text-align: center;"><b>135,000</b></td>
                               
                            </tr>
                          <tr>
                              <th colspan="3">Expenses</th>
                            </tr>
                             <tr>
                                <td>Cost for Application Sold</td>
                                <td style="text-align: right;">10,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Advertisement Cost</td>
                                <td style="text-align: right;">5,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Rent</td>
                                <td style="text-align: right;">15,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Salary</td>
                                <td style="text-align: right;">45,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Depreciation</td>
                                <td style="text-align: right;">7,000.00</td>
                                <td></td>
                               
                            </tr>
                            <tr>
                                <td>Utilities</td>
                                <td style="text-align: right;">2,000.00</td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                <td>Petty Cash</td>
                                <td style="text-align: right;">3,000.00</td>
                                <td></td>
                               
                            </tr>
                           
                             <tr>
                                <td><b>Total Expenses (B)</b></td>
                                <td>-</td>
                                <td style="text-align: center;"><b>87,000</b></td>
                               
                            </tr>
                            <tr>
                                <td><b>Net Profit (A - B)</b></td>
                                <td>-</td>
                                <td style="text-align: center;"><b>48,000</b></td>
                               
                            </tr>
                           

                        </table>
                      </div>
                    </div>
                      

        </div>
    </form>
</div>
</div>
