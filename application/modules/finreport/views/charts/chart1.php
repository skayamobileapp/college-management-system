<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

    <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Balance Sheet</h3>
    </div>

     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Balance Sheet for YTD
            </a>
          </h4>
        </div>


      
        <form id="form_award" action="" method="post">

        <div class="form-container">

			<div class="row">
                <div class="col-sm-6">
                    <table class="table">
                        <thead>
                            <tr>
                              <th colspan="2">Assets</th>
                            </tr>
                            <tr>
                              <th colspan="2">Current Assets</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Cash and Cash Equivalents</td>
                                <td>100,000</td>
                               
                            </tr>
                             <tr>
                                <td>Accounts Receivable</td>
                                <td>20,000</td>
                               
                            </tr>
                             <tr>
                                <td>Inventory</td>
                                <td>15,000</td>
                               
                            </tr>
                             <tr>
                                <td>Investment</td>
                                <td>10,000</td>
                               
                            </tr>
                           
                             <tr>
                                <td><b>Total Current Assets</b></td>
                                <td><b>145,000</b></td>
                               
                            </tr>
                             <thead>

                            <tr>
                              <th colspan="2"><br/>Property and Equipments</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Land</td>
                                <td>10,000</td>
                               
                            </tr>
                             <tr>
                                <td>Building and improvements</td>
                                <td>250,000</td>
                               
                            </tr>
                             <tr>
                                <td>Equipments</td>
                                <td>50,000</td>
                               
                            </tr>
                             <tr>
                                <td>Less Accumulated Depreciation</td>
                                <td>(5,000)</td>
                               
                            </tr>
                             <tr>
                                <td><b>Total Assets</b></td>
                                <td><b>450,000</b></td>
                               
                            </tr>

                        </tbody>
                    </table>
                </div>

                                <div class="col-sm-6">
                    <table class="table">
                        <thead>
                            <tr>
                              <th colspan="2">Liabilities and Share holder Equity</th>
                            </tr>
                            <tr>
                              <th colspan="2">Current Liabilities</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Accounts Payable</td>
                                <td>40,000</td>
                               
                            </tr>
                             <tr>
                                <td>Accured Expenses</td>
                                <td>20,000</td>
                               
                            </tr>
                             <tr>
                                <td>Deferred Revenue</td>
                                <td>15,000</td>
                               
                            </tr>
                           
                           
                             <tr>
                                <td><b>Total Current Liabilities</b></td>
                                <td><b>75,000</b></td>
                               
                            </tr>
                              <tr>
                                <td>Long Terms Debt</td>
                                <td>100,000</td>
                               
                            </tr>

                             <tr>
                                <td><b>Total Liabilities</b></td>
                                <td><b>175,000</b></td>
                               
                            </tr>

                             <thead>

                            <tr>
                              <th colspan="2"><br/>Share Holder's Equity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Common Stock</td>
                                <td>30,000</td>
                               
                            </tr>
                             <tr>
                                <td>Additional Paid In Capital</td>
                                <td>200,000</td>
                               
                            </tr>
                             <tr>
                                <td>Retained Earnings</td>
                                <td>45,000</td>
                               
                            </tr>
                            
                             <tr>
                                <td><b>Total Liabilities and Share Holder Equity</b></td>
                                <td><b>450,000</b></td>
                               
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</form>
</div>
</div>
