<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RoomBooking extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_booking_model');
        $this->load->model('room_setup_model');

        $this->isLoggedIn();
    }


    function bookinglist()
    {

        if ($this->checkAccess('room_booking.summary_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            
            $data['roomList'] = $this->room_booking_model->bookinglist($formData);
            $this->global['pageTitle'] = 'Campus Management System : List Room Setup';
            $this->loadViews("booking/bookinglist", $this->global, $data, NULL);
        }
    }

    function list()
    {

        if ($this->checkAccess('room_booking.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            
            $data['roomList'] = $this->room_booking_model->roomListListSearch($formData);
            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : List Room Setup';
            $this->loadViews("booking/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('room_booking.book_room') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/roomSetup/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
               
                $booking_name = $this->security->xss_clean($this->input->post('booking_name'));
                $booking_mobile = $this->security->xss_clean($this->input->post('booking_mobile'));
                $booking_email = $this->security->xss_clean($this->input->post('booking_email'));
                $booking_start_date = $this->security->xss_clean($this->input->post('booking_start_date'));
                $booking_end_date = $this->security->xss_clean($this->input->post('booking_end_date'));
                $booking_remarks = $this->security->xss_clean($this->input->post('booking_remarks'));
                $booking_type = $this->security->xss_clean($this->input->post('booking_type'));

            
                $data = array(
                    'booking_name' => $booking_name,
                    'booking_type' => $booking_type,
                    'booking_mobile' => $booking_mobile,
                    'booking_email' => $booking_email,
                    'booking_start_date' => $booking_start_date,
                    'booking_end_date' => $booking_end_date,
                    'booking_remarks' => $booking_remarks,
                    'id_room' => $id,
                );

                // $duplicate_row = $this->room_setup_model->checkRoomTypeDuplicationEdit($data,$id);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }
                
                $result = $this->room_booking_model->addbooking($data);
                redirect('/facility/RoomSetup/list');
            }
            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['buildingList'] = $this->room_setup_model->getBuildingList();
            $data['blockList'] = $this->room_setup_model->blockList();
            $data['roomSetup'] = $this->room_setup_model->getHostelRoom($id);
            $data['roomTypeList'] = $this->room_setup_model->getRoomTypeListByStatus('1');
            $data['equipmentList'] = $this->room_setup_model->getEquipment('1');
            
            $data['inventoryList'] = $this->room_setup_model->inventoryListByStatus('1');
            $data['id_room'] = $id;
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Room Setup';
            $this->loadViews("booking/edit", $this->global, $data, NULL);
        }
    }
}
    