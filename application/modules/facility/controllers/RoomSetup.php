<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RoomSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('facility_room_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $formData['id_building'] = $this->security->xss_clean($this->input->post('id_building'));
            $formData['id_block'] = $this->security->xss_clean($this->input->post('id_block'));
            $data['searchParam'] = $formData;

            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['apartmentList'] = $this->room_setup_model->getBuildingListByStatus('1');
            $data['blockList'] = $this->room_setup_model->blockListByStatus('1');
            $data['roomList'] = $this->room_setup_model->roomListListSearch($formData);
            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : List Room Setup';
            $this->loadViews("room_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('facility_room_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {

                $equipmentids = 0;
                for($l=0;$l<count($this->input->post('id_equipment'));$l++) {
                    $equipmentids = $equipmentids.','.$this->input->post('id_equipment')[$l];
                }
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_facility_room_type = $this->security->xss_clean($this->input->post('id_facility_room_type'));
                $id_facility_building_registration = $this->security->xss_clean($this->input->post('id_facility_building_registration'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $cost = $this->security->xss_clean($this->input->post('cost'));
                $cost_hour = $this->security->xss_clean($this->input->post('cost_hour'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'id_facility_building_registration' => $id_facility_building_registration,
                    'id_facility_room_type' => $id_facility_room_type,
                    'max_capacity' => $max_capacity,
                    'name' => $name,
                    'status' => $status,
                    'id_equipment' => $equipmentids,
                    'cost' => $cost,
                    'cost_hour' => $cost_hour,
                    'created_by' => $user_id
                );

                $result = $this->room_setup_model->addNewHostelRoom($data);

               
                redirect('/facility/RoomSetup/list');
            }
            
            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['roomTypeList'] = $this->room_setup_model->getRoomTypeListByStatus('1');
            $data['equipmentList'] = $this->room_setup_model->getEquipment('1');

            $data['inventoryList'] = $this->room_setup_model->inventoryListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Room Setup';
            $this->loadViews("room_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('facility_room_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/roomSetup/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
               $equipmentids = 0;
                for($l=0;$l<count($this->input->post('id_equipment'));$l++) {
                    $equipmentids = $equipmentids.','.$this->input->post('id_equipment')[$l];
                }
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_facility_room_type = $this->security->xss_clean($this->input->post('id_facility_room_type'));
                $id_facility_building_registration = $this->security->xss_clean($this->input->post('id_facility_building_registration'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $cost = $this->security->xss_clean($this->input->post('cost'));
                $cost_hour = $this->security->xss_clean($this->input->post('cost_hour'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'id_facility_building_registration' => $id_facility_building_registration,
                    'id_facility_room_type' => $id_facility_room_type,
                    'max_capacity' => $max_capacity,
                    'name' => $name,
                    'status' => $status,
                    'id_equipment' => $equipmentids,
                    'cost' => $cost,
                    'cost_hour' => $cost_hour,
                    'created_by' => $user_id
                );

                // $duplicate_row = $this->room_setup_model->checkRoomTypeDuplicationEdit($data,$id);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }
                
                $result = $this->room_setup_model->editHostelRoom($data,$id);
                redirect('/facility/RoomSetup/list');
            }
            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['buildingList'] = $this->room_setup_model->getBuildingList();
            $data['blockList'] = $this->room_setup_model->blockList();
            $data['roomSetup'] = $this->room_setup_model->getHostelRoom($id);
            $data['roomTypeList'] = $this->room_setup_model->getRoomTypeListByStatus('1');
            $data['equipmentList'] = $this->room_setup_model->getEquipment('1');
            
            $data['inventoryList'] = $this->room_setup_model->inventoryListByStatus('1');
            $data['id_room'] = $id;
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Room Setup';
            $this->loadViews("room_setup/edit", $this->global, $data, NULL);
        }
    }

}
