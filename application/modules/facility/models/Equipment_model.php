<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Equipment_model extends CI_Model
{

    function roomTypeListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('equipment');
        if (!empty($data))
        {
            $likeCriteria = "(code  LIKE '%" . $data . "%' or name  LIKE '%" . $data . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getRoomType($id)
    {
        $this->db->select('*');
        $this->db->from('equipment');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkRoomTypeDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('equipment');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkRoomTypeDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('equipment');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewRoomType($data)
    {
        $this->db->trans_start();
        $this->db->insert('equipment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editRoomType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('equipment', $data);
        return TRUE;
    }
}

