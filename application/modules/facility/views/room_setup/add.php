<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Room Setup</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Room Setup Details</h4>

            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Building<span class='error-text'>*</span></label>
                        <select name="id_facility_building_registration" id="id_facility_building_registration" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
          

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Room Type <span class='error-text'>*</span></label>
                        <select name="id_facility_room_type" id="id_facility_room_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($roomTypeList))
                            {
                                foreach ($roomTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
           

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Capacity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_capacity" name="max_capacity" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Cost per Day <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="cost" name="cost" >
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Equipment <span class='error-text'>*</span></label>
                        <select name="id_equipment[]" id="id_equipment" class="form-control" multiple="true">
                            <?php
                            if (!empty($equipmentList))
                            {
                                foreach ($equipmentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>
    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>

<script type="text/javascript">
    $('select').select2();
</script>