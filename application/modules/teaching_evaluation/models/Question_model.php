<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model
{
    function questionList()
    {
        $this->db->select('*');
        $this->db->from('question_pool');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function questionPoolListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('question_pool');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function questionListSearch($data)
    {
        $this->db->select('q.*, qp.name as pool_name');
        $this->db->from('question as q');
        $this->db->join('question_pool as qp', 'q.id_pool = qp.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(question  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_pool'] != '')
        {
            $this->db->where('id_pool', $data['id_pool']);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getQuestionPool($id_pool)
    {
        $this->db->select('*');
        $this->db->from('question_pool');
        $this->db->where('id', $id_pool);
        $query = $this->db->get();
        return $query->row();
    }

    function getQuestion($id)
    {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getQuestionByPool($id_pool)
    {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id_pool', $id_pool);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewQuestion($data)
    {
        $this->db->trans_start();
        $this->db->insert('question', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editQuestion($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('question', $data);
        return TRUE;
    }
    
}

