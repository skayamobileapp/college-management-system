<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Publish extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('publish_form_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('publish_form.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            

            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_branch'] = $this->security->xss_clean($this->input->post('id_branch'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '1';
            $formData['is_publish'] = '0';

 
            $data['searchParam'] = $formData;

            $data['setFormList'] = $this->publish_form_model->setFormListSearch($formData);
            $data['semesterList'] = $this->publish_form_model->semesterListByStatus('1');
            $data['branchList'] = $this->publish_form_model->organisationListByStatus('1');
            $data['programList'] = $this->publish_form_model->programListByStatus('1');

            // echo "<Pre>";print_r($data['setFormList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : List Set Form';
            $this->loadViews("publish_form/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('publish_form.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            if ($id == null)
            {
                redirect('/teaching_evaluation/publish/list');
            }
            if($this->input->post())
            {
                $is_publish = $this->security->xss_clean($this->input->post('is_publish'));

            
                $data = array(
                    'is_publish' => $is_publish,
                    'published_by' => $user_id
                );
                
                $result = $this->publish_form_model->editSetForm($data,$id);
                redirect('/teaching_evaluation/publish/list');
            }

            $data['setForm'] = $this->publish_form_model->getSetFormDetails($id);
            $data['setFormQuestionPoolList'] = $this->publish_form_model->getSetFormHasQuestionPoolByFormId($id);
            $data['semesterList'] = $this->publish_form_model->semesterListByStatus('1');
            $data['organisationList'] = $this->publish_form_model->organisationListByStatus('1');
            $data['programList'] = $this->publish_form_model->programListByStatus('1');
            $data['questionPoolList'] = $this->publish_form_model->questionPoolListByStatus('1');

                // echo "<Pre>"; print_r($data['setFormQuestionPoolList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Set Form';
            $this->loadViews("publish_form/edit", $this->global, $data, NULL);
        }
    }
}
