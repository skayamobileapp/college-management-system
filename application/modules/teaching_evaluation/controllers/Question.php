<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Question extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('question_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('question.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_pool'] = '';
            $data['searchParam'] = $formData;

            $data['questionPoolList'] = $this->question_model->questionPoolListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : List Question ';
            //print_r($subjectDetails);exit;
            $this->loadViews("question/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_pool)
    {
        if ($this->checkAccess('question.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $question = $this->security->xss_clean($this->input->post('question'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_pool' => $id_pool,
                    'question' => $question,
                    'status' => $status
                );

            // echo "<Pre>";print_r($data);exit;
                $result = $this->question_model->addNewQuestion($data);
                redirect('/teaching_evaluation/question/viewPool/'.$id_pool);
            }
            $data['questionPool'] = $this->question_model->getQuestionPool($id_pool);
            
            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Add Question ';
            $this->loadViews("question/add", $this->global, $data, NULL);
        }
    }

    function viewPool($id_pool)
    {
        if ($this->checkAccess('question.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_pool == null)
            {
                redirect('/teaching_evaluation/question/list');
            }
            // if($this->input->post())
            // {
                // $question = $this->security->xss_clean($this->input->post('question'));
                // $status = $this->security->xss_clean($this->input->post('status'));

            
                // $data = array(
                //     'id_pool' => $id_pool,
                //     'question' => $question,
                //     'status' => $status
                // );
                
                // $result = $this->question_model->addNewQuestion($data,$id_pool);
                // redirect('/teaching_evaluation/question/viewPool/'.$id_pool);
            // }
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_pool'] = $id_pool;
            $data['searchParam'] = $formData;

            $data['id_pool'] = $id_pool;
            $data['questionList'] = $this->question_model->questionListSearch($formData);

            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Add Question ';
            $this->loadViews("question/list_question", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL,$id_pool)
    {
        if ($this->checkAccess('question.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/teaching_evaluation/question/list');
            }
            if($this->input->post())
            {
                $question = $this->security->xss_clean($this->input->post('question'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_pool' => $id_pool,
                    'question' => $question,
                    'status' => $status
                );
                
                $result = $this->question_model->editQuestion($data,$id);
                redirect('/teaching_evaluation/question/viewPool/'.$id_pool);
            }
            $data['questionPool'] = $this->question_model->getQuestionPool($id_pool);
            $data['question'] = $this->question_model->getQuestion($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Question ';
            $this->loadViews("question/edit", $this->global, $data, NULL);
        }
    }


}
