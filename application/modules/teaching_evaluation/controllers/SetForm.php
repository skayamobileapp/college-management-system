<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SetForm extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('set_form_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('set_form.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            

            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_branch'] = $this->security->xss_clean($this->input->post('id_branch'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['setFormList'] = $this->set_form_model->setFormListSearch($formData);
            $data['semesterList'] = $this->set_form_model->semesterListByStatus('1');
            $data['branchList'] = $this->set_form_model->organisationListByStatus('1');
            $data['programList'] = $this->set_form_model->programListByStatus('1');

            // echo "<Pre>";print_r($data['setFormList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : List Set Form';
            $this->loadViews("set_form/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('set_form.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'id_branch' => $id_branch,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'status' => $status,
                    // 'id_pool' => $id_pool,
                    'created_by' => $user_id,
                );
            
                $inserted_id = $this->set_form_model->addNewSetForm($data);
                if($inserted_id && isset($id_pool))
                {
                        foreach ($id_pool as $id_p)
                        {
                            // echo "<Pre>";print_r($id_pool);exit();
                            $detail_data = array(
                                 'id_set_form' => $inserted_id,
                                 'id_pool' => $id_p
                            );
                            $add_detail_pool = $this->set_form_model->addNewSetFormHasQuestionPool($detail_data);
                            # code...
                        }
                }
                redirect('/teaching_evaluation/setForm/list');
            }

            $data['semesterList'] = $this->set_form_model->semesterListByStatus('1');
            $data['organisationList'] = $this->set_form_model->organisationListByStatus('1');
            $data['programList'] = $this->set_form_model->programListByStatus('1');
            $data['questionPoolList'] = $this->set_form_model->questionPoolListByStatus('1');

            
            $this->global['pageTitle'] = 'Campus Management System : Add Set Form';
            $this->loadViews("set_form/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('set_form.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/teaching_evaluation/set_form/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->set_form_model->editSetForm($data,$id);
                redirect('/teaching_evaluation/setForm/list');
            }

            $data['setForm'] = $this->set_form_model->getSetFormDetails($id);
            $data['setFormQuestionPoolList'] = $this->set_form_model->getSetFormHasQuestionPoolByFormId($id);
            $data['semesterList'] = $this->set_form_model->semesterListByStatus('1');
            $data['organisationList'] = $this->set_form_model->organisationListByStatus('1');
            $data['programList'] = $this->set_form_model->programListByStatus('1');
            $data['questionPoolList'] = $this->set_form_model->questionPoolListByStatus('1');

                // echo "<Pre>"; print_r($data['setFormQuestionPoolList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Set Form';
            $this->loadViews("set_form/edit", $this->global, $data, NULL);
        }
    }
}
