<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Set Form</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Set Form Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Set Form Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Branch <span class='error-text'>*</span></label>
                        <select name="id_branch" id="id_branch" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($organisationList))
                            {
                                foreach ($organisationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name . " - " . $record->permanent_address;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
            </div>

            <div class="row">
                <br>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>List Of Question Pool <span class='error-text'>*</span></p>
                        <?php 
                        foreach ($questionPoolList as  $record)
                        {
                            ?>
                    
                            <label><input type="checkbox" value="<?php echo $record->id; ?>" name="id_pool[]"> <?php echo $record->name; ?></label>
                            <br>
                        <?php 
                        }
                        ?>
                        </div>                         
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 id_branch: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                status: {
                    required: true
                },
                 id_pool: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Proram</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_pool: {
                    required: "<p class='error-text'>Select Pool Question</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
