<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        // echo "string";exit();
        // $test = new BaseController();
        // $test->isScholarshipLoggedIn();
        parent::__construct();
        $this->load->model('profile_model');
        $this->isScholarApplicantLoggedIn();
    }

    public function index()
    {
        $this->list();
    }

    function demoView()
    {
        $this->load->view('vie');
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Scholarship Portal : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
            $data['profileList'] = $this->profile_model->profileList();
            $this->global['pageTitle'] = 'Scholarship Portal : Payment Type List';
            $this->loadViews("profile/list", $this->global, $data, NULL);
    }

    function logout()
    {
        // echo "string";exit();
        $sessionArray = array('id_scholar_applicant'=> '',                    
                    'scholar_applicant_name'=> '',
                    'scholar_applicant_email_id'=> '',
                    'scholar_applicant_last_login'=> '',
                    'isScholarApplicantLoggedIn' => FALSE
            );

     $this->session->set_userdata($sessionArray);
     // $this->session->sess_destroy();
     // redirect($_SERVER['HTTP_REFERER']);
     $this->isScholarApplicantLoggedIn();
    }
}
