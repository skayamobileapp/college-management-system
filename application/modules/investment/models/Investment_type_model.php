<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Investment_type_model extends CI_Model
{
    function investmentTypeListSearch($data)
    {
        $this->db->select('invt.*, accc.name as account_name, actc.name as activity_name, dept.name as department_name, func.name as fund_name ');
        $this->db->from('investment_type as invt');
        $this->db->join('account_code as accc', 'invt.account_code = accc.code');
        $this->db->join('activity_code as actc', 'invt.activity_code = actc.code');
        $this->db->join('department_code as dept', 'invt.department_code = dept.code');
        $this->db->join('fund_code as func', 'invt.fund_code = func.code');
        if ($data['name'] != '')
        {
            $likeCriteria = "(invt.type LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['contact_number'] != '')
        {
            $likeCriteria = "(invt.contact_number  LIKE '%" . $data['contact_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['account_code'] != '')
        {
            $this->db->where('invt.account_code', $data['account_code']);
        }
        if ($data['activity_code'] != '')
        {
            $this->db->where('invt.activity_code', $data['activity_code']);
        }
        if ($data['department_code'] != '')
        {
            $this->db->where('invt.department_code', $data['department_code']);
        }
        if ($data['fund_code'] != '')
        {
            $this->db->where('invt.fund_code', $data['fund_code']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('invt.status', $data['status']);
        }
        $this->db->order_by("type", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    function getInvestmentType($id)
    {
        $this->db->select('*');
        $this->db->from('investment_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewInvestmentType($data)
    {
        $this->db->trans_start();
        $this->db->insert('investment_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editInvestmentType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('investment_type', $data);
        return TRUE;
    }

}