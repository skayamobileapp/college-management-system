<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Investment_application_model extends CI_Model
{

    function getBankList()
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function investmentInstitutionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('investment_institution');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getInvestmentApplicationListSearch($data)
    {
        $this->db->select('ina.*, bnk.name as investment_bank_name, bnk.code as investment_bank_code');
        $this->db->from('investment_application as ina');
        $this->db->join('bank_registration as bnk','ina.id_investment_bank = bnk.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['name'] . "%' or ina.application_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_investment_bank'] !='')
        {
            $this->db->where('ina.id_investment_bank', $data['id_investment_bank']);
        }
        if ($data['status'] !='')
        {
         // echo "<Pre>";print_r($data);exit();     

            $this->db->where('ina.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function generateInvestmentApplicationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('investment_application');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
            $generated_number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addInvestmentApplication($data) {
        $this->db->trans_start();
        $this->db->insert('investment_application', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function investmentTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('investment_type');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_investment_application_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempInvestmentApplicationDetailsBySession($sessionid)
    {
        $this->db->select('tpe.*, invt.type as investment_type, invi.code as investment_institution_code, invi.name as investment_institution_name, bnk.code as bank_code, bnk.name as bank_name');
        $this->db->from('temp_investment_application_details as tpe');
        $this->db->join('investment_type as invt', 'tpe.id_investment_type = invt.id');
        $this->db->join('investment_institution as invi', 'tpe.id_institution = invi.id');
        $this->db->join('bank_registration as bnk', 'tpe.id_bank = bnk.id');
        $this->db->where('tpe.id_session', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id) {
        $this->db->where('id', $id);
        $this->db->delete('temp_investment_application_details');
        return TRUE;
    }

    function getTempDetailsBySession($sessionid)
    {
        $this->db->select('tpe.*');
        $this->db->from('temp_investment_application_details as tpe');
        $this->db->where('tpe.id_session', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewInvestmentApplicationDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('investment_application_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempDetailsBySession($id_session) {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_investment_application_details');
        return TRUE;
    }

    function getInvestmentApplication($id)
    {
        $this->db->select('tpe.*, bnk.code as bank_code, bnk.name as bank_name');
        $this->db->from('investment_application as tpe');
        $this->db->join('bank_registration as bnk', 'tpe.id_investment_bank = bnk.id');
        $this->db->where('tpe.id', $id);
        $query = $this->db->get();
        return $query->row();

    }

    function getInvestmentApplicationDetails($id_application)
    {
        $this->db->select('tpe.*, invt.type as investment_type, invi.code as investment_institution_code, invi.name as investment_institution_name, bnk.code as bank_code, bnk.name as bank_name');
        $this->db->from('investment_application_details as tpe');
        $this->db->join('investment_type as invt', 'tpe.id_investment_type = invt.id');
        $this->db->join('investment_institution as invi', 'tpe.id_institution = invi.id');
        $this->db->join('bank_registration as bnk', 'tpe.id_bank = bnk.id');
        $this->db->where('tpe.id_application', $id_application);
        $query = $this->db->get();
        return $query->result();
    }

    function editInvestmentApplication($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('investment_application', $data);
        return TRUE;
    }

    function editInvestmentApplicationDetails($data,$id_detail,$id_application)
    {
        $this->db->where('id', $id_detail);
        $this->db->update('investment_application_details', $data);

        $pending_data = $this->getInvestmentApplicationByStatusNId($id_application);


        if($pending_data == '')
        {
        // echo "<Pre>";print_r($pending_data);exit;
        $approve = array(
                    'status' => 1
                );

        $this->updateInvestmentApplication($approve,$id_application);

        }
        elseif ($pending_data->status == '2')
        {

            # code...
        }
        else
        {
            // echo "<Pre>";print_r("Pending");exit;
        }
        return TRUE;
    }

    function getInvestmentApplicationByStatusNId($id_application)
    {
        $status1 = '0';
        $status2 = '2';
        $this->db->select('tpe.*');
        $this->db->from('investment_application_details as tpe');
        $this->db->where('tpe.id_application', $id_application);

        $likeCriteria = "(tpe.status  = '" . $status1 . "' or tpe.status  = '" . $status2 . "')";
            $this->db->where($likeCriteria);

        $query = $this->db->get();
        return $query->row();

    }

    function updateInvestmentApplication($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('investment_application', $data);
        return TRUE;
    }
}
