<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Investment_institution_model extends CI_Model
{
    function investmentInstitutionListSearch($data)
    {
        $this->db->select('ii.*, bnk.name as bank_name, co.name as country_name, st.name as state_name');
        $this->db->from('investment_institution as ii');
        $this->db->join('bank_registration as bnk', 'ii.id_bank = bnk.id');
        $this->db->join('state as st', 'ii.id_state = st.id');
        $this->db->join('country as co', 'ii.id_country = co.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(ii.name  LIKE '%" . $data['name'] . "%' or ii.code  LIKE '%" . $data['name'] . "%' or ii.branch  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['contact_number'] != '')
        {
            $likeCriteria = "(ii.contact_name  LIKE '%" . $data['contact_number'] . "%' or ii.contact_number  LIKE '%" . $data['contact_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['status'] != '')
        {
            $this->db->where('ii.status', $data['status']);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getInvestmentInstitution($id)
    {
        $this->db->select('*');
        $this->db->from('investment_institution');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewInvestmentInstitution($data)
    {
        $this->db->trans_start();
        $this->db->insert('investment_institution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editInvestmentInstitution($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('investment_institution', $data);
        return TRUE;
    }

    function getCountryList()
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    function getBankList()
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }
}