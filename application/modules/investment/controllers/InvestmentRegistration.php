<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvestmentRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('investment_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('investment_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $data['investmentBankList'] = $this->investment_registration_model->getBankList();
            // $data['investmentInstitutionList'] = $this->investment_registration_model->investmentInstitutionListByStatus('1');

            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['investmentRegistrationList'] = $this->investment_registration_model->getInvestmentRegistrationListSearch($formData);
            // echo "<Pre>";print_r($data['investmentRegistrationList']);exit;

            $this->global['pageTitle'] = 'FIMS : List Investment Registration';
            $this->loadViews("investment_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('investment_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $generated_number = $this->investment_registration_model->generateInvestmentRegistrationNumber();

                $id_application = $this->security->xss_clean($this->input->post('id_application'));
                $id_application_detail = $this->security->xss_clean($this->input->post('id_application_detail'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $contact_person_one = $this->security->xss_clean($this->input->post('contact_person_one'));
                $contact_email_one = $this->security->xss_clean($this->input->post('contact_email_one'));
                $rate_of_interest = $this->security->xss_clean($this->input->post('rate_of_interest'));
                $contact_person_two = $this->security->xss_clean($this->input->post('contact_person_two'));
                $contact_email_two = $this->security->xss_clean($this->input->post('contact_email_two'));
                $interest_day = $this->security->xss_clean($this->input->post('interest_day'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $interest_to_bank = $this->security->xss_clean($this->input->post('interest_to_bank'));
                $profit_amount = $this->security->xss_clean($this->input->post('profit_amount'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));



                $detail_data = $this->investment_registration_model->getInvestmentApplicationDetailById($id_application_detail);

                // echo "<Pre>";print_r($detail_data);exit();

                // For Investment Registration
                // investment_status Field
                // 1-> Initial Entry - Registered
                // 0-> Added To Withdraw Application
                // 2-> Approved Withdraw Application
                // 3-> Reinvested Application

                $data = array(
                    'id_application' => $id_application,
                    'id_application_detail' => $id_application_detail,
                    'registration_number' => $generated_number,
                    'name' => $name,
                    'duration' => $detail_data->duration,
                    'duration_type' => $detail_data->duration_type,
                    'amount' => $detail_data->total_amount,
                    'id_institution' => $detail_data->id_institution,
                    'id_investment_type' => $detail_data->id_investment_type,
                    'id_bank' => $detail_data->id_bank,
                    'profit_rate' => $detail_data->profit_rate,
                    'maturity_date' => $detail_data->maturity_date,
                    'effective_date' => date("Y-m-d", strtotime($effective_date)),
                    'contact_person_one' => $contact_person_one,
                    'contact_email_one' => $contact_email_one,
                    'rate_of_interest' => $rate_of_interest,
                    'interest_to_bank' => $interest_to_bank,
                    'contact_person_two' => $contact_person_two,
                    'contact_email_two' => $contact_email_two,
                    'interest_day' => $interest_day,
                    'profit_amount' => $profit_amount,
                    'account_code' => $account_code,
                    'activity_code' => $activity_code,
                    'department_code' => $department_code,
                    'fund_code' => $fund_code,
                    'created_by' => $user_id,
                    'status'=>'1',
                    'investment_status'=>'1'
                );

                 // echo "<Pre>";print_r($data);exit();
                $inserted_id = $this->investment_registration_model->addInvestmentRegistration($data);

                if($inserted_id)
                {

                    $detail_data_update = $this->investment_registration_model->updateInvestmentApplicationDetailById($id_application_detail,$id_application,$inserted_id);
                }


                redirect('/investment/investmentRegistration/list');


            }

            $data['investmentApplicationList'] = $this->investment_registration_model->investmentApplicationListForRegistration();

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Investment Registration';
            $this->loadViews("investment_registration/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('investment_registration.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/investment/investmentApplication/list');
            }
            
            
            $data['investmentRegistration'] = $this->investment_registration_model->getInvestmentRegistration($id);

            // echo "<Pre>";print_r($data['investmentRegistration']);exit();
            

            $this->global['pageTitle'] = 'FIMS : View Investment Registration';
            $this->loadViews("investment_registration/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('investment_registration.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/investment/investmentApplication/reinvestmentList');
            }
            
            
            $data['investmentRegistration'] = $this->investment_registration_model->getInvestmentRegistration($id);

            // echo "<Pre>";print_r($data['investmentRegistration']);exit();
            

            $this->global['pageTitle'] = 'FIMS : View Investment Registration';
            $this->loadViews("investment_registration/view", $this->global, $data, NULL);
        }
    }

    function reinvestmentList()
    {

        if ($this->checkAccess('investment_reinvest.list') == 1)
        // if ($this->checkAccess('investment_withdraw.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'reinvest':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         // echo "<Pre>";print_r($id);exit();
                         
                         $result = $this->investment_registration_model->reinvest($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;
                case 'search':

                  
                     
                     break;
                 
                default:
                     break;
             }
                
            }
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['investmentRegistrationList'] = $this->investment_registration_model->getInvestmentRegistrationListSearchForReinvestment($formData);
            // echo "<Pre>";print_r($data['investmentRegistrationList']);exit;

            $this->global['pageTitle'] = 'FIMS : List Investment Registration For Reinvestment';
            $this->loadViews("investment_registration/reinvesment_list", $this->global, $data, NULL);
        }
    }

    function getApplicationDetails($id_application)
    {
        if($id_application)
        {
       
        $results = $this->investment_registration_model->getInvestmentApplicationDetailsForRegistration($id_application);            
        
        // echo "<Pre>";print_r($results);exit();


            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_application_detail' id='id_application_detail' class='form-control' onchange='getApplicationData(this.value)'>";

            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $investment_type = $results[$i]->investment_type;
            $table.="<option value=".$id.">".$investment_type . "</option>";

            }
            $table.="</select>";

            echo $table;
        }
    }


    function getApplicationData($id_application_detail)
    {
        if($id_application_detail)
        {
       
        $results = $this->investment_registration_model->getInvestmentApplicationDetail($id_application_detail);
        
        // echo "<Pre>";print_r($results);exit();

        $id = $results->id;
        $duration = $results->duration;
        $duration_type = $results->duration_type;
        $total_amount = $results->total_amount;
        $profit_rate = $results->profit_rate;
        $maturity_date = date("d-m-Y", strtotime($results->maturity_date));

        $remarks = $results->remarks;
        $investment_type = $results->investment_type;
        $investment_institution_code = $results->investment_institution_code;
        $investment_institution_name = $results->investment_institution_name;
        $bank_code = $results->bank_code;
        $bank_name = $results->bank_name;


            $table="

             <script type='text/javascript'>
                $('select').select2();
            </script>


            <div class='page-title clearfix'>
                <h3>Investment Application Details</h3>
            </div>
           

        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Duration <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $duration $duration_type'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Profit Rate <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $profit_rate'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Type <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $investment_type '>
                    </div>
            </div>            

        </div>

        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Institution <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $investment_institution_code - $investment_institution_name '>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Bank <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $bank_code - $bank_name'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Maturity Date <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='maturity_date' name='maturity_date' readonly='readonly' value='$maturity_date' autocomplete='off'>
                    </div>
            </div>

        </div>

        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Amount <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value='$total_amount'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Remarks If Any <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value='$remarks'>
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Interest To Bank <span class='error-text'>*</span></label>
                        <input type='number' class='form-control' id='interest_to_bank' name='interest_to_bank' >
                    </div>
            </div>

        </div>

        <div class='row'>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Description For Registration <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='name' name='name' >
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person Name <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_person_one' name='contact_person_one' >
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person Email <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_email_one' name='contact_email_one' >
                    </div>
            </div>

        </div>


        <div class='row'>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Rate Of Interest <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='rate_of_interest' name='rate_of_interest' >
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person 2 Name <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_person_two' name='contact_person_two' >
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person 2 Email <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_email_two' name='contact_email_two' >
                    </div>
            </div>

        </div>


        <div class='row'>

        

         <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Interest Day <span class='error-text'>*</span></label>
                        <input type='number' class='form-control' id='interest_day' name='interest_day' >
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Profit Amount <span class='error-text'>*</span></label>
                        <input type='number' class='form-control' id='profit_amount' name='profit_amount' >
                    </div>
            </div>

            ";

        $accountCodeList = $this->investment_registration_model->getAccountCodeList();
        $activityCodeList = $this->investment_registration_model->getActivityCodeList();
        $departmentCodeList = $this->investment_registration_model->getDepartmentCodeList();
        $fundCodeList = $this->investment_registration_model->getFundCodeList();



            $table.="
            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Account Code <span class='error-text'>*</span></label>

            <select name='account_code' id='account_code' class='form-control'>

            <option value=''>Select</option>";

            for($i=0;$i<count($accountCodeList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $accountCodeList[$i]->id;
            $code = $accountCodeList[$i]->code;
            $name = $accountCodeList[$i]->name;
            $table.="<option value=".$code.">".$code ." - " . $name . "</option>";

            }

            $table.="</select>
                </div>
            </div>
            </div>

            <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Activity Code <span class='error-text'>*</span></label>

            <select name='activity_code' id='activity_code' class='form-control'>

            <option value=''>Select</option>";

            for($i=0;$i<count($activityCodeList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $activityCodeList[$i]->id;
            $code = $activityCodeList[$i]->code;
            $name = $activityCodeList[$i]->name;
            $table.="<option value=".$code.">".$code ." - " . $name . "</option>";

            }

            $table.="</select>
                </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Department Code <span class='error-text'>*</span></label>

            <select name='department_code' id='department_code' class='form-control'>

            <option value=''>Select</option>
                ";


            for($i=0;$i<count($departmentCodeList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $departmentCodeList[$i]->id;
            $code = $departmentCodeList[$i]->code;
            $name = $departmentCodeList[$i]->name;
            $table.="<option value=".$code.">".$code ." - " . $name . "</option>";

            }





                $table.="
                </select>
                </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Fund Code <span class='error-text'>*</span></label>

            <select name='fund_code' id='fund_code' class='form-control'>

            <option value=''>Select</option>";

            for($i=0;$i<count($fundCodeList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $fundCodeList[$i]->id;
            $code = $fundCodeList[$i]->code;
            $name = $fundCodeList[$i]->name;
            $table.="<option value=".$code.">".$code ." - " . $name . "</option>";

            }

            $table.="
            </select>
                </div>
            </div>
            </div>

            <div class='row'>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type='text' class='form-control datepicker' id='effective_date' name='effective_date' autocomplete='off'>
                    </div>
            </div>

            </div>

            <script>
              $( function() {
                $( '.datepicker' ).datepicker({
                    changeYear: true,
                    changeMonth: true,
                });
              } );
            </script>


            ";


            echo $table;
        }
    }
}