<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvestmentInstitution extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('investment_institution_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('investment_institution.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['contact_number'] = $this->security->xss_clean($this->input->post('contact_number'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['investmentInstitutionList'] = $this->investment_institution_model->investmentInstitutionListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($formData);exit();


            $this->global['pageTitle'] = 'FIMS : List Investment Institution';
            $this->loadViews("investment_institution/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('investment_institution.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $contact_name = $this->security->xss_clean($this->input->post('contact_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $branch = $this->security->xss_clean($this->input->post('branch'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
					'name' => $name,
					'code' => $code,
					'id_bank' => $id_bank,
					'contact_name' => $contact_name,
					'contact_number' => $contact_number,
					'address' => $address,
					'id_country' => $id_country,
					'id_state' => $id_state,
					'landmark' => $landmark,
					'branch' => $branch,
					'city' => $city,
					'zipcode' => $zipcode,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->investment_institution_model->addNewInvestmentInstitution($data);
                redirect('/investment/investmentInstitution/list');
            }

            $data['countryList'] = $this->investment_institution_model->getCountryList();
            $data['stateList'] = $this->investment_institution_model->getStateList();
            $data['bankList'] = $this->investment_institution_model->getBankList();



            $this->global['pageTitle'] = 'FIMS : Add Investment Institution';
            $this->loadViews("investment_institution/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('investment_institution.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/investment/investmentInstitution/list');
            }
            if($this->input->post())
            {
               $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $contact_name = $this->security->xss_clean($this->input->post('contact_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $branch = $this->security->xss_clean($this->input->post('branch'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
					'name' => $name,
					'code' => $code,
					'id_bank' => $id_bank,
					'contact_name' => $contact_name,
					'contact_number' => $contact_number,
					'address' => $address,
					'id_country' => $id_country,
					'id_state' => $id_state,
					'landmark' => $landmark,
					'branch' => $branch,
					'city' => $city,
					'zipcode' => $zipcode,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->investment_institution_model->editInvestmentInstitution($data,$id);
                redirect('/investment/investmentInstitution/list');
            }

            $data['countryList'] = $this->investment_institution_model->getCountryList();
            $data['stateList'] = $this->investment_institution_model->getStateList();
            $data['bankList'] = $this->investment_institution_model->getBankList();

            $data['investmentInstitution'] = $this->investment_institution_model->getInvestmentInstitution($id);

            $this->global['pageTitle'] = 'FIMS : Edit Investment Institution';
            $this->loadViews("investment_institution/edit", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
        // echo "<Pre>"; print_r($id_country);exit;
        $results = $this->investment_institution_model->getStateByCountryId($id_country);

        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='id_state' id='id_state' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}
