<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Investment Registration Withdraw</h3>
        </div>

        <br>
        <form id="form_investment_institution" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Investment Withdraw</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Institution <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->investment_institution_code . " - " . $investmentWithdraw->investment_institution_name; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Bank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->bank_code . " - " . $investmentWithdraw->bank_name; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->investment_type; ?>" readonly="readonly">
                    </div>
                </div>

            </div>





            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Withdraw Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $investmentWithdraw->reference_number; ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $investmentWithdraw->registration_number; ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $investmentWithdraw->name; ?>" readonly="readonly">
                    </div>
                </div>      

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name" value="<?php echo $investmentWithdraw->rate_of_interest; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_number" name="contact_number" value="<?php echo $investmentWithdraw->contact_person_one; ?>" readonly="readonly">
                    </div>
                </div>
            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $investmentWithdraw->contact_email_one; ?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 2 Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="branch" name="branch" value="<?php echo $investmentWithdraw->contact_person_two; ?>"  readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Two Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $investmentWithdraw->contact_email_two; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo date("d-m-Y", strtotime($investmentWithdraw->effective_date)); ?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Maturity Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo date("d-m-Y", strtotime($investmentWithdraw->maturity_date)); ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Profit Rate <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->profit_rate; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Profit Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->profit_amount; ?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Interest To Bank <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->interest_to_bank; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Interest Day <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->interest_day; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Duration <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->duration . " - " . $investmentWithdraw->duration_type; ?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->account_code . " - " . $investmentWithdraw->account_code_name; ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->activity_code . " - " . $investmentWithdraw->activity_code_name; ?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->department_code . " - " . $investmentWithdraw->department_code_name; ?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fund Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentWithdraw->fund_code . " - " . $investmentWithdraw->fund_code_name; ?>" readonly="readonly">
                        </div>
                </div>

            </div>

        </div>





            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
