<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Investment Type</h3>
        </div>
        <form id="form_invrestment_type" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Investment Type Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="type" name="type">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Mobile <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" maxlength="10" minlength="10">
                    </div>
                </div>

            </div>



            <div class="row">

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <select name="fund_code" id="fund_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($fundCodeList))
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <select name="activity_code" id="activity_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active</label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive</label>                              
                        </div>                         
                </div>

            </div>

        </div>
            
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_invrestment_type").validate({
            rules: {
                type: {
                    required: true
                },
                 contact_name: {
                    required: true
                },
                 contact_number: {
                    required: true
                },
                 account_code: {
                    required: true
                },
                 activity_code: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 fund_code: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Investment Type Required</p>",
                },
                contact_name: {
                    required: "<p class='error-text'>Contact Name Required</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                activity_code: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                fund_code: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>