<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Ledger Entry</h3>
            </div>

            <div class="form-container">
            <h4 class="form-group-title">Ledger Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$ledgerEntry->id_financial_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$ledgerEntry->id_budget_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

               

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Journal Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y', strtotime($ledgerEntry->date));?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Descriptoin <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $ledgerEntry->description;?>" readonly="readonly">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Ledger Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="ledger_number" name="ledger_number" value="<?php echo $ledgerEntry->ledger_number;?>" readonly="readonly">
                    </div>
                </div>

               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Journal Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $ledgerEntry->journal_number;?>" readonly="readonly">
                    </div>
                </div> -->


                
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $ledgerEntry->total_amount; ?>" readonly="readonly">
                    </div>
               </div>
                
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php 
                        if($ledgerEntry->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($ledgerEntry->status == '1')
                        {
                            echo "Approved";
                        } 
                        elseif($ledgerEntry->status == '2')
                        {
                            echo "Rejected";
                        } 
                        ?>
                        " readonly="readonly">
                    </div>
                </div>
                
            </div>
        </div>


        <h3>Ledger Details</h3>

    <div class="form-container">
            <h4 class="form-group-title">Ledger Entry Details</h4>


         <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>Journal Type</th>
                     <th>GL Code</th>
                     <th>Credit Amount</th>
                     <th>Debit Amount</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                 $total_credit = 0;
                 $total_debit = 0;
                  for($i=0;$i<count($ledgerEntryDetails);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $ledgerEntryDetails[$i]->journal_type;?></td>
                    <td><?php echo $ledgerEntryDetails[$i]->fund_code . " - " . $ledgerEntryDetails[$i]->department_code . " - " . $ledgerEntryDetails[$i]->activity_code . " - " . $ledgerEntryDetails[$i]->account_code;?></td>
                    <td><?php echo $ledgerEntryDetails[$i]->credit_amount;?></td>
                    <td><?php echo $ledgerEntryDetails[$i]->debit_amount;?></td>

                     </tr>
                  <?php 
                  $total_credit = $total_credit + $ledgerEntryDetails[$i]->credit_amount;
                  $total_debit = $total_debit + $ledgerEntryDetails[$i]->debit_amount;
              } 
              $total_credit = number_format($total_credit, 2, '.', ',');
              $total_debit = number_format($total_debit, 2, '.', ',');
              ?>
                <tr>
                    <td bgcolor="" colspan="2"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total_credit; ?></b></td>
                    <td bgcolor=""><b><?php echo $total_debit; ?></b></td>
                </tr>

                    </tbody>
                </table>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );
</script>