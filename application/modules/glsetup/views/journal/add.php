<?php $this->load->helper("form"); ?>
<form id="form_journal" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Journal Entry</h3>
            </div>

            <div class="form-container">
            <h4 class="form-group-title">Journal Entry</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Financial Year <span class='error-text'>*</span></label>
                            <select name="id_financial_year" id="id_financial_year" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($financialYearList))
                                {
                                    foreach ($financialYearList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Budget Year <span class='error-text'>*</span></label>
                            <select name="id_budget_year" id="id_budget_year" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($budgetYearList))
                                {
                                    foreach ($budgetYearList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Journal Entry Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="date" name="date" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="total_amount" name="total_amount" readonly="readonly" value="0">
                        </div>

                    </div>

                </div>

            </div>

            <h3>Journal Details</h3><button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>
            
            <br>

            <div class="row">
                <div id="view"></div>
            </div>
            <br>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>

<!-- Modal -->
<form id="form_journal_details" action="" method="post">
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Journal Details</h4>
      </div>
      <div class="modal-body">

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Journal Type <span class='error-text'>*</span></label>
                        <select name="journal_type" id="journal_type" class="form-cchangeJournalTypeontrol" style="width: 196px" onchange="changeJournalType(this.value)">
                            <option value="">Select</option>
                            <option value="Credit">Credit</option>
                            <option value="Debit">Debit</option>
                        </select>
                    </div>
                </div>

            </div>

            <h4>GL Codes</h4>
            
            <div class="row">

                 <div class="col-sm-3">
                    <div class="form-group">
                        <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <select name="fund_code" id="fund_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                        </div>
                    </div>
                </div>


                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code; ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                        </div>
                    </div>
                </div>

            
               


                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <select name="activity_code" id="activity_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                        </div>
                    </div>
                </div>

                

               

                 <div class="col-sm-3">
                    <div class="form-group">
                        <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row" id="view_detail_amount"  style="display: none;">


                <div class="col-sm-3">
                    <div class="form-group" style="width: 196px">
                        <label>Debit Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="debit_amount" name="debit_amount">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group" style="width: 196px">
                        <label>Credit Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="credit_amount" name="credit_amount">
                    </div>
                </div>
                
            </div>


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" onclick="saveData()">Add</button>
        <button type="button" class="btn btn-default"  onclick="closeData()">Close</button>
      
      </div>
    </div>

  </div>
</div>

</form>
<script>

    function opendialog(){
         if($('#form_journal').valid())
        {
        //  $("#journal_type").val('');
        // $("#dt_department").val('');
        // $("#dt_activity").val('');
        // $("#dt_account").val('');
        // $("#cr_fund").val('');
        // $("#cr_department").val('');
        // $("#cr_activity").val('');
        // $("#cr_account").val('');
        // $("#type").val('');
        // $("#id_category").val('');
        // $("#id_sub_category").val('');
        // $("#id_item").val('');
        // $("#quantity").val('');
        // $("#price").val('');
        // $("#id_tax").val('');
        // $("#tax_price").val('');
        // $("#total_final").val('');
        // $("#id").val('0');
        $('#myModal').modal('show');
        }

    }

    function closeData()
    {
        $('#view_detail_amount').modal('hide');
        $('#myModal').modal('hide');
    }


    function saveData()
    {

         if($('#form_journal_details').valid())
        {

        var tempPR = {};
        tempPR['journal_type'] = $("#journal_type").val();
        tempPR['account_code'] = $("#account_code").val();
        tempPR['activity_code'] = $("#activity_code").val();
        tempPR['department_code'] = $("#department_code").val();
        tempPR['fund_code'] = $("#fund_code").val();
        tempPR['debit_amount'] = $("#debit_amount").val();
        tempPR['credit_amount'] = $("#credit_amount").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/glsetup/journal/tempadd',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(amount);

                $("#view").html(result);
                $('#view_detail_amount').modal('hide');
                $('#myModal').modal('hide');

                var total_cr = $("#total_cr").val();
                var total_dt = $("#total_dt").val();
                var total = $("#total").val();
                var amount = total_cr + total_dt;
                $("#total_amount").val(total);

               }
            });
        }
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/glsetup/journal/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);

                var amount = $("#total").val();
                $("#total_amount").val(amount);
               }
            });
    }


    function changeJournalType(type)
    {
        // alert(type);
        if(type == 'Debit')
        {
            $('#view_detail_amount').modal('show');

            $('#credit_amount').val('0');
            document.getElementById('credit_amount').readOnly = true;

            $('#debit_amount').val('');
            document.getElementById('debit_amount').readOnly = false;
        }
        else if(type == 'Credit')
        {
            $('#view_detail_amount').modal('show');

            $('#debit_amount').val('0');
            document.getElementById('debit_amount').readOnly = true;

            $('#credit_amount').val('');
            document.getElementById('credit_amount').readOnly = false;
        }
    }


    function validateDetailsData()
    {
        if($('#form_journal').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Journal Details");
            }
            else
            {
                var cr_amount = $("#total_cr").val();
                var dt_amount = $("#total_dt").val();
                if(dt_amount == cr_amount)
                {
                    $('#form_journal').submit();
                }
                else
                {
                    alert('Credit Amount and Debit Amount Should Be Equal');
                }

            }
        }    
    }



    $(document).ready(function() {
        $("#form_journal").validate({
            rules: {
                id_financial_year: {
                    required: true
                },
                id_budget_year: {
                    required: true
                },
                 description: {
                    required: true
                }
            },
            messages: {
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     $(document).ready(function() {
        $("#form_journal_details").validate({
            rules: {
                journal_type: {
                    required: true
                },
                 account_code: {
                    required: true
                },
                 activity_code: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 fund_code: {
                    required: true
                },
                 debit_amount: {
                    required: true
                },
                 credit_amount: {
                    required: true
                }
            },
            messages: {
                journal_type: {
                    required: "<p class='error-text'>Select Journl Type</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                activity_code: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                fund_code: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                debit_amount: {
                    required: "<p class='error-text'>Emter Debit Amount</p>",
                },
                credit_amount: {
                    required: "<p class='error-text'>Enter Credit Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>