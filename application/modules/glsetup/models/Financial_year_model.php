<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Financial_year_model extends CI_Model
{
    function financialYearList()
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->order_by("year", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function financialYearListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        if (!empty($search))
        {
            $likeCriteria = "(year  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("year", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getFinancialYear($id)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function checkFinancialYearDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('year', $data['year']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkFinancialYearDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('year', $data['year']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewFinancialYear($data)
    {
        $this->db->trans_start();
        $this->db->insert('financial_year', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFinancialYear($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('financial_year', $data);
        return TRUE;
    }
}

