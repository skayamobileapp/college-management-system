<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class JournalApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('journal_model');
        $this->isLoggedIn();
    }

    function list()
    {
       // echo "<Pre>"; print_r($this->session->userId);exit;
        if ($this->checkAccess('journal_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['journalList'] = $this->journal_model->journalApprovalListSearch('');
            $this->global['pageTitle'] = 'FIMS : Journal List';

             $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array)) {

                $result = $this->journal_model->approveJouranl($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->loadViews("journal_approval/list", $this->global, $data, NULL);
        }
    }
    
   
}