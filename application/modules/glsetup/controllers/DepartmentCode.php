<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DepartmentCode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('department_code_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('department_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['departmentCodeList'] = $this->department_code_model->departmentCodeListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Department Code';
            //print_r($subjectDetails);exit;
            $this->loadViews("department_code/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('department_code.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
            
                $duplicate_row = $this->department_code_model->checkDepartmentCodeDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Department Code Not Allowed";exit();
                }

                $result = $this->department_code_model->addNewDepartmentCode($data);
                redirect('/glsetup/departmentCode/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Department Code';
            $this->loadViews("department_code/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('department_code.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/departmentCode/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                $duplicate_row = $this->department_code_model->checkDepartmentCodeDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Department Code Not Allowed";exit();
                }
                
                $result = $this->department_code_model->editDepartmentCode($data,$id);
                redirect('/glsetup/departmentCode/list');
            }
            $data['departmentCode'] = $this->department_code_model->getDepartmentCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit Department Code';
            $this->loadViews("department_code/edit", $this->global, $data, NULL);
        }
    }
}
