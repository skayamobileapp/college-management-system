<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <!-- <h3>Edit Student</h3> -->
        </div>
  
<form id="form_vendor" action="" method="post">
   <div class="row">
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="container" id="tabs">
        <ul class="nav nav-tabs offers-tab text-center" role="tablist" >
            <li role="nav-item" class="active"><a href="#personal" class="nav-link active border rounded text-center"
                    aria-controls="personal" aria-selected="true"
                    role="tab" data-toggle="tab">Vendor / Company Details</a>
            </li>
            <li role="nav-item"><a href="#company" class="nav-link company border rounded text-center" aria-selected="false"
                    aria-controls="company" role="tab" data-toggle="tab">Company Details</a>
            </li>
            <li role="nav-item"><a href="#bank" class="nav-link bank border rounded text-center" aria-selected="false"
                    aria-controls="bank" role="tab" data-toggle="tab">Bank Details</a>
            </li>
            <li role="presentation"><a href="#billing" class="nav-link border rounded text-center"
                    aria-controls="billing" role="tab" data-toggle="tab">Billing Person Details</a>
            </li>
        </ul>

        <div class="tab-content offers-tab-content">
          <div role="tabpanel" class="tab-pane active" id="personal">
            
            <div class="col-12 mt-4">

         <br>
        <h4>Vendor Details</h4>

        <div class="form-container">
            <h4 class="form-group-title">Vendor Details</h4>
                


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendor Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vname" name="vname">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vemail" name="vemail" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vphone" name="vphone">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Gender <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Male" checked="checked"><span class="check-radio"></span> Male
                             <span class='error-text'>*</span></label>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Female"><span class="check-radio"></span> Female
                             <span class='error-text'>*</span></label> 
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Others"><span class="check-radio"></span> Others
                             <span class='error-text'>*</span></label>                              
                        </div>                         
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address One <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_one" name="vaddress_one" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Two <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_two" name="vaddress_two"  >
                        </div>
                    </div>

                </div>

                <div class="row">
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="vcountry" id="vcountry" class="form-control" onchange="getStateByCountry(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id='view_state'></span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vnric" name="vnric" >
                        </div>
                    </div>

                </div>


                <div class="row">

                   
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vmobile" name="vmobile" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>zipcode / Pincode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="vzipcode" name="vzipcode" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off">
                        </div>
                    </div>

                </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group pull-right">
                <button type="button" class="btn btn-primary btn-lg btnNext" onclick="validateVendor()">Next</button>
            </div>
        </div>
            
         </div> <!-- END col-12 -->  
        <!-- </form> -->
        </div>

        <div role="tabpanel" class="tab-pane" id="company">
        <!-- <form id="form_company" action="" method="post"> -->

        

        <div class="col-12 mt-4">
        <br>
            <div class="form-container">
                <h4 class="form-group-title">Company Details</h4>


                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Company Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="cname" name="cname">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="cemail" name="cemail" >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Phone Number <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="cphone" name="cphone">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Tax Number <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="tax_no" name="tax_no"  >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Address One <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="caddress_one" name="caddress_one"  >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Address Two <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="caddress_two" name="caddress_two"  >
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Country <span class='error-text'>*</span></label>
                                <select name="ccountry" id="ccountry" class="form-control" onchange="getStateByCountryCompany(this.value)" style="width: 360px">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($countryList))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>State <span class='error-text'>*</span></label>
                                <span id='view_company_state'></span>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>zipcode / Pincode <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="czipcode" name="czipcode" >
                            </div>
                        </div>
                    </div>

            </div> 


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg btnNext2">Next</button>
                </div>
            </div>  

        </div><!-- END col-12 -->  

        </form>
        
        </div>

        <div role="tabpanel" class="tab-pane" id="bank">
        <form id="form_bank" action="" method="post">

            <div class="col-12 mt-4">
                <br>

        <div class="form-container">
        <h4 class="form-group-title">Bank Details</h4>



                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Bank Name <span class='error-text'>*</span></label>
                            <select name="id_bank" id="id_bank" class="form-control" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($bankList))
                                {
                                    foreach ($bankList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address" >
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="bcountry" id="bcountry" class="form-control" onchange="getStateByCountryBank(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id='view_bank_state'></span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="city" name="city" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode/Pincode <span class='error-text'>*</span></label>
                            <input type="number" name="zipcode1" id="zipcode1" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Branch Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="branch_no" name="branch_no">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Account Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="acc_no" name="acc_no" >
                        </div>
                    </div>
                </div>

                

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group pull-right">
                <button type="button" id="addBankData" class="btn btn-success btn-lg" >Add</button>
                <button type="button" class="btn btn-primary btn-lg btnNext3" >Next</button>
            </div>
        </div>

        <div class="form-container" id="view_bank_visible" style="display: none;">
                <h4 class="form-group-title">Bank Details</h4>

            <div class="custom-table">
                <div id="view_bank"></div>
            </div>

        </div>
             
         </div> <!-- END col-12 -->  
        </form>
        </div>

        <div role="tabpanel" class="tab-pane" id="billing">
        <form id="form_billing" action="" method="post">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
                <h4 class="form-group-title">Billing Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name1" name="name1">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric1" name="nric1" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email1" name="email1">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="phone1" name="phone1">
                        </div>
                    </div>
                </div>

            </div>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" id="addBillingData" class="btn btn-success btn-lg" >Add</button>
                    <!-- <button type="button" class="btn btn-primary btn-lg btnNext3" >Next</button> -->
                </div>
            </div>


         <div class="form-container" id="view_billing_visible" style="display: none;">
                <h4 class="form-group-title">Billing Details</h4>

                <div class="custom-table">
                    <div id="view_billing"></div>
                </div>

        </div>
             
         </div> <!-- END col-12 -->  
        </form>
        </div>

      </div>
    </div>

   </div> <!-- END row-->
   

            <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">


    function getStateByCountry(id)
    {
        $.get("/procurement/vendor/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }

    function getStateByCountryCompany(id)
    {
        $.get("/procurement/vendor/getStateByCountryCompany/"+id, function(data, status){
       
            $("#view_company_state").html(data);
        });

    }

    function getStateByCountryBank(id)
    {
        $.get("/procurement/vendor/getStateByCountryBank/"+id, function(data, status){
       
            $("#view_bank_state").html(data);
        });
    }


    $(document).ready(function() {
      $('.btnNext').click(function() {
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
      });
      $('.btnNext2').click(function() {
        $('.nav-tabs .company').parent().next('li').find('a').trigger('click');
      });
      $('.btnNext3').click(function() {
        $('.nav-tabs .bank').parent().next('li').find('a').trigger('click');
      });
    });



    $("#addBankData").click(function()
    {
        if($('#form_bank').valid())
        {

        var tempPR = {};
            tempPR['id_bank'] = $("#id_bank").val();
            tempPR['address'] = $("input[name='address']").val();
            tempPR['country'] = $("#bcountry").val();
           tempPR['state'] = $("#bstate").val();
           tempPR['city'] = $("input[name='city']").val();
           tempPR['zipcode'] = $("input[name='zipcode1']").val();
           tempPR['branch_no'] = $("input[name='branch_no']").val();
           tempPR['acc_no'] = $("input[name='acc_no']").val();
            // alert(tempPR['bcountry']);
            // alert(country1);

            $.ajax(
            {
               url: '/procurement/vendor/tempAddBank',
               type: 'POST',
               data:
               {
                tempData:tempPR
               },
               error: function()
               {
                // alert(data);
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_bank").html(result);
                if(result != '')
                {

                    $("#view_bank_visible").show();
                }else
                {
                    $("#view_bank_visible").hide();
                }

                //alert("Record added successfully");  
               }
            });
        }
    });

    function deleteTempBank(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/procurement/vendor/deleteTempBank/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_bank").html(result);
                    if(result != '')
                    {
                        $("#view_bank_visible").show();
                    }
                    else
                    {
                        $("#view_bank_visible").hide();
                    }
               }
            });
    }

    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                id_bank: {
                    required: true
                },
                 address: {
                    required: true
                },
                 bcountry: {
                    required: true
                },
                 bstate: {
                    required: true
                },
                 city: {
                    required: true
                },
                 zipcode1: {
                    required: true
                },
                 branch_no: {
                    required: true
                },
                 acc_no: {
                    required: true
                }
            },
            messages: {
                id_bank: {
                    required: "<p class='error-text'>Ban ID Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                bcountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                bstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode1: {
                    required: "<p class='error-text'>Zpcode Required</p>",
                },
                branch_no: {
                    required: "<p class='error-text'>Branch Number Required</p>",
                },
                acc_no: {
                    required: "<p class='error-text'>Account Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $("#addBillingData").click(function()
    {
        if($('#form_billing').valid())
        {

           var name = $("input[name='name1']").val();
           var nric = $("input[name='nric1']").val();
           var email = $("input[name='email1']").val();
           var phone = $("input[name='phone1']").val();

            $.ajax(
            {
               url: '/procurement/vendor/tempAddBilling',
               type: 'POST',
               data:
               {
                name: name,
                nric: nric,
                email: email,
                phone: phone
               },
               error: function()
               {
                alert(data);
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_billing").html(result);
                    if(result != '')
                    {
                        $("#view_billing_visible").show();
                    }else
                    {
                        $("#view_billing_visible").hide();
                    }
                }
            });
        }
    });

    function deleteTempBillById(id)
    {
        // alert(id);
        $.ajax(
            {
               url: '/procurement/vendor/deleteTempBill/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_billing").html(result);
                    if(result != '')
                    {
                        $("#view_billing_visible").show();
                    }else
                    {
                        $("#view_billing_visible").hide();
                    }
                }
            });
    }


    $(document).ready(function() {
        $("#form_billing").validate({
            rules: {
                name1: {
                    required: true
                },
                 nric1: {
                    required: true
                },
                 email1: {
                    required: true
                },
                 phone1: {
                    required: true
                }
            },
            messages: {
                name1: {
                    required: "<p class='error-text'>Billing Person Name Required</p>",
                },
                nric1: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                email1: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                phone1: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_vendor").validate({
            rules: {
                vname: {
                    required: true
                },
                 vemail: {
                    required: true
                },
                 vphone: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 vnric: {
                    required: true
                },
                 vmobile: {
                    required: true
                },
                 vaddress_one: {
                    required: true
                },
                 vaddress_two: {
                    required: true
                },
                 vcountry: {
                    required: true
                },
                 vzipcode: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 vstate: {
                    required: true
                },
                 cname: {
                    required: true
                },
                 cphone: {
                    required: true
                },
                 caddress_one: {
                    required: true
                },
                 ccountry: {
                    required: true
                },
                 cemail: {
                    required: true
                },
                 tax_no: {
                    required: true
                },
                 caddress_two: {
                    required: true
                },
                 czipcode: {
                    required: true
                },
                 cstate: {
                    required: true
                }
            },
            messages: {
                vname: {
                    required: "<p class='error-text'>Vendor Person Name Required</p>",
                },
                vemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                vphone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                vnric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                vmobile: {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                vaddress_one: {
                    required: "<p class='error-text'>Vendor Address One Required</p>",
                },
                vaddress_two: {
                    required: "<p class='error-text'>Vendor Address One  Required</p>",
                },
                vcountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                vzipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>DOB Required</p>",
                },
                vstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
                cname: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                cphone: {
                    required: "<p class='error-text'>Company Phone Required</p>",
                },
                caddress_one: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                ccountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                cemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                tax_no: {
                    required: "<p class='error-text'>Tax Number Required</p>",
                },
                caddress_two: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                czipcode: {
                    required: "<p class='error-text'>Company Zipcode Required</p>",
                },
                cstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateDetailsData()
    {
        if($('#form_vendor').valid())
        {
            if($('#form_bank').valid())
            {
                if($('#form_billing').valid())
                {
                    console.log($("#view_billing").html());
                    var addedBank = $("#view_bank").html();
                    var addedProgam = $("#view_billing").html();
                    if(addedBank=='' || addedBank == undefined)
                    {
                        alert("Add Bank Details");
                    }
                    else if(addedProgam=='' || addedProgam == undefined)
                    {
                        alert("Add Billing Details");
                    }else
                    {
                        $('#form_vendor').submit();
                    }
                }
            }
        }    
    }

    $('select').select2();

</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>