<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProcurementSubCategory extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('procurement_sub_category_model');
            $this->load->model('procurement_category_model');
            $this->isLoggedIn();
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function list()
    {

        if ($this->checkAccess('procurement_sub_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r("dasda");exit;
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['pr_category_code'] = $this->security->xss_clean($this->input->post('pr_category_code'));

            $data['searchParameters'] = $formData; 

            $data['procurementSubCategoryList'] = $this->procurement_sub_category_model->procurementSubCategoryListSearch($formData);
            $this->global['pageTitle'] = 'FIMS : List Procurement Sub Category';
            $this->loadViews("procurement_sub_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('procurement_sub_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_procurement_category = $this->security->xss_clean($this->input->post('id_procurement_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_procurement_category' => $id_procurement_category,
                    'status' => $status
                );
            
                $result = $this->procurement_sub_category_model->addNewProcurementSubCategory($data);
                redirect('/procurement/procurementSubCategory/list');
            }
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $this->global['pageTitle'] = 'FIMS : Add Procurement Sub Category';
            $this->loadViews("procurement_sub_category/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_sub_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/procurement_sub_category/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_procurement_category = $this->security->xss_clean($this->input->post('id_procurement_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_procurement_category' => $id_procurement_category,
                    'status' => $status
                );
                
                $result = $this->procurement_sub_category_model->editProcurementSubCategory($data,$id);
                redirect('/procurement/procurementSubCategory/list');
            }
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['procurementSubCategory'] = $this->procurement_sub_category_model->getProcurementSubCategory($id);
            // echo "<Pre>";print_r($data['procurementSubCategory']);exit;
            
            $this->global['pageTitle'] = 'FIMS : Edit Procurement Sub Category';
            $this->loadViews("procurement_sub_category/edit", $this->global, $data, NULL);
        }
    }
}
