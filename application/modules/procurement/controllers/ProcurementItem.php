<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProcurementItem extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('procurement_sub_category_model');
            $this->load->model('procurement_category_model');
            $this->load->model('procurement_item_model');
            $this->isLoggedIn();
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function list()
    {

        if ($this->checkAccess('procurement_sub_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r("dasda");exit;
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['procurementSubCategoryList'] = $this->procurement_sub_category_model->procurementSubCategoryList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_procurement_category'] = $this->security->xss_clean($this->input->post('id_procurement_category'));
            $formData['id_procurement_sub_category'] = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));

            $data['searchParameters'] = $formData; 

            $data['procurementItemList'] = $this->procurement_item_model->procurementItemListSearch($formData);
            $this->global['pageTitle'] = 'FIMS : List Procurement Item';
            $this->loadViews("procurement_item/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('procurement_item.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_procurement_category = $this->security->xss_clean($this->input->post('id_procurement_category'));
                $id_procurement_sub_category = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_procurement_category' => $id_procurement_category,
                    'id_procurement_sub_category' => $id_procurement_sub_category,
                    'status' => $status,
                    'created_by' => $id_user
                );
            // echo "<Pre>"; print_r($data);exit;
            
                $result = $this->procurement_item_model->addNewProcurementItem($data);
                redirect('/procurement/procurementItem/list');
            }
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['procurementSubCategoryList'] = $this->procurement_sub_category_model->procurementSubCategoryList();
            $this->global['pageTitle'] = 'FIMS : Add Procurement Sub Category';
            $this->loadViews("procurement_item/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_item.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/procurement_item/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_procurement_category = $this->security->xss_clean($this->input->post('id_procurement_category'));
                $id_procurement_sub_category = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_procurement_category' => $id_procurement_category,
                    'id_procurement_sub_category' => $id_procurement_sub_category,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                // print_r($results);exit;
                $result = $this->procurement_item_model->editProcurementItem($data,$id);
                redirect('/procurement/procurementItem/list');
            }
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['procurementSubCategoryList'] = $this->procurement_sub_category_model->procurementSubCategoryList();
            $data['procurementItem'] = $this->procurement_item_model->getProcurementItem($id);

            // echo "<Pre>";print_r($data['procurementItem']);exit;

            $this->global['pageTitle'] = 'FIMS : Edit Procurement Sub Category';
            $this->loadViews("procurement_item/edit", $this->global, $data, NULL);
        }
    }


     function getSubcategory($id)
     {       
            // print_r("dasda");exit;
            $results = $this->procurement_sub_category_model->procurementSubCategoryListByProcurementId($id);
            // echo "<Pre>"; print_r($results);exit;
            $table="

            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_procurement_sub_category' id='id_procurement_sub_category' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $categoryname = $results[$i]->code;
            $table.='<option value="'. $id.'">'.$categoryname.'</option>';

            }
            $table.="</select>";
            echo $table;
            exit;
    }

}
