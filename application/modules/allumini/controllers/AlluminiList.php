<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AlluminiList extends BaseController
{
    public function __construct()
    {
         parent::__construct();
        $this->load->model('allumini_model');
        $this->load->model('admission/applicant_model');
        $this->load->model('records/student_model');
        $this->load->model('setup/country_model');
        $this->load->model('setup/state_model');
        $this->load->model('admission/applicant_approval_model');
        $this->load->model('registration/course_registration_model');
        $this->load->model('examination/category_type_model');
        $this->load->model('examination/mode_of_program_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('allumini.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->student_model->intakeList();
            $data['programList'] = $this->student_model->programList();
            $data['awardList'] = $this->allumini_model->awardListByStatus('1');


                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                // $formData['last_name'] = $this->security->xss_clean($this->input->post('last_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
                $formData['id_award'] = $this->security->xss_clean($this->input->post('id_award'));
                $formData['applicant_status'] = 'Graduated';
                $data['searchParam'] = $formData;
 
            $data['applicantList'] = $this->allumini_model->applicantList($formData);



            $this->global['pageTitle'] = 'Campus Management System : Alumni List';
            // echo "<Pre>";print_r($data);exit;
            $this->loadViews("alumni/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('allumini.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/allumini/allumini/list');
            }
            if($this->input->post())
            {

            }
            $data['countryList'] = $this->country_model->countryList();
            $data['stateList'] = $this->state_model->stateList();

            $data['getStudentData'] = $this->student_model->getStudentData($id);
            $data['studentDetails'] = $this->student_model->getStudentDetails($id);

            $data['examDetails'] = $this->student_model->getExamDetails($id);
            $data['proficiencyDetails'] = $this->student_model->getProficiencyDetails($id);
            $data['employmentDetails'] = $this->student_model->getEmploymentDetails($id);
            $data['profileDetails'] = $this->student_model->getProfileDetails($id);
            $data['visaDetails'] = $this->student_model->getVisaDetails($id);
            $data['otherDocuments'] = $this->student_model->getOtherDocuments($id);
            $data['courseRegistrationList'] = $this->student_model->courseRegistrationList($id);

            // echo "<Pre>";print_r($data['otherDocuments']);exit();
            $this->global['pageTitle'] = 'Campus Management System : View Alumni';
            $this->loadViews("alumni/edit", $this->global, $data, NULL);
        }
    }
}
