<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <!-- <h3>Edit Student</h3> -->
        </div>




         <h4 class='sub-title'>Alumni Profile</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Alumni Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Alumni NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Alumni Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->program ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

        <br>

    <form id="form_programgrade" action="" method="post">
  
   <div>
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Education Details</a>
            </li>
            <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">English Proficiency Details</a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Employment Status</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Profile Details</a>
            </li>
            <li role="presentation"><a href="#visa" class="nav-link border rounded text-center"
                    aria-controls="visa" role="tab" data-toggle="tab">Visa Details</a>
            </li>
            <li role="presentation"><a href="#other" class="nav-link border rounded text-center"
                    aria-controls="other" role="tab" data-toggle="tab">Other Documents</a>
            </li>
            <li role="presentation"><a href="#course" class="nav-link border rounded text-center"
                    aria-controls="course" role="tab" data-toggle="tab">Course Registration</a>
            </li>
        </ul>
        <div class="tab-content offers-tab-content">
          <div role="tabpanel" class="tab-pane active" id="education">
            <div class="col-12 mt-4">
                <br>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Qualification Level</th>
            <th>Degree Awarded</th>
            <th>Result / CGPA</th>
            <th>Year Of Graduation</th>
            <th>College Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examDetails))
          {
            foreach ($examDetails as $record) {
          ?>
              <tr>
                <td><?php echo $record->qualification_level ?></td>
                <td><?php echo $record->degree_awarded ?></td>
                <td><?php echo $record->result ?></td>
                <td><?php echo $record->year ?></td>
                <td><?php echo $record->college_name ?></td>
                <td class="text-center"><?php echo anchor('admission/student/delete_exam?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="proficiency">
            <div class="col-12 mt-4">
                <br>

            

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Test</th>
            <th>Date</th>
            <th>Score</th>
            <th>File</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($proficiencyDetails))
          {
            foreach ($proficiencyDetails as $record)
            {
          ?>
              <tr>
                <td><?php echo $record->test ?></td>
                <td><?php echo $record->date ?></td>
                <td><?php echo $record->score ?></td>
                <td><?php echo $record->file ?></td>
                <td class="text-center"><?php echo anchor('admission/student/delete_english?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>


        <div role="tabpanel" class="tab-pane" id="employment">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Employmet Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control" id="company_name" name="company_name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Company Address</label>
                            <input type="text" class="form-control" id="company_address" name="company_address" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Telephone Number</label>
                            <input type="text" class="form-control" id="telephone" name="telephone">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax Number</label>
                            <input type="text" class="form-control" id="fax_num" name="fax_num">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Designation</label>
                            <input type="text" class="form-control" id="designation" name="designation" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Position Level</label>
                            <select class="form-control" id="position" name="position" style="width: 405px;">
                                <option value="">SELECT</option>
                                <option value="Senior Manager">Senior Manager</option>
                                <option value="Manager">Manager</option>
                                <option value="Senior Executive">Senior Executive</option>
                                <option value="Junior Executive">Junior Executive</option>
                                <option value="Non-Executive">Non-Executive</option>
                                <option value="Fresh Entry">Fresh Entry</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Year Of Service</label>
                            <input type="text" class="form-control" id="service_year" name="service_year">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Industry</label>
                            <input type="text" class="form-control" id="industry" name="industry" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Job Description</label>
                            <input type="text" class="form-control" id="job_desc" name="job_desc">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Employment Letter</label>
                            <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                </div>
            </div>

            <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Company Name</th>
            <th>Company Address</th>
            <th>Designation</th>
            <th>Position</th>
            <th>Year Of Service</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($employmentDetails))
          {
            foreach ($employmentDetails as $record) {
          ?>
              <tr>
                <td><?php echo $record->company_name ?></td>
                <td><?php echo $record->company_address ?></td>
                <td><?php echo $record->designation ?></td>
                <td><?php echo $record->position ?></td>
                <td><?php if($record->service_year == ""){ echo "0";} else {
                    echo $record->service_year; } ?></td>
                <td class="text-center"><?php echo anchor('admission/student/delete_employment?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Profile Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Salutation <span class='error-text'>*</span></label>
                        <select name="salutation" id="salutation" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <option value="Miss" <?php if($getStudentData->salutation=='Miss'){ echo "selected"; } ?>>Miss</option>
                            <option value="Mr" <?php if($getStudentData->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                            <option value="Mrs" <?php if($getStudentData->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                            <option value="Dr" <?php if($getStudentData->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>
                        </select>
                    </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php if(!empty($getStudentData->first_name)){ echo $getStudentData->first_name; } ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php if(!empty($getStudentData->last_name)){ echo $getStudentData->last_name; } ?>" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>ID Type</label>
                            <input type="text" class="form-control" id="id_type" name="id_type" value="<?php if(!empty($profileDetails->id_type)){ echo $profileDetails->id_type; } ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>MyKad Passport National ID Military ID/Police ID MyPR ID Number</label>
                            <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php if(!empty($profileDetails->passport_number)){ echo $profileDetails->passport_number; } ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Passport Expiry Date</label>
                            <input type="text" class="form-control datepicker" autocomplete="off" id="passport_expiry_date" name="passport_expiry_date" value="<?php if(!empty($profileDetails->passport_expiry_date)){ echo date('d-m-Y', strtotime($profileDetails->passport_expiry_date)); } ?>" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getStudentData->phone ?>" readonly>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control" id="gender" name="gender" style="width: 405px;" disabled="disabled">
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getStudentData->gender == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getStudentData->gender == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others" <?php if($getStudentData->gender == "Others"){ echo "selected=selected"; } ?>>OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth</label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php if(!empty($getStudentData->date_of_birth)){ echo date('d-m-Y', strtotime($getStudentData->date_of_birth)); } ?>" autocomplete="off" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status</label>
                            <select class="form-control" id="martial_status" name="martial_status" style="width: 405px;" disabled="disabled">
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getStudentData->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getStudentData->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getStudentData->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion</label>
                            <select name="religion" id="religion" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <option value="<?php echo 'Islam';?>"
                                <?php 
                                if ($getStudentData->religion == 'Islam')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Islam";  ?>
                            </option>

                            <option value="<?php echo 'Buddhism';?>"
                                <?php 
                                if ($getStudentData->religion == 'Buddhism')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Buddhism";  ?>
                            </option>

                            <option value="<?php echo 'Christianity';?>"
                                <?php 
                                if ($getStudentData->religion == 'Christianity')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Christianity";  ?>
                            </option>

                            <option value="<?php echo 'Hinduism'?>"
                                <?php 
                                if ($getStudentData->religion == 'Hinduism')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Hinduism";  ?>
                            </option>

                            <option value="<?php echo 'Other';?>"
                                <?php 
                                if ($getStudentData->religion == 'Other')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Other";  ?>
                            </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                        <label>Type Of Nationality</label>

                         <select name="nationality" id="nationality" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <option value="<?php echo 'Malaysian';?>"
                                <?php 
                                if ($getStudentData->nationality == 'Malaysian')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Malaysian";  ?>
                            </option>

                            <option value="<?php echo 'Other';?>"
                                <?php 
                                if ($getStudentData->nationality == 'Other')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Other";  ?>
                            </option>
                        </select>


                        </div>
                    </div>


                </div>


                <div class="row">

                    <div class="col-sm-4">
                    <div class="form-group">
                        <label> Race </label>
                        <select name="id_race" id="id_race" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <option value="<?php echo 'Bhumiputra';?>"
                                <?php 
                                if ($getStudentData->id_race == 'Bhumiputra')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Bhumiputra";  ?>
                            </option>

                            <option value="<?php echo 'International';?>"
                                <?php 
                                if ($getStudentData->id_race == 'International')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "International";  ?>
                            </option>
                        </select>
                    </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email ID</label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php if(!empty($getStudentData->email_id)){ echo $getStudentData->email_id; } ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Employment Letter</label>
                            <input type="file" class="form-control" id="employment_letter" name="employment_letter" readonly>
                        </div>
                    </div>

                </div>

      

        </div>

        <br>


        <div class="form-container">
            <h4 class="form-group-title">Mailing Address</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Address 1</label>
                        <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getStudentData->mail_address1 ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Address 2</label>
                        <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getStudentData->mail_address2 ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Country</label>
                        <select name="mailing_country" id="mailing_country" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($getStudentData->mailing_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing State</label>
                        <select name="mailing_state" id="mailing_state" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($getStudentData->mailing_state==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing City</label>
                        <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getStudentData->mailing_city ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Zipcode</label>
                        <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getStudentData->mailing_zipcode ?>" readonly>
                    </div>
                </div>
            </div>

        </div>

        
        <br>

        <div class="form-container">
            <h4 class="form-group-title">Permanent Address</h4>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Address 1</label>
                        <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getStudentData->permanent_address1 ?>" readonly>
                    </div>
                </div><div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Address 2</label>
                        <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getStudentData->permanent_address2 ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Country</label>
                        <select name="permanent_country" id="permanent_country" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($getStudentData->permanent_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent State</label>
                        <select name="permanent_state" id="permanent_state" class="form-control" style="width: 405px;" disabled="disabled">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($getStudentData->permanent_state==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent City</label>
                        <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getStudentData->permanent_city ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Zipcode</label>
                        <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getStudentData->permanent_zipcode ?>" readonly>
                    </div>
                </div>
            </div>

        </div>


        

            <!-- <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                </div>
            </div> -->
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="visa">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Visa Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do You Hold a Malaysian Visa <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="malaysian_visa" id="visa1" value="Yes" 
                              <?php if(isset($visaDetails->malaysian_visa) && $visaDetails->malaysian_visa =='Yes')
                              {
                                 echo "checked=checked";
                              }
                              ;?> disabled>
                              <span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="malaysian_visa" id="visa2" value="No" 
                              <?php if(isset($visaDetails->malaysian_visa) && $visaDetails->malaysian_visa=='No') {
                                 echo "checked=checked";
                              };?> disabled>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Visa Expiry Date</label>
                            <input type="text" class="form-control datepicker" id="visa_expiry_date" name="visa_expiry_date" autocomplete="off" value="<?php
                            if(isset($visaDetails->visa_expiry_date))
                            {
                             echo date('d-m-Y', strtotime($visaDetails->visa_expiry_date));
                            }
                              ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Visa Status</label>
                            <input type="text" class="form-control" id="visa_status" name="visa_status" value="<?php
                            if(isset($visaDetails->visa_status))
                            {
                             echo $visaDetails->visa_status;
                             }
                              ?>" readonly>
                        </div>
                    </div>
                </div>

            </div>


           <!--  <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                </div>
            </div> -->
             
         </div> <!-- END col-12 -->  


        </div>

        <div role="tabpanel" class="tab-pane" id="other">
            <div class="col-12 mt-4">
                <br>

           

      <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>File</th>
            <th>Remarks</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($otherDocuments))
          {
            foreach ($otherDocuments as $record) {
          ?>
              <tr>
                <td><?php echo $record->doc_name ?></td>
                <td><?php echo $record->doc_file ?></td>
                <td><?php echo $record->remarks ?></td>
                <td class="text-center"><?php echo anchor('admission/student/delete_document?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="course">
            <div class="col-12 mt-4">
                <br>

            <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Intake</th>
            <th>Programme</th>
            <th>Student</th>
            <th>Course</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($courseRegistrationList))
          {
            foreach ($courseRegistrationList as $record) {
          ?>
              <tr>
                <td><?php echo $record->intake ?></td>
                <td><?php echo $record->program ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->course ?></td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>


      </div>
    </div>

   </div> <!-- END row-->
   

            <!-- <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div> -->
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    // $('select').select2();
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
  } );
  </script>