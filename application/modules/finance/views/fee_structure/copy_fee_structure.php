<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Copy Existing Fee Structure </h3>
            <a href="<?php echo '../../addFeeStructure/'. $getProgrammeLandscape->id; ?>" class="btn btn-link"> < Back</a>
        </div>


          <div class="form-container">
                <h4 class="form-group-title">Program Landscape Details</h4> 

            <div class="row">
                  

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme Landscape Name</label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $getProgrammeLandscape->name; ?>" readonly="readonly">
                      </div>
                  </div>     


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name . " - " . $programme->code; ?>" readonly="readonly">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Intake Year</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $intake->year; ?>" readonly="readonly">
                      </div>
                  </div>

            </div>

            <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Intake Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $intake->name; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Start Date</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo date("d-m-Y", strtotime($intake->start_date)); ?>" readonly="readonly" >
                      </div>
                  </div>

                  
            </div>

          </div>











        <br>



          <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Copy Fee Structure</a>
                    </li>              
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">








                        <form id="form_one" action="" method="post">

                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (Malaysian) Details</h4> 

                              <div class="row">

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Programme Landscape <span class='error-text'>*</span></label>
                                              <select name="id_program_landscape" id="id_program_landscape" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($programmeLandscapeList))
                                                  {
                                                      foreach ($programmeLandscapeList as $record)
                                                      {
                                                        if($record->id != $getProgrammeLandscape->id)
                                                        {

                                                        ?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                        }
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>

                                  </div>


                                  <div class="row">
                                
                                      <div class="col-sm-3">
                                          <button type="sub-tabs" class="btn btn-primary btn-lg form-row-btn">Add</button>
                                      </div>

                                  </div>

                            </div>


                        </form>



                        </div> 
                    </div>



                </div>

            </div>
        </div> 



           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
  


    $(document).ready(function() {
        $("#form_one").validate({
            rules: {
                id_program_landscape: {
                    required: true
                }
            },
            messages: {
                id_program_landscape: {
                    required: "<p class='error-text'>Select Programme Landscape</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
