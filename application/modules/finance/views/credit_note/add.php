<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Add Credit Note</h3>
        </div>

                
        




        <div class="form-container">
            <h4 class="form-group-title">Invoice Details For Credit Note Details</h4> 

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type<span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control" onchange="getStudentByProgramme()">
                            <option value="">Select</option>
                            <option value="Applicant">Applicant</option>
                            <option value="Student">Student</option>
                            <option value="Sponsor">Sponsor</option>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4" id="view_program">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getStudentByProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4" id="view_intake">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control" onchange="getStudentByProgramme()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

            

                <div class="col-sm-4" id="view_sponser" style="display: none;">
                    <div class="form-group">
                        <label>Sponsor <span class='error-text'>*</span></label>
                        <select name="id_sponser" id="id_sponser" class="form-control" onchange="getStudentBySponser()" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($sponserList))
                            {
                                foreach ($sponserList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                <span id='student'></span>

                <span id='view_student_details' style="display: none;"></span>




            </div>
            <div class="row">                                
            
                
                <!-- <div class="col-sm-4">
                    <div class="form-group"> -->
                        <!-- <label>Select Student <span class='error-text'>*</span></label> -->
                        
                   <!--  </div>
                </div>   --> 

            </div>
        </div>


        <div class="form-container">
            <h4 class="form-group-title"> Credit Note Details</h4> 

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="receipt_date" name="receipt_date" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Ratio <span class='error-text'>*</span></label>
                        <select name="ratio" id="ratio" class="form-control">
                            <option value="">Select</option>
                            <option value="Amount">Amount</option>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="amount" name="amount">
                    </div>
                </div>


            </div>



            <div class="row">

                <div class="col-sm-4" id="view_program">
                    <div class="form-group">
                        <label>ID Type <span class='error-text'>*</span></label>
                        <select name="id_type" id="id_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($creditNoteTypeList))
                            {
                                foreach ($creditNoteTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description </label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>


            </div>

        </div>

        

        <div id="view_details"  style="display: none;">
        </div>





        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>                    
        </div>

        
            

           </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>




</form>
<script>

    $('select').select2();

    // function getStudentByProgramme(id)
    // {

    //  $.get("/finance/receipt/getStudentByProgrammeId/"+id, function(data, status){
   
    //     $("#student").html(data);
    //     });
    // }

    function getStudentByProgramme()
    {
        if($("#type").val() != 'Sponsor')
        {
            $("#view_sponser").hide();
            $("#view_intake").show();
            $("#view_program").show();
            $("#student").hide();
            $("#view_student_details").hide();
            $("#view_details").hide();
            

            var tempPR = {};
            tempPR['id_program'] = $("#id_programme").val();
            tempPR['id_intake'] = $("#id_intake").val();
            tempPR['type'] = $("#type").val();
            // tempPR['id'] = $("#id").val();
            if(tempPR['id_program'] != '' && tempPR['id_intake'] != '' && tempPR['type'] != '')
            {
                $.ajax(
                {
                   url: '/finance/creditNote/getStudentByProgram',
                    type: 'POST',
                   data:
                   {
                    formData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    $("#student").html(result);
                    $("#student").show();
                   }
                });
            }   
        }
        else
        {
            $("#view_intake").hide();
            $("#view_program").hide();
            $("#view_sponser").show();
            $("#student").hide();
            $("#view_student_details").hide();
            $("#view_details").hide();


        }
    }

    function getStudentBySponser()
    {
        var tempPR = {};
        tempPR['id_sponser'] = $("#id_sponser").val();
        tempPR['type'] = $("#type").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_sponser'] != '' && tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/creditNote/getStudentBySponser',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#student").html(result);
                $("#student").show();
                $("#view_details").hide();
               }
            });
        }   
    }

    function getStudentByStudentId(id)
    {

     $.get("/finance/creditNote/getStudentByStudentId/"+id, function(data, status){

        $("#view_student_details").html(data);
        $("#view_student_details").show();
        });
    }

    function getApplicantByApplicantId(id)
     {

         $.get("/finance/creditNote/getApplicantByApplicantId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

     function getInvoiceByInvoiceId(id)
     {
        if(id != '')
        {

            $.get("/finance/creditNote/getInvoiceByInvoiceId/"+id, function(data, status){
           
                $("#view_details").html(data);
                $("#view_details").show();
            });
        }
     }

    function opendialog()
    {
        $("#id_main_invoice").val('');
        $("#invoice_amount").val('');
        $("#paid_amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/creditNote/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
                    var ita = $("#invoice_total_amount").val();
                    $("#receipt_amount").val(ita);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/creditNote/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }

    function payableAmountChane() {
         var tempAmount = {};
        tempAmount['payable_amount'] = $("#payable_amount").val();
        // alert(tempAmount['payable_amount']);

        $.ajax(
            {
               url: '/finance/creditNote/payableAmountAdd',
                type: 'POST',
               data:
               {
                tempAmount: tempAmount
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // var total_payable = $("#total_amount_payable").val();
                // alert(result);
                $("#receipt_amount").val(result);
               }
            });
        
    }


    function getStudentByStudentIdNSponser()
    {
        var tempPR = {};
        tempPR['id_student'] = $("#id_student").val();
        tempPR['id_sponser'] = $("#id_sponser").val();
        tempPR['type'] = $("#type").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_student'] != '' && tempPR['id_sponser'] != '' && tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/creditNote/getStudentByStudentIdNSponser',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_details").html(result);
                $("#view_student_details").show();
                
               }
            });
        }
        else
        {
            $("#view_student_details").hide();
        }
    }

 $(document).ready(function() {
        $("#form_receipt").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_student: {
                    required: true
                },
                type: {
                    required: true
                },
                id_sponser: {
                    required: true
                },
                ratio: {
                    required: true
                },
                amount: {
                    required: true
                },
                id_invoice: {
                    required: true
                },
                id_type : {
                    required: true
                }
            },
            messages: {
                id_programme:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student / Applicant</p>",
                },
                type:
                {
                    required: "<p class='error-text'>Select Credit Note Type</p>",
                },
                id_sponser:
                {
                    required: "<p class='error-text'>Select Sponsor</p>",
                },
                ratio:
                {
                    required: "<p class='error-text'>Select Ratio</p>",
                },
                amount:
                {
                    required: "<p class='error-text'>Credit Note Amount Required</p>",
                },
                id_invoice:
                {
                    required: "<p class='error-text'>Select Invoice</p>",
                },
                id_type:
                {
                    required: "<p class='error-text'>Select Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>


