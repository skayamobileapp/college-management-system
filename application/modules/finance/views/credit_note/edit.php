<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_creditNote" action="" method="post">
        <div class="page-title clearfix">
            <h3>View Credit Note</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>

            <div class="form-container">
                <h4 class="form-group-title">Credit Note Details</h4> 

      
              <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Reference Number</label>
                          <input type="text" class="form-control" id="creditNote_number" name="creditNote_number" readonly="readonly" value="<?php echo $creditNote->reference_number;?>" >
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Amount <span class='error-text'>*</span></label>
                          <input type="Amount" class="form-control" id="amount" name="amount" readonly="readonly" value="<?php echo number_format($creditNote->amount, 2, '.', ',');?>" >
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Ratio</label>
                          <input type="text" readonly="readonly" class="form-control" id="ratio" name="ratio" value="<?php echo $creditNote->ratio;?>">
                      </div>
                  </div>
              </div>

              <div class="row">



                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Date </label>
                          <input type="text" readonly="readonly" class="form-control" id="remarks" name="remarks" value="<?php echo date('d-m-Y', strtotime($creditNote->created_dt_tm));?>">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Status </label>
                          <input type="text" class="form-control" id="status" name="status" value="<?php 
                          if($creditNote->status == '0')
                            {
                                echo 'Pending';
                            }
                            elseif($creditNote->status == '1')
                            {
                                echo 'Approved';
                            }
                            elseif($creditNote->status == '2')
                            {
                                echo 'Rejected';
                            }?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Id Type <span class='error-text'>*</span></label>
                            <select name="id_type" id="id_type" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($creditNoteTypeList))
                                {
                                    foreach ($creditNoteTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_type)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                  </div>
                
              </div>


              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" readonly="readonly" class="form-control" id="remarks" name="remarks" value="<?php echo $creditNote->description;?>">
                    </div>
                </div>


                

              </div>


            </div>









            <?php
                if($creditNote->type == 'Applicant')
                {
                 ?>

              <div class="form-container">
                <h4 class="form-group-title">Credit Note Generated For</h4> 

      
              <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Type</label>
                          <input type="text" readonly="readonly" class="form-control" id="type" name="type" value="<?php echo $creditNote->type;?>">
                      </div>
                  </div>


                  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_program)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                  </div>



                  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_intake" id="id_intake" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_intake)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->year . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                  


              </div>

              <div class="row">

              <?php
                if($creditNote->type == 'Applicant')
                {
                 ?>






                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Applicant <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_student" id="id_student" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($applicantList))
                                {
                                    foreach ($applicantList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_student)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->nric . " - " . $record->full_name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


              <?php
                }
                ?>




                <?php
                if($creditNote->type == 'Student')
                {
                 ?>






                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Student <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_student" id="id_student" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_student)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->nric . " - " . $record->full_name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


              <?php
                }
                ?>





              </div>


                    

                <?php
                }
                ?>














          <?php
                if($creditNote->type == 'Sponsor')
                {
                 ?>

              <div class="form-container">
                <h4 class="form-group-title">Credit Note Generated For</h4> 

      
              <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Type</label>
                          <input type="text" readonly="readonly" class="form-control" id="type" name="type" value="<?php echo $creditNote->type;?>">
                      </div>
                  </div>


                  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sponsor <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_sponser" id="id_sponser" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($sponserList))
                                {
                                    foreach ($sponserList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_sponser)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>






                <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Student <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                            </a>
                            <select name="id_student" id="id_student" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $creditNote->id_student)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->nric . " - " . $record->full_name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> -->


                </div>


            </div>



                    <?php
                if($studentDetails)
                {
                 ?>


                   <div class="form-container">
                  <h4 class="form-group-title">Student Details</h4>
                  <div class='data-list'>
                      <div class='row'> 
                          <div class='col-sm-6'>
                              <dl>
                                  <dt>Student Name :</dt>
                                  <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                              </dl>
                              <dl>
                                  <dt>Student NRIC :</dt>
                                  <dd><?php echo date('d-m-Y', strtotime($studentDetails->nric));?></dd>
                              </dl>
                              <dl>
                                  <dt>Student Email :</dt>
                                  <dd><?php echo ucwords($studentDetails->email_id);?></dd>
                              </dl>
                              <dl>
                                  <dt>Program Scheme :</dt>
                                  <dd>$
                                    <?php echo $studentDetails->program_scheme;?>

                                  </dd>
                              </dl>                      
                          </div>        
                          
                          <div class='col-sm-6'>                           
                              <dl>
                                  <dt>Intake :</dt>
                                  <dd>
                                      <?php echo $studentDetails->intake_name;?>
                                  </dd>
                              </dl>
                              <dl>
                                  <dt>Program :</dt>
                                  <dd>
                                    <?php echo $studentDetails->programme_name;?>
                                  </dd>
                              </dl>
                              <dl>
                                  <dt>Advisor :</dt>
                                  <dd>
                                    <?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor_name;?>

                                  </dd>
                              </dl>
                          </div>
                      </div>
                  </div>
              </div>


              <br>



                   <?php
               }
               ?>






              


                    

                <?php
                }
                ?>


              








        <?php
                if($invoiceDetails)
                {
                 ?>




                 <div class="form-container">
                <h4 class="form-group-title">Invoice Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Invoice Number :</dt>
                                <dd><?php echo ucwords($invoiceDetails->invoice_number);?></dd>
                            </dl>
                            <dl>
                                <dt>Invoice Date :</dt>
                                <dd><?php echo date('d-m-Y', strtotime($invoiceDetails->date_time));?></dd>
                            </dl>
                            <dl>
                                <dt>Invoice Type :</dt>
                                <dd><?php echo ucwords($invoiceDetails->type);?></dd>
                            </dl>                       
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Invoice Total :</dt>
                                <dd>
                                    <?php echo $invoiceDetails->invoice_total;?>
                                </dd>
                            </dl>
                            <dl>
                                <dt>Payable Amount :</dt>
                                <dd>
                                  <?php echo $invoiceDetails->total_amount;?>
                                </dd>
                            </dl>
                            <dl>
                                <dt>Balance Amount</dt>
                                <dd>$
                                  <?php echo $invoiceDetails->balance_amount;?>

                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


            <br>



                 <?php
             }
             ?>




      

           </form>

    

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>
<script>
    $('select').select2();
</script>