<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Partner University Fee</h3>
      <a href="add" class="btn btn-primary">+ Add Partner Universty Fee</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Partner University </label>
                    <div class="col-sm-8">
                      <select name="id_partner_university" id="id_partner_university" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($partnerUniversityList)) {
                          foreach ($partnerUniversityList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_partner_university']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Fee Type </label>
                    <div class="col-sm-8">
                      <select name="fee_type" id="fee_type" class="form-control">
                        <option value="">Select</option>
                        <option value="Student Wise" <?php if($searchParam['fee_type']=='Student Wise'){ echo "selected"; } ?>>Student Wise</option>
                        <option value="Program Wise" <?php if($searchParam['fee_type']=='Program Wise'){ echo "selected"; } ?>>Program Wise</option>
                        <option value="Course Wise" <?php if($searchParam['fee_type']=='ACourse Wise'){ echo "selected"; } ?>>Course Wise</option>
                        
                      </select>
                    </div>
                  </div>
                </div> 

              </div>

                <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Program </label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Intake </label>
                    <div class="col-sm-8">
                      <select name="id_intake" id="id_intake" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($intakeList)) {
                          foreach ($intakeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_intake']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Partner University</th>
            <th>Program</th>
            <th>Intake</th>
            <th>Fee Type</th>
            <th>Amount</th>
            <th>status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($partnerFeeList)) {
            $i=1;
            foreach ($partnerFeeList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->university_code . " - " . $record->university_name;  ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name;  ?></td>
                <td><?php echo $record->intake ?></td>
                <td><?php echo $record->fee_type ?></td>
                <td><?php echo $record->amount ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
</script>