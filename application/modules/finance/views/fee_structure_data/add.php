<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Fee Structure</h3>
            </div>


    <form id="form_programme" action="" method="post">
        
    <div class="form-container">
        <h4 class="form-group-title">Fee Structure Details</h4>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debt Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="debt_account_code" name="debt_account_code">
                    </div>
                </div>

                

                
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

              

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Currency <span class='error-text'>*</span></label>
                        <select name="currency" id="currency" class="form-control">
                            <option value="">Select</option>
                            <option value="MYR">MYR</option>
                            <option value="USD">USD</option>
                        </select>
                    </div>
                </div>


            </div>

        </div>

    



    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>


</form>
           

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $('select').select2();

    function validateDetailsData()
    {
        if($('#form_programme').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam == '')
            {
                alert("Add Head Of Department to the Program");
            }
            else
            {
                $('#form_programme').submit();
            }
        }    
    }  




    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                debt_account_code: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                currency: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                debt_account_code: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                currency: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>