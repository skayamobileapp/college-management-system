<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Currency Rate Setup</h3>
        </div>
        <form id="form_currency" action="" method="post">




        <div class="form-container">
            <h4 class="form-group-title">Currency Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $currency->code; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $currency->code; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Default Language Name </label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $currency->name_optional_language; ?>" readonly>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Symbol Prefix <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="prefix" name="prefix" value="<?php echo $currency->prefix; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Symbol Syffix <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="suffix" name="suffix" value="<?php echo $currency->suffix; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Decimal Places <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="decimal_place" name="decimal_place" value="<?php echo $currency->decimal_place; ?>" readonly>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($currency->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($currency->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>


            </div>

        </div>



        <div class="form-container">
            <h4 class="form-group-title">Currency Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Exchange Rate <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="exchange_rate" name="exchange_rate">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min Rate <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_rate" name="min_rate">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max Rate <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_rate" name="max_rate">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="effective_date" name="effective_date">
                    </div>
                </div>

            </div>


        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>





         <?php

        if(!empty($currencyRateSetup))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Currency Rate Setup Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Exchange Rate</th>
                            <th>Min. Rate</th>
                            <th>Max. Rate</th>
                            <th>Effective Date</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($currencyRateSetup);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $currencyRateSetup[$i]->exchange_rate; ?></td>
                            <td><?php echo $currencyRateSetup[$i]->min_rate; ?></td>
                            <td><?php echo $currencyRateSetup[$i]->max_rate; ?></td>
                            <td><?php echo date('d-m-Y', strtotime($currencyRateSetup[$i]->effective_date)); ?></td>

                            <td class="text-center">
                            <a onclick="deleteRateSetup(<?php echo $currencyRateSetup[$i]->id; ?>)">Delete</a>
                            </td>
                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>







        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );



   $(document).ready(function() {
        $("#form_currency").validate({
            rules: {
                exchange_rate: {
                    required: true
                },
                min_rate: {
                    required: true
                },
                max_rate: {
                    required: true
                },
                effective_date: {
                    required: true
                }
            },
            messages: {
                exchange_rate: {
                    required: "<p class='error-text'>Exchange Rate Required</p>",
                },
                min_rate: {
                    required: "<p class='error-text'>Min. Rate Required</p>",
                },
                max_rate: {
                    required: "<p class='error-text'>Max. Rate Required</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Effective Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

   function deleteRateSetup(id)
      {
         $.ajax(
            {
               url: '/finance/currencyRateSetup/deleteRateSetup/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
      }


</script>
