<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Class_subject_tagging_model extends CI_Model
{
    function getClassSubjectTaggingList()
    {
        $this->db->select('*');
        $this->db->from('class_details');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getClassDetails($id)
    {
        $this->db->select('*');
        $this->db->from('class_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewClassSubjectTaggingDetails($insertData)
    {
        $this->db->trans_start();
        $this->db->insert('class_details', $insertData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editClassDetails($updateData, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('class_details', $updateData);
        return TRUE;
    }
}

