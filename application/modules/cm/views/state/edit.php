<?php
$stateId = $stateInfo->stateId;
$name = $stateInfo->name;
$idCountry = $stateInfo->idCountry;
?>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit State</h3>
        </div>
        <form id="form" action="/setup/state/editState" method="post">

        <div class="form-container">
            <h4 class="form-group-title">State Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $name; ?>">
                        <input type="hidden" value="<?php echo $stateId; ?>" name="stateId" id="stateId" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="idCountry" id="idCountry" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countries)) {
                                foreach ($countries as $record)
                                {?>
                                    <option value="<?php echo $record->countryId;  ?>"
                                        <?php 
                                        if($record->countryId == $idCountry)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($stateInfo->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($stateInfo->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                name: {
                    required: true
                },
                idCountry: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                idCountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>