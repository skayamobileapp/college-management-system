<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Class Subject Tagging</h3>
        </div>
        <form id="form_school" action="add" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Class Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Class Name *</label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList)) {
                                foreach ($stateList as $record)
                                    {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Class Teacher *</label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList)) {
                                foreach ($stateList as $record)
                                    {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>

        </form>
           

        <form id="form_school" action="addClassSubjectTaggingDetails" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Subject Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Subject Name *</label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList)) {
                                foreach ($stateList as $record)
                                    {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Hours Of Teaching *</label>
                        <input type="text" class="form-control" id="address_one" name="address_one" >
                    </div>
                </div>
            </div>

         </div>

        </form>

         <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_school").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                reg_no: {
                    required: true,
                    reg_no: true
                },
                phone_number: {
                    required: true,
                    phone_number: true
                },
                address_one: {
                    required: true,
                    address_one: true
                },
                address_two: {
                    required: true,
                    address_two: true
                },
                mobile_number: {
                    required: true,
                    mobile_number: true
                },
                id_state: {
                    required: true,
                    id_state: true
                },
                id_city: {
                    required: true,
                    id_city: true
                },
                pincode: {
                    required: true,
                    pincode: true
                }
            },
            messages:
            {
                name:
                {
                    required: "School Name Required",
                },
                reg_no:
                {
                    required: "Registration No. Required",
                },
                phone_number:
                {
                    required: "Phone No. Required",
                },
                address_one:
                {
                    required: "Address 1 Required",
                },
                address_two:
                {
                    required: "Address 2 Required",
                },
                mobile_number:
                {
                    required: "Mobile No. Required",
                },
                id_state:
                {
                    required: "State Required",
                },
                id_city:
                {
                    required: "City Required",
                },
                pincode:
                {
                    required: "Pincode Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>