<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Role</h3>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Role Edit</li>
                </ol>
            </div>
        </div>


        <form id="form_scheme" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Role Details</h4>

            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fname">Role</label>
                        <input type="text" class="form-control" id="fname" placeholder="Role Name" name="role" value="<?php echo $roleInfo->role; ?>" maxlength="128">
                        <input type="hidden" id="roleId" name="roleId" value="<?php echo $roleInfo->roleId;?>">
                        
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status *</p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($roleInfo->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($roleInfo->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>

            </div>


        <div class="form-container">


            <div class="clearfix">
                  
                <ul class="menu-tree">
                                        
                      <li>
                          <span class="icon folder collapsed" data-toggle="collapse" href="#menu99" aria-expanded="false" aria-controls="menu99"></span>
                          <div class="checkbox">
                              <label>
                                <input type="checkbox">
                                <span class="check-radio"></span>
                                User Management
                              </label>
                          </div> 
                            
                          <div class="collapse" id="menu99">
                              <ul>

                                  <?php for($i=0;$i<count($role);$i++) { ?>


                                    <li>
                                      <span class="icon folder collapsed" data-toggle="collapse" href="#menu<?php echo $role[$i]->id;?>" aria-expanded="false" aria-controls="menu0"></span>
                                      <div class="checkbox">
                                          <label>
                                            <input type="checkbox">
                                            <span class="check-radio"></span>
                                            <?php echo $role[$i]->menu_name;?>                                            
                                        </label>
                                      </div> 
                                        
                                      <div class="collapse" id="menu<?php echo $role[$i]->id;?>">
                                          <ul>
                                          <?php 
                                          $getrolemenu = $this->role_model->getUsermenu($role[$i]->id); 
                                         ?>

                                         <?php for($m=0;$m<count($getrolemenu);$m++)
                                         {
                                            $permissionGranted = $this->role_model->checkPermission($roleInfo->roleId,$getrolemenu[$m]->id);
                                               $checked = "";
                                            if($permissionGranted)
                                            {
                                               $checked="checked=checked";
                                            }?>
                                              <li>
                                                <span class="icon page"></span>
                                                  <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" name="checkrole[]" value="<?php echo $getrolemenu[$m]->id;?>" <?php echo $checked;?>>
                                                        <span class="check-radio"></span>
                                                        <?php echo $getrolemenu[$m]->description;?>
                                                      </label>
                                                  </div>                                         
                                              </li>
                                            <?php
                                          }
                                          ?> 
                                             
                                          </ul>
                                      </div>                              
                                  </li>

                                <?php }  ?> 
                                  
                                                              
                              </ul>
                          </div>                              
                      </li>
                  </ul>
              
            </div>


        </div>


      <!--   <div class="row">
                <br>


                <div class="col-sm-6">
                    <label for="fname">Corporate admin role navigation</label>
                    <div class="form-group">
                        <?php
                        if (!empty($permissions))
                        {
                            foreach ($permissions as $record)
                            {
                                $checked = '';
                                foreach ($rolepermissions as $rolepermission) {
                                    if ($record->permissionId == $rolepermission->permissionId) {
                                        $checked = 'checked';break;
                                    }
                                }

                        ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="<?php echo $record->code ?>" value="<?php echo $record->permissionId ?>" <?php echo $checked; ?> name="permissions[]">
                                    <label for="<?php echo $record->code ?>" class="custom-control-label"><?php echo $record->description ?></label>
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>

            </div>

        </div> -->



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_scheme").validate({
            rules: {
                description: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>