<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Asset Item</h3>
      <a href="add" class="btn btn-primary">+ Add Item</a>
    </div>

     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Code / Description</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParameters['name']; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Sub-Category Code</label>
                      <div class="col-sm-8">
                        <select name="id_asset_sub_category" id="id_asset_sub_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($assetSubCategoryList))
                            {
                                foreach ($assetSubCategoryList as $record)
                                {?>
                                   <option value="<?php echo $record->id; ?>"
                                        <?php 
                                        if($record->id == $searchParameters['id_asset_sub_category'])
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->description;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category Code</label>
                      <div class="col-sm-8">
                        <select name="id_asset_category" id="id_asset_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($assetCategoryList))
                            {
                                foreach ($assetCategoryList as $record)
                                {?>
                                   <option value="<?php echo $record->code; ?>"
                                        <?php 
                                        if($record->code == $searchParameters['id_asset_category'])
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->description;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Description</th>
            <th>Category</th>
            <th>Sub-Category</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($assetItemList))
          {
            $i=1;
            foreach ($assetItemList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->description ?></td>
                <td><?php echo $record->asset_category_code . " - " . $record->asset_category_description; ?></td>
                <td><?php echo $record->asset_sub_category_code . " - " . $record->asset_sub_category_description; ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
</script>