<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Account Statement</h3>
        </div>    
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd><?php echo ucwords($applicantDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $applicantDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Learning Mode :</dt>
                                <dd><?php echo $applicantDetails->program_scheme; ?></dd>
                            </dl>
                                                       
                        </div>
                        
                        <div class='col-sm-6'>  
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $applicantDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $applicantDetails->nationality ?></dd>
                            </dl> 
                            
                        </div>
                    </div>
                </div>
            </div>


    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
