<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Supervisor_model extends CI_Model
{
    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('scholarship_education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addNewChangeSupervisorApplication($data)
    {
        $this->db->trans_start();
        $this->db->insert('supervisor_change_application', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getSupervisorHistoryListByStudentId($id_student)
    {
        $this->db->select('st.*, rs.full_name as supervisor_name, rs.type as supervisor_type, u.name as created_by');
        $this->db->from('supervisor_tagging as st');
        $this->db->join('research_supervisor as rs','st.id_supervisor = rs.id');
        $this->db->join('users as u','st.created_by = u.id');
        $this->db->where('st.id_student', $id_student);
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(rd.topic  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getSupervisorChangeApplicationListByStudentId($id_student)
    {
        $this->db->select('st.*, rs.full_name as old_supervisor_name, rs.type as old_supervisor_type, rs1.full_name as new_supervisor_name, rs1.type as new_supervisor_type, u.name as updated_by');
        $this->db->from('supervisor_change_application as st');
        $this->db->join('research_supervisor as rs','st.id_supervisor_old = rs.id');
        $this->db->join('research_supervisor as rs1','st.id_supervisor_new = rs1.id','left');
        $this->db->join('users as u','st.updated_by = u.id','left');
        $this->db->where('st.id_student', $id_student);
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(rd.topic  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDeliverables($id)
    {
        $this->db->select('*');
        $this->db->from('research_deliverables_student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    
    function editDeliverables($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_deliverables_student', $data);
        return TRUE;
    }

    function generateDeliverableApplicationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('research_deliverables_student as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "DEL" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }
}

