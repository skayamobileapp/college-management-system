<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Project Report Submission</h3>
        </div>

        <form id="form_receipt" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Discount Plan :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>




       

        
        <div class="form-container">
            <h4 class="form-group-title">Project Report Submission Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" readonly="readonly" value="<?php echo $projectReportSubmission->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" readonly="readonly" value="<?php echo $projectReportSubmission->description;?>">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Duration (Months) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="duration" name="duration" readonly="readonly" value="<?php echo $projectReportSubmission->duration;?>">
                    </div>
                </div>
                

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($projectReportSubmission->from_dt));?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($projectReportSubmission->to_dt));?>">
                    </div>
                </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($projectReportSubmission->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($projectReportSubmission->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($projectReportSubmission->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">


                <?php
            if($projectReportSubmission->status == '2')
            {
             ?>

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $projectReportSubmission->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div>


        </div>


       </form>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


<script>
    $('select').select2();
</script>