<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Course Withdraw</h3>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->program ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

            </div>


    <form id="form_pr_entry" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Course Withdraw</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control" onchange="getCourseRegisteredBySemesterNStudentId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Course <span class='error-text'>*</span></label>
                     <span id="view_course_register">
                          <select class="form-control" id='id_course_registration' name='id_course_registration'>
                            <option value=''></option>
                          </select>

                     </span>
                  </div>
                </div>



                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_exam_register" id="id_exam_register" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseRegistrationList))
                            {
                                foreach ($courseRegistrationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->course_code . " - " . $record->course_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                </div>
              
                <div class="col-sm-4">
                    <button type="button" onclick="checkValidation()" class="btn btn-primary btn-lg form-row-btn">Add</button>
                </div>
            </div>

        </div>

            

        </form>






          <div class="form-container">
            <h4 class="form-group-title">Course Withdraw List</h4>

          <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>Course </th>
                     <th>Reason </th>
                     <th>Withdraw On </th>
                     <!-- <th class="text-center">Action </th> -->
                    </tr>
                </thead>
                <tbody>
                     <?php
                 $total = 0;
                  for($i=0;$i<count($bulkWithdrawList);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $bulkWithdrawList[$i]->course_code . " - " . $bulkWithdrawList[$i]->course_name;?></td>
                    <td><?php echo $bulkWithdrawList[$i]->reason;?></td>
                    <td><?php echo date('d-m-Y', strtotime($bulkWithdrawList[$i]->created_dt_tm));?></td>
                    <!-- <td class="text-center">
                        <?php $id = $bulkWithdrawList[$i]->id; ?>
                        <a class='btn btn-sm btn-edit' onclick='deleteCourseRegistration(<?php echo $id; ?>)'>Delete</a>
                    </td> -->
                  <?php 
                 
              }
              ?>
                </tbody>
            	</table>
        	</div>
        </div>


       


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();

    function getCourseRegisteredBySemesterNStudentId(id)
    {
        if(id != '')
        {

        $.get("/student/courseRegistration/getCourseRegisteredBySemesterNStudentId/"+id,
            function(data, status)
            {
                $("#view_course_register").html(data);
                // $("#view_branch").show();
            });
        }
        else
        {
            // var idstateselected = 0;
            // $("#view_course_register").html(data);
            //$("#id_course_registration").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //$('select').select2();
        }
    }



    function deleteCourseRegistration(id)
    {
        // alert(id);
        $.ajax(
        {
           url: '/student/courseRegistration/deleteCourseRegistration/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
            window.location.reload();
           }
        });
    }


    function checkValidation()
    {
        if($('#form_pr_entry').valid())
        {
            // $('#form_pr_entry').submit();

            var tempPR = {};

            tempPR['reason'] = $("#reason").val();
            tempPR['status'] = 1;
            tempPR['id_course_registration'] = $("#id_course_registration").val();
            tempPR['id_semester'] = $("#id_semester").val();

            // alert(tempPR['id_course_registration']);
            
            // if ($tempPR['id_tax'] != '' && $tempPR['quantity'] != '' && $tempPR['price'] != '')
            // {

                $.ajax(
                {
                   url: '/student/courseRegistration/addCourseWithdraw',
                   type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    alert('Course Withdraw Sucessfully');
                     window.location.reload();                   
                   }
                });

        }    
    }



  $(document).ready(function()
  {
        $("#form_pr_entry").validate({
            rules: {
                id_semester: {
                    required: true
                },
                id_course_registration: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_course_registration: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>