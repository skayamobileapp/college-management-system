<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Research TOC Reporting</h3>
        </div>




        <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>






        <form id="form_internship" action="" method="post" enctype="multipart/form-data">

        <div class="form-container">
            <h4 class="form-group-title">Research TOC Reporting Details</h4>


    

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload File <span class='error-text'>*</span></label>
                        <input type="file" class="form-control" id="upload_file" name="upload_file">
                    </div>
                </div>

            </div>



            <div class="row">


                <div class="col-sm-12">
                    <div class="form-group shadow-textarea">
                      <label for="message">Description <span class='error-text'>*</span></label>
                      <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"></textarea>
                    </div>
                </div>

            </div>



        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo 'list'; ?>" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>

<script>

    $('select').select2();


    $(function(){
        $( ".datepicker" ).datepicker();
      });



    CKEDITOR.replace('description',{

      width: "800px",
      height: "200px"

    });


    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                upload_file: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                upload_file: {
                    required: "<p class='error-text'>Select File To Upload</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Reuired</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>