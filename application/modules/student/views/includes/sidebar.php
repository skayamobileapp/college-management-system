            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="/student/profile" class="user-profile-link">
                        <span>
                            <img src="<?php echo BASE_PATH; ?>assets/img/default_profile.jpg">
                        </span> <?php echo $student_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/student/profile/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>

                <div class="sidebar-nav">                        
                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_one">Profile <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_one">                  
                        <li><a href="/student/editProfile/edit">Edit Profile</a></li>
                        <li><a href="/student/studentRecord/view">Record View</a></li>
                    </ul>

                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_two">Scholarship <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_two">                  
                        <li><a href="/student/scholarshipApplication/list">Scholarship</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_three">Finance <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_three">                  
                        <li><a href="/student/statementOfAccount/viewInvoice">Invoice</a></li>
                        <li><a href="/student/statementOfAccount/viewReceipt">Payment & Receipt</a></li>
                        <li><a href="/student/statementOfAccount/viewSummary">Account Summary</a></li>
                        <li><a href="/student/statementOfAccount/viewDiscount">Discount List</a></li>
                        <li><a href="/student/statementOfAccount/viewSponser">Sponser Info. If Any</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_four">Internship / Project <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_four">                  
                        <li><a href="/student/internshipApplication/list">Internship Application</a></li>
                        <li><a href="/student/projectReportSubmission/list">Project Report Submission</a></li>
                        <li><a href="/student/placement/list">Placement Details</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_five">Registration <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_five">                  
                        <li><a href="/student/courseRegistration/add">Course Registration</a></li>
                        <li><a href="/student/courseRegistration/courseWithdraw">Course Withdraw</a></li>
                        <li><a href="/student/applyChangeProgramme/list">Apply Change Program</a></li>
                        <li><a href="/student/applyChangeLearningMode/list">Apply Change Learning Mode</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_six">Examination <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_six">                  
                        <li><a href="/student/examination/timeTableSlip">Exam Timetable & Slip</a></li>
                        <li><a href="/student/examination/latestExamResults">Latest Exam Result</a></li>
                        <li><a href="/student/examination/partialTranscript">Partial Transcript</a></li>
                        <li><a href="/student/examination/resitApplication">Resit Application</a></li>
                        <li><a href="/student/examination/remarkingApplication">Remarking Application</a></li>
                    </ul>

                    <?php
                    if($student_education_level  == 'POSTGRADUATE' || $student_education_level  == 'MASTER')
                    {
                        ?>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_seven">Research <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_seven">                  
                        <li><a href="/student/supervisor/list">Apply Supervisor Change</a></li>
                        <li><a href="/student/colloquium/list">Apply Colloquium</a></li>
                    </ul>



                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_eight">Research Stage 1 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_eight">                  
                        <li><a href="/student/deliverables/list">Research Proposal</a></li>
                        <li><a href="/student/proposalReporting/list">Research Proposal Reporting</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_nine">Research Stage 2 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_nine">                  
                        <li><a href="/student/proposalReporting/list2">Listing of students and their research progress report (Chapter 3)</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_ten">Research Stage 3 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_ten">                  
                        <li><a href="/student/proposalReporting/list3">Listing of students and their research progress report (Chapter 4)</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_eleven">Stage 4 ( Reporting ) <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_eleven">   
                        <li><a href="/student/toc/list">Research TOC</a></li>
                        <li><a href="/student/abstractt/list">Research Abstract</a></li>
                        <li><a href="/student/bound/list">5 copies of hard bound submission</a></li>
                        <li><a href="/student/sco/list">Research Soft Copy Of File</a></li>
                        <li><a href="/student/ppt/list">Research Power Point Presentation</a></li>
                    </ul>


                     <?php
                    }
                    ?>




                </div>




                
            </div>

