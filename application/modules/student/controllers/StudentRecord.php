<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentRecord extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_record_model');
        $this->isStudentLoggedIn();
    }
    
    function view()
    {

        $id_student = $this->session->id_student;
        $id = $id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;
        $id_program_landscape = $this->session->id_program_landscape;

         

        $data['studentDetails'] = $this->student_record_model->getStudentByStudentId($id);

        // echo "<Pre>";print_r($data['studentDetails']);exit;
        
        // $data['getInvoiceByStudentId'] = $this->student_record_model->getInvoiceByStudentId($id,$data['studentDetails']->id_applicant);
        // $data['getReceiptByStudentId'] = $this->student_record_model->getReceiptByStudentId($id,$data['studentDetails']->id_applicant);
        // $data['creditNoteByStudentId'] = $this->student_record_model->getCreditNoteByStudentId($id);
        // $data['debitNoteByStudentId'] = $this->student_record_model->getDebitNoteByStudentId($id);


        // $data['barringByStudentId'] = $this->student_record_model->getBarringByStudentId($id);
        // $data['releaseByStudentId'] = $this->student_record_model->getReleaseByStudentId($id);
        $data['barrReleaseByStudentId'] = $this->student_record_model->barrReleaseByStudentId($id);
        $data['courseWithdrawByStudentId'] = $this->student_record_model->getCourseWithdrawByStudentId($id);
        $data['creditTransferByStudentId'] = $this->student_record_model->getCreditTransferByStudentId($id);

        $data['examDetails'] = $this->student_record_model->getExamDetails($id);
        $data['proficiencyDetails'] = $this->student_record_model->getProficiencyDetails($id);
        $data['employmentDetails'] = $this->student_record_model->getEmploymentDetails($id);
        $data['profileDetails'] = $this->student_record_model->getProfileDetails($id);
        $data['visaDetails'] = $this->student_record_model->getVisaDetails($id);
        $data['hostelRoomDetails'] = $this->student_record_model->getHostelRoomAllotmentDetails($id);
        $data['emergencyContactDetails'] = $this->student_record_model->emergencyContactDetails($id);
        $data['advisorTaggingDetails'] = $this->student_record_model->advisorTaggingDetails($id);
        $data['marksEntry'] = $this->student_record_model->studentSemesterResultRMarksEntry($id);

        $data['courseRegisteredLandscape'] = $this->student_record_model->getCourseRegisteredLandscape($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id,$id_program_landscape);
        $data['otherDocuments'] = $this->student_record_model->getOtherDocuments($id);

        // $data['courseCompletedDetails'] = $this->student_record_model->courseCompletedDetails($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);


        $data['organisationDetails'] = $this->student_record_model->getOrganisation();


        $data['courseRegisteredByStudentId'] = $this->student_record_model->getCourseRegisteredByStudentId($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);

        $data['landscapeCount'] = $this->student_record_model->getLandscapeCount($data['studentDetails']->id_program,$data['studentDetails']->id_intake);
        $data['landscapeRegisteredCount'] = $this->student_record_model->getLandscapeRegisteredCount($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);




        $data['studentStatus'] = $this->student_record_model->studentStatus($id);
        $data['courseRegisteredList'] = $this->student_record_model->courseRegisteredList($id);



        $program_landscape = $this->student_record_model->getProgramLandscape($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$data['studentDetails']->id_program_scheme,$data['studentDetails']->id_program_has_scheme);

        // echo "<Pre>";print_r($program_landscape);exit;

        if($program_landscape)
        {

        $id_program_landscape = $program_landscape->id;

        $data['courseRegisteredLandscapeFBySemester'] = $this->student_record_model->getCourseRegisteredLandscapeBySemesterForDisplay($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id_program_landscape,$id);

        }

        // echo "<Pre>";print_r($data['courseRegisteredList']);exit;

        $this->global['studentPageCode'] = 'student_record.list';
        $this->global['pageTitle'] = 'Campus Management System : View Student Account Statements';
        $this->loadViews("student_record/view", $this->global, $data, NULL);
    }

    function addStudentNote()
    {
        $user_id = $this->session->userId; 

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;

        if($tempData['note'] != '')
        {
            $tempData['created_by'] = $user_id;
            $inserted_id = $this->student_record_model->addStudentNote($tempData);
            $data = $this->displayStudentNote($tempData['id_student']);
        }else
        {
        // echo "<Pre>";print_r($tempData);exit;
            $data = $this->displayStudentNote($tempData['id_student']);
        }
        echo $data;exit();       
    }

    function displayStudentNote($id_student)
    {        
        $temp_details = $this->student_record_model->getStudentNote($id_student); 
        // echo "<Pre>";print_r($id_student);exit;
        if(!empty($temp_details))
        {

        $table = "
        <div class='row'>

                                <div class='col-sm-2'>
                                    <div class='form-group'>
                                    </div>
                                </div>


                                <div class='col-sm-8'>";




                           
    $table.= "

    <div class='form-container'>
        <h4 class='form-group-title'>Student Note Details</h4>

        <table  class='table' id='list-table'>
                    <tr>
                    <th>Sl. No</th>
                    <th class='text-center'>Note</th>
                    <th class='text-center'>Action</th>
                    </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $note = $temp_details[$i]->note;
                    $user = $temp_details[$i]->user;
                    $created_dt_tm = date('h:i:s A ( d-m-Y )', strtotime($temp_details[$i]->created_dt_tm));

                    $j = $i+1;
                    
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$note
                            <br>
                            <br>
                            <p style='text-align: left;'>
                            Created By - <b> $user </b>, Created On - <b> $created_dt_tm </b></p>
                            </td>                          
                            <td style='text-align: center;'>
                                <a onclick='deleteStudentNote($id)'>Delete</a>
                            </td>
                        </tr>";
                    }

                    $table .= "";

        $table.= "</table>

            </div>";

         $table.= "   
                                </div>


                                <div class='col-sm-2'>
                                    <div class='form-group'>
                                    </div>
                                </div>



                            </div>";


        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function deleteStudentNote()
    {
        // echo "<Pre>";  print_r($id);exit;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        $id_student = $tempData['id_student'];

        $inserted_id = $this->student_record_model->deleteStudentNote($id);
        $data = $this->displayStudentNote($id_student);

        echo $data;exit(); 
    }
}