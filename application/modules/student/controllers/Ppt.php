<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Ppt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ppt_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {       
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        
        $data['studentDetails'] = $this->ppt_model->getStudentByStudentId($id_student);
        $supervisor = $this->ppt_model->getSupervisor($data['studentDetails']->id_supervisor);

        if($supervisor)
        {
            $data['supervisor'] = $supervisor;
        }
        else
        {
            $data['supervisor'] = array();
        }

        $data['pptList'] = $this->ppt_model->getPptByStudentId($id_student);

        // echo "<Pre>";print_r($data['pptList']);exit();

        $this->global['studentPageCode'] = 'ppt.list';
        $this->global['pageTitle'] = 'Student Portal : List PPT';
        $this->loadViews("ppt/list", $this->global, $data, NULL);
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;
            $description = $this->security->xss_clean($this->input->post('description'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $student = $this->ppt_model->getStudent($id_student);


            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }
            

            $data = array(
                'id_student' => $id_student,
                'id_supervisor' => $student->id_supervisor,
                'description' => $description,
                'status' => 1
            );

            if($upload_file)
            {
                $data['upload_file'] = $upload_file;
            }
            // echo "<Pre>";print_r($check_limit);exit();
            $insert_id = $this->ppt_model->addPpt($data);
            redirect('/student/ppt/list');
        }

        $data['studentDetails'] = $this->ppt_model->getStudentByStudentId($id_student);

        $this->global['studentPageCode'] = 'ppt.add';
        $this->global['pageTitle'] = 'Student Portal : Add Deliverables Form';
        $this->loadViews("ppt/add", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/ppt/list');
        }
        $data['ppt'] = $this->ppt_model->getPpt($id);
        $data['supervisor'] = $this->ppt_model->getSupervisor($data['ppt']->id_supervisor);
        $data['studentDetails'] = $this->ppt_model->getStudentByStudentId($id_student);
        $data['pptCommentsDetails'] = $this->ppt_model->pptCommentsDetails($id);
            
        // echo "<Pre>"; print_r($data);exit;

        $this->global['studentPageCode'] = 'ppt.edit';
        $this->global['pageTitle'] = 'Student Portal : View Ppt Reporting';
        $this->loadViews("ppt/view", $this->global, $data, NULL);
    }
}