<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Deliverables extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('deliverables_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {       
        $id = $this->session->id_student;
        
        $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($id);
        $supervisor = $this->deliverables_model->getSupervisor($data['studentDetails']->id_supervisor);
        if($supervisor)
        {
            $data['supervisor'] = $supervisor;
        }
        else
        {
            $data['supervisor'] = array();
        }
        $data['deliverablesList'] = $this->deliverables_model->getDeliverablesListByStudentId($id);

        // echo "<Pre>";print_r($data['deliverablesList']);exit();

        $this->global['studentPageCode'] = 'deliverables.list';
        $this->global['pageTitle'] = 'Student Portal : List Deliverables Application';
        $this->loadViews("deliverables/list", $this->global, $data, NULL);
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;

            $id_phd_duration = $this->security->xss_clean($this->input->post('id_phd_duration'));
            $id_chapter = $this->security->xss_clean($this->input->post('id_chapter'));
            $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $student = $this->deliverables_model->getStudent($id_student);


            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }
            
            $generated_number = $this->deliverables_model->generateDeliverableApplicationNumber();

            $data = array(
                'application_number' => $generated_number,
                'id_student' => $id_student,
                'id_supervisor' => $student->id_supervisor,
                'id_phd_duration' => $id_phd_duration,
                'id_chapter' => $id_chapter,
                'id_topic' => $id_topic,
                'description' => $description,
                'stage' => 1,
                'status' => $status
            );


            if($upload_file)
            {
                $data['upload_file'] = $upload_file;
            }


            // $check_limit = $this->deliverables_model->checkStudentDeliverables($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Deliverables Application");exit();
            // }            
                 // echo "<Pre>";print_r($check_limit);exit();
            $insert_id = $this->deliverables_model->addNewDeliverables($data);
            redirect('/student/deliverables/list');
        }

        $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($id_student);
        $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
        $data['researchTopicList'] = $this->deliverables_model->researchTopicListByStatus('1');
        $data['researchStatusList'] = $this->deliverables_model->researchStatusListByStatus('1');

        $this->global['studentPageCode'] = 'deliverables.add';
        $this->global['pageTitle'] = 'Student Portal : Add Deliverables Form';
        $this->loadViews("deliverables/add", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/deliverablesApplication/list');
        }
        $data['deliverables'] = $this->deliverables_model->getDeliverables($id);
        $data['supervisor'] = $this->deliverables_model->getSupervisor($data['deliverables']->id_supervisor);
        $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($id_student);

        $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
        $data['chapterList'] = $this->deliverables_model->chapterListByStatus('1');
        $data['topicList'] = $this->deliverables_model->researchTopicListByStatus('1');
            
        // echo "<Pre>"; print_r($data);exit;

        $this->global['studentPageCode'] = 'deliverables.view';
        $this->global['pageTitle'] = 'Student Portal : View Deliverables Form';
        $this->loadViews("deliverables/view", $this->global, $data, NULL);
    }

    function getChapterByDuration($id_duration)
    {
        $results = $this->deliverables_model->getChapterByDuration($id_duration);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_chapter' id='id_chapter' class='form-control' onchange='getTopicByData()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getTopicByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->deliverables_model->getTopicByData($tempData);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_topic' id='id_topic' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->topic;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

