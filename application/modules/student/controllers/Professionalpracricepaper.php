<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Professionalpracricepaper extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('research_professionalpracricepaper_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;


            // echo "<Pre>";print_r($student_education_level);exit();
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_student'] = $id_student;
            $formData['status'] = '';



            $data['searchParam'] = $formData;
            $data['researchProfessionalpracricepaperList'] = $this->research_professionalpracricepaper_model->researchProfessionalpracricepaperListSearch($formData);


            // echo "<Pre>";print_r($data['researchProfessionalpracricepaperList']);exit();

            $this->global['studentPageCode'] = 'ppp.list';
            $this->global['pageTitle'] = 'Campus Management System : Professional Pracrice Paper List';
            $this->loadViews("research_professionalpracricepaper/list", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }
    
    function add()
    {
        $student_education_level = $this->session->student_education_level;

        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;


            if($this->input->post())
            {

                // $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $skill_enhancement = $this->security->xss_clean($this->input->post('skill_enhancement'));
                $other_details = $this->security->xss_clean($this->input->post('other_details'));
                $proposed_area_ppp = $this->security->xss_clean($this->input->post('proposed_area_ppp'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
            
                if($start_date)
                {
                    $start_date = date('Y-m-d', strtotime($start_date));
                }

                if($end_date)
                {
                    $end_date = date('Y-m-d', strtotime($end_date));
                }

                $data = array(
                    'id_student' => $id_student,
                    'skill_enhancement' => $skill_enhancement,
                    'other_details' => $other_details,
                    'proposed_area_ppp' => $proposed_area_ppp,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'status' => 0,
                    'created_by' => 0
                );

                $inserted_id = $this->research_professionalpracricepaper_model->addNewResearchProfessionalpracricepaper($data);

                if($inserted_id)
                {
                    $moved = $this->research_professionalpracricepaper_model->moveTempToDetails($inserted_id);
                }

                redirect('/student/professionalpracricepaper/list');
            }
            else
            {
                $this->research_professionalpracricepaper_model->deleteTempResearchProfessionalpracricepaperHasSupervisorBySessionId($id_student);
                $this->research_professionalpracricepaper_model->deleteTempResearchProfessionalpracricepaperHasExaminerBySessionId($id_student);
                $this->research_professionalpracricepaper_model->deleteTempResearchProfessionalpracricepaperHasEmploymentBySessionId($id_student);
            }

            $data['staffList'] = $this->research_professionalpracricepaper_model->staffListByStatus('1');
            $data['programList'] = $this->research_professionalpracricepaper_model->programListByStatus('1');

            $data['supervisorRoleList'] = $this->research_professionalpracricepaper_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_professionalpracricepaper_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_professionalpracricepaper_model->examinerRoleListByStatus('1');
            $data['studentDetails'] = $this->research_professionalpracricepaper_model->getStudentByStudentId($id_student);

            // echo "<Pre>";print_r($data['examinerList']);exit();

            $this->global['studentPageCode'] = 'ppp.add';
            $this->global['pageTitle'] = 'Campus Management System : Add Professional Pracrice Paper';
            $this->loadViews("research_professionalpracricepaper/add", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }


    function edit($id = NULL)
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            if ($id == null)
            {
                redirect('/student/professionalpracricepaper/list');
            }

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;

            if($this->input->post())
            {
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $skill_enhancement = $this->security->xss_clean($this->input->post('skill_enhancement'));
                $other_details = $this->security->xss_clean($this->input->post('other_details'));
                $proposed_area_ppp = $this->security->xss_clean($this->input->post('proposed_area_ppp'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
            
                if($start_date)
                {
                    $start_date = date('Y-m-d', strtotime($start_date));
                }

                if($end_date)
                {
                    $end_date = date('Y-m-d', strtotime($end_date));
                }

                $data = array(
                    'skill_enhancement' => $skill_enhancement,
                    'other_details' => $other_details,
                    'proposed_area_ppp' => $proposed_area_ppp,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'status' => 0,
                    'updated_by' => 0
                );

                $result = $this->research_professionalpracricepaper_model->editResearchProfessionalpracricepaperDetails($data,$id);
                redirect('/student/professionalpracricepaper/list');
            }


            $data['researchProfessionalpracricepaper'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaper($id);




            $data['researchProfessionalpracricepaperHasSupervisor'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasSupervisor($id);
            $data['researchProfessionalpracricepaperHasExaminer'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasExaminer($id);
            $data['researchProfessionalpracricepaperHasEmployment'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasEmployment($id);


            $data['studentDetails'] = $this->research_professionalpracricepaper_model->getStudentByStudentId($id_student);


            $data['staffList'] = $this->research_professionalpracricepaper_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_professionalpracricepaper_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_professionalpracricepaper_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->research_professionalpracricepaper_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_professionalpracricepaper_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_professionalpracricepaper_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchProfessionalpracricepaperHasExaminer']);exit;

            $this->global['studentPageCode'] = 'ppp.view';
            $this->global['pageTitle'] = 'Campus Management System : Edit Professional Pracrice Paper';
            $this->loadViews("research_professionalpracricepaper/edit", $this->global, $data, NULL);

        }
        else
        {
            $this->loadAccessRestricted();
        }
    }


    function view($id = NULL)
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            if ($id == null)
            {
                redirect('/student/professionalpracricepaper/list');
            }

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;


            if($this->input->post())
            {
                redirect('/student/professionalpracricepaper/approvalList');
            }


            $data['researchProfessionalpracricepaper'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaper($id);




            $data['researchProfessionalpracricepaperHasSupervisor'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasSupervisor($id);
            $data['researchProfessionalpracricepaperHasExaminer'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasExaminer($id);
            $data['researchProfessionalpracricepaperHasEmployment'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasEmployment($id);




            $data['studentDetails'] = $this->research_professionalpracricepaper_model->getStudentByStudentId($id_student);



            $data['staffList'] = $this->research_professionalpracricepaper_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_professionalpracricepaper_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_professionalpracricepaper_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->research_professionalpracricepaper_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_professionalpracricepaper_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_professionalpracricepaper_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchProfessionalpracricepaperHasExaminer']);exit;

            $this->global['studentPageCode'] = 'ppp.view';
            $this->global['pageTitle'] = 'Campus Management System : View Professional Pracrice Paper';
            $this->loadViews("research_professionalpracricepaper/view", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }


    function approvalList()
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_student'] = $id_student;
            $formData['status'] = '0';



            $data['searchParam'] = $formData;
            $data['researchProfessionalpracricepaperList'] = $this->research_professionalpracricepaper_model->researchProfessionalpracricepaperListSearch($formData);


            // echo "<Pre>";print_r($data['researchProfessionalpracricepaperList']);exit();

            $this->global['studentPageCode'] = 'ppp.approval_list';
            $this->global['pageTitle'] = 'Campus Management System : Professional Pracrice Paper Approval List';
            $this->loadViews("research_professionalpracricepaper/approval_list", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }



    function approve($id = NULL)
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            if ($id == null)
            {
                redirect('/student/professionalpracricepaper/list');
            }

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;

            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->research_professionalpracricepaper_model->editResearchProfessionalpracricepaperDetails($data,$id);
                redirect('/student/professionalpracricepaper/approvalList');
            }


            $data['researchProfessionalpracricepaper'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaper($id);




            $data['researchProfessionalpracricepaperHasSupervisor'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasSupervisor($id);
            $data['researchProfessionalpracricepaperHasExaminer'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasExaminer($id);
            $data['researchProfessionalpracricepaperHasEmployment'] = $this->research_professionalpracricepaper_model->getResearchProfessionalpracricepaperHasEmployment($id);




            $data['studentDetails'] = $this->research_professionalpracricepaper_model->getStudentByStudentId($id_student);



            $data['staffList'] = $this->research_professionalpracricepaper_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_professionalpracricepaper_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_professionalpracricepaper_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->research_professionalpracricepaper_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_professionalpracricepaper_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_professionalpracricepaper_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchProfessionalpracricepaperHasExaminer']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Approve Professional Pracrice Paper';
            $this->loadViews("research_professionalpracricepaper/approve", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }


    function getIntakeByProgramme($id_programme)
    {
        $intake_data = $this->research_professionalpracricepaper_model->getIntakeByProgrammeId($id_programme);

        // echo "<Pre>"; print_r($intake_data);exit;

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByData()'>
        <option value=''>Select</option>
        ";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $year = $intake_data[$i]->year;

        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id_programme = $tempData['id_programme'];
        // $id_program_scheme = $tempData['id_program_scheme'];
        // $id_intake = $tempData['id_intake'];

        $student_data = $this->research_professionalpracricepaper_model->getStudentByData($tempData);

        // echo "<Pre>";print_r($student_data);exit();

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>
        <option value=''>Select</option>";

        for($i=0;$i<count($student_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_data[$i]->id;
        $full_name = $student_data[$i]->full_name;
        $nric = $student_data[$i]->nric;

        $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->research_professionalpracricepaper_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $nationality = $student_data->nationality;

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';
            }elseif($nationality == 'Other')
            {
                $currency = 'USD';
            }


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' />
                                    <input type='hidden' name='currency' id='currency' value='$currency' />
                                    $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";

                
            echo $table;
            exit;
    }




    function tempAddResearchProfessionalpracricepaperHasSupervisor()
    {
        $id_student = $this->session->id_student;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_student;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_professionalpracricepaper_model->tempAddResearchProfessionalpracricepaperHasSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempSupervisordata();
        
        echo $data;        
    }

    function displayTempSupervisordata()
    {
        $id_student = $this->session->id_student;
        
        $temp_details = $this->research_professionalpracricepaper_model->getTempResearchProfessionalpracricepaperHasSupervisorBySession($id_student); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Supervisor</th>
                    <th>Supervisor Role</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $supervisor_role = $temp_details[$i]->supervisor_role;
                    $status = $temp_details[$i]->status;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $staff_name</td>                         
                            <td>$supervisor_role</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchProfessionalpracricepaperHasSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProfessionalpracricepaperHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchProfessionalpracricepaperHasSupervisor($id)
    {
        $inserted_id = $this->research_professionalpracricepaper_model->deleteTempResearchProfessionalpracricepaperHasSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displayTempSupervisordata();
            echo $data;  
        }
    }



    function tempAddResearchProfessionalpracricepaperHasEmployment()
    {
        $id_student = $this->session->id_student;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_student;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_professionalpracricepaper_model->tempAddResearchProfessionalpracricepaperHasEmployment($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempEmploymentdata();
        
        echo $data;        
    }

    function displayTempEmploymentdata()
    {
        $id_student = $this->session->id_student;
        
        $temp_details = $this->research_professionalpracricepaper_model->getTempResearchProfessionalpracricepaperHasEmploymentBySession($id_student); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Company Name</th>
                    <th>Address</th>
                    <th>Position</th>
                    <th>Job Function</th>
                    <th>Year</th>
                    <th>Reference Number</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {

                    $id = $temp_details[$i]->id;
                    $company_name = $temp_details[$i]->company_name;
                    $address = $temp_details[$i]->address;
                    $position = $temp_details[$i]->position;
                    $job_function = $temp_details[$i]->job_function;
                    $from_year = $temp_details[$i]->from_year;
                    $to_year = $temp_details[$i]->to_year;
                    $reference_number = $temp_details[$i]->reference_number;
                    $reference_address   = $temp_details[$i]->reference_address  ;
                    $status = $temp_details[$i]->status;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$company_name</td>
                            <td>$address</td>
                            <td>$position</td>
                            <td>$job_function</td>
                            <td>$from_year - $to_year</td>
                            <td>$reference_number</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchProfessionalpracricepaperHasEmployment($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProfessionalpracricepaperHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchProfessionalpracricepaperHasEmployment($id)
    {
        $inserted_id = $this->research_professionalpracricepaper_model->deleteTempResearchProfessionalpracricepaperHasEmployment($id);
        if($inserted_id)
        {
            $data = $this->displayTempEmploymentdata();
            echo $data;  
        }
    }



    function tempAddResearchProfessionalpracricepaperHasExaminer()
    {
        $id_student = $this->session->id_student;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_student;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_professionalpracricepaper_model->tempAddResearchProfessionalpracricepaperHasExaminer($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempExaminerdata();
        
        echo $data;        
    }

    function displayTempExaminerdata()
    {
        $id_student = $this->session->id_student;
        
        $temp_details = $this->research_professionalpracricepaper_model->getTempResearchProfessionalpracricepaperHasExaminerBySession($id_student); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Type</th>
                    <th>Examiner</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>";


                    // <th>Status</th>
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $type = $temp_details[$i]->type;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $full_name = $temp_details[$i]->full_name;
                    $status = $temp_details[$i]->status;
                    $examiner_role = $temp_details[$i]->examiner_role;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }


                    if($type == 1)
                    {
                        $type = 'Internal';
                        $staff = $ic_no . " - " . $staff_name;
                    }else
                    {
                        $type = 'External';
                        $staff = $full_name;
                    }

                    $j = $i+1;
                            // <td>$status</td>

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$type</td>
                            <td>$staff</td>                         
                            <td>$examiner_role</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchProfessionalpracricepaperHasExaminer($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProfessionalpracricepaperHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchProfessionalpracricepaperHasExaminer($id)
    {
        $inserted_id = $this->research_professionalpracricepaper_model->deleteTempResearchProfessionalpracricepaperHasExaminer($id);
        if($inserted_id)
        {
            $data = $this->displayTempExaminerdata();
            echo $data;  
        }
    }




    function addResearchProfessionalpracricepaperHasSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_professionalpracricepaper_model->addNewResearchProfessionalpracricepaperHasSupervisors($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchProfessionalpracricepaperHasSupervisor($id)
    {
        $inserted_id = $this->research_professionalpracricepaper_model->deleteResearchProfessionalpracricepaperHasSupervisor($id);
        echo "Success"; 
    }





    function addResearchProfessionalpracricepaperHasExaminer()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_professionalpracricepaper_model->addNewResearchProfessionalpracricepaperHasExaminer($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchProfessionalpracricepaperHasExaminer($id)
    {
        $inserted_id = $this->research_professionalpracricepaper_model->deleteResearchProfessionalpracricepaperHasExaminer($id);
        echo "Success"; 
    }




    function addResearchProfessionalpracricepaperHasEmployment()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_professionalpracricepaper_model->addNewResearchProfessionalpracricepaperHasEmployment($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchProfessionalpracricepaperHasEmployment($id)
    {
        $inserted_id = $this->research_professionalpracricepaper_model->deleteResearchProfessionalpracricepaperHasEmployment($id);
        echo "Success"; 
    }
}
