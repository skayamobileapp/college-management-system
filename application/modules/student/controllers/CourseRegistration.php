<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseRegistration extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('course_registration_model');
        $this->load->model('edit_profile_model');
        $this->isStudentLoggedIn();
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;
        $id_program_landscape = $this->session->id_program_landscape;
        $student_semester = $this->session->student_semester;


        // echo "<Pre>";print_r($id_program_scheme);exit();



        if($this->input->post())
        {
            // echo "<Pre>";print_r($this->input->post());exit();
            

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;

            $id_course_registered_landscapes = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));
            $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
     
            // echo "<Pre>"; print_r($tempData);exit();

            $master_data = array(
                'id_intake' => $id_intake,
                'id_semester' => $id_semester,
                'id_programme' => $id_program,
                // 'id_course_registered_landscape' => $id_course_registered_landscape,
                'id_student' => $id_student,
                // 'id_course' => $landscape_course_details->id_course,
                'created_by' => $id_student
            );

                // echo "<Pre>"; print_r($master_data);exit();
            $id_course_register = $this->course_registration_model->addCoureRegister($master_data);

                // echo "<Pre>"; print_r($tempData);exit();


            if($id_course_register)
            {
                $details = array();
                
                foreach ($id_course_registered_landscapes as $id_course_registered_landscape)
                {

                    $landscape_course_details = $this->course_registration_model->getCoursesIdByLandscapeIdForDetailsAdd($id_course_registered_landscape);

                    $student_data = $this->course_registration_model->getStudent($id_student);

                    $data = array(
                            'id_course_register' => $id_course_register,
                            'id_course_registered_landscape' => $id_course_registered_landscape,
                            'id_course' => $landscape_course_details->id_course,
                            'id_intake' => $id_intake,
                            'student_current_semester' => $student_data->current_semester,
                            'id_semester' => $id_semester,
                            'id_programme' => $id_program,
                            'id_student' => $id_student,
                            'by_student' => $id_student
                        );

                    $result = $this->course_registration_model->addCoureRegistration($data);



                    $detail['id_course'] = $landscape_course_details->id_course;
                    $detail['id_course_registered'] = $id_course_register;

                    array_push($details, $detail);
                }

                $student = $this->course_registration_model->getStudent($id_student);
                $id_university = $student->id_university;
                $partner_university = $this->course_registration_model->getPartnerUniversity($id_university);



                if($id_university == 1)
                {
                    $invoice_generated = $this->course_registration_model->generateNewMainInvoiceForCourseRegistration($details,$id_student,$id_semester);
                }
                elseif($partner_university->billing_to == 'Student')
                {
                        // $detail['id_course'] = $landscape_course_details->id_course;
                        // $detail['id_course_registered'] = $insert_id;

                     // echo "<Pre>";print_r($details);exit();
                     
                    // Hided Generate Main Invoice oN Course Registration 
                        $invoice_generated = $this->course_registration_model->generateNewMainInvoiceForCourseRegistration($details,$id_student,$id_semester);
                }
            }
            redirect($_SERVER['HTTP_REFERER']);
        }

                 // echo "<Pre>";print_r($id_program_scheme);exit();
        // $data['courseList'] = $this->course_registration_model->getPerogramLandscape($id_intake,$id_program);
        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        $data['courseRegisteredList'] = $this->course_registration_model->getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification,$id_program_scheme);
        $data['semesterList'] = $this->course_registration_model->semesterListByStatus('1');
        $data['courseList'] = $this->course_registration_model->getPerogramLandscape($id_intake,$id_program,$id_program_scheme,$id_program_landscape,$data['getStudentData']->current_semester);


        // if(!empty($data['courseRegisteredList']))
        // {
        //     $$data['courseRegisteredList']->
        // }

        // echo "<Pre>";print_r($data['courseRegisteredList']);exit();

        $this->global['studentPageCode'] = 'course_registration.add';
        $this->global['pageTitle'] = 'Student Portal : Add Course Registration';
        $this->loadViews("course_registration/add", $this->global, $data, NULL);
    }

     function deleteCourseRegistration($id_course_registration)
    {
        $student_list_data = $this->course_registration_model->deleteCourseRegistration($id_course_registration);
        echo $id_course_registration;
        exit();
    }

    function courseWithdraw()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_programme = $this->session->id_program;
        $id_program_landscape = $this->session->id_program_landscape;
        $id_program_scheme = $this->session->id_program_scheme;
        $user_id = $this->session->userId;


        // echo $id_student;exit();

        // if($this->input->post())
        // {
        //     // $id_course = $this->security->xss_clean($this->input->post('id_course'));
        //     $reason = $this->security->xss_clean($this->input->post('reason'));
        //     $id_exam_register = $this->security->xss_clean($this->input->post('id_exam_register'));
        //     // $reason = $this->security->xss_clean($this->input->post('reason'));

        //         $exam_registration_data = $this->course_registration_model->getExamRegistration($id_exam_register);
        //     // echo "<Pre>";print_r($exam_registration_data);exit();

        //          $data = array(
        //                 'id_exam_register' => $id_exam_register,
        //                 'id_exam_center' => $exam_registration_data->id_exam_center,
        //                 'id_course' => $exam_registration_data->id_course,
        //                 'id_semester' => $exam_registration_data->id_semester,
        //                 'id_student' => $exam_registration_data->id_student,
        //                 'id_intake' => $exam_registration_data->id_intake,
        //                 'id_programme' => $exam_registration_data->id_programme,
        //                 'reason' => $reason,
        //                 'status' => '0',
        //                 'created_by' => $user_id
        //             );
        //              // echo "<Pre>";print_r($data);exit();
        //         $insert_id = $this->course_registration_model->addBulkWithdraw($data);
        //         if ($insert_id)
        //         {
        //             $update = array(
        //                 'is_bulk_withdraw' => $insert_id
        //             );
        //             $updated = $this->course_registration_model->updateExamRegistration($update,$id_exam_register);                       
        //         }
        //     redirect($_SERVER['HTTP_REFERER']);
        // }

        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        // $data['courseRegistrationList'] = $this->course_registration_model->getCourseFromExamRegister($id_intake,$id_programme,$id_student);
        // $data['courseRegistrationList'] = $this->course_registration_model->getCourseRegisteredByStudentIdForWithdraw($id_intake,$id_programme,$id_student);
        $data['bulkWithdrawList'] = $this->course_registration_model->bulkWithdrawList($id_student);
        $data['semesterList'] = $this->course_registration_model->semesterListByIdStudentFromCourseRegister($id_student);
        
        // echo "<Pre>";print_r($data['bulkWithdrawList']);exit();

        $this->global['pageTitle'] = 'Campus Management System : Add Student To Exam Center';
        $this->loadViews("course_registration/bulk_withdraw", $this->global, $data, NULL);
    }

    function addCourseRegistration()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
 
        echo "<Pre>"; print_r($tempData);exit();

        $master_data = array(
           'id_intake' => $id_intake,
           'id_semester' => $tempData['id_semester'],
            'id_programme' => $id_program,
            // 'id_course_registered_landscape' => $id_course_registered_landscape,
            'id_student' => $id_student,
            // 'id_course' => $landscape_course_details->id_course,
            'created_by' => $id_student
        );

        $id_course_register = $this->course_registration_model->addCoureRegister($master_data);

            // echo "<Pre>"; print_r($tempData);exit();

        if($id_course_register)
        {
            foreach ($tempData['id_course_registered_landscape'] as $id_course_registered_landscape)
            {

                $landscape_course_details = $this->course_registration_model->getCoursesIdByLandscapeIdForDetailsAdd($id_course_registered_landscape);

                 $data = array(
                        'id_course_register' => $id_course_register,
                        'id_course_registered_landscape' => $id_course_registered_landscape,
                        'id_course' => $landscape_course_details->id_course,
                        'id_intake' => $id_intake,
                        'student_current_semester' => $landscape_course_details->id_semester,
                        'id_semester' => $tempData['id_semester'],
                        'id_programme' => $id_program,
                        'id_student' => $id_student,
                        'by_student' => $id_student
                    );

                    $result = $this->course_registration_model->addCoureRegistration($data);
            }
        }
    }

    function addCourseWithdraw()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $id_course_registration = $tempData['id_course_registration'];
        $reason = $tempData['reason'];


        $tempData['status'] = 1;
        $tempData['id_intake'] = $id_intake;
        $tempData['id_programme'] = $id_program;
        $tempData['id_course'] = $id_intake;

        $course_registration = $this->course_registration_model->getCourseRegistration($id_course_registration);

            
        // echo "<Pre>"; print_r($course_registation);exit();
        
        if($course_registration)
        {
            $id_course_registered_landscape = $course_registration->id_course_registered_landscape;

            $course_landscape = $this->course_registration_model->getCourseRegisteredLandscape($id_course_registered_landscape);


            // echo "<Pre>"; print_r($course_landscape);exit();


            $master_data = array(
                'id_programme' => $id_program,
                'id_intake' => $id_intake,
                'id_course' => $course_landscape->id,
                'id_course_registered_landscape' => $id_course_registered_landscape,
                'reason' => $reason,
                'status' => 1,
                'created_by' => 0
            );

                    
            $inserted_master_id = $this->course_registration_model->addBulkWithdrawMaster($master_data);
                
            // echo "<Pre>";print_r($inserted_master_id);exit();

            if($inserted_master_id)
            {
                    // $course_registration = $this->course_registration_model->getCourseRegistration($id_course_registration);

                    
                // echo "<Pre>";print_r($course_registration);exit();

                     $data = array(
                    'id_course_registered' => $id_course_registration,
                    'id_exam_register' => 0,
                    'id_bulk_withdraw_master' => $inserted_master_id,
                    // 'id_exam_center' => $course_registration->id_exam_event,
                    'id_course' => $course_registration->id_course,
                    'id_course_registered_landscape' => $course_registration->id_course_registered_landscape,
                    'id_semester' => $course_registration->id_semester,
                    'id_student' => $course_registration->id_student,
                    'id_intake' => $course_registration->id_intake,
                    'id_programme' => $course_registration->id_programme,
                    'reason' => $reason,
                    'status' => 1,
                    'created_by' => 0
                );
                
                // echo "<Pre>";print_r($data);exit();
                
                $insert_id = $this->course_registration_model->addBulkWithdraw($data);

                if ($insert_id)
                {
                    $update = array(
                        'is_bulk_withdraw' => $insert_id
                    );
                    // $updated = $this->bulk_withdraw_model->updateExamRegistration($update,$exam_registration_data->id);                       
                    $updated = $this->course_registration_model->updateCourseRegistration($update,$course_registration->id); 

                    // $check_apply_status = $this->course_registration_model->getFeeStructureActivityType('COURSE WITHDRAW','Application Level',$course_registration->id_programme);
                    // if($check_apply_status)
                    // {
                    //     $data['add'] = 1;
                    //     $this->bulk_withdraw_model->generateMainInvoice($data,$insert_id);
                    // }

                echo "Course Withdraw Sucessful";exit();

                }
            }


        }

    }

    function getCourseRegisteredBySemesterNStudentId($id_semester)
    {
        $id_student = $this->session->id_student;

        $results = $this->course_registration_model->getCourseRegisteredBySemesterNStudentId($id_student,$id_semester);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
            $('select').select2();
             </script>
     ";

        $table.="
        <select name='id_course_registration' id='id_course_registration' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->course_name;
        $code = $results[$i]->course_code;
        $table.="<option value=".$id.">" . $code .  " - ".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }
}

