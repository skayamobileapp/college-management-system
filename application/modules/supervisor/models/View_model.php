<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class View_model extends CI_Model
{

    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentListByIdSupervisor($id_supervisor)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_supervisor', $id_supervisor);
        $query = $this->db->get();
        $result = $query->result();
         return $result;
    }
}