<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Bound extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bound_model');
        $this->isSupervisorLoggedIn();
    }

    function list()
    {       
        $id_supervisor = $this->session->id_supervisor;
        $data['supervisor'] = $this->bound_model->getSupervisor($id_supervisor);
        $data['boundList'] = $this->bound_model->getBoundListBySupervisorId($id_supervisor);

        // echo "<Pre>";print_r($data['boundList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Bound Reporting';
        $this->loadViews("bound/list", $this->global, $data, NULL);
    }
    
    function edit($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/supervisor/bound/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_bound' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }
            $added_comments = $this->bound_model->addBoundReportingComments($comments_data);
            
            redirect($_SERVER['HTTP_REFERER']);
         }

        $data['bound'] = $this->bound_model->getBound($id);
        $data['boundReportingComments'] = $this->bound_model->boundCommentsDetails($id);

        $data['studentDetails'] = $this->bound_model->getStudentByStudentId($data['bound']->id_student);
        $data['organisationDetails'] = $this->bound_model->getOrganisation();

            
        // echo "<Pre>"; print_r($data['researchStatusList']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Bound Reporting';
        $this->loadViews("bound/edit", $this->global, $data, NULL);
    }

    function view($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/supervisor/bound/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_bound' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }
            $added_comments = $this->bound_model->addBoundReportingComments($comments_data);
            
            redirect($_SERVER['HTTP_REFERER']);
         }


        // $data['studentDetails'] = $this->bound_model->getStudentByStudentId($id_student);
        $data['bound'] = $this->bound_model->getBound($id);
        $data['boundReportingComments'] = $this->bound_model->boundCommentsDetails($id);

        $data['studentDetails'] = $this->bound_model->getStudentByStudentId($data['bound']->id_student);
        $data['organisationDetails'] = $this->bound_model->getOrganisation();

        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Bound Reporting';
        $this->loadViews("bound/edit", $this->global, $data, NULL);
    }
}