<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StatementOfAccount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('statement_of_account_model');
        $this->isStudentLoggedIn();
    }

    function viewInvoice()
    {       
        $id = $this->session->id_student;
        
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id,$data['studentDetails']->id_applicant);
        $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id,$data['studentDetails']->id_applicant);
        // echo "<Pre>";print_r($data['getInvoiceByStudentId']);exit();

        $this->global['pageTitle'] = 'Student Portal : View Student Invoice';
        $this->loadViews("statement_of_account/view_invoice", $this->global, $data, NULL);
    }

    function viewReceipt()
    {       
        $id = $this->session->id_student;
        
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id,$data['studentDetails']->id_applicant);
        $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id,$data['studentDetails']->id_applicant);
        // echo "<Pre>";print_r($data['getReceiptByStudentId']);exit();

        $this->global['pageTitle'] = 'Student Portal : View Student Receipt';
        $this->loadViews("statement_of_account/view_receipt", $this->global, $data, NULL);
    }

    function viewSummary()
    {       
        $id = $this->session->id_student;
        
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id,$data['studentDetails']->id_applicant);
        $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id,$data['studentDetails']->id_applicant);

        $this->global['pageTitle'] = 'Student Portal : View Student Statements Summary';
        $this->loadViews("statement_of_account/view_summary", $this->global, $data, NULL);
    }

    function viewDiscount()
    {
        $id = $this->session->id_student;
        
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id,$data['studentDetails']->id_applicant);
        $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id,$data['studentDetails']->id_applicant);
        $data['studentDiscountList'] = $this->statement_of_account_model->getStudentDiscountList($id,$data['studentDetails']->id_applicant);
        
        // echo "<Pre>";print_r($data);exit();
        $this->global['pageTitle'] = 'Student Portal : View Discounts';
        $this->loadViews("statement_of_account/view_discount", $this->global, $data, NULL);
    }

    function viewSponser()
    {
        $id = $this->session->id_student;
        
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        // $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id);
        // $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id);
        $data['getSponserList'] = $this->statement_of_account_model->getStudentSponserList($id);

        // echo "<Pre>";print_r($data);exit();
        
        $this->global['pageTitle'] = 'Student Portal : View Sponser List';
        $this->loadViews("statement_of_account/view_sponser", $this->global, $data, NULL);
    }

    function showInvoice($id_invoice,$route)
    {
        $id = $this->session->id_student;
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        $data['mainInvoice'] = $this->statement_of_account_model->getMainInvoice($id_invoice);
        $data['mainInvoiceDetailsList'] = $this->statement_of_account_model->getMainInvoiceDetails($id_invoice);
        $data['mainInvoiceDiscountDetailsList'] = $this->statement_of_account_model->getMainInvoiceDiscountDetails($id_invoice);
        if($data['mainInvoice']->type == 'Applicant')
        {
            $data['invoiceFor'] = $this->statement_of_account_model->getMainInvoiceApplicantData($data['mainInvoice']->id_student);
        }elseif($data['mainInvoice']->type == 'Student')
        {
            $data['invoiceFor'] = $this->statement_of_account_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
        }
        $data['degreeTypeList'] = $this->statement_of_account_model->qualificationList();

        $data['route'] = $route;
        // echo "<Pre>";  print_r($data['route']);exit;
        // echo "<Pre>";  print_r($data['mainInvoiceDetailsList']);exit;

        $this->global['pageTitle'] = 'Student Portal : View Main Invoice';
        $this->loadViews("statement_of_account/show_invoice", $this->global, $data, NULL);
    }

    function showReceipt($id_receipt,$route)
    {
        $id = $this->session->id_student;
        $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);
        $data['receipt'] = $this->statement_of_account_model->getReceipt($id_receipt);
        if($data['receipt']->type == 'Applicant')
        {
            $data['receiptFor'] = $this->statement_of_account_model->getMainInvoiceApplicantData($data['receipt']->id_student);
        }elseif($data['receipt']->type == 'Student')
        {
            $data['receiptFor'] = $this->statement_of_account_model->getMainInvoiceStudentData($data['receipt']->id_student);
        }
            
        $data['invoiceDetails'] = $this->statement_of_account_model->getReceiptInvoiceDetails($id_receipt);
        $data['paymentDetails'] = $this->statement_of_account_model->getReceiptPaymentDetails($id_receipt);
        $data['route'] = $route;
        // echo "<Pre>"; print_r($data['invoiceDetails']);exit;

        $this->global['pageTitle'] = 'Student Portal : View Receipt';
        $this->loadViews("statement_of_account/show_receipt", $this->global, $data, NULL);

    }
}
