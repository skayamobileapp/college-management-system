<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Deliverables extends BaseController
{
    public function __construct()
    {
        // echo "<Pre>";print_r('sasa');exit();
        parent::__construct();
        $this->load->model('deliverables_model');
        $this->isSupervisorLoggedIn();
    }

    function list()
    {       
        $id_supervisor = $this->session->id_supervisor;
        
        // $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($id);

        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_phd_duration'] = $this->security->xss_clean($this->input->post('id_phd_duration'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));

        $data['searchParam'] = $formData;

        $formData['id_supervisor'] = $id_supervisor;


        $data['supervisor'] = $this->deliverables_model->getSupervisor($id_supervisor);
        $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
        $data['deliverablesList'] = $this->deliverables_model->getDeliverablesListBySupervisorId($formData);

        // echo "<Pre>";print_r($data['deliverablesList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Deliverables Application';
        $this->loadViews("deliverables/list", $this->global, $data, NULL);
    }
    
    function edit($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/supervisor/deliverables/list');
        }

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;

            $status = $this->security->xss_clean($this->input->post('status'));
            $reason = $this->security->xss_clean($this->input->post('reason'));


            $data = array(
                'status' => $status,
                'reason' => $reason,
                'approved_on' => date('Y-m-d')
            );

            $result = $this->deliverables_model->editDeliverables($data,$id);
            redirect('/supervisor/deliverables/list');
         }

        $data['deliverables'] = $this->deliverables_model->getDeliverables($id);
        $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($data['deliverables']->id_student);
        $data['organisationDetails'] = $this->deliverables_model->getOrganisation();

        // $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
        // $data['chapterList'] = $this->deliverables_model->chapterListByStatus('1');
        $data['topicList'] = $this->deliverables_model->topicListByStatus('1');
        $data['researchStatusList'] = $this->deliverables_model->researchStatusListByStatus('1');
            
        // echo "<Pre>"; print_r($data['researchStatusList']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Deliverables Form';
        $this->loadViews("deliverables/edit", $this->global, $data, NULL);
    }

    function view($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/supervisor/deliverables/list');
        }
        // $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($id_student);
        $data['deliverables'] = $this->deliverables_model->getDeliverables($id);
        $data['studentDetails'] = $this->deliverables_model->getStudentByStudentId($data['deliverables']->id_student);

        $data['organisationDetails'] = $this->deliverables_model->getOrganisation();
        $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
        $data['chapterList'] = $this->deliverables_model->chapterListByStatus('1');
        $data['topicList'] = $this->deliverables_model->topicListByStatus('1');
            
        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Deliverables Form';
        $this->loadViews("deliverables/view", $this->global, $data, NULL);
    }

    function getChapterByDuration($id_duration)
    {
        $results = $this->deliverables_model->getChapterByDuration($id_duration);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_chapter' id='id_chapter' class='form-control' onchange='getTopicByData()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getTopicByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->deliverables_model->getTopicByData($tempData);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_topic' id='id_topic' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->topic;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

