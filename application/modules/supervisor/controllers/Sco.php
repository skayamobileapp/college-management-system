<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Sco extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sco_model');
        $this->isSupervisorLoggedIn();
    }

    function list()
    {       
        $id_supervisor = $this->session->id_supervisor;
        $data['supervisor'] = $this->sco_model->getSupervisor($id_supervisor);
        $data['scoList'] = $this->sco_model->getScoListBySupervisorId($id_supervisor);

        // echo "<Pre>";print_r($data['scoList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Sco Reporting';
        $this->loadViews("sco/list", $this->global, $data, NULL);
    }
    
    function edit($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/supervisor/sco/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_sco' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }
            $added_comments = $this->sco_model->addScoReportingComments($comments_data);
            
            redirect($_SERVER['HTTP_REFERER']);
         }

        $data['sco'] = $this->sco_model->getSco($id);
        $data['scoReportingComments'] = $this->sco_model->scoCommentsDetails($id);

        $data['studentDetails'] = $this->sco_model->getStudentByStudentId($data['sco']->id_student);
        $data['organisationDetails'] = $this->sco_model->getOrganisation();

            
        // echo "<Pre>"; print_r($data['researchStatusList']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Sco Reporting';
        $this->loadViews("sco/edit", $this->global, $data, NULL);
    }

    function view($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/supervisor/sco/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_sco' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }
            $added_comments = $this->sco_model->addScoReportingComments($comments_data);
            
            redirect($_SERVER['HTTP_REFERER']);
         }


        // $data['studentDetails'] = $this->sco_model->getStudentByStudentId($id_student);
        $data['sco'] = $this->sco_model->getSco($id);
        $data['scoReportingComments'] = $this->sco_model->scoCommentsDetails($id);

        $data['studentDetails'] = $this->sco_model->getStudentByStudentId($data['sco']->id_student);
        $data['organisationDetails'] = $this->sco_model->getOrganisation();

        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Sco Reporting';
        $this->loadViews("sco/edit", $this->global, $data, NULL);
    }
}