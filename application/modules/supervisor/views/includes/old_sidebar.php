
                    <h4>Profile</h4>
                    <ul>
                        <li><a href="/supervisor/profile/view">Profile</a></li>
                        <li><a href="/supervisor/viewStudents/view">View Students</a></li>
                    </ul>
                    
                    <h4>Stage 1</h4>
                    <ul>
                        <li><a href="/supervisor/deliverables/list">Research Proposal Application</a></li>
                        <li><a href="/supervisor/proposalReporting/list">Research Proposal Reporting</a></li>
                    </ul>
                    
                    <h4>Stage 2</h4>
                    <ul>
                        <li><a href="/supervisor/proposalReporting/list2">Listing of students and their research progress report (Chapter 3)</a></li>
                    </ul>

                    <h4>Stage 3</h4>
                    <ul>
                        <li><a href="/supervisor/proposalReporting/list3">Listing of students and their research progress report (Chapter 4)</a></li>
                    </ul>

                    <h4>Stage 4 ( Reporting )</h4>
                    <ul>
                        <li><a href="/supervisor/toc/list">Research TOC</a></li>
                        <li><a href="/supervisor/abstractt/list">Research Abstract</a></li>
                        <li><a href="/supervisor/bound/list">5 copies of hard bound submission</a></li>
                        <li><a href="/supervisor/sco/list">Research Soft Copy Of File</a></li>
                        <li><a href="/supervisor/ppt/list">Research Power Point Presentation</a></li>
                    </ul>