<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Award</h3>
        </div>
        <form id="form_award" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Award Details</h4>               
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Program <span class='error-text'>*</span></label>
                            <select name="id_programme" id="id_programme" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programmeList))
                                {
                                    foreach ($programmeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <select name="id_intake" id="id_intake" class="form-control" style="width: 408px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type <span class='error-text'>*</span></label>
                            <select name="type" id="type" class="form-control">
                                <option value="">Select</option>
                                <option value="Graduate">Graduate</option>
                                <option value="Post Graduate">Post Graduate</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>From CGPA <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="from_cgpa" name="from_cgpa">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>To CGPA <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="to_cgpa" name="to_cgpa">
                        </div>
                    </div>


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                description: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                type: {
                    required: true
                },
                from_cgpa: {
                    required: true
                },
                to_cgpa: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Award Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Award Description required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                from_cgpa: {
                    required: "<p class='error-text'>From CGPA Required</p>",
                },
                to_cgpa: {
                    required: "<p class='error-text'>To CGPA Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>