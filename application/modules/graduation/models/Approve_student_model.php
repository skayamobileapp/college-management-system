<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Approve_student_model extends CI_Model
{
    function graduationList()
    {
        $this->db->select('s.id as sid, s.first_name, s.last_name, g.id, g.status, in.name as intake');
        $this->db->from('student as s');
        $this->db->join('intake as in', 's.id_intake = in.id');
        $this->db->join('graduation_list as g', 'g.id_student = s.id');
        $this->db->order_by("g.id", "DESC");
         $query = $this->db->get();
         $result = $query->result(); 
         // print_r($result);exit();      
         return $result;
    }

    function graduationListSearch($search)
    {
        // $date = 
        $this->db->select('s.*, in.name as intake');
        $this->db->from('student as s');
        $this->db->join('intake as in', 's.id_intake = in.id');
        if (!empty($search))
        {
            $likeCriteria = "(first_name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editGraduationList($array)
    {
      $status = ['status'=>'Approved'];

        $this->db->where_in('id', $array);
      $this->db->update('graduation_list', $status);
      // $this->db->set('status', $status);
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getGraduationList($id)
    {
        $this->db->select('*');
        $this->db->from('convocation');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGraduation_list($data)
    {
        $this->db->trans_start();
        $this->db->insert('convocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGraduation_list($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('convocation', $data);
        return TRUE;
    }
}