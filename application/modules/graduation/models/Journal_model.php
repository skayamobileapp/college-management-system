<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Journal_model extends CI_Model
{
    function journalList()
    {
        $this->db->select('j.*, fy.year as financial_year');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function journalListSearch($formData)
    {
        $this->db->select('j.*, fy.year as financial_year');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getJournal($id)
    {
        $this->db->select('j.*');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
        $this->db->where('r.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewJournal($data)
    {
        $this->db->trans_start();
        $this->db->insert('journal', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewJournalDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('journal_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editJournal($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('journal', $data);
        return TRUE;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewTempJournalDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;
        $this->db->trans_start();
        $this->db->insert('temp_journal_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
// echo "<Pre>";  print_r($db);exit;
        return $insert_id;
    }

    function getTempJournalDetails($id_session)
    {
         $this->db->select('trd.*, dep.name as department_name, dep.code as department_code, acc.name as account_name, acc.code as account_code, actc.name as activity_name, actc.code as activity_code, fc.name as fund_name, fc.code as fund_code');
        $this->db->from('temp_journal_details as trd');
        $this->db->join('department as dep', 'trd.id_department = dep.id');        
        $this->db->join('account_code as acc', 'trd.id_account_code = acc.id');        
        $this->db->join('activity_code as actc', 'trd.id_activity_code = actc.id');        
        $this->db->join('fund_code as fc', 'trd.id_fund_code = fc.id');        
        $this->db->where('trd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_journal_details');
    }

    function generateJRNumber($data)
    {

        $year = date('y');
        $Year = date('Y');
        $type = $data['type'];
        // print_r($type);exit;
        // echo "<Pre>";  print_r($type);exit;


        switch ($type)
        {
            case '':
            // $query = 'SELECT journal_number from journal order by id desc limit 0,1';
            // $sql = $this->db->query($query);
            // $result = $sql->result();


            $this->db->select('j.*');
            $this->db->from('journal as j');
            $this->db->limit(0, 1);
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->row();
            if ($query->num_rows() > 0)
            {
                $result = $query->first_row();
            }
            $data=$result['journal_number'];
            $value=substr($data, -9);
            // echo "<Pre>";  print_r($data);exit;
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "JR" .(sprintf("%'06d", $count)). "/" . $Year;
            break;
        }
        return "JR0000001/2020";
    }
}

