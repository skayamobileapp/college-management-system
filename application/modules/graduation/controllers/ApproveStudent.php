<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApproveStudent extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('graduation_list_model');
        $this->load->model('approve_student_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('graduation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $data['programList'] = $this->graduation_list_model->programList();
            
            $student_name = $this->security->xss_clean($this->input->post('student_name'));
            // $name = $this->security->xss_clean($this->input->post('name'));
            // $name = $this->security->xss_clean($this->input->post('name'));

            $data['searchName'] = $student_name;
            $data['graduationList'] = $this->approve_student_model->graduationList();
            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array)) {

                $result = $this->approve_student_model->editGraduationList($array);
                }


            $this->global['pageTitle'] = 'Campus Management System : Approve student';
            $this->loadViews("approve_student/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('graduation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id = $this->security->xss_clean($this->input->post('id'));
                $convocation_session = $this->security->xss_clean($this->input->post('convocation_session'));
                $session_capacity = $this->security->xss_clean($this->input->post('session_capacity'));
                $number_of_guest = $this->security->xss_clean($this->input->post('number_of_guest'));
                $weblink = $this->security->xss_clean($this->input->post('weblink'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $confirm_attendence_end_date = $this->security->xss_clean($this->input->post('confirm_attendence_end_date'));
                $record_verification_end_date = $this->security->xss_clean($this->input->post('record_verification_end_date'));
                $graduation_guest_application_end_date = $this->security->xss_clean($this->input->post('graduation_guest_application_end_date'));
                $student_portal_end_date = $this->security->xss_clean($this->input->post('student_portal_end_date'));
                $award = $this->security->xss_clean($this->input->post('award'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                   'convocation_session' => $convocation_session,
                    'session_capacity' => $session_capacity,
                    'number_of_guest' => $number_of_guest,
                    'weblink' => $weblink,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'confirm_attendence_end_date' => $confirm_attendence_end_date,
                    'record_verification_end_date' => $record_verification_end_date,
                    'graduation_guest_application_end_date' => $graduation_guest_application_end_date,
                    'student_portal_end_date' => $student_portal_end_date,
                    'award' => $award,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->graduation_list_model->addNewGraduationList($data);
                redirect('/graduation/graduationList/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Graduation List';
            $this->loadViews("graduation_list/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('graduation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/graduationList/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $convocation_session = $this->security->xss_clean($this->input->post('convocation_session'));
                $session_capacity = $this->security->xss_clean($this->input->post('session_capacity'));
                $number_of_guest = $this->security->xss_clean($this->input->post('number_of_guest'));
                $weblink = $this->security->xss_clean($this->input->post('weblink'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $confirm_attendence_end_date = $this->security->xss_clean($this->input->post('confirm_attendence_end_date'));
                $record_verification_end_date = $this->security->xss_clean($this->input->post('record_verification_end_date'));
                $graduation_guest_application_end_date = $this->security->xss_clean($this->input->post('graduation_guest_application_end_date'));
                $student_portal_end_date = $this->security->xss_clean($this->input->post('student_portal_end_date'));
                $award = $this->security->xss_clean($this->input->post('award'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                   'convocation_session' => $convocation_session,
                    'session_capacity' => $session_capacity,
                    'number_of_guest' => $number_of_guest,
                    'weblink' => $weblink,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'confirm_attendence_end_date' => $confirm_attendence_end_date,
                    'record_verification_end_date' => $record_verification_end_date,
                    'graduation_guest_application_end_date' => $graduation_guest_application_end_date,
                    'student_portal_end_date' => $student_portal_end_date,
                    'award' => $award,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->graduation_list_model->editGraduationList($data,$id);
                redirect('/graduation/graduationList/list');
            }
            $data['convocation'] = $this->graduation_list_model->getGraduationList($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit GraduationList';
            $this->loadViews("graduation_list/edit", $this->global, $data, NULL);
        }
    }
}
