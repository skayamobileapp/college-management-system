<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Guest extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest_model');
        $this->load->model('convocation_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('guest.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['convocationList'] = $this->convocation_model->convocationList();

            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['guestList'] = $this->guest_model->guestListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Guest List';
            $this->loadViews("guest/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('guest.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name =  $this->security->xss_clean($this->input->post('name'));
                $email =  $this->security->xss_clean($this->input->post('email'));
                $description =  $this->security->xss_clean($this->input->post('description'));
                $nric =  $this->security->xss_clean($this->input->post('nric'));
                $mobile =  $this->security->xss_clean($this->input->post('mobile'));
                $id_convocation =  $this->security->xss_clean($this->input->post('id_convocation'));
                $status =  $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                   'name' => $name,
                    'email' => $email,
                    'description' => $description,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    'id_convocation' => $id_convocation,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->guest_model->addNewGuest($data);
                redirect('/graduation/guest/list');
            }
            $data['convocationList'] = $this->convocation_model->convocationList();

            $this->global['pageTitle'] = 'Campus Management System : Add Guest';
            $this->loadViews("guest/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('guest.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/guest/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name =  $this->security->xss_clean($this->input->post('name'));
                $email =  $this->security->xss_clean($this->input->post('email'));
                $description =  $this->security->xss_clean($this->input->post('description'));
                $nric =  $this->security->xss_clean($this->input->post('nric'));
                $mobile =  $this->security->xss_clean($this->input->post('mobile'));
                $id_convocation =  $this->security->xss_clean($this->input->post('id_convocation'));
                $status =  $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                   'name' => $name,
                    'email' => $email,
                    'description' => $description,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    'id_convocation' => $id_convocation,
                    'status' => $status,
                    'updated_by' => $id_user
                );


                $result = $this->guest_model->editGuest($data,$id);
                redirect('/graduation/guest/list');
            }
            $data['convocationList'] = $this->convocation_model->convocationList();
            $data['guest'] = $this->guest_model->getGuest($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Guest';
            $this->loadViews("guest/edit", $this->global, $data, NULL);
        }
    }
}
