<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Convocation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('convocation_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('convocation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['convocationList'] = $this->convocation_model->convocationListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Convocation List';
            $this->loadViews("convocation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('convocation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $convocation_session = $this->security->xss_clean($this->input->post('convocation_session'));
                $session_capacity = $this->security->xss_clean($this->input->post('session_capacity'));
                $number_of_guest = $this->security->xss_clean($this->input->post('number_of_guest'));
                $weblink = $this->security->xss_clean($this->input->post('weblink'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $confirm_attendence_end_date = $this->security->xss_clean($this->input->post('confirm_attendence_end_date'));
                $record_verification_end_date = $this->security->xss_clean($this->input->post('record_verification_end_date'));
                $graduation_guest_application_end_date = $this->security->xss_clean($this->input->post('graduation_guest_application_end_date'));
                $student_portal_end_date = $this->security->xss_clean($this->input->post('student_portal_end_date'));
                $award = $this->security->xss_clean($this->input->post('award'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                   'convocation_session' => $convocation_session,
                    'session_capacity' => $session_capacity,
                    'number_of_guest' => $number_of_guest,
                    'weblink' => $weblink,
                    'start_date' => date("Y-m-d", strtotime($start_date)),
                    'end_date' => date("Y-m-d", strtotime($end_date)),
                    'confirm_attendence_end_date' => date("Y-m-d", strtotime($confirm_attendence_end_date)),
                    'record_verification_end_date' => date("Y-m-d", strtotime($record_verification_end_date)),
                    'graduation_guest_application_end_date' => date("Y-m-d", strtotime($graduation_guest_application_end_date)),
                    'student_portal_end_date' => date("Y-m-d", strtotime($student_portal_end_date)),
                    'year' => $year,
                    'award' => $award,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->convocation_model->addNewConvocation($data);
                redirect('/graduation/convocation/edit/'.$result);
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Convocation';
            $this->loadViews("convocation/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('convocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/graduation/convocation/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $convocation_session = $this->security->xss_clean($this->input->post('convocation_session'));
                $session_capacity = $this->security->xss_clean($this->input->post('session_capacity'));
                $number_of_guest = $this->security->xss_clean($this->input->post('number_of_guest'));
                $weblink = $this->security->xss_clean($this->input->post('weblink'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $confirm_attendence_end_date = $this->security->xss_clean($this->input->post('confirm_attendence_end_date'));
                $record_verification_end_date = $this->security->xss_clean($this->input->post('record_verification_end_date'));
                $graduation_guest_application_end_date = $this->security->xss_clean($this->input->post('graduation_guest_application_end_date'));
                $student_portal_end_date = $this->security->xss_clean($this->input->post('student_portal_end_date'));
                $award = $this->security->xss_clean($this->input->post('award'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                   'convocation_session' => $convocation_session,
                    'session_capacity' => $session_capacity,
                    'number_of_guest' => $number_of_guest,
                    'weblink' => $weblink,
                    'start_date' => date("Y-m-d", strtotime($start_date)),
                    'end_date' => date("Y-m-d", strtotime($end_date)),
                    'confirm_attendence_end_date' => date("Y-m-d", strtotime($confirm_attendence_end_date)),
                    'record_verification_end_date' => date("Y-m-d", strtotime($record_verification_end_date)),
                    'graduation_guest_application_end_date' => date("Y-m-d", strtotime($graduation_guest_application_end_date)),
                    'student_portal_end_date' => date("Y-m-d", strtotime($student_portal_end_date)),
                    'year' => $year,
                    'award' => $award,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->convocation_model->editConvocation($data,$id);
                redirect('/graduation/convocation/list');
            }
            $data['convocation'] = $this->convocation_model->getConvocation($id);
            $data['convocationDetails'] = $this->convocation_model->getConvocationDetails($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Convocation';
            $this->loadViews("convocation/edit", $this->global, $data, NULL);
        }
    }

    function saveDetailData()
    {
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->convocation_model->addConvocationDetails($tempData);

        // $data = $this->displayTempData();

        echo "success";exit();
    }

    function deleteDetailData($id)
    {
        $deleted = $this->convocation_model->deleteConvocationDetails($id);
        echo 'success';exit;
    }
}