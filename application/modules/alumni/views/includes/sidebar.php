            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="/alumni/profile" class="user-profile-link">
                        <span>
                            <img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg">
                        </span> <?php echo $alumni_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/alumni/profile/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Profile</h4>
                    <ul>
                        <li><a href="/alumni/editProfile/edit">View Profile</a></li>
                    </ul>
                    <h4>Scholarship</h4>
                    <ul>
                        <li><a href="/alumni/scholarshipApplication/list">Scholarship</a></li>
                    </ul>
                    <h4>Finance</h4>
                    <ul>
                        <li><a href="/alumni/statementOfAccount/viewInvoice">Invoice</a></li>
                        <li><a href="/alumni/statementOfAccount/viewReceipt">Payment & Receipt</a></li>
                        <li><a href="/alumni/statementOfAccount/viewSummary">Account Summary</a></li>
                        <li><a href="/alumni/statementOfAccount/viewDiscount">Discount List</a></li>
                        <li><a href="/alumni/statementOfAccount/viewSponser">Sponser Info. If Any</a></li>
                    </ul>
                    <h4>Internship / Project</h4>
                    <ul>
                        <li><a href="/alumni/internshipApplication/list">Internship Application</a></li>
                        <li><a href="/alumni/projectReportSubmission/list">Project Report Submission</a></li>
                        <li><a href="/alumni/placement/list">Placement Details</a></li>
                        <!-- <li><a href="/alumni/courseRegistration/courseWithdraw">Course Withdraw</a></li> -->
                    </ul>
                   <!--  <h4>Registration</h4>
                    <ul>
                        <li><a href="/alumni/courseRegistration/add">Course Registration</a></li>
                        <li><a href="/alumni/courseRegistration/courseWithdraw">Course Withdraw</a></li>
                    </ul> -->
                    <h4>Examination</h4>
                    <ul>
                        <!-- <li><a href="/alumni/examination/timeTableSlip">Exam Timetable & Slip</a></li> -->
                        <li><a href="/alumni/examination/latestExamResults">Latest Exam Result</a></li>
                        <!-- <li><a href="/alumni/examination/partialTranscript">Partial Transcript</a></li> -->
                        <!-- <li><a href="/alumni/examination/resitApplication">Resit Application</a></li> -->
                        <!-- <li><a href="/alumni/examination/remarkingApplication">Remarking Application</a></li> -->
                    </ul>
                </div>
            </div>