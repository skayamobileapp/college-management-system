<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Scholarship Application</h3>
        </div>

        <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Scholarship Application Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Scheme <span class='error-text'>*</span></label>
                        <select name="id_scholarship_scheme" id="id_scholarship_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($schemeList))
                            {
                                foreach ($schemeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Father Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="father_name" name="father_name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mother Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="mother_name" name="mother_name">
                    </div>
                </div>

                

            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Father Deceased <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="father_deceased" id="father_deceased" value="1"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="father_deceased" id="father_deceased" value="0" checked="checked"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="year" name="year" autocomplete="off" value="<?php echo date('Y'); ?>" readonly>
                    </div>
               </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Father Occupation <span class='error-text'>*</span></label>
                        <select name="father_occupation" id="father_occupation" class="form-control">
                            <option value="">Select</option>
                            <option value="Employee">Employee</option>
                            <option value="Government Servent">Government Servent</option>
                            <option value="Business">Business</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>

                        

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>No Of Siblings <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="no_siblings" name="no_siblings">
                    </div>
                </div>   

               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Estimated Fee Amount(RM) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="est_fee" name="est_fee">
                    </div>
                </div>  

            </div>

        </div>


        <div class="form-container">
            <h4 class="form-group-title">Previous Marks Details</h4>


            <div class="row">

                 <div class="col-sm-2">
                    <div class="form-group">
                        <label>Result Item <span class='error-text'>*</span></label>
                        <select name="result_item" id="result_item" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <option value="Band">Band</option>
                            <option value="CGPA">CGPA</option>
                            <option value="Grade">Grade</option>
                            <option value="Marks">Marks</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Max. Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_marks" name="max_marks">
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Obtained Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="obtained_marks" name="obtained_marks" onchange="getPercentage()">
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Percentage <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="percentage" name="percentage" readonly>
                    </div>
                </div>

                

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
        <!-- <?php

            if($check_limit == 0)
            {
            ?> -->
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <!-- <?php
            }else
            {
            ?> -->
                    <!-- <h3 class="text-center"><span class='error-text'><?php echo $student_name; ?> You Have Exceeded The Max Application Limit</span></h3> -->
           <!--  <?php
            }
        ?> -->
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $('select').select2();

    function getPercentage()
    {
        if($("#max_marks").val() != '' && $("#obtained_marks").val() != '')
        {

            var tempPR = {};
            tempPR['max_marks'] = $("#max_marks").val();
            tempPR['obtained_marks'] = $("#obtained_marks").val();
            tempPR['type'] = $("#result_item").val();
            // alert(tempPR['id_group']);

            $.ajax(
            {
               url: '/student/scholarshipApplication/getPercentage',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result > 100)
                {
                    alert('Wrong Marks Entry');
                    $("#max_marks").val('');
                    $("#obtained_marks").val('');
                }
                else
                {
                    $("#percentage").val(result);
                }
               }
            });  
        }
    }




    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                id_scholarship_scheme: {
                    required: true
                },
                 father_name: {
                    required: true
                },
                 mother_name: {
                    required: true
                },
                 father_deceased: {
                    required: true
                },
                 father_occupation: {
                    required: true
                },
                 no_siblings: {
                    required: true
                },
                 result_item: {
                    required: true
                },
                 est_fee: {
                    required: true
                },
                 max_marks: {
                    required: true
                },
                 obtained_marks: {
                    required: true
                },
                 percentage: {
                    required: true
                }
            },
            messages: {
                id_scholarship_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                },
                father_name: {
                    required: "<p class='error-text'>Father Name Required</p>",
                },
                mother_name: {
                    required: "<p class='error-text'>Mother Name Required</p>",
                },
                father_deceased: {
                    required: "<p class='error-text'>Select Deceased Status</p>",
                },
                father_occupation: {
                    required: "<p class='error-text'>Select Father Occupation</p>",
                },
                no_siblings: {
                    required: "<p class='error-text'>No Of Siblings Required</p>",
                },
                result_item: {
                    required: "<p class='error-text'>Select Result Item</p>",
                },
                est_fee: {
                    required: "<p class='error-text'>Estimated Fee Amount Required</p>",
                },
                max_marks: {
                    required: "<p class='error-text'>Max. Marks Required</p>",
                },
                obtained_marks: {
                    required: "<p class='error-text'>Obtained Mars Required</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Click Here To Calculate Percentage</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>