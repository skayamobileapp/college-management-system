<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EditProfile extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('edit_profile_model');
        $this->isAlumniLoggedIn();
    }

    function edit()
    {
             $id_student = $this->session->id_alumni;
             $id = $this->session->id_alumni;

            // if ($id == null)
            // {
            //     redirect('/student/profile');
            // }
            if($this->input->post())
            {

                $qualification_level = $this->security->xss_clean($this->input->post('qualification_level'));
                $degree_awarded = $this->security->xss_clean($this->input->post('degree_awarded'));
                $specialization = $this->security->xss_clean($this->input->post('specialization'));
                $class_degree = $this->security->xss_clean($this->input->post('class_degree'));
                $result = $this->security->xss_clean($this->input->post('result'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $medium = $this->security->xss_clean($this->input->post('medium'));
                $college_country = $this->security->xss_clean($this->input->post('college_country'));
                $college_name = $this->security->xss_clean($this->input->post('college_name'));
                $certificate = $this->security->xss_clean($this->input->post('certificate'));
                $transcript = $this->security->xss_clean($this->input->post('transcript'));


                $data = array(
                    'id_student' => $id_student,
                    'qualification_level' => $qualification_level,
                    'degree_awarded' => $degree_awarded,
                    'specialization' => $specialization,
                    'class_degree' => $class_degree,
                    'result' => $result,
                    'year' => $year,
                    'medium' => $medium,
                    'college_country' => $college_country,
                    'college_name' => $college_name,
                    'certificate' => $certificate,
                    'transcript' => $transcript
                );
                if ($qualification_level != "") {
                    $result = $this->edit_profile_model->addExamDetails($data);
                }

                $id_student = $id;
               $test = $this->security->xss_clean($this->input->post('test'));
                $date = $this->security->xss_clean($this->input->post('date'));
                $score = $this->security->xss_clean($this->input->post('score'));
                $file = $this->security->xss_clean($this->input->post('file'));

                $data = array(
                    'id_student' => $id_student,
                    'test' => $test,
                    'date' => date("Y-m-d", strtotime($date)),
                    'score' => $score,
                    'file' => $file
                );
                if ($test != "") {
                    $result = $this->edit_profile_model->addProficiencyDetails($data);
                }

                $id_student = $id;
               $company_name = $this->security->xss_clean($this->input->post('company_name'));
                $company_address = $this->security->xss_clean($this->input->post('company_address'));
                $telephone = $this->security->xss_clean($this->input->post('telephone'));
                $fax_num = $this->security->xss_clean($this->input->post('fax_num'));
                $designation = $this->security->xss_clean($this->input->post('designation'));
                $position = $this->security->xss_clean($this->input->post('position'));
                $service_year = $this->security->xss_clean($this->input->post('service_year'));
                $industry = $this->security->xss_clean($this->input->post('industry'));
                $job_desc = $this->security->xss_clean($this->input->post('job_desc'));
                $employment_letter = $this->security->xss_clean($this->input->post('employment_letter'));

                $data = array(
                    'id_student' => $id_student,
                    'company_name' => $company_name,
                    'company_address' => $company_address,
                    'telephone' => $telephone,
                    'fax_num' => $fax_num,
                    'designation' => $designation,
                    'position' => $position,
                    'service_year' => $service_year,
                    'industry' => $industry,
                    'job_desc' => $job_desc,
                    'employment_letter' => $employment_letter
                );
                if ($company_name != "") {
                    $result = $this->edit_profile_model->addEmploymentDetails($data);
                }

                $id_student = $id;
               $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $id_type = $this->security->xss_clean($this->input->post('id_type'));
                $id_number = $this->security->xss_clean($this->input->post('id_number'));
                $passport_expiry_date = $this->security->xss_clean($this->input->post('passport_expiry_date'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
                $religion = $this->security->xss_clean($this->input->post('religion'));
                $nationality = $this->security->xss_clean($this->input->post('nationality'));
                $nationality_type = $this->security->xss_clean($this->input->post('nationality_type'));
                $race = $this->security->xss_clean($this->input->post('id_race'));
                $email_id = $this->security->xss_clean($this->input->post('email_id'));
                $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
                $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
                $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
                $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
                $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
                $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
                $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
                $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
                $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
                $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
                $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
                $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
                $passport_number = $this->security->xss_clean($this->input->post('passport_number'));




                $data = array(
                    'id_student' => $id_student,
                    'full_name' => $salutation.". ".$first_name." ".$last_name,
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'id_type' => $id_type,
                    'passport_number' => $passport_number,
                    'passport_expiry_date' => $passport_expiry_date,
                    'gender' => $gender,
                    'date_of_birth' => date('Y-m-d',strtotime($date_of_birth)),
                    'martial_status' => $martial_status,
                    'religion' => $religion,
                    'nationality' => $nationality,
                    'nationality_type' => $nationality_type,
                    'id_race' => $race,
                    'email_id' => $email_id,
                    'mail_address1' => $mail_address1,
                    'mail_address2' => $mail_address2,
                    'permanent_address1' => $permanent_address1,
                    'permanent_address2' => $permanent_address2,
                    'mailing_zipcode' => $mailing_zipcode,
                    'permanent_zipcode' => $permanent_zipcode,
                    'mailing_country' => $mailing_country,
                    'permanent_country' => $permanent_country,
                    'mailing_state' => $mailing_state,
                    'permanent_state' => $permanent_state,
                    'mailing_city' => $mailing_city,
                    'permanent_city' => $permanent_city
                );

                // $checkDuplicate = $this->edit_profile_model->checkDuplicateStudent($data,$id_student);
                // if($checkDuplicate)
                // {
                //     echo "Entered E-Mail / Phone / NRIC Already Exist";exit();
                // }

                $updated_student = $this->edit_profile_model->updateStudentData($data);

                if ($first_name != "") {
                    $result = $this->edit_profile_model->editProfileDetails($data, $id_student);
                }

                $id_student = $id;
                $malaysian_visa = $this->security->xss_clean($this->input->post('malaysian_visa'));
                $visa_expiry_date = $this->security->xss_clean($this->input->post('visa_expiry_date'));
                $visa_status = $this->security->xss_clean($this->input->post('visa_status'));

                $data = array(
                    'id_student' => $id_student,
                    'malaysian_visa' => $malaysian_visa,
                    'visa_expiry_date' => date('Y-m-d',strtotime($visa_expiry_date)),
                    'visa_status' => $visa_status
                );



                // echo "<Pre>";print_r($data);exit();
                // if ($malaysian_visa != "")
                // {

                    // $result = $this->edit_profile_model->addVisaDetails($data);
                // }
                // echo "<Pre>";print_r($malaysian_visa);exit();
                    $result = $this->edit_profile_model->updateVisaDetails($data);

                $id_student = $id;
                $doc_name = $this->security->xss_clean($this->input->post('doc_name'));
                $doc_file = $this->security->xss_clean($this->input->post('doc_file'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));

                $data = array(
                    'id_student' => $id_student,
                    'doc_name' => $doc_name,
                    'doc_file' => $doc_file,
                    'remarks' => $remarks
                );
                
                // echo "<Pre>";print_r($malaysian_visa);exit();

                if ($doc_name != "" && $remarks != "")
                {
                    $result = $this->edit_profile_model->addOtherDocuments($data);
                }
                    redirect($_SERVER['HTTP_REFERER']);
            }
            $data['countryList'] = $this->edit_profile_model->countryList();
            $data['stateList'] = $this->edit_profile_model->stateList();
            $data['raceList'] = $this->edit_profile_model->raceListByStatus('1');
            $data['religionList'] = $this->edit_profile_model->religionListByStatus('1');

            $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
            $data['studentDetails'] = $this->edit_profile_model->getStudentDetails($id_student);

            $data['examDetails'] = $this->edit_profile_model->getExamDetails($id_student);
            $data['proficiencyDetails'] = $this->edit_profile_model->getProficiencyDetails($id_student);
            $data['employmentDetails'] = $this->edit_profile_model->getEmploymentDetails($id_student);
            $data['profileDetails'] = $this->edit_profile_model->getProfileDetails($id_student);
            $data['visaDetails'] = $this->edit_profile_model->getVisaDetails($id_student);
            $data['otherDocuments'] = $this->edit_profile_model->getOtherDocuments($id_student);
            $data['courseRegistrationList'] = $this->edit_profile_model->courseRegistrationList($id_student);

            // echo "<Pre>";print_r($data['visaDetails']);exit();
            $this->global['pageTitle'] = 'Alumni Portal : View Alumni';
            $this->loadViews("profile/edit", $this->global, $data, NULL);
    }

    function delete_exam()
    {
        $id = $this->input->get('id');

       $this->edit_profile_model->deleteExamDetails($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_english()
    {
        $id = $this->input->get('id');

       $this->edit_profile_model->deleteProficiencyDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_employment()
    {
        $id = $this->input->get('id');

       $this->edit_profile_model->deleteEmploymentDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_document()
    {
        $id = $this->input->get('id');

       $this->edit_profile_model->deleteOtherDocument($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }
}
