<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        // echo "string";exit();
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('profile_model');
        $this->isAlumniLoggedIn();
    }

    public function index()
    {
        $this->list();
    }

    function demoView()
    {
        $this->load->view('vie');
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Alumni Portal : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
            $data['profileList'] = $this->profile_model->profileList();
            $this->global['pageTitle'] = 'Alumni Portal : Payment Type List';
            $this->loadViews("profile/list", $this->global, $data, NULL);
    }
    
    function add()
    {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->profile_model->addNewPaymentType($data);
                redirect('/student/profile/list');
            }
            //print_r($data['stateList']);exit;
            $this->global['pageTitle'] = 'Alumni Portal : Add Sponser';
            $this->loadViews("profile/add", $this->global, NULL, NULL);
    }


    function edit($id = NULL)
    {
            if ($id == null)
            {
                redirect('/student/profile/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                
                $result = $this->profile_model->editPaymentType($data,$id);
                redirect('/student/profile/list');
            }

            $data['profileDetails'] = $this->profile_model->getPaymentType($id);
            $this->global['pageTitle'] = 'Alumni Portal : Edit Sponser';
            $this->loadViews("profile/edit", $this->global, $data, NULL);
    }


    function logout()
    {
        // echo "string";exit();
        $sessionArray = array('id_alumni'=> '',                    
                    'alumni_name'=> '',
                    'alumni_email_id'=> '',
                    'alumni_nric'=> '',
                    'alumni_id_intake'=> '',
                    'alumni_id_program'=> '',
                    'alumni_id_qualification'=>  '',
                    'alumni_last_login'=>  '',
                    'isAlumniLoggedIn' => FALSE
            );

     $this->session->set_userdata($sessionArray);
     // $this->session->sess_destroy();
     // redirect($_SERVER['HTTP_REFERER']);
     $this->isAlumniLoggedIn();
    }
}
