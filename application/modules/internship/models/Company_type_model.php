<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_type_model extends CI_Model
{
    function companyTypeListSearch($data)
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_type as invt');
        if ($data['name'] != '')
        {
            $likeCriteria = "(invt.name LIKE '%" . $data['name'] . "%' or invt.name_in_malay  LIKE '%" . $data['name'] . "%' or invt.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getCompanyType($id)
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCompanyType($data)
    {
        $this->db->trans_start();
        $this->db->insert('internship_company_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCompanyType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('internship_company_type', $data);
        return TRUE;
    }

}