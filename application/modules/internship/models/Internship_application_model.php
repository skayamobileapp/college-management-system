<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Internship_application_model extends CI_Model
{
    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

	function getInternshipApplicationListSearch($data)
	{
        $this->db->select('ia.*, ict.name as company_type_name, ict.code as company_type_code, icr.name as company_name, icr.registration_no, p.code as program_code, p.name as program_name, i.name as intake, s.nric, s.full_name as student_name');
        $this->db->from('internship_application as ia');
        $this->db->join('internship_company_type as ict', 'ia.id_company_type = ict.id');
        $this->db->join('internship_company_registration as icr', 'ia.id_company = icr.id');
        $this->db->join('programme as p', 'ia.id_program = p.id');
        $this->db->join('intake as i', 'ia.id_intake = i.id');
        $this->db->join('student as s', 'ia.id_student = s.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(ia.description LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_company_type'] != '')
        {
            $this->db->where('ia.id_company_type', $data['id_company_type']);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('ia.id_company', $data['id_company']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('ia.id_intake', $data['id_intake']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('ia.id_program', $data['id_program']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('ia.status', $data['status']);
        }
        $this->db->order_by('ia.id', "ASC");
        $query = $this->db->get();
         return $query->result();
    }

    function companyTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyTypeList()
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyRegistrationListByStatus($status)
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_registration as invt');
        $this->db->where('invt.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function companyRegistrationList()
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_registration as invt');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCompanyByCompanyTypeId($id_company_type)
    {
        $this->db->select('*');
        $this->db->from('internship_company_registration');
        $this->db->where('id_company_type', $id_company_type);
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addInternshipApplication($data)
    {
        $this->db->trans_start();
        $this->db->insert('internship_application', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getInternshipApplication($id)
    {
        $this->db->select('ia.*, ict.name as company_type_name, ict.code as company_type_code, icr.name as company_name, icr.registration_no, p.code as program_code, p.name as program_name, i.name as intake, s.nric, s.full_name as student_name');
        $this->db->from('internship_application as ia');
        $this->db->join('internship_company_type as ict', 'ia.id_company_type = ict.id');
        $this->db->join('internship_company_registration as icr', 'ia.id_company = icr.id');
        $this->db->join('programme as p', 'ia.id_program = p.id');
        $this->db->join('intake as i', 'ia.id_intake = i.id');
        $this->db->join('student as s', 'ia.id_student = s.id');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function intakeListByStatus($status)
    {
        $this->db->select('invt.*');
        $this->db->from('intake as invt');
        $this->db->where('invt.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('invt.*');
        $this->db->from('programme as invt');
        $this->db->where('invt.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editInternshipApplication($data,$id)
    {
        $this->db->where_in('id', $id);
      $this->db->update('internship_application', $data);
      return TRUE;
    }
}