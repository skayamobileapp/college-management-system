<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Internship extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('internship_model');
        $this->isLoggedIn();
    }

    function demo()
    {
        if ($this->checkAccess('internship.list') == 1)
        // if ($this->checkAccess('internship.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;


            $this->global['pageTitle'] = 'Campus Management System : Internship List';
            //print_r($subjectDetails);exit;
            $this->loadViews("intern/list", $this->global, $data, NULL);
        }
    }

    

    function list()
    {
        $this->limit();
    }

    

    function limit()
    {
        if ($this->checkAccess('internship_limit.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                 $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $max_limit = $this->security->xss_clean($this->input->post('max_limit'));
                $id = $this->security->xss_clean($this->input->post('id'));

                $data = array(
                    'max_limit' => $max_limit,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d h:i:s')
                );
                
                $result = $this->internship_model->editInternshipLimit($data,$id);
                redirect($_SERVER['HTTP_REFERER']);
            }
            $data['internshipLimit'] = $this->internship_model->getInternshipLimit();

                // echo "<Pre>"; print_r($data['internshipLimit']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Partner University';
            $this->loadViews("intern/limit", $this->global, $data, NULL);
        }
    }
}
