<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Intake_model extends CI_Model
{
    function intakeList()
    {
        $this->db->select('i.*');
        $this->db->from('intake as i');
        $this->db->order_by("i.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('i.*');
        $this->db->from('intake as i');
        $this->db->where("i.status", $status);
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function academicYearList()
    {
        $this->db->select('*');
        $this->db->from('academic_year');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function branchList()
    {
        $this->db->select('i.*');
        $this->db->from('organisation_has_training_center as i');
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  

         return $result;
    }

    function intakeListSearch($search)
    {
        $this->db->select('i.*');
        $this->db->from('intake as i');
        if (!empty($search))
        {
            $likeCriteria = "(i.name  LIKE '%" . $search . "%' or i.name_in_malay  LIKE '%" . $search . "%' or i.year  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getIntakeDetails($id)
    {
        $this->db->select('i.*');
        $this->db->from('intake as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewIntake($data)
    {
        $this->db->trans_start();
        $this->db->insert('intake', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editIntakeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('intake', $data);
        return TRUE;
    }

    function programmeList()
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_intake_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewIntakeHasProgramme($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('intake_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addIntakeHasBranch($data)
    {
        $this->db->trans_start();
        $this->db->insert('intake_has_branch', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_intake_has_programme', $data);
        return TRUE;
    }

    function getIntakeHasBranch($id)
    {
        $this->db->select('tihp.*, p.name as branch_name, p.code as branch_code');
        $this->db->from('intake_has_branch as tihp');
        $this->db->join('organisation_has_training_center as p', 'tihp.id_branch = p.id');     
        $this->db->where('tihp.id_intake', $id);
        $this->db->order_by("p.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeHasProgramme($id)
    {
        $this->db->select('tihp.*, p.name as programme, p.code as program_code');
        $this->db->from('intake_has_programme as tihp');
        $this->db->join('programme as p', 'tihp.id_programme = p.id');     
        $this->db->where('tihp.id_intake', $id);
        $this->db->order_by("p.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getTempIntakeHasProgramme($id_session)
    {
        $this->db->select('tihp.*, p.name as programme, p.code as programme_code');
        $this->db->from('temp_intake_has_programme as tihp');
        $this->db->join('programme as p', 'tihp.id_programme = p.id');     
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_intake_has_programme');
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_intake_has_programme');
    }

    function deleteIntakeHasProgramme($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('intake_has_programme');
       return TRUE;
    }

    function deleteTempIntakeHasProgramme($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_intake_has_programme');
       return TRUE;
    }

    function deleteIntakeTrainingCenter($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('intake_has_branch');
       return TRUE;
    }

    function schemeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('scheme as s');
        $this->db->where('s.status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function intakeHasScheme($id_intake)
    {
        $this->db->select('shn.*, n.description as scheme_name, n.code as scheme_code');
        $this->db->from('intake_has_scheme as shn');
        $this->db->join('scheme as n','shn.id_scheme = n.id');
        $this->db->where('shn.id_intake', $id_intake);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function addIntakeHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('intake_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteIntakeHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('intake_has_scheme');
        return TRUE;
    }
}

