<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_has_dean_model extends CI_Model
{
    function programmeHasDeanList()
    {
        $this->db->select('phd.*,s.salutation, s.name as staff, p.name as programme');
        $this->db->from('programme_has_dean as phd');
        $this->db->join('staff as s','phd.id_staff = s.id');
        $this->db->join('programme as p','phd.id_programme = p.id');
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         //print_r($result);exit();     
         return $result;
    }

    function programmeHasDeanListSearch($formData)
    {
        $this->db->select('phd.*,s.salutation, s.name as staff, p.name as programme');
        $this->db->from('programme_has_dean as phd');
        $this->db->join('staff as s','phd.id_staff = s.id');
        $this->db->join('programme as p','phd.id_programme = p.id');
        if($formData['id_programme']) {
            $likeCriteria = "(phd.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_staff']) {
            $likeCriteria = "(phd.id_staff  LIKE '%" . $formData['id_staff'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         //print_r($result);exit();     
         return $result;
    }

    function getProgrammeHasDean($id)
    {
        $this->db->select('phd.*,s.salutation, s.name as staff, p.name as programme');
        $this->db->from('programme_has_dean as phd');
        $this->db->join('staff as s','phd.id_staff = s.id');
        $this->db->join('programme as p','phd.id_programme = p.id');
        $this->db->where('phd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeHasDeanByProgrammeId($id)
    {
        $this->db->select('phd.*,s.salutation, s.name as staff_name');
        $this->db->from('programme_has_dean as phd');
        $this->db->join('staff as s','phd.id_staff = s.id');
        $this->db->join('programme as p','phd.id_programme = p.id');
        $this->db->where('phd.id_programme', $id);
        $this->db->order_by("s.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
         //print_r($result);exit();     
         return $result;
    }
    
    function addNewProgrammeHasDean($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeHasDean($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_dean', $data);
        return TRUE;
    }

    function deleteProgrammeHasDean($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_dean');
    }
}