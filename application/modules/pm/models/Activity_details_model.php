<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_details_model extends CI_Model
{
    function activityDetailsList()
    {
        $this->db->select('*');
        $this->db->from('activity_details');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function activityDetailsListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('activity_details');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or description  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();       
         return $result;
    }

    function getActivityDetails($id)
    {
        $this->db->select('*');
        $this->db->from('activity_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewActivityDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('activity_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editActivityDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('activity_details', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('activity_details', $countryInfo);
        return $this->db->affected_rows();
    }
}

