<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Documents For Program</h3>
        </div>
        <form id="form_intake" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Documents For Program Details</h4>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Document *</label>
                            <select name="id_document" id="id_document" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($documentsList))
                                {
                                    foreach ($documentsList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $documentsProgram->id_document)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $documentsProgram->description; ?>">
                        </div>
                    </div>

                </div>

            </div>

        <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
        </div>


        </form>



        <form id="form_programme_intake" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Documents For Programme Details</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                  
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    </div>
                </div>

                <div class="row">
                    <div id="view"></div>
                </div>

            </div>
        </form>


           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

        function saveData()
        {

        // if($('#form_programme_intake').valid())
        // {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id'] = <?php echo $idDocumentsProgram;?>;
            $.ajax(
            {
               url: '/setup/documentsProgram/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        // }
    }

    function deleteDocumentsProgram(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/setup/documentsProgram/deleteDocumentsProgram/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    function validateDetailsData()
    {
        if($('#form_intake').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Program to the Intake");
            }
            else
            {
                $('#form_intake').submit();
            }
        }    
    }

    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_program: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function() {
        saveData();
        $("#form_intake").validate({
            rules: {
                id_document: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                id_document: {
                    required: "<p class='error-text'>Select Document</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>