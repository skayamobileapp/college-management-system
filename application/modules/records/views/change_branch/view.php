<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Change Branch</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>
        <form id="form_apply_change_programme" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Student Profile</h4> 
                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $studentDetails->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $studentDetails->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $studentDetails->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $studentDetails->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $studentDetails->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
            </div>








            <div class="form-container">
                <h4 class="form-group-title">Previous Branch Details</h4> 
                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Branch Name :</dt>
                                <dd><?php echo ucwords($oldBranch->name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Branch Code :</dt>
                                <dd><?php echo $oldBranch->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Branch City :</dt>
                                <dd><?php echo $oldBranch->city ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Email :</dt>
                                <dd><?php echo $oldBranch->email ?></dd>
                            </dl>
                            <dl>
                                <dt>Contact Number :</dt>
                                <dd><?php echo $oldBranch->contact_number ?></dd>
                            </dl>
                            <dl>
                                <dt>Zipcode :</dt>
                                <dd><?php echo $oldBranch->zipcode ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
            </div>


            <div class="form-container">
                <h4 class="form-group-title">New Branch Details</h4> 
                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Branch Name :</dt>
                                <dd><?php echo ucwords($newBranch->name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Branch Code :</dt>
                                <dd><?php echo $newBranch->code ?></dd>
                            </dl>
                             <dl>
                                <dt>Branch City :</dt>
                                <dd><?php echo $newBranch->city ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Email :</dt>
                                <dd><?php echo $newBranch->email ?></dd>
                            </dl>
                            <dl>
                                <dt>Contact Number :</dt>
                                <dd><?php echo $newBranch->contact_number ?></dd>
                            </dl>
                            <dl>
                                <dt>Zipcode :</dt>
                                <dd><?php echo $newBranch->zipcode ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
            </div>




            <div class="form-container">
                <h4 class="form-group-title">Change Branch Details</h4> 
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class="error-text">*</span></label>
                            <select name="id_programme" disabled="disabled" id="id_programme" class="form-control"> <option value="">Select</option>
                                <?php
                                if (!empty($programmeList))
                                {
                                    foreach ($programmeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $changeBranch->id_programme)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class="error-text">*</span></label>
                            <select name="id_intake" disabled="disabled" id="id_intake" class="form-control" onchange="getFeeByProgrammeNIntake()">
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $changeBranch->id_intake)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class="error-text">*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $changeBranch->reason; ?>" readonly>
                        </div>
                    </div>   

                    

                </div>

                <div class="row">

                   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class="error-text">*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason" value="<?php if($changeBranch->status == 1){
                             echo 'Approved';
                            }elseif($changeBranch->status == 0)
                            { 
                                echo 'Pending';
                                }elseif($changeBranch->status == 2)
                            { 
                                echo 'Rejected';
                            }
                              ?>" readonly>
                        </div>
                    </div>      

                </div>
            </div>


           



        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    function getFeeByProgrammeNIntake() {


        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
            $.ajax(
            {
               url: '/records/changeBranch/getFeeByProgrammeNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $('#div_fee').modal('hide');
                $('#div_fee_v').modal('show');
                $("#view_fee").html(result);
               }
            });
        
    }


    $(document).ready(function()
    {
        $("#form_apply_change_programme").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                fee:
                {
                    required: true
                },
                reason:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_programme:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                fee:
                {
                    required: "<p class='error-text'>Select Program & Intake For Fee</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>