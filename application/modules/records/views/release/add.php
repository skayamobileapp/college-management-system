<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Release</h3>
        </div>
        <form id="form_release" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Rrelease Details</h4>          
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Semester <span class='error-text'>*</span></label>
                            <select name="id_semester" id="id_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>     

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Barring Type <span class='error-text'>*</span></label>
                            <select name="id_barring_type" id="id_barring_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($barringTypeList))
                                {
                                    foreach ($barringTypeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Student <span class='error-text'>*</span></label>
                            <select name="id_student" id="id_student" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->nric . " - " . $record->full_name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason">
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Release Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="release_date" name="release_date" autocomplete="off">
                        </div>
                    </div>      

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_release").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_barring_type:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                release_date:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_barring_type:
                {
                    required: "<p class='error-text'>Select Barring Type</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                release_date:
                {
                    required: "<p class='error-text'>Select Release Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
  </script>