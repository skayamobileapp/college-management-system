<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Release</h3>
      <a href="add" class="btn btn-primary">+ Add Release</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Reason</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $searchParameters['name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student</label>
                      <div class="col-sm-8">
                        <select name="id_student" id="id_student" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($studentList)) {
                            foreach ($studentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParameters['id_student'])
                              {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo $record->name;  ?>
                                  
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>


                  </div>
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($semesterList)) {
                            foreach ($semesterList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_semester']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>><?php echo $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Barring Type</label>
                      <div class="col-sm-8">
                        <select name="id_barring_type" id="id_barring_type" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($barringTypeList)) {
                            foreach ($barringTypeList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_barring_type']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>><?php echo $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Student</th>
            <th>Semester</th>
            <th>Barring Type</th>
            <th>Reason</th>
            <th>Release Date</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($releaseList)) {
            $i=1;
            foreach ($releaseList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->student ?></td>
                <td><?php echo $record->semester ?></td>
                <td><?php echo $record->barring_type ?></td>
                <td><?php echo $record->reason ?></td>
                 <td><?php echo date("d-m-Y", strtotime($record->release_date)) ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                 <!--  <a class="btn btn-sm btn-danger deleteRole" href="#" data-id="<?php echo $record->stateId; ?>" title="Delete"><i class="fa fa-trash"></i></a> -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();

    function clearSearchForm()
    {
      window.location.reload();
    }
</script>