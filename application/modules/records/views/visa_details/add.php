<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <!-- <h3>Edit Student</h3> -->
        </div>




         <h4 class='sub-title'>Visa Details : Student Passport</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $studentDetails->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $studentDetails->mailing_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $studentDetails->program_code . " - " . $studentDetails->program_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $studentDetails->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $studentDetails->permanent_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Passport Number :</dt>
                                <dd><?php echo $studentDetails->passport_number; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>



    
   <div>
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#tab_1" class="nav-link border rounded text-center"
                    aria-controls="tab_1" aria-selected="true"
                    role="tab" data-toggle="tab">Visa Details</a>
            </li>
            <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center"
                    aria-controls="tab_2" role="tab" data-toggle="tab">Insurance Details</a>
            </li>
            <li role="presentation"><a href="#tab_3" class="nav-link border rounded text-center"
                    aria-controls="tab_3" role="tab" data-toggle="tab">Passport Details</a>
            </li>
        </ul>
        <div class="tab-content offers-tab-content">



        <div role="tabpanel"  class="tab-pane active" id="tab_1">
            <div class="col-12 mt-4">
                <br>

            <form id="form_visa" action="" method="post" enctype="multipart/form-data"> 


                <div class="form-container">
                <h4 class="form-group-title">Visa Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Passport <span class='error-text'>*</span></label>
                                <select class="form-control" id="visa_passport_no" name="visa_passport_no">
                                    <option value="">SELECT</option>
                                    <option value="<?php echo $studentDetails->passport_number; ?>"><?php echo $studentDetails->passport_number; ?></option>
                                </select>
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Visa Type <span class='error-text'>*</span></label>
                                <select class="form-control" id="visa_type" name="visa_type">
                                    <option value="">SELECT</option>
                                    <option value="Student Pass">Student Pass</option>
                                    <option value="Special Pass 1">Special Pass 1</option>
                                    <option value="Special Pass 2">Special Pass 2</option>
                                    <option value="Special Pass 3">Special Pass 3</option>
                                    
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Special Pass Type <span class='error-text'>*</span></label>
                                <select class="form-control" id="special_pass_type" name="special_pass_type">
                                    <option value="">SELECT</option>
                                    <option value="Pending Decision For Student Pass">Pending Decision For Student Pass</option>
                                    <option value="Making Arrangement To Leave The Country">Making Arrangement To Leave The Country</option>
                                    <option value="COM">COM</option>
                                    <option value="Others">Others</option>
                                    
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Special Pass Remarks <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="special_pass_remarks" name="special_pass_remarks" >
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Visa No. <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="visa_number" name="visa_number" >
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Issue <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="date_issue" name="date_issue" autocomplete="false">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Expiry Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="expiry_date" name="expiry_date" autocomplete="false">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Front Page Visa Image</label>
                                <input type="file" class="form-control" id="image_visa_1" name="image_visa_1">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Back Page Visa Image </label>
                                <input type="file" class="form-control" id="image_visa_2" name="image_visa_2">
                            </div>
                        </div>

                    </div>

                </div>

                <br>

                <div class="form-container">
                <h4 class="form-group-title">Special Pass</h4>

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Special Pass Reference No. <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="special_pass_number" name="special_pass_number" >
                                <input type="hidden" name="btn_add" value="1" >
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Issue <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="special_pass_date_issue" name="date_issue" autocomplete="false">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Expiry Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="special_pass_expiry_date" name="expiry_date" autocomplete="false">
                            </div>
                        </div>

                    </div>

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Insurance Coverage <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="insurance_coverage" id="visa2" value="0" >
                                  <span class="check-radio" checked="checked"></span> No
                                </label> 

                                <label class="radio-inline">
                                  <input type="radio" name="insurance_coverage" id="visa1" value="1" >
                                  <span class="check-radio"></span> Yes
                                </label>
                                                             
                            </div>                         
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Front Page Student Pass</label>
                                <input type="file" class="form-control" id="special_pass_image_1" name="special_pass_image_1">
                            </div>
                        </div>
                        
                    </div>

                </div>



                <div class="form-container">
                <h4 class="form-group-title">Reminder</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reminder Type <span class='error-text'>*</span></label>
                                <select class="form-control" id="reminder_type" name="reminder_type">
                                    <option value="">SELECT</option>
                                    <option value="Reminder-Expiry">Reminder-Expiry</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Month(s) Before Expiry <span class='error-text'>*</span></label>
                                <select class="form-control" id="reminder_months_expiry" name="reminder_months_expiry">
                                    <option value="">SELECT</option>
                                    <?php

                                    for ($i=1; $i < 13; $i++)
                                    { 
                                        ?>

                                    <option value="<?php echo $i; ?>"><?php echo $i . " Months"; ?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Template Tag <span class='error-text'>*</span></label>
                                <select name="reminder_template" id="reminder_template" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Reminder-passport Renewal">Reminder-passport Renewal</option>
                                    <option value="Reminder-Dependent passport Renewal">Reminder-Dependent passport Renewal</option>
                                    <!-- <?php
                                    if (!empty($programmeList))
                                    {
                                        foreach ($programmeList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?> -->
                                </select>
                            </div>
                        </div> 

                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="reminder_remarks" name="reminder_remarks" autocomplete="false">
                            </div>
                        </div>
                        
                    </div>

                </div>

            </form>


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateVIsaDetails()">Save</button>
                </div>
            </div>

            <?php 
            if(!empty($visaDetailsList))
            {

            ?>


            <div class="form-container">
            <h4 class="form-group-title">Visa Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Visa Type</th>
                        <th>Visa Number</th>
                        <th>Passport No.</th>
                        <th>Date Of Issue</th>
                        <th>Expire Date</th>
                        <th class="text-center">Front Page Visa</th>
                        <th>Special Pass No</th>
                        <th>Special Pass Type</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($visaDetailsList))
                      {
                        $i=1;
                        foreach ($visaDetailsList as $record)
                        {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->visa_type ?></td>
                            <td><?php echo $record->visa_number ?></td>
                            <td><?php echo $record->passport_no ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->date_issue)) ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->expiry_date)) ?></td>
                            <td class="text-center">
                            <?php 
                            if($record->image_visa_1)
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $record->image_visa_1; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->image_visa_1; ?>)" title="<?php echo $record->image_visa_1; ?>">
                                View
                            </a>
                            <?php
                            }else
                            {
                                echo "<a title='No File '> No File Uploaded </a>";
                            }
                            ?>

                            </td>
                            <td><?php echo $record->special_pass_number ?></td>
                            <td><?php echo $record->special_pass_type ?></td>
                            <td class="text-center">
                                <a onclick="deleteVisaDetails(<?php echo $record->id; ?>)">Delete</a>
                            </td>
                          </tr>
                      <?php
                        $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>

            <?php
            }
            ?>
             
         </div> <!-- END col-12 -->  


        </div>


        <div role="tabpanel" class="tab-pane" id="tab_2">
            <div class="col-12 mt-4">
                <br>

            <form id="form_insurance" action="" method="post" enctype="multipart/form-data"> 


                <div class="form-container">
                <h4 class="form-group-title">Insurance Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Tag To Visa <span class='error-text'>*</span></label>
                                <select name="insurance_id_visa" id="insurance_id_visa" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($visaDetails))
                                    {
                                        foreach ($visaDetails as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->visa_number . "  (" . date('d-m-Y',strtotime($record->visa_expiry_date)) . ") ";?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Insurance Type <span class='error-text'>*</span></label>
                                <select name="insurance_insurance_type" id="insurance_insurance_type" class="form-control">
                                    <option value="">Select</option>
                                    <option value="International">International</option>
                                </select>
                            </div>
                        </div> 

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Insurance Reference No. <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="insurance_reference_no" name="insurance_reference_no" >
                                <input type="hidden" name="btn_add" value="2" >
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Issue <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="insurance_date_issue" name="insurance_date_issue" autocomplete="false">
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Expiry Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="insurance_expiry_date" name="insurance_expiry_date" autocomplete="false">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Cover Letter</label>
                                <input type="file" class="form-control" id="insurance_cover_letter" name="insurance_cover_letter">
                            </div>
                        </div>
                        
                    </div>

                </div>


                <div class="form-container">
                <h4 class="form-group-title">Reminder</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reminder Type <span class='error-text'>*</span></label>
                                <select class="form-control" id="insurance_reminder_type" name="insurance_reminder_type">
                                    <option value="">SELECT</option>
                                    <option value="Reminder-Expiry">Reminder-Expiry</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Month(s) Before Expiry <span class='error-text'>*</span></label>
                                <select class="form-control" id="insurance_reminder_months_expiry" name="insurance_reminder_months_expiry">
                                    <option value="">SELECT</option>
                                    <?php

                                    for ($i=1; $i < 13; $i++)
                                    { 
                                        ?>

                                    <option value="<?php echo $i; ?>"><?php echo $i . " Months"; ?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Template Tag <span class='error-text'>*</span></label>
                                <select name="insurance_reminder_template" id="insurance_reminder_template" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Reminder-passport Renewal">Reminder-passport Renewal</option>
                                    <option value="Reminder-Dependent passport Renewal">Reminder-Dependent passport Renewal</option>
                                    <!-- <?php
                                    if (!empty($programmeList))
                                    {
                                        foreach ($programmeList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?> -->
                                </select>
                            </div>
                        </div> 

                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="insurance_reminder_remarks" name="insurance_reminder_remarks" autocomplete="false">
                            </div>
                        </div>
                        
                    </div>

                </div>



            </form>


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateInsuranceDetails()">Save</button>
                </div>
            </div>

            <?php 
            if(!empty($insuranceDetails))
            {

            ?>


            <div class="form-container">
            <h4 class="form-group-title">Insurance Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Insurance Type</th>
                        <th>Insurance No.</th>
                        <th>Visa No.</th>
                        <th>Date Of Issue</th>
                        <th>Expiry Date</th>
                        <th class="text-center">Front Page Insurance</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($insuranceDetails))
                      {
                        $i=1;
                        foreach ($insuranceDetails as $record)
                        {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->insurance_insurance_type ?></td>
                            <td><?php echo $record->insurance_reference_no ?></td>
                            <td><?php echo $record->visa_number . " - " . date('d-m-Y', strtotime($record->visa_expiry_date)); ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->insurance_date_issue)); ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->insurance_expiry_date)); ?></td>
                            <td class="text-center">
                            <?php 
                            if($record->insurance_cover_letter)
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $record->insurance_cover_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->insurance_cover_letter; ?>)" title="<?php echo $record->insurance_cover_letter; ?>">
                                View
                            </a>
                            <?php
                            }else
                            {
                                echo "<a title='No File '> No File Uploaded </a>";
                            }
                            ?>

                            </td>
                            <td class="text-center">
                                <a onclick="deleteInsuranceDetails(<?php echo $record->id; ?>)">Delete</a>
                            </td>
                          </tr>
                      <?php
                        $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>

            <?php
            }
            ?>
             
         </div> <!-- END col-12 -->  


        </div>



        <div role="tabpanel" class="tab-pane" id="tab_3">
            <div class="col-12 mt-4">
                <br>

            <form id="form_passport" action=""  method="post" enctype="multipart/form-data"> 


                <div class="form-container">
                <h4 class="form-group-title">Passport Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Passport No. <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="passport_no" name="passport_no">
                                <input type="hidden" name="btn_add" value="3" >
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="passport_name" name="passport_name">
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Country Of Issue <span class='error-text'>*</span></label>
                                <select name="passport_country_of_issue" id="passport_country_of_issue" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($countryList))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Issue <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="passport_date_issue" name="passport_date_issue" autocomplete="false">
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Expiry Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="passport_expiry_date" name="passport_expiry_date" autocomplete="false">
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Cover Letter</label>
                                <input type="file" class="form-control" id="passport_cover_letter" name="passport_cover_letter">
                            </div>
                        </div>
                        
                    </div>

                </div>

                <div class="form-container">
                <h4 class="form-group-title">Reminder</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reminder Type <span class='error-text'>*</span></label>
                                <select class="form-control" id="passport_reminder_type" name="passport_reminder_type">
                                    <option value="">SELECT</option>
                                    <option value="Reminder-Expiry">Reminder-Expiry</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Month(s) Before Expiry <span class='error-text'>*</span></label>
                                <select class="form-control" id="passport_reminder_months_expiry" name="passport_reminder_months_expiry">
                                    <option value="">SELECT</option>
                                    <?php

                                    for ($i=1; $i < 13; $i++)
                                    { 
                                        ?>

                                    <option value="<?php echo $i; ?>"><?php echo $i . " Months"; ?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Template Tag <span class='error-text'>*</span></label>
                                <select name="passport_reminder_template" id="passport_reminder_template" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Reminder-passport Renewal">Reminder-passport Renewal</option>
                                    <option value="Reminder-Dependent passport Renewal">Reminder-Dependent passport Renewal</option>
                                    <!-- <?php
                                    if (!empty($programmeList))
                                    {
                                        foreach ($programmeList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?> -->
                                </select>
                            </div>
                        </div> 

                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="passport_reminder_remarks" name="passport_reminder_remarks" autocomplete="false">
                            </div>
                        </div>
                        
                    </div>

                </div>

            </form>


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" name="btn_add" value="3" class="btn btn-primary btn-lg" onclick="validatePassportDetails()">Save</button>
                </div>
            </div>

            <?php 
            if(!empty($passportDetails))
            {

            ?>


            <div class="form-container">
            <h4 class="form-group-title">Passport Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Passport No.</th>
                        <th>Name</th>
                        <th>Country Of Issue</th>
                        <th>Date Of Issue</th>
                        <th>Expiry Date</th>
                        <th class="text-center">Front Page Passport</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($passportDetails))
                      {
                        $i=1;
                        foreach ($passportDetails as $record)
                        {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->passport_number ?></td>
                            <td><?php echo $record->passport_name ?></td>
                            <td><?php echo $record->country ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->passport_date_issue)); ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->passport_expiry_date)); ?></td>
                            <td class="text-center">
                            <?php 
                            if($record->passport_cover_letter)
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $record->passport_cover_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->passport_cover_letter; ?>)" title="<?php echo $record->passport_cover_letter; ?>">
                                View
                            </a>
                            <?php
                            }else
                            {
                                echo "<a title='No File '> No File Uploaded </a>";
                            }
                            ?>

                            </td>
                            <td class="text-center">
                                <a onclick="deletePassportDetails(<?php echo $record->id; ?>)">Delete</a>
                            </td>
                          </tr>
                      <?php
                        $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>

            <?php
            }
            ?>
             
         </div> <!-- END col-12 -->  


        </div>


        



        </div>


      </div>
    </div>

   </div> <!-- END row-->
   

    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>


    </div>
</div>
<script type="text/javascript">
    
    $('select').select2();

    $( function()
    {
        $( ".datepicker" ).datepicker(
        {
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
        });
  });


    function validateVIsaDetails()
    {
        if($('#form_visa').valid())
        {
            $('#form_visa').submit();
        }
    }

  $(document).ready(function() {
        $("#form_visa").validate({
            rules: {
                visa_passport_no: {
                    required: true
                },
                visa_type: {
                    required: true
                },
                special_pass_type: {
                    required: true
                },
                special_pass_remarks: {
                    required: true
                },
                visa_number: {
                    required: true
                },
                date_issue: {
                    required: true
                },
                expiry_date: {
                    required: true
                },
                special_pass_remarks: {
                    required: true
                },
                special_pass_date_issue: {
                    required: true
                },
                special_pass_expiry_date: {
                    required: true
                },
                insurance_coverage: {
                    required: true
                },
                reminder_type: {
                    required: true
                },
                reminder_months_expiry: {
                    required: true
                },
                reminder_template: {
                    required: true
                },
                reminder_remarks: {
                    required: true
                },
                special_pass_number: {
                    required: true
                }
            },
            messages: {
                visa_passport_no: {
                    required: "<p class='error-text'>Select Passport</p>",
                },
                visa_type: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                special_pass_type: {
                    required: "<p class='error-text'> Select Special Pass Type</p>",
                },
                special_pass_remarks: {
                    required: "<p class='error-text'>Special Pass Remarks Required</p>",
                },
                visa_number: {
                    required: "<p class='error-text'>Visa No. Required</p>",
                },
                date_issue: {
                    required: "<p class='error-text'>Select Issue Date</p>",
                },
                expiry_date: {
                    required: "<p class='error-text'>Select Expire Date</p>",
                },
                special_pass_remarks: {
                    required: "<p class='error-text'>Special Type Remarks Required</p>",
                },
                special_pass_date_issue: {
                    required: "<p class='error-text'>Select Pass Date</p>",
                },
                special_pass_expiry_date: {
                    required: "<p class='error-text'>Select Special Pass Expire Date</p>",
                },
                insurance_coverage: {
                    required: "<p class='error-text'>Insurance Coverage Required</p>",
                },
                reminder_type: {
                    required: "<p class='error-text'>Select Reminder Type</p>",
                },
                reminder_months_expiry: {
                    required: "<p class='error-text'>Select Reminder Monts</p>",
                },
                reminder_template: {
                    required: "<p class='error-text'>Select Reminder Template</p>",
                },
                reminder_remarks: {
                    required: "<p class='error-text'>Reminder Remarks Required</p>",
                },
                special_pass_number: {
                    required: "<p class='error-text'>Special Pass No. Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    function validateInsuranceDetails()
    {
        if($('#form_insurance').valid())
        {
            $('#form_insurance').submit();
        }
    }

  $(document).ready(function() {
        $("#form_insurance").validate({
            rules: {
                insurance_id_visa: {
                    required: true
                },
                insurance_insurance_type: {
                    required: true
                },
                insurance_reference_no: {
                    required: true
                },
                insurance_date_issue: {
                    required: true
                },
                insurance_expiry_date: {
                    required: true
                },
                insurance_reminder_type: {
                    required: true
                },
                insurance_reminder_months_expiry: {
                    required: true
                },
                insurance_reminder_template: {
                    required: true
                },
                insurance_reminder_remarks: {
                    required: true
                }
            },
            messages: {
                insurance_id_visa: {
                    required: "<p class='error-text'>Select Visa</p>",
                },
                insurance_insurance_type: {
                    required: "<p class='error-text'>Select Insurance Type</p>",
                },
                insurance_reference_no: {
                    required: "<p class='error-text'>Insurance Reference No. Required</p>",
                },
                insurance_date_issue: {
                    required: "<p class='error-text'>Select Insurance Issue Date</p>",
                },
                insurance_expiry_date: {
                    required: "<p class='error-text'>Select Insurance Expirt Date</p>",
                },
                insurance_reminder_type: {
                    required: "<p class='error-text'>Select Reminder Type</p>",
                },
                insurance_reminder_months_expiry: {
                    required: "<p class='error-text'>Select months Before Expiry</p>",
                },
                insurance_reminder_template: {
                    required: "<p class='error-text'>Select Reminder Template</p>",
                },
                insurance_reminder_remarks: {
                    required: "<p class='error-text'>Reminder Remarks Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    function validatePassportDetails()
    {
        if($('#form_passport').valid())
        {
            $('#form_passport').submit();
        }
    }

  $(document).ready(function() {
        $("#form_passport").validate({
            rules: {
                passport_no: {
                    required: true
                },
                passport_name: {
                    required: true
                },
                passport_country_of_issue: {
                    required: true
                },
                passport_date_issue: {
                    required: true
                },
                passport_expiry_date: {
                    required: true
                },
                passport_reminder_type: {
                    required: true
                },
                passport_number: {
                    required: true
                },
                passport_reminder_months_expiry: {
                    required: true
                },
                passport_reminder_template: {
                    required: true
                },
                passport_reminder_remarks: {
                    required: true
                }
            },
            messages: {
                passport_no: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                passport_name: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                passport_country_of_issue: {
                    required: "<p class='error-text'> Select Special Pass Type</p>",
                },
                passport_date_issue: {
                    required: "<p class='error-text'>Special Pass Remarks Required</p>",
                },
                passport_expiry_date: {
                    required: "<p class='error-text'>Visa No. Required</p>",
                },
                passport_reminder_type: {
                    required: "<p class='error-text'>Select Expire Date</p>",
                },
                passport_number: {
                    required: "<p class='error-text'>Passport Number Required</p>",
                },
                passport_reminder_months_expiry: {
                    required: "<p class='error-text'>Select months Before Expiry</p>",
                },
                passport_reminder_template: {
                    required: "<p class='error-text'>Select Reminder Template</p>",
                },
                passport_reminder_remarks: {
                    required: "<p class='error-text'>Insurance Coverage Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function deleteVisaDetails(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/records/visaDetails/deleteVisaDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }

    function deleteInsuranceDetails(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/records/visaDetails/deleteInsuranceDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    function deletePassportDetails(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/records/visaDetails/deletePassportDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }
  </script>