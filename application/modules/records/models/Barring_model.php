<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Barring_model extends CI_Model
{
    
    function barringList()
    {
        $this->db->select('b.*, stu.full_name as student, sem.code as program_code, sem.name as program_name, bt.name as barring_type');
        $this->db->from('barring as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('programme as sem', 'b.id_program = sem.id');
        $this->db->join('barring_type as bt', 'b.id_barring_type = bt.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function barringListSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, sem.code as program_code, sem.name as program_name, bt.name as barring_type');
        $this->db->from('barring as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('programme as sem', 'b.id_program = sem.id');
        $this->db->join('barring_type as bt', 'b.id_barring_type = bt.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_program']))
        {
            $this->db->where('b.id_program', $formData['id_program']);
        }
        if (!empty($formData['id_student']))
        {
            $this->db->where('b.id_student', $formData['id_student']);
        }
        if (!empty($formData['id_barring_type']))
        {
            $this->db->where('b.id_barring_type', $formData['id_barring_type']);
        }
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getBarring($id)
    {
        $this->db->select('*');
        $this->db->from('barring');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    

    function addNewBarring($data)
    {
        $this->db->trans_start();
        $this->db->insert('barring', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editBarring($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('barring', $data);

        return TRUE;
    }

    function deleteBarring($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, first_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }
    

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }


    function getStudentByProgNIntake($data)
    {
        $this->db->select('DISTINCT(er.id) as id, er.*');
        $this->db->from('student as er');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.id as id_intake, i.name as intake_name, st.ic_no, st.name as advisor_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }


}
