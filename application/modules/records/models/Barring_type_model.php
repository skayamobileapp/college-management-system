<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Barring_type_model extends CI_Model
{
    function barringTypeList()
    {
        $this->db->select('*');
        $this->db->from('barring_type');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function barringTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('barring_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getBarringType($id)
    {
        $this->db->select('*');
        $this->db->from('barring_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewBarringType($data)
    {
        $this->db->trans_start();
        $this->db->insert('barring_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editBarringType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('barring_type', $data);
        return TRUE;
    }
}

