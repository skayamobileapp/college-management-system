<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Timetable_model extends CI_Model
{

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function countryListByActivity($status)
    {
    	$this->db->select('a.*');
        $this->db->from('country as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }


    function staffListByActivity($status)
    {
        $this->db->select('a.*');
        $this->db->from('staff as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

    function editOrganisation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('organisation', $data);
        return TRUE;
    }
}

