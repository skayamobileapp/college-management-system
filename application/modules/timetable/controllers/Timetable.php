<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Timetable extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('timetable_model');
        $this->isLoggedIn();
    }

    function list()
    {
        // if ($this->checkAccess('timetable.list') == 0)
        if ($this->checkAccess('timetable.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_country'] = $this->security->xss_clean($this->input->post('id_country'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['url'] = $this->security->xss_clean($this->input->post('url'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['contact_number'] = $this->security->xss_clean($this->input->post('contact_number'));
 
            $data['searchParam'] = $formData;
            // $data['partnerUniversityList'] = $this->timetable_model->partnerUniversityListSearch($formData);
            // $data['countryList'] = $this->timetable_model->countryListByActivity('1');

            $this->global['pageTitle'] = 'Campus Management System : Timetable List';
            //print_r($subjectDetails);exit;
            $this->loadViews("timetab/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('timetable.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                 $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $permanent_address = $this->security->xss_clean($this->input->post('permanent_address'));
                $correspondence_address = $this->security->xss_clean($this->input->post('correspondence_address'));
                $email = $this->security->xss_clean($this->input->post('email'));

            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'permanent_address' => $permanent_address,
                    'correspondence_address' => $correspondence_address,
                    'email' => $email,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->timetable_model->addNewPartnerUniversity($data);
                redirect('/setup/partnerUniversity/list');
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('timetable_model');
            $data['countryList'] = $this->timetable_model->countryListByActivity('1');
            $this->global['pageTitle'] = 'Campus Management System : Add Partner University';
            $this->loadViews("timetable/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('timetable.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/partnerUniversity/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                 $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $permanent_address = $this->security->xss_clean($this->input->post('permanent_address'));
                $correspondence_address = $this->security->xss_clean($this->input->post('correspondence_address'));
                $email = $this->security->xss_clean($this->input->post('email'));

            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'permanent_address' => $permanent_address,
                    'correspondence_address' => $correspondence_address,
                    'email' => $email,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                
                $result = $this->timetable_model->editPartnerUniversity($data,$id);
                redirect('/setup/partnerUniversity/list');
            }
            $data['countryList'] = $this->timetable_model->countryListByActivity('1');
            $data['partnerUniversity'] = $this->timetable_model->getPartnerUniversity($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Partner University';
            $this->loadViews("timetable/edit", $this->global, $data, NULL);
        }
    }
}
