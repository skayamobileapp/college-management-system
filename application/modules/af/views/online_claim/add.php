<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add AF Online Claim</h3>
        </div>
        <form id="form_data" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">AF Online Claim Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Select</option>
                            <option value="Academic Facilitator" selected>Academic Facilitator</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Academic Facilitator <span class='error-text'>*</span></label>
                       <select name="id_staff" id="id_staff" class="form-control selitemIcon" >
                          <option value="">Select</option>
                          <?php
                             if (!empty($staffList))
                             {
                                 foreach ($staffList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>">
                             <?php echo $record->ic_no . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                 </div>


                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Partner University <span class='error-text'>*</span></label>
                       <select name="id_university" id="id_university" class="form-control selitemIcon"  onchange="getBranchesByPartnerUniversity(this.value)">
                          <option value="">Select</option>
                          <?php
                             if (!empty($organisationList))
                             {
                                 foreach ($organisationList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>">
                             <?php echo $record->code . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                </div> 


              </div>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Learning Center <span class='error-text'>*</span></label>
                       <span id="view_branch">
                           <select class="form-control" id='id_branch' name='id_branch'>
                              <option value=''></option>
                            </select>
                       </span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Programme <span class='error-text'>*</span></label>
                       <select name="id_programme" id="id_programme" class="form-control selitemIcon">
                          <option value="">Select</option>
                          <?php
                             if (!empty($programmeList))
                             {
                                 foreach ($programmeList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>">
                             <?php echo $record->code . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Semester <span class='error-text'>*</span></label>
                       <select name="id_semester" id="id_semester" class="form-control selitemIcon">
                          <option value="">Select</option>
                          <?php
                             if (!empty($semesterList))
                             {
                                 foreach ($semesterList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>">
                             <?php echo $record->code . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" readonly>
                    </div>
                </div>
              

            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" ondblclick="validateDetailsData()" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>

        </form>




        <!-- <h4 class="form-group-title">AF Online Claim Details</h4> -->

        <form id="form_detail" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Claim Details</h4> 

                <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Course <span class='error-text'>*</span></label>
                            <select name="id_course" id="id_course" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($courseList))
                                {
                                    foreach ($courseList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_time" name="date_time">
                        </div>
                    </div>


                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Hours <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="hours" name="hours">
                        </div>
                    </div>





                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Payment Per Hour <span class='error-text'>*</span></label>
                            <select name="id_payment_rate" id="id_payment_rate" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($paymentRateList))
                                {
                                    foreach ($paymentRateList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->learning_mode . " - " . $record->teaching_component . " ( " . $record->amount . " ) " ;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>




                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>


                  


                    <div class="col-sm-3">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addTempClaimDetails()">Add</button>
                    </div>


                </div>

            </div>

        </form>

        <br>

        <div class="clearfix">
            <div id="view">
                
            </div>
        </div>











        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

     $('select').select2();

    $(function()
    {
        $(".datepicker").datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
    });

     function getBranchesByPartnerUniversity(id)
     {
        $.get("/af/onlineClaim/getBranchesByPartnerUniversity/"+id, function(data, status)
        {
          $("#view_branch").html(data);
          $("#view_branch").show();
        });
     }


     function addTempClaimDetails()
    {
        if($('#form_detail').valid())
        {

            var tempPR = {};
            tempPR['id_course'] = $("#id_course").val();
            tempPR['date_time'] = $("#date_time").val();
            tempPR['hours'] = $("#hours").val();
            tempPR['id_payment_rate'] = $("#id_payment_rate").val();
            tempPR['amount'] = $("#amount").val();
                $.ajax(
                {
                   url: '/af/onlineClaim/addTempClaimDetails',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    $("#view").html(result);

                    var ta = $("#total_detail_amount").val();
                    $("#total_amount").val(ta);

                   }
                });
        }
        
    }


    function deleteTempClaimDetails(id)
    {
     $.ajax(
        {
           url: '/af/onlineClaim/deleteTempClaimDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
                $("#view").html(result);
                var ta = $("#total_detail_amount").val();
                $("#total_amount").val(ta);
           }
        });
    }


    function validateDetailsData()
    {
        if($('#form_data').valid())
        {
            console.log($("#view").html());
            var added_detail = $("#view").html();
            if(added_detail == '' || added_detail == undefined)
            {
                alert("Add Details To The Online Claim.");
            }
            else
            {
                $('#form_data').submit();
            }
        }    
    }




    $(document).ready(function(){
        $("#form_detail").validate(
        {
            rules:
            {
                id_course:
                {
                    required: true
                },
                date_time:
                {
                    required: true
                },
                hours:
                {
                    required: true
                },
                id_payment_rate:
                {
                    required: true
                },
                amount:
                {
                    required: true
                }
            },
            messages:
            {
                id_course:
                {
                    required: "<p class='error-text'>Select Course</p>",
                },
                date_time:
                {
                    required: "<p class='error-text'>Select Date</p>",
                },
                hours:
                {
                    required: "<p class='error-text'>Hours Required</p>",
                },
                id_payment_rate:
                {
                    required: "<p class='error-text'>Select Payment Per Hour</p>",
                },
                amount:
                {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function(){
        $("#form_data").validate(
        {
            rules:
            {
                type:
                {
                    required: true
                },
                id_staff:
                {
                    required: true
                },
                id_university:
                {
                    required: true
                },
                id_branch:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                total_amount:
                {
                    required: true
                }
            },
            messages:
            {
                type:
                {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_staff:
                {
                    required: "<p class='error-text'>Select Academic Facilitator</p>",
                },
                id_university:
                {
                    required: "<p class='error-text'>Select Organisation</p>",
                },
                id_branch:
                {
                    required: "<p class='error-text'>Select Learning Center</p>",
                },
                id_programme:
                {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                total_amount:
                {
                    required: "<p class='error-text'>Add Details For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>