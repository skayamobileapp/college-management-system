<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Program Landscape</h3>
      <a href="<?php echo '../add/' . $id_programme; ?>" class="btn btn-primary">+ Add Program Landscape</a>
    </div>
      <br>

      <div class="form-container">
            <h4 class="form-group-title">Program Details</h4>


        <div class="row">

          <div class="col-sm-4">
              <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name; ?>" readonly="readonly">
              </div>
          </div>

          <div class="col-sm-4">
              <div class="form-group">
                  <label>Code</label>
                  <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->code; ?>" readonly="readonly">
              </div>
          </div>

          <div class="col-sm-4">
              <div class="form-group">
                  <label>Name In Other Language</label>
                  <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
              </div>
          </div>

        </div>



        <div class="row">

          <div class="col-sm-4">
              <div class="form-group">
                  <label>Education Level </label>
                  <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->education_level_name; ?>" readonly="readonly">
              </div>
          </div>


        </div>


    </div>

      <br>

      <div class="button-block clearfix">
          <div class="bttn-group">
              <a href="<?php echo '../list' ?>" class="btn btn-link">Back</a>
          </div>
      </div>



    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Landscape Name</th>
            <th>Landscape Code</th>
            <th>Type</th>
            <th>Intake</th>
            <th>Program</th>
            <!-- <th>Scheme</th> -->
            <!-- <th>Learning Mode</th> 
            <th>Min Total Cr. Hrs</th>-->
            <th>Total</th>
            <!-- <th>Min Pass Subjects</th> -->
            <th>Action</th>
            <th class="text-center">Add Course</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (!empty($programmeLandscapeList)) {
              $i=1;
            	foreach ($programmeLandscapeList as $record) {
            		?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->program_landscape_type ?></td>
                <td><?php echo $record->intake_year . " - " . $record->intake ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme ?></td>
                <!-- <td><?php echo $record->scheme_name ?></td> -->
                <!-- <td><?php echo $record->mode_of_program . " - " . $record->mode_of_study ?></td>
                <td><?php echo $record->min_total_cr_hrs ?></td> -->
                <td><?php echo $record->total_semester ?></td>
                <!-- <td><?php echo $record->min_pass_subject ?></td> -->
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo '../edit/' . $record->id . '/' . $id_programme; ?>" title="Edit">Edit</a>
                </td>
                <td class="text-center">
                  <a href="<?php echo '../addcourse/' . $record->id . '/' . $programme->id . '/' . $record->id_intake; ?>" title="Subject Registration">Add</a>
                </td>
              </tr>
          <?php
          $i++;
              }
            }
            ?>
        </tbody>
      </table>
    </div>

    

  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>