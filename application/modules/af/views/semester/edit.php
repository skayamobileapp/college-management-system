<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Semester</h3>
        </div>
        <form id="form_semester" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Semester Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $semesterDetails->code;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $semesterDetails->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language </label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $semesterDetails->name_optional_language; ?>">
                    </div>
                </div>

                
            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Academic Year <span class='error-text'>*</span></label>
                        <select name="id_academic_year" id="id_academic_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($academicYearList))
                            {
                                foreach ($academicYearList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $semesterDetails->id_academic_year)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date("Y-m-d", strtotime($semesterDetails->start_date));?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date("Y-m-d", strtotime($semesterDetails->end_date));?>">
                    </div>
                </div>

                
      
            </div>

            <div class="row">

              <!--   <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Scheme <span class='error-text'>*</span></label>
                        <select name="id_scheme" id="id_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($schemeList))
                            {
                                foreach ($schemeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $semesterDetails->id_scheme)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->description;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="semester_type" id="semester_type" class="form-control">
                             <option value="">Select</option>
                            <option value="<?php echo $semesterDetails->semester_type;?>"
                                <?php 
                                if ($semesterDetails->semester_type == 'Long')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Long";  ?>
                            </option>

                            <option value="<?php echo $semesterDetails->semester_type;?>"
                                <?php 
                                if ($semesterDetails->semester_type == 'Short')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Short";  ?>
                            </option>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Sequence <span class='error-text'>*</span></label>
                        <select name="semester_sequence" id="semester_sequence" class="form-control">
                        <option value="">Select</option>
                        <option value='January' <?php if($semesterDetails->semester_sequence=='January') { echo "selected=selected"; }?> >January</option>
                        <option value='February' <?php if($semesterDetails->semester_sequence=='February') { echo "selected=selected"; }?> >February</option>
                        <option value='March' <?php if($semesterDetails->semester_sequence=='March') { echo "selected=selected"; }?> >March</option>
                        <option value='April' <?php if($semesterDetails->semester_sequence=='April') { echo "selected=selected"; }?> >April</option>
                        <option value='May' <?php if($semesterDetails->semester_sequence=='May') { echo "selected=selected"; }?> >May</option>
                        <option value='June' <?php if($semesterDetails->semester_sequence=='June') { echo "selected=selected"; }?> >June</option>
                        <option value='July' <?php if($semesterDetails->semester_sequence=='July') { echo "selected=selected"; }?> >July</option>
                        <option value='August' <?php if($semesterDetails->semester_sequence=='August') { echo "selected=selected"; }?> >August</option>
                        <option value='September' <?php if($semesterDetails->semester_sequence=='September') { echo "selected=selected"; }?> >September</option>
                        <option value='October' <?php if($semesterDetails->semester_sequence=='October') { echo "selected=selected"; }?> >October</option>
                        <option value='November' <?php if($semesterDetails->semester_sequence=='November') { echo "selected=selected"; }?> >November</option>
                        <option value='December' <?php if($semesterDetails->semester_sequence=='December') { echo "selected=selected"; }?> >December</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Is Countable <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_countable" id="is_countable" value="1" <?php if($semesterDetails->is_countable=='1')
                          {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio">    
                          </span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_countable" id="is_countable" value="0" <?php if($semesterDetails->is_countable=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>  



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Is Special Semester <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="special_semester" id="special_semester" value="1" <?php if($semesterDetails->special_semester=='1')
                          {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio">    
                          </span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="special_semester" id="special_semester" value="0" <?php if($semesterDetails->special_semester=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>  

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($semesterDetails->status=='1')
                          {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio">    
                          </span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($semesterDetails->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>                              
                    </div>                         
                </div>                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>




        <div class="form-container">
            <h4 class="form-group-title"> Semester Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Scheme Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_scheme_nationality" action="" method="post">
                            <div class="form-container">
                                <h4 class="form-group-title">Scheme Details</h4>


                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Scheme <span class='error-text'>*</span></label>
                                            <select name="id_scheme_detail" id="id_scheme_detail" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($schemeList))
                                                {
                                                    foreach ($schemeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->description; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                  
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addSemesterHasScheme()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        <?php

                            if(!empty($semesterHasScheme))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                <th>Scheme</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($semesterHasScheme);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $semesterHasScheme[$i]->scheme_code . " - " . $semesterHasScheme[$i]->scheme_name;?></td>
                                                <td>
                                                <a onclick="deleteSemesterHasScheme(<?php echo $semesterHasScheme[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>







                </div>

            </div>
        </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
  </script>
<script>

    function addSemesterHasScheme()
    {
        if($('#form_scheme_nationality').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme_detail").val();
        tempPR['id_semester'] = <?php echo $semesterDetails->id; ?>;
            $.ajax(
            {
               url: '/setup/semester/addSemesterHasScheme',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                window.location.reload();
                // $("#view").html(result);
               }
            });
        }
    }

    function deleteSemesterHasScheme(id)
    {
        // alert(id);
            $.ajax(
            {
               url: '/setup/semester/deleteSemesterHasScheme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    

    $(document).ready(function()
    {
        $("#form_scheme_nationality").validate({
            rules: {
                id_scheme_detail: {
                    required: true
                }
            },
            messages: {
                id_scheme_detail: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                id_academic_year:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                start_date:
                {
                    required: true
                },
                end_date:
                {
                    required: true
                },
                semester_type:
                {
                    required: true
                },
                semester_sequence:
                {
                    required: true
                },
                id_scheme:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_academic_year:
                {
                    required: "<p class='error-text'>Academic Year Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Start Date Required</p>",
                },
                end_date:
                {
                    required: "<p class='error-text'>End Date Required</p>",
                },
                semester_type:
                {
                    required: "<p class='error-text'>Type Required</p>",
                },
                semester_sequence:
                {
                    required: "<p class='error-text'>Sequence Required</p>",
                },
                id_scheme:
                {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>