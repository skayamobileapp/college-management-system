<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Unit_model extends CI_Model
{
    function unitList()
    {
        $this->db->select('*');
        $this->db->from('unit');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function unitListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('unit');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getUnit($id)
    {
        $this->db->select('*');
        $this->db->from('unit');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewUnit($data)
    {
        $this->db->trans_start();
        $this->db->insert('unit', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editUnit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('unit', $data);
        return TRUE;
    }
}

