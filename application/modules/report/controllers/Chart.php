<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Chart extends BaseController
{
	 public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
    }


     function chart1()
    {
       
            $data['nama'] = 'AEU';
            $this->global['pageTitle'] = 'Report : Student By  intake';
            $this->loadViews("charts/chart1", $this->global, $data, NULL);
        
    }

    function chart2()
    {
       
            $data['nama'] = 'AEU';
            $this->global['pageTitle'] = 'Report : Applicant V/S Lead V/s Student';
            $this->loadViews("charts/chart2", $this->global, $data, NULL);
       
    }

    function chart3()
    {
       
            $data['nama'] = 'AEU';
            $this->global['pageTitle'] = 'Report : Course Wise Data';
            $this->loadViews("charts/chart3", $this->global, $data, NULL);
        
    }

    function chart4()
    {
        $data['nama'] = 'AEU';
        $this->loadViews("charts/chart4", $this->global, $data, NULL);
    }

    function chart5()
    {
        $data['nama'] = 'AEU';
        $this->loadViews("charts/chart5", $this->global, $data, NULL);
    }

    function chart6()
    {
        $data['nama'] = 'AEU';
        $this->loadViews("charts/chart6", $this->global, $data, NULL);
    }
}
