<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hostel_room_model extends CI_Model
{
    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getRoomTypeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function hostelRegistrationListSearch($data)
    {
        $this->db->select('fc.*, c.name as country, s.name as state, st.name as staff_name, st.ic_no');
        $this->db->from('hostel_registration as fc');
        $this->db->join('staff as st', 'fc.id_staff = st.id');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(fc.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_staff'] != '')
        {
            $this->db->where('fc.id_staff', $data['id_staff']);
        }
        if ($data['type'] != '')
        {
            $this->db->where('fc.type', $data['type']);
        }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('fc.id_intake', $data['id_intake']);
        // }

        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function hostelRoomListByLevel($data)
    {
        $this->db->select('hr.*, hrt.code as room_type_code, hrt.name as room_type_name');
        $this->db->from('hostel_room as hr');
        $this->db->join('hostel_room_type as hrt', 'hr.id_room_type = hrt.id');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
         if($data['id_parent'] != '')
        {
            $this->db->where('id_parent', $data['id_parent']);
        }
        if($data['id_hostel'] != '')
        {
            $this->db->where('id_hostel', $data['id_hostel']);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getInventoryAllotmentList($data)
    {
        $this->db->select('ia.*, hir.name as invenroty_name, hir.code as invenroty_code');
        $this->db->from('inventory_allotment ia');
        $this->db->join('hostel_inventory_registration as hir', 'ia.id_inventory = hir.id');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
        if($data['id_room'] != '')
        {
            $this->db->where('id_room', $data['id_room']);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getHostelRegistration($id)
    {
        $this->db->select('fc.*, c.name as country, s.name as state, st.name as staff_name, st.ic_no');
        $this->db->from('hostel_registration as fc');
        $this->db->join('staff as st', 'fc.id_staff = st.id');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function inventoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_inventory_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function getHostelRoom($id)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkHostelRoomDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewHostelRoom($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_room', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editHostelRoom($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_room', $data);
        return TRUE;
    }

    function addInventoryAllotment($data)
    {
        $this->db->trans_start();
        $this->db->insert('inventory_allotment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteInventoryAllotment($id_inventory_allotment)
    {
        $this->db->where('id', $id_inventory_allotment);
        $this->db->delete('inventory_allotment');
        return TRUE;
    }
}

