<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Room_vacancy_model extends CI_Model
{
    function roomVacancyListSearch($data)
    {
        $this->db->select('DISTINCT(hr.id) as id, hr.code as room_code, hr.name as room_name, hr.max_capacity, hrt.code as room_type_code, hrt.name as room_type_name');
        $this->db->from('hostel_room as hr');
        $this->db->join('hostel_room_type as hrt', 'hr.id_room_type = hrt.id');
        // if ($data['id_student'] != '')
        // {
        //     $this->db->where('fc.id_student', $data['id_student']);
        // }
        if ($data['id_hostel'] != '')
        {
            $this->db->where('fc.id_hostel', $data['id_hostel']);
        }
        $this->db->where('hr.level', '3');
        $this->db->where('hr.status', '1');
        // $this->db->order_by("hr.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();   
         //print_r($result);exit();
         $allotments =  array();
         foreach ($results as $result)
         {
            $filled_count = $this->getRoomAllotmentDetailsByRoomId($result->id);
            $result->filled_count = $filled_count;
            $result->vacant_count = $result->max_capacity - $filled_count;
            array_push($allotments, $result);
         }
         //print_r($result);exit();     
         return $allotments;
    }

    function getRoomAllotmentDetailsByRoomId($id_room)
    {
        $this->db->select('hr.*');
        $this->db->from('room_allotment as hr');

        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_room', $id_room);
        $query = $this->db->get();
        $count = $query->num_rows();

        return $count;
    }

    function getRoomByRoomId($id_room)
    {
        $this->db->select('hr.*, hrt.id as id_room_type, hrt.name as room_type_name, hrt.code as room_type_code, hstr.name as hostel_name, hstr.code as hostel_code');
        $this->db->from('hostel_room as hr');
        $this->db->join('hostel_room_type as hrt', 'hr.id_room_type = hrt.id');
        $this->db->join('hostel_registration as hstr', 'hr.id_hostel = hstr.id');
        $this->db->where('hr.id', $id_room);
        $query = $this->db->get();
        $result = $query->row();

        $filled_count = $this->getRoomAllotmentDetailsByRoomId($id_room);
        $result->filled_count = $filled_count;
        $result->vacant_count = $result->max_capacity - $filled_count;

        return $result;

    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function hostelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function hostelList()
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function roomTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function roomTypeList()
    {
        $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function buildingList()
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();
         return $result;
    }

    function buildingListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getRoomAllotmentListByRoomId($id_room)
    {
        $this->db->select('hr.*, stu.nric, stu.full_name, ink.year as intake_year, ink.name as intake_name, p.code as program_code, p.name as program_name, qs.code as qualification_code, qs.name as qualification_name');
        $this->db->from('room_allotment as hr');
        $this->db->join('student as stu', 'hr.id_student = stu.id');
        $this->db->join('intake as ink', 'stu.id_intake = ink.id');
        $this->db->join('programme as p', 'stu.id_program = p.id');
        $this->db->join('qualification_setup as qs', 'stu.id_degree_type = qs.id');
        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_room', $id_room);
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

     function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getRoomAllotmentDetailsByStudentId($id_student)
    {
        $this->db->select('hr.*');
        $this->db->from('room_allotment as hr');

        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_student', $id_student);
        $query = $this->db->get();
        $count = $query->num_rows();

        return $count;
    }

    function getRoomAllotmentByStud($id_student)
    {
        $this->db->select('hr.*, hstr.name as room_name, hstr.code as room_code, hstrt.code as room_type_code, hstrt.name as room_type_name');
        $this->db->from('room_allotment as hr');
        $this->db->join('hostel_room as hstr', 'hr.id_room = hstr.id');
        $this->db->join('hostel_room_type as hstrt', 'hr.id_room_type = hstrt.id');
        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }













    function getRoomAllotment($id)
    {
        $this->db->select('*');
        $this->db->from('room_allotment');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewRoomAllotment($data)
    {
        $this->db->trans_start();
        $this->db->insert('room_allotment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    // function editTaxCode($data, $id)
    // {
    //     $this->db->where('id', $id);
    //     $this->db->update('room_allotment', $data);
    //     return TRUE;
    // }
}

