<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Room Allotment</h3>
        </div>
        <form id="form_grade" action="" method="post">

            <div class="form-container">
              <h4 class="form-group-title">Room Details</h4>
              <div class='data-list'>
                  <div class='row'> 
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Room Name :</dt>
                              <dd><?php echo ucwords($roomDetail->code . " - " . $roomDetail->name);?></dd>
                          </dl>
                          <dl>
                              <dt>Room Type :</dt>
                              <dd><?php echo $roomDetail->code ?></dd>
                          </dl>
                          <dl>
                              <dt>Max Capacity :</dt>
                              <dd><?php echo $roomDetail->max_capacity; ?></dd>
                          </dl>                         
                      </div>        
                      
                      <div class='col-sm-6'>                           
                          <dl>
                              <dt>Hostel :</dt>
                              <dd><?php echo $roomDetail->hostel_name . " - " . $roomDetail->hostel_code ?></dd>
                          </dl>
                          <dl>
                              <dt>Vacant Seats :</dt>
                              <dd><?php echo $roomDetail->max_capacity - $roomDetail->filled_count; ?></dd>
                          </dl>
                          <dl>
                              <dt>Filled Seats :</dt>
                              <dd><?php echo $roomDetail->filled_count; ?></dd>
                          </dl>
                      </div>
                  </div>
              </div>
          </div>




          <div class="form-container">
            <h4 class="form-group-title">Room Allocation Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Allotment Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">
                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Student</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Program </th>
                                        <th>Intake</th>
                                        <th>Qualification Type</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($roomAllocationList))
                                    {
                                         $i=1;
                                        foreach ($roomAllocationList as $record)
                                        {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->nric . " - " . $record->full_name ?></td>
                                            <td><?php echo $record->from_dt ?></td>
                                            <td><?php echo $record->to_dt ?></td>
                                            <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                                            <td><?php echo $record->intake_year . " - " . $record->intake_name ?></td>
                                            <td><?php echo $record->qualification_code . " - " . $record->qualification_name ?></td>
                                            
                                        </tr>
                                    <?php
                                        $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

        </div>












            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->

                <?php 
            if($root == 'allotment')
            {
            ?>
                <a href="../../../roomAllotment/add" class="btn btn-link btn-back">‹ Back</a>

            <?php 
            }elseif($root == 'vacancy')
            {
            ?>
                <a href="../../list" class="btn btn-link btn-back">‹ Back</a>

            <?php 
            }elseif($root == 'vacancy_add')
            {
            ?>
                <a href="<?php echo '../../add/' . $roomDetail->id ?>" class="btn btn-link btn-back">‹ Back</a>
            <?php 
            }
            ?>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
   $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                },
                 percentage: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                percentage: {
                    required: "<p class='error-text'>percentage required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>