<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Room Allotment</h3>
            </div>

        <div class="form-container">
              <h4 class="form-group-title">Room Details</h4>
              <div class='data-list'>
                  <div class='row'> 
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Room Name :</dt>
                              <dd><?php echo ucwords($roomDetail->code . " - " . $roomDetail->name);?></dd>
                          </dl>
                          <dl>
                              <dt>Room Type :</dt>
                              <dd><?php echo $roomDetail->code ?></dd>
                          </dl>
                          <dl>
                              <dt>Max Capacity :</dt>
                              <dd><?php echo $roomDetail->max_capacity; ?></dd>
                          </dl>                         
                      </div>        
                      
                      <div class='col-sm-6'>                           
                          <dl>
                              <dt>Hostel :</dt>
                              <dd><?php echo $roomDetail->hostel_name . " - " . $roomDetail->hostel_code ?></dd>
                          </dl>
                          <dl>
                              <dt>Vacant Seats :</dt>
                              <dd><?php echo $roomDetail->max_capacity - $roomDetail->filled_count; ?></dd>
                          </dl>
                          <dl>
                              <dt>Filled Seats :</dt>
                              <dd><?php echo $roomDetail->filled_count; ?></dd>
                          </dl>
                      </div>
                  </div>
              </div>
          </div>



      <form id="form_pr_entry" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>


            <div class="row">

            
              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakes()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_intake">Intake <span class='error-text'>*</span></label>
                        <span id='view_intake' ></span>
                    </div>
                </div>

                <div class="col-sm-4" style="display: none;" id="display_degree">
                    <div class="form-group">
                        <label >Degree Type <span class='error-text'>*</span></label>
                        <select name="id_degree_type" id="id_degree_type" class="form-control" onchange="getStudentByProgNIntake()" style="width: 398px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($degreeTypeList))
                            {
                                foreach ($degreeTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
              </div>


            </div>

            <div class="row">
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_student">Student <span class='error-text'>*</span></label>
                        <span id='view_student' ></span>
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>From Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                        </div>
                </div>

                <div class="col-sm-4">
                      <div class="form-group">
                          <label>To Date <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off">
                      </div>
                </div>

            </div>

        </div>




        
        <div class="custom-table">
              <div id="view"></div>
        </div>






      <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="formValidate()">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
      </div>

    </form>


        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>



</form>
<script>

    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/hostel/roomAllotment/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                  $("#display_intake").show();
                  $("#display_degree").show();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }


    function getStudentByProgNIntake()
    {

     
        // alert(tempPR);

        if($("#id_programme").val() != '' && $("#id_intake").val() != '' && $("#id_degree_type").val() != '')
        {

          var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_degree_type'] = $("#id_degree_type").val();

            $.ajax(
            {
               url: '/hostel/roomAllotment/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                  $("#display_student").show();
                  $("#view_student").html(result);
                

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
          }
    }

    function displaydata()
    {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_student = $("#id_student").val();
        // alert(id_student);

            $.ajax(
            {
               url: '/hostel/roomVacancy/displaydata',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if (id_programme != '' && id_intake != '')
                {
                  $("#view").html(result);
                  
                  var student_allotment_count = $("#student_allotment_count").val();
                  // alert(student_allotment_count);
                  if(student_allotment_count > 0)
                  {
                     alert('Room Already Alloted For this Student, Allotment Restricted Only One Seat For Student');
                  }else
                  {

                  }
                }
               }

            });        
    }


    function formValidate()
    {
      if($('#form_pr_entry').valid())
      {
        var vacant_seats = $("#vacant_seats").val();
        // alert(vacant_seats);
        if(vacant_seats == 0)
        {
          alert('No Vacancy Available In This Room, Select Another Room');
        }
        else
        {
          $('#form_pr_entry').submit();
        }
      }
    }


    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_programme: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 to_dt: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Type</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select From Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select To Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>