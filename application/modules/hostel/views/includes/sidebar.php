            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/hostel/roomType/list">Room Type</a></li>
                        <li><a href="/hostel/hostelInventory/list">Inventory</a></li>
                        <!-- <li><a href="/hostel/hostelItemRegistration/list">Item Registration</a></li> -->
                        <li><a href="/hostel/hostelRegistration/list">Hostel Registration</a></li>
                        <!-- <li><a href="/hostel/hostelRoom/list">Accommodation Setup</a></li> -->
                    </ul>
                    <h4>Accomodation Setup</h4>
                    <ul>
                        <li><a href="/hostel/buildingSetup/list">Apartment Setup</a></li>
                        <li><a href="/hostel/blockSetup/list">Unit Setup</a></li>
                        <li><a href="/hostel/roomSetup/list">Room Setup</a></li>
                    </ul>
                   <h4>Accomodation Process</h4>
                    <ul>
                        <li><a href="/hostel/roomAllotment/list">Room Allotment</a></li>
                        <li><a href="/hostel/roomAllotment/summaryList">Room Allotment Summary</a></li>
                        <li><a href="/hostel/roomVacancy/list">Room Vacancy</a></li>
                        <!-- <li><a href="/hostel/roomAllotment/summaryList">Allotment Summary</a></li> -->
                    </ul>
                </div>
            </div>