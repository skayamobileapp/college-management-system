<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        
        <form id="form_grade" action="" method="post">

            <div class="page-title clearfix">
            <h3>Hostel Details</h3>
            </div>
            <div class="form-container">
                <h4 class="form-group-title">Hostel Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Hostel Name :</dt>
                                <dd><?php echo ucwords($hostelDetails->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Staff Incharge :</dt>
                                <dd><?php echo $hostelDetails->ic_no . " - " . $hostelDetails->staff_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel Address :</dt>
                                <dd>
                                    <?php echo $hostelDetails->address ?></dd>
                            </dl> 
                            <dl>
                                <dt>Hostel City :</dt>
                                <dd><?php echo $hostelDetails->city ?></dd>
                            </dl>  
                                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Hostel Code :</dt>
                                <dd><?php echo $hostelDetails->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Contact Number :</dt>
                                <dd><?php echo $hostelDetails->contact_number ?></dd>
                            </dl>  
                            <dl>
                                <dt>Hostel Landmark :</dt>
                                <dd><?php echo $hostelDetails->landmark; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel State :</dt>
                                <dd><?php echo $hostelDetails->state; ?></dd>
                            </dl>
                            
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-container">
                <h4 class="form-group-title">Building Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Building Name :</dt>
                                <dd><?php echo ucwords($hostelRoomBuilding->name);?></dd>
                            </dl>   
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Building Code :</dt>
                                <dd><?php echo $hostelRoomBuilding->short_code ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <div class="page-title clearfix">
        <h3>Edit Block / Level</h3>
        </div>

        <div class="form-container">
                <h4 class="form-group-title">Block / Level Details</h4>

            <div class="row">
               

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $hostelRoom->short_code; ?>">
                        <input type="hidden" class="form-control" id="code1" name="code1" value="<?php echo $hostelRoomBuilding->code; ?>">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $hostelRoom->name; ?>">
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($fundCode->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($fundCode->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>       -->
                
            </div>
        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo '../../../level2/' .$hostelRoomBuilding->id . '/' . $hostelDetails->id; ?>" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>

        <div class="page-title clearfix">
        <h3>Inventory Details</h3>
        </div>

        <form id="form_programme_intake" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Inventory Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Inventory <span class='error-text'>*</span></label>
                        <select name="id_inventory" id="id_inventory" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($inventoryList))
                            {
                                foreach ($inventoryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quntity <span class='error-text'>*</span></label>
                        <input type="numer" class="form-control" id="quantity" name="quantity" min="1">
                    </div>
                </div>

              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>

        </div>

         <br>
            <div class="form-container">
                <h4 class="form-group-title">Inventory Allotment List</h4>

             <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                        <th>Inventory</th>
                         <th>Quantity</th>
                         <th style="text-align:center; ">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($inventoryAllotmentList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $inventoryAllotmentList[$i]->invenroty_code . " - " . $inventoryAllotmentList[$i]->invenroty_name;?></td>
                        <td><?php echo $inventoryAllotmentList[$i]->quantity;?></td>
                        <td style="text-align: center;">
                            
                        <a class="btn btn-sm btn-edit" onclick="deleteInventoryAllotment(<?php echo $inventoryAllotmentList[$i]->id ?>)">Delete</a>
                        <!-- <a class="btn btn-sm btn-edit" href="<?php echo '../../level2Edit/' . $hostelRoomList[$i]->id. '/' . $hostelRoomList[$i]->id_parent . '/'. $hostelDetails->id; ?>" title="Edit">Edit</a> -->
                        </td>
                         </tr>
                      <?php 
                  } 
                  ?>
                    </tbody>
                </table>
            </div>
        </div>


    </form>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function saveData()
    {
        if($('#form_programme_intake').valid())
        {

        var tempPR = {};
        tempPR['id_inventory'] = $("#id_inventory").val();
        tempPR['quantity'] = $("#quantity").val();
        tempPR['level'] = 2;
        tempPR['id_room'] = <?php echo $hostelRoom->id ?>;
            $.ajax(
            {
               url: '/hostel/hostelRoom/addInventoryAllotment',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
        }
    }

    function deleteInventoryAllotment(id)
    {
        // alert(id);
        $.ajax(
            {
               url: '/hostel/hostelRoom/deleteInventoryAllotment/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                window.location.reload();
               }
            });

    }


     $('select').select2();

     $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_inventory: {
                    required: true
                },
                 quantity: {
                    required: true
                }
            },
            messages: {
                id_inventory: {
                    required: "<p class='error-text'>Select Inventory</p>",
                },
                quantity: {
                    required: "<p class='error-text'>Quantity Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


   $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>