<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PhdDuration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('phd_duration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_phd_duration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['phdDurationList'] = $this->phd_duration_model->phdDurationListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Research Phd Duration List';
            $this->loadViews("phd_duration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_phd_duration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->phd_duration_model->addNewPhdDuration($data);
                redirect('/research/phdDuration/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Phd Duration';
            $this->loadViews("phd_duration/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_phd_duration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/phdDuration/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );

                $result = $this->phd_duration_model->editPhdDuration($data,$id);
                redirect('/research/phdDuration/list');
            }
            $data['phdDuration'] = $this->phd_duration_model->getPhdDuration($id);

            $this->global['pageTitle'] = 'College Management System : Edit Research Phd Duration';
            $this->loadViews("phd_duration/edit", $this->global, $data, NULL);
        }
    }
}
