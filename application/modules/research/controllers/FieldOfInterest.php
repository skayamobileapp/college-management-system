<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FieldOfInterest extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fieldofinterest_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_fieldofinterest.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            $data['fieldOfInterestList'] = $this->fieldofinterest_model->fieldOfInterestListSearch($formData);
            $this->global['pageTitle'] = 'College Management System : Research Field Of Interest List';
            $this->loadViews("fieldofinterest/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_fieldofinterest.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->fieldofinterest_model->addNewFieldOfInterest($data);
                redirect('/research/fieldOfInterest/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Field Of Interest';
            $this->loadViews("fieldofinterest/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_fieldofinterest.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/fieldOfInterest/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );

                $result = $this->fieldofinterest_model->editFieldOfInterest($data,$id);
                redirect('/research/fieldOfInterest/list');
            }
            $data['fieldOfInterest'] = $this->fieldofinterest_model->getFieldOfInterest($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Field Of Interest';
            $this->loadViews("fieldofinterest/edit", $this->global, $data, NULL);
        }
    }
}
