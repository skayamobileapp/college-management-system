<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MileStone extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mile_stone_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_mile_stone.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['order'] = $this->security->xss_clean($this->input->post('order'));

            $data['searchParameters'] = $formData;
            $data['mileStoneList'] = $this->mile_stone_model->mileStoneListSearch($formData);
            
            $this->global['pageTitle'] = 'Campus Management System : Mile Stone List';
            $this->loadViews("mile_stone/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_mile_stone.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $order = $this->security->xss_clean($this->input->post('order'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'order' => $order,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->mile_stone_model->addNewMileStone($data);
                redirect('/research/mileStone/list');
            }



            $this->global['pageTitle'] = 'Campus Management System : Add Mile Stone';
            $this->loadViews("mile_stone/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_mile_stone.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/mileStone/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $order = $this->security->xss_clean($this->input->post('order'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'order' => $order,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->mile_stone_model->editMileStone($data,$id);
                redirect('/research/mileStone/list');
            }

            $data['mileStone'] = $this->mile_stone_model->getMileStone($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Mile Stone';
            $this->loadViews("mile_stone/edit", $this->global, $data, NULL);
        }
    }
}
