<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Activity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('activity_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_activity.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['activityList'] = $this->activity_model->activityListSearch($name);
            $this->global['pageTitle'] = 'College Management System : Research Activity List';
            $this->loadViews("activity/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_activity.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->activity_model->addNewActivity($data);
                redirect('/research/activity/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Activity';
            $this->loadViews("activity/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_activity.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/activity/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );

                $result = $this->activity_model->editActivity($data,$id);
                redirect('/research/activity/list');
            }
            $data['activity'] = $this->activity_model->getActivity($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Activity';
            $this->loadViews("activity/edit", $this->global, $data, NULL);
        }
    }
}
