<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MileStoneToSemester extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mile_stone_semester_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('mile_stone_semester.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_stage'] = $this->security->xss_clean($this->input->post('id_stage'));
            $formData['id_mile_stone'] = $this->security->xss_clean($this->input->post('id_mile_stone'));

            $data['searchParam'] = $formData;
            $data['mileStoneSemesterList'] = $this->mile_stone_semester_model->mileStoneSemesterListSearch($formData);

            $data['stageList'] = $this->mile_stone_semester_model->stageListByStatus('1');
            $data['mileStoneList'] = $this->mile_stone_semester_model->mileStoneListByStatus('1');
            
            $this->global['pageTitle'] = 'Campus Management System : Mile Stone Tag Semester List';
            $this->loadViews("mile_stone_semester/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('mile_stone_semester.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_stage = $this->security->xss_clean($this->input->post('id_stage'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_mile_stone = $this->security->xss_clean($this->input->post('id_mile_stone'));
           
                $data = array(
                    'type' => $type,
                    'id_stage' => $id_stage,
                    'id_semester' => $id_semester,
                    'id_mile_stone' => $id_mile_stone,
                    'status' => 1,
                    'created_by' => $id_user
                );

                $result = $this->mile_stone_semester_model->addNewMileStoneSemester($data);
                redirect('/research/mileStoneToSemester/list');
            }

            $data['stageList'] = $this->mile_stone_semester_model->stageListByStatus('1');
            $data['mileStoneList'] = $this->mile_stone_semester_model->mileStoneListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Mile Stone Tag Semester';
            $this->loadViews("mile_stone_semester/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('mile_stone_semester.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/mileStoneToSemester/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_stage = $this->security->xss_clean($this->input->post('id_stage'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_mile_stone = $this->security->xss_clean($this->input->post('id_mile_stone'));
           
                $data = array(
                    'type' => $type,
                    'id_stage' => $id_stage,
                    'id_semester' => $id_semester,
                    'id_mile_stone' => $id_mile_stone,
                    'status' => 1,
                    'updated_by' => $id_user
                );

                $result = $this->mile_stone_semester_model->editMileStoneSemester($data,$id);
                redirect('/research/mileStoneToSemester/list');
            }

            $data['mileStoneSemester'] = $this->mile_stone_semester_model->getMileStoneSemester($id);
            $data['stageList'] = $this->mile_stone_semester_model->stageListByStatus('1');
            $data['mileStoneList'] = $this->mile_stone_semester_model->mileStoneListByStatus('1');

            // echo "<Pre>";print_r($data['mileStoneSemester']);exit();


            $this->global['pageTitle'] = 'Campus Management System : Edit Mile Stone Tag Semester';
            $this->loadViews("mile_stone_semester/edit", $this->global, $data, NULL);
        }
    }

    function getStageByType()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        

        $data = $this->mile_stone_semester_model->getStageByType($tempData['type']);

        // echo "<Pre>"; print_r($data);exit;
         $table="
        <script type='text/javascript'>

        $('select').select2();

            
        </script>


        <select name='id_stage' id='id_stage' class='form-control' onchange='getSemesterByStage()'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $data[$i]->id;
        $name = $data[$i]->name;

        $table.="<option value=".$id.">". $name.
                "</option>";

        }
        $table.="</select>";

        echo $table;exit;

    }

    function getSemesterByStage()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        

        $data = $this->mile_stone_semester_model->getSemesterStage($tempData);

        // echo "<Pre>"; print_r($data);exit;
         $table="
        <script type='text/javascript'>

        $('select').select2();

            
        </script>


        <select name='id_semester' id='id_semester' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $data[$i]->id;
        $name = $data[$i]->name;

        $table.="<option value=".$id.">". $name.
                "</option>";

        }
        $table.="</select>";

        echo $table;exit;
    }
}
