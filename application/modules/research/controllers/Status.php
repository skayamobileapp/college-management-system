<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Status extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('status_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));

            $data['searchParam'] = $formData;

            $data['statusList'] = $this->status_model->statusListSearch($formData);

            $data['programList'] = $this->status_model->programListByStatus('1');
            $data['courseList'] = $this->status_model->courseListByStatus('1');

            $this->global['pageTitle'] = 'College Management System : Research Status List';
            $this->loadViews("status/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_status.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $is_pass = $this->security->xss_clean($this->input->post('is_pass'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_program' => $id_program,
                    'id_course' => $id_course,
					'is_pass' => $is_pass,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->status_model->addNewStatus($data);
                redirect('/research/status/list');
            }

            $data['programList'] = $this->status_model->programListByStatus('1');
            $data['courseList'] = $this->status_model->courseListByStatus('1');

            $this->global['pageTitle'] = 'College Management System : Add Research Status';
            $this->loadViews("status/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_status.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/status/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $is_pass = $this->security->xss_clean($this->input->post('is_pass'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_program' => $id_program,
                    'id_course' => $id_course,
                    'is_pass' => $is_pass,
                    'status' => $status
                );

                $result = $this->status_model->editStatus($data,$id);
                redirect('/research/status/list');
            }
            $data['status'] = $this->status_model->getStatus($id);

            $data['programList'] = $this->status_model->programListByStatus('1');
            $data['courseList'] = $this->status_model->courseListByStatus('1');

            $this->global['pageTitle'] = 'College Management System : Edit Research Status';
            $this->loadViews("status/edit", $this->global, $data, NULL);
        }
    }
}
