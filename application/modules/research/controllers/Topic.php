<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Topic extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('topic_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('topic.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_topic_category'] = $this->security->xss_clean($this->input->post('id_topic_category'));

            $data['searchParam'] = $formData;
            $data['topicList'] = $this->topic_model->topicListSearch($formData);
            $data['researchCategoryList'] = $this->topic_model->researchTopicCategoryListByStatus('1');

            // echo "<Pre>";print_r($data['topicList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Topic List';
            $this->loadViews("topic/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('topic.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $id_topic_category = $this->security->xss_clean($this->input->post('id_topic_category'));
                $max_candidates = $this->security->xss_clean($this->input->post('max_candidates'));
                $assignment_requirement = $this->security->xss_clean($this->input->post('assignment_requirement'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'id_topic_category' => $id_topic_category,
                    'max_candidates' => $max_candidates,
                    'assignment_requirement' => $assignment_requirement,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->topic_model->addNewTopic($data);
                if($inserted_id)
                {
                    $moved = $this->topic_model->moveTempToDetails($inserted_id);
                }

                redirect('/research/topic/list');
            }
            else
            {
                $this->topic_model->deleteTempTopicHasSupervisorBySessionId($id_session);
                
            }
            $data['researchCategoryList'] = $this->topic_model->researchTopicCategoryListByStatus('1');
            $data['staffList'] = $this->topic_model->staffListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Topic';
            $this->loadViews("topic/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('topic.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/topic/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_topic_category = $this->security->xss_clean($this->input->post('id_topic_category'));
                $max_candidates = $this->security->xss_clean($this->input->post('max_candidates'));
                $assignment_requirement = $this->security->xss_clean($this->input->post('assignment_requirement'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'id_topic_category' => $id_topic_category,
                    'max_candidates' => $max_candidates,
                    'assignment_requirement' => $assignment_requirement,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->topic_model->editTopicDetails($data,$id);
                redirect('/research/topic/list');
            }
            $data['topic'] = $this->topic_model->getTopic($id);
            $data['topicHasSupervisor'] = $this->topic_model->getTopicHasSupervisor($id);

            $data['staffList'] = $this->topic_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->topic_model->researchTopicCategoryListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : Edit Topic';
            // echo "<Pre>";print_r($data);exit;
            $this->loadViews("topic/edit", $this->global, $data, NULL);
        }
    }

    function tempAddTopicHasSupervisor()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->topic_model->tempAddTopicHasSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->topic_model->getTempTopicHasSupervisorBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Supervisor</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $staff_name</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempTopicHasSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempTopicHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempTopicHasSupervisor($id)
    {
        $inserted_id = $this->topic_model->deleteTempTopicHasSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displaytempdata();
            echo $data;  
        }
        
        // echo "Success"; 
    }

    function addTopicHasSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->topic_model->addNewTopicHasSupervisors($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteTopicHasSupervisor($id)
    {
        $inserted_id = $this->topic_model->deleteTopicHasSupervisor($id);
        echo "Success"; 
    }
}
