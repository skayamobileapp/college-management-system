<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Sco extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sco_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if($this->checkAccess('research_sco.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));

            $data['searchParam'] = $formData;

            $data['scoList'] = $this->sco_model->scoListSearch($formData);

            $data['supervisorList'] = $this->sco_model->supervisorListByStatus('1');

            // echo "<Pre>";print_r($data['scoList']);exit();

            $this->global['pageTitle'] = 'College Management System : List TOC Reporting';
            $this->loadViews("sco/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if($this->checkAccess('research_sco.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($id == null)
            {
                redirect('/research/scot/list');
            }
            $data['sco'] = $this->sco_model->getSco($id);
            $data['scoReportingComments'] = $this->sco_model->scoCommentsDetails($id);

            $data['organisationDetails'] = $this->sco_model->getOrganisation();
            $data['supervisor'] = $this->sco_model->getSupervisor($data['sco']->id_supervisor);
            $data['studentDetails'] = $this->sco_model->getStudentByStudentId($data['sco']->id_student);
                
            // echo "<Pre>"; print_r($data['scoCommentsDetails']);exit;

            $this->global['pageTitle'] = 'College Management System : View TOC Reporting';
            $this->loadViews("sco/edit", $this->global, $data, NULL);
        }
    }
}