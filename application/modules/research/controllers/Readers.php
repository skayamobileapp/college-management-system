<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Readers extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('readers_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_readers.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['readersList'] = $this->readers_model->readersListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Research Readers List';
            $this->loadViews("readers/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_readers.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->readers_model->addNewReaders($data);
                redirect('/research/readers/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Readers';
            $this->loadViews("readers/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_readers.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/readers/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'status' => $status
                );

                $result = $this->readers_model->editReaders($data,$id);
                redirect('/research/readers/list');
            }
            $data['readers'] = $this->readers_model->getReaders($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Readers';
            $this->loadViews("readers/edit", $this->global, $data, NULL);
        }
    }
}
