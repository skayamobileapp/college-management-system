<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Supervisor extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('supervisor_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_supervisor.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['supervisorList'] = $this->supervisor_model->supervisorListSearch($formData);

            // echo "<Pre>"; print_r($data['supervisorList']);exit;


            $this->global['pageTitle'] = 'College Management System : Research Supervisor List';
            $this->loadViews("supervisor/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_supervisor.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();
                // $full_name = $this->security->xss_clean($this->input->post('full_name'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_specialisation = $this->security->xss_clean($this->input->post('id_specialisation'));
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $contact_no = $this->security->xss_clean($this->input->post('contact_no'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                if($type == 1)
                {
                    $salutation = '';
                    $full_name = '';
                    $first_name = '';
                    $address = '';
                    $last_name = '';
                    $contact_no = '';

                }
                elseif($type == 0)
                {
                    $id_staff = 0;
                }

                if($end_date)
                {
                   $end_date = date('Y-m-d', strtotime($end_date));
                }
                else
                {
                    $end_date = '';
                }

                if($start_date)
                {
                   $start_date = date('Y-m-d', strtotime($start_date));
                }
                else
                {
                    $start_date = '';
                }

                if($salutation)
                {
                    $salutationDetails = $this->supervisor_model->getSalutationDetail($salutation);
                }


                

                if($type == 0)
                {
                    $salutationDetails = $this->supervisor_model->getSalutationDetail($salutation);

                    $data = array(
                        'type' =>$type,
                        'id_staff' => $id_staff,
                        'id_specialisation' => $id_specialisation,
                        'salutation' => $salutation,
                        'full_name' => $salutationDetails->name . ". " . $first_name . " " . $last_name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        // 'email' => $email,
                        // 'password' => md5($password),
                        'contact_no' => $contact_no,
                        'address' => $address,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => 1
                    );
                }
                elseif($type == 1)
                {
                    $staffDetails = $this->supervisor_model->getStaffDetail($id_staff);

                    $data = array(
                        'type' =>$type,
                        'id_staff' => $id_staff,
                        'id_specialisation' => $id_specialisation,
                        'salutation' => $salutation,
                        'full_name' => $staffDetails->name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'email' => $email,
                        'password' => md5($password),
                        'contact_no' => $contact_no,
                        'address' => $address,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => 1
                    );
                }
                // echo "<Pre>"; print_r($data);exit;

                $inserted_id = $this->supervisor_model->addNewSupervisor($data);

                if($inserted_id)
                {
                    $moved = $this->supervisor_model->moveTempToDetails($inserted_id);
                }

                redirect('/research/supervisor/list');
            }
            else
            {
                $deleted_crash = $this->supervisor_model->deleteTempSpecialisationToSupervisorBySessionId($id_session);
            }
            
            $data['staffList'] = $this->supervisor_model->staffListByStatus('1');
            $data['salutationList'] = $this->supervisor_model->salutationListByStatus('1');
            $data['specialisationList'] = $this->supervisor_model->specialisatoinListByStatus('1');



            $this->global['pageTitle'] = 'College Management System : Add Research Supervisor';
            $this->loadViews("supervisor/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_supervisor.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/supervisor/list');
            }
            if($this->input->post())
            {
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_specialisation = $this->security->xss_clean($this->input->post('id_specialisation'));
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $contact_no = $this->security->xss_clean($this->input->post('contact_no'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                if($type == 1)
                {
                    $salutation = '';
                    $full_name = '';
                    $first_name = '';
                    $address = '';
                    $last_name = '';
                    $contact_no = '';

                }
                elseif($type == 0)
                {
                    $id_staff = 0;
                }

                if($end_date)
                {
                   $end_date = date('Y-m-d', strtotime($end_date));
                }
                else
                {
                    $end_date = '';
                }

                if($start_date)
                {
                   $start_date = date('Y-m-d', strtotime($start_date));
                }
                else
                {
                    $start_date = '';
                }

                if($salutation)
                {
                    $salutationDetails = $this->supervisor_model->getSalutationDetail($salutation);
                }


                

                if($type == 0)
                {
                    $salutationDetails = $this->supervisor_model->getSalutationDetail($salutation);

                    $data = array(
                        'type' =>$type,
                        'id_staff' => $id_staff,
                        'id_specialisation' => $id_specialisation,
                        'salutation' => $salutation,
                        'full_name' => $salutationDetails->name . ". " . $first_name . " " . $last_name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        // 'email' => $email,
                        // 'password' => md5($password),
                        'contact_no' => $contact_no,
                        'address' => $address,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => 1
                    );
                }
                elseif($type == 1)
                {
                    $staffDetails = $this->supervisor_model->getStaffDetail($id_staff);

                    $data = array(
                        'type' =>$type,
                        'id_staff' => $id_staff,
                        'id_specialisation' => $id_specialisation,
                        'salutation' => $salutation,
                        'full_name' => $staffDetails->name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        // 'email' => $email,
                        // 'password' => md5($password),
                        'contact_no' => $contact_no,
                        'address' => $address,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => 1
                    );
                }

                $result = $this->supervisor_model->editSupervisor($data,$id);
                redirect('/research/supervisor/list');
            }

            $data['staffList'] = $this->supervisor_model->staffListByStatus('1');
            $data['salutationList'] = $this->supervisor_model->salutationListByStatus('1');
            $data['specialisationList'] = $this->supervisor_model->specialisatoinListByStatus('1');;
            
            $data['supervisor'] = $this->supervisor_model->getSupervisor($id);
            $data['specialisationListBySupervisor'] = $this->supervisor_model->getSpecialisationToSupervisorByIdSupervisor($id);
            // $data['studentListBySupervisor'] = $this->supervisor_model->getStudentListByIdSupervisor($id);

            // echo "<Pre>";print_r($data['specialisationListBySupervisor']);exit;


            $this->global['pageTitle'] = 'College Management System : Edit Research Supervisor';
            $this->loadViews("supervisor/edit", $this->global, $data, NULL);
        }
    }

    function viewStudents($id)
    {
        if ($this->checkAccess('research_supervisor.view_student') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/supervisor/list');
            }
            if($this->input->post())
            {
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_specialisation = $this->security->xss_clean($this->input->post('id_specialisation'));
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $contact_no = $this->security->xss_clean($this->input->post('contact_no'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                if($type == 1)
                {
                    $salutation = '';
                    $full_name = '';
                    $first_name = '';
                    $address = '';
                    $last_name = '';
                    $email = '';
                    $password = '';
                    $contact_no = '';

                }
                elseif($type == 0)
                {
                    $id_staff = 0;
                }

                if($end_date)
                {
                   $end_date = date('Y-m-d', strtotime($end_date));
                }
                else
                {
                    $end_date = '';
                }

                if($start_date)
                {
                   $start_date = date('Y-m-d', strtotime($start_date));
                }
                else
                {
                    $start_date = '';
                }

                if($salutation)
                {
                    $salutationDetails = $this->supervisor_model->getSalutationDetail($salutation);
                }


                

                if($type == 0)
                {
                    $salutationDetails = $this->supervisor_model->getSalutationDetail($salutation);

                    $data = array(
                        'type' =>$type,
                        'id_staff' => $id_staff,
                        'id_specialisation' => $id_specialisation,
                        'salutation' => $salutation,
                        'full_name' => $salutationDetails->name . ". " . $first_name . " " . $last_name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        // 'email' => $email,
                        // 'password' => $password,
                        'contact_no' => $contact_no,
                        'address' => $address,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => 1
                    );
                }
                elseif($type == 1)
                {
                    $staffDetails = $this->supervisor_model->getStaffDetail($id_staff);

                    $data = array(
                        'type' =>$type,
                        'id_staff' => $id_staff,
                        'id_specialisation' => $id_specialisation,
                        'salutation' => $salutation,
                        'full_name' => $staffDetails->name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        // 'email' => $email,
                        // 'password' => $password,
                        'contact_no' => $contact_no,
                        'address' => $address,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => 1
                    );
                }

                $result = $this->supervisor_model->editSupervisor($data,$id);
                redirect('/research/supervisor/list');
            }

            $data['staffList'] = $this->supervisor_model->staffListByStatus('1');
            $data['salutationList'] = $this->supervisor_model->salutationListByStatus('1');
            $data['specialisationList'] = $this->supervisor_model->specialisatoinListByStatus('1');;
            
            $data['supervisor'] = $this->supervisor_model->getSupervisor($id);
            $data['specialisationListBySupervisor'] = $this->supervisor_model->getSpecialisationToSupervisorByIdSupervisor($id);
            $data['studentListBySupervisor'] = $this->supervisor_model->getStudentListByIdSupervisor($id);

            // echo "<Pre>";print_r($data['specialisationListBySupervisor']);exit;


            $this->global['pageTitle'] = 'College Management System : View Supervisor';
            $this->loadViews("supervisor/view_students", $this->global, $data, NULL);
        }
    }

    function tempAddSpecialisationToSupervisor()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->supervisor_model->tempAddSpecialisationToSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displaySpecialisationSupervisor();
        
        echo $data;        
    }

    function displaySpecialisationSupervisor()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->supervisor_model->getTempSpecialisationToSupervisorBySession($id_session); 
        
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Specialization</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $specialization = $temp_details[$i]->specialization;

                    // if($status == 1)
                    // {
                    //     $status = 'Active';
                    // }else
                    // {
                    //     $status = 'In-Active';
                    // }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>                       
                            <td>$specialization</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempSpecialisationSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProposalHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempSpecialisationSupervisor($id)
    {
        $inserted_id = $this->supervisor_model->deleteTempSpecialisationSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displaySpecialisationSupervisor();
            echo $data;  
        }
    }



    function addSpecialisationSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->supervisor_model->addNewSpecialisationToSupervisor($tempData);
        echo "<Pre>";print_r($inserted_id);exit();

        echo "success";exit;
    }

    function deleteSpecialisationSupervisor($id)
    {
        $inserted_id = $this->supervisor_model->deleteSpecialisationSupervisor($id);
        echo "Success"; 
    }
}
