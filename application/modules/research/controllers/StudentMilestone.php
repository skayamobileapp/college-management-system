<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentMilestone extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_milestone_model');
        $this->isLoggedIn();
    }

    function add()
    {
        if ($this->checkAccess('student_duration_tagging.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;
                
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $formData = $this->input->post();
                
                $id_student = $this->security->xss_clean($this->input->post('id_student'));



                for($i=0;$i<count($formData['id_student']);$i++)
                {
                    $id_student = $formData['id_student'][$i];

                    if($id_student > 0)
                    {
                        $student = $this->student_milestone_model->getStudent($id_student);

                        // echo "<Pre>"; print_r($student);exit;
                        if($student)
                        {
                            $current_deliverable = $student->current_deliverable;
                        


                            $old_deliverable_month = date('M', strtotime($current_deliverable));
                            $old_deliverable_month_integer = date('m', strtotime($current_deliverable));
                            $old_deliverable_year = date('Y', strtotime($current_deliverable));
                            $new_deliverable_year = $old_deliverable_year;

                            // echo "<Pre>"; print_r($old_deliverable_month_integer);exit;
                            


                            if($old_deliverable_month_integer > 6)
                            {
                                $new_deliverable_month_integer = $old_deliverable_month_integer + 6;
                                $new_deliverable_month_integer = fmod($new_deliverable_month_integer, 12);

                                $new_deliverable_year = $old_deliverable_year + 1;
                            }
                            elseif($old_deliverable_month_integer <= 6)
                            {
                                $new_deliverable_month_integer = $old_deliverable_month_integer + 6;

                            }
                            // echo "<Pre>"; print_r($new_deliverable_month_integer);exit;


                            // switch ($old_deliverable_month)
                            // {
                            //     case 'Jun':
                            //         $old_deliverable_month = 'Dec';
                            //         break;
                            //     case 'Dec':
                            //         $old_deliverable_month = 'Jun';
                            //         $old_deliverable_year = $old_deliverable_year + 1;
                            //         break;
                                
                            //     default:
                            //         # code...
                            //         break;
                            // }

                            // echo "<Pre>"; print_r($new_deliverable_month_integer);exit;


                            $months = array (1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec');
                            $new_deliverable_month = $months[(int)$new_deliverable_month_integer];




                            // $new_deliverable_month = date('M', strtotime($new_deliverable_month_integer));
                            

                            // echo "<Pre>"; print_r($new_deliverable_year);exit;


                            $new_current_deliverable = $new_deliverable_month . '-' . $new_deliverable_year;

                            // echo "<Pre>"; print_r($new_current_deliverable);exit;

                            $data = array(
                                'current_deliverable'=>$new_current_deliverable,
                                'phd_duration'=>$student->phd_duration + 1,
                                'updated_by'=>$id_user
                            );

                        // echo "<Pre>"; print_r($data);exit;

                        $updated_student_data = $this->student_milestone_model->updateStudent($data,$id_student);

                            if($updated_student_data)
                            {
                                $data_advisor = array(
                                'old_deliverable'=>$current_deliverable,
                                'new_deliverable'=>$new_current_deliverable,
                                'id_student'=>$id_student,
                                'old_phd_duration'=>$student->phd_duration,
                                'new_phd_duration'=> $student->phd_duration + 1,
                                'status'=> 1,
                                'created_by'=>$id_user
                                );
                                
                        // echo "<Pre>"; print_r($data_advisor);exit;
                                
                                $added_advisor_data = $this->student_milestone_model->addNewDurationHistory($data_advisor);
                            
                            }
                        }
                    }
                }
                
                redirect($_SERVER['HTTP_REFERER']);
            }


            $data['intakeList'] = $this->student_milestone_model->intakeListByStatus('1');
            $data['programList'] = $this->student_milestone_model->programListByStatus('1');
            $data['semesterList'] = $this->student_milestone_model->semesterListByStatus('1');
            $data['staffList'] = $this->student_milestone_model->staffListByStatus('1');
            $data['qualificationList'] = $this->student_milestone_model->qualificationListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Advisor Taagging';
            $this->loadViews("student_milestone/add", $this->global, $data, NULL);
        }
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $staffList = $this->student_milestone_model->staffListByStatus('1');
        
        $student_data = $this->student_milestone_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         // $table = "

         // <script type='text/javascript'>
         //     $('select').select2();
         // </script>

         // <h4>Supervisor Tagging For Students</h4>

         // <div class='row'>
         //    <div class='col-sm-4'>
         //        <div class='form-group'>
         //        <label>Select Supervisor </label>
         //        <select name='id_student' id='id_student' class='form-control'>";
         //    $table.="<option value=''>Select</option>";

         //    for($i=0;$i<count($staffList);$i++)
         //    {

         //    // $id = $results[$i]->id_procurement_category;
         //    $id = $staffList[$i]->id;
         //    $type = $staffList[$i]->type;
         //    if($type != '' && $type == 0)
         //    {
         //        $type = 'External';

         //    }elseif($type == 1)
         //    {
         //        $type = 'Internal';
         //    }
         //    $full_name = $staffList[$i]->full_name;
         //    $table.="<option value=".$id.">".$type . " - " . $full_name .
         //            "</option>";

         //    }
         //    $table .="
         //        </select>
         //        </div>
         //      </div>

         //    </div>
         //    ";


         $table = "
         <br>
         <h4> Select Students For Duration Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Duration</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Supervisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $type = $student_data[$i]->type;
                $advisor_name = $student_data[$i]->advisor_name;
                $phd_duration = $student_data[$i]->phd_duration;

                if($type != '' && $type == 0)
                {
                    $type = 'External';

                }elseif($type == 1)
                {
                    $type = 'Internal';
                }

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>
                    <td>$nric</td>
                    <td>$phd_duration</td>
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                           
                    <td>$qualification_name - $qualification_name</td>                           
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$type - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}