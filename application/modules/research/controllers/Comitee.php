<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Comitee extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('comitee_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_comitee.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['comiteeList'] = $this->comitee_model->comiteeListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Proposal Defense Comitee List';
            $this->loadViews("comitee/list", $this->global, $data, NULL);
        }
    }

    function updateStatusList()
    {
        if ($this->checkAccess('research_comitee.update_status_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['comiteeList'] = $this->comitee_model->comiteeListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Proposal Defense Comitee List';
            $this->loadViews("comitee/update_list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_comitee.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $id_supervisor = $this->security->xss_clean($this->input->post('id_supervisor'));
                $reader_one = $this->security->xss_clean($this->input->post('reader_one'));
                $reader_two = $this->security->xss_clean($this->input->post('reader_two'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'year' => $year,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'chairman' => $chairman,
                    'id_supervisor' => $id_supervisor,
                    'reader_one' => $reader_one,
                    'reader_two' => $reader_two,
                    'status' => $status
                );

                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->comitee_model->addNewComitee($data);
                redirect('/research/comitee/list');
            }

            $data['supervisorList'] = $this->comitee_model->supervisorListByStatus('1');
            $data['readerList'] = $this->comitee_model->readerListByStatus('1');
                
            // echo "<Pre>"; print_r($data['supervisorList']);exit;

            $this->global['pageTitle'] = 'College Management System : Add Proposal Defense Comitee';
            $this->loadViews("comitee/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_comitee.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/comitee/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $id_supervisor = $this->security->xss_clean($this->input->post('id_supervisor'));
                $reader_one = $this->security->xss_clean($this->input->post('reader_one'));
                $reader_two = $this->security->xss_clean($this->input->post('reader_two'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'year' => $year,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'chairman' => $chairman,
                    'id_supervisor' => $id_supervisor,
                    'reader_one' => $reader_one,
                    'reader_two' => $reader_two,
                    'status' => $status
                );

                $result = $this->comitee_model->editComitee($data,$id);
                redirect('/research/comitee/list');
            }
            $data['supervisorList'] = $this->comitee_model->supervisorListByStatus('1');
            $data['readerList'] = $this->comitee_model->readerListByStatus('1');
            
            $data['comitee'] = $this->comitee_model->getComitee($id);

            $this->global['pageTitle'] = 'College Management System : Edit Proposal Defense Comitee';
            $this->loadViews("comitee/edit", $this->global, $data, NULL);
        }
    }

    function addStudent($id)
    {
        if ($this->checkAccess('research_comitee.add_student') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));

                for($i=0;$i<count($id_student);$i++)
                {

                    $data = array(
                        'id_comitee' => $id,
                        'id_student' => $id_student[$i],
                        'by_student' => 0,
                        'date_time' => date('Y-m-d h:i:s'),
                        'status' => 1,
                        'created_by' => $id_user
                    );

                    $result = $this->comitee_model->addComiteeInterest($data);

                }
                redirect('/research/comitee/list');
            }

            $data['supervisorList'] = $this->comitee_model->supervisorListByStatus('1');
            $data['readerList'] = $this->comitee_model->readerListByStatus('1');
            $data['comitee'] = $this->comitee_model->getComitee($id);
            $data['comiteeStudentList'] = $this->comitee_model->getComiteeStudentList($id);
                
            // echo "<Pre>"; print_r($data['comiteeStudentList']);exit;

            $this->global['pageTitle'] = 'College Management System : Add Proposal Defense Comitee';
            $this->loadViews("comitee/add_student", $this->global, $data, NULL);
        }
    }


    function updateStatus($id)
    {
        if ($this->checkAccess('research_comitee.update_student') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_comitee_registration = $this->security->xss_clean($this->input->post('id_comitee_registration'));
                $id_research_status = $this->security->xss_clean($this->input->post('id_research_status'));

                for($i=0;$i<count($id_research_status);$i++)
                {

                    $data = array(
                        'id_research_status' => $id_research_status[$i],
                        'updated_by' => $id_user
                    );

                    $result = $this->comitee_model->updateComiteeInterest($data, $id_comitee_registration[$i]);

                }
                redirect('/research/comitee/updateStatusList');
            }

            $data['supervisorList'] = $this->comitee_model->supervisorListByStatus('1');
            $data['readerList'] = $this->comitee_model->readerListByStatus('1');
            $data['researchStatusList'] = $this->comitee_model->researchStatusListByStatus('1');

            $data['comitee'] = $this->comitee_model->getComitee($id);
            $data['comiteeStudentList'] = $this->comitee_model->getComiteeStudentList($id);
                
            // echo "<Pre>"; print_r($data['comiteeStudentList']);exit;

            $this->global['pageTitle'] = 'College Management System : Update Defense Comitee Student Status';
            $this->loadViews("comitee/update_status", $this->global, $data, NULL);
        }
    }



    function searchStudentsByData()
    {


        $tempData = $this->security->xss_clean($this->input->post('tempData'));
         // echo "<Pre>"; print_r($tempData);exit();

        $temp_details = $this->comitee_model->searchStudentsByData($tempData);

        // echo "<Pre>"; print_r($temp_details);exit();



        $table = "

        <h4 class='sub-title'>Select Student For Colloquium Registration</h4>

        <div class='custom-table'><table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Email Id</th>
                    <th>NRIC</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th>Program Scheme</th>
                    <th>DOB</th>
                    <th class='text-center'>Select Student</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $j = $i+1;

                    $id_student = $temp_details[$i]->id;
                    $full_name = $temp_details[$i]->full_name;
                    $nric = $temp_details[$i]->nric;
                    $phone = $temp_details[$i]->phone;
                    $email_id = $temp_details[$i]->email_id;
                    $gender = $temp_details[$i]->gender;
                    $program_scheme = $temp_details[$i]->program_scheme;
                    $date_of_birth = $temp_details[$i]->date_of_birth;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;
                    $intake_name = $temp_details[$i]->intake_name;


                    if($date_of_birth)
                    {
                        $date_of_birth = date('d-m-Y', strtotime($date_of_birth));
                    }


                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$email_id</td>
                            <td>$nric</td>
                            <td>$phone</td>
                            <td>$gender</td>
                            <td>$program_code - $program_name</td>
                            <td>$intake_name</td>
                            <td>$program_scheme</td>
                            <td>$date_of_birth</td>
                            <td class='text-center'><input type='checkbox' name='id_student[]' id='id_student' value='$id_student'></td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }
}
