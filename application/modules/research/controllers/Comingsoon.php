<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Comingsoon extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('activity_model');
        $this->isLoggedIn();
    }

    function list()
    {
        $data['msz'] = 'Coming Soon';
        $this->global['pageTitle'] = 'College Management System : Coming Soon';
        $this->loadViews("comingsoon/list", $this->global, $data, NULL);
    }
}