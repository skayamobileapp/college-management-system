<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SubmittedDeliverables extends BaseController
{
    public function __construct()
    {
        // echo "<Pre>";print_r('sasa');exit();
        parent::__construct();
        $this->load->model('submitted_deliverables_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('submitted_deliverables.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            $data['searchParam'] = $formData;

            $data['statusList'] = $this->submitted_deliverables_model->researchStatusListByStatus('1');
            $data['topicList'] = $this->submitted_deliverables_model->topicListByStatus('1');
            $data['supervisorList'] = $this->submitted_deliverables_model->supervisorListByStatus('1');
            
            $data['deliverablesList'] = $this->submitted_deliverables_model->getDeliverablesListSearch($formData);

            // echo "<Pre>";print_r($formData);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Submitted Deliverables';
            $this->loadViews("submitted_deliverables/list", $this->global, $data, NULL);
        }
    }
    
    function edit($id = NULL)
    {
        if ($this->checkAccess('submitted_deliverables.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/research/submittedDeliverables/list');
            }

            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason,
                    'approved_on' => date('Y-m-d')
                );

                $result = $this->submitted_deliverables_model->editDeliverables($data,$id);
                redirect('/research/submittedDeliverables/list');
             }

            $data['deliverables'] = $this->submitted_deliverables_model->getDeliverables($id);

            $data['durationList'] = $this->submitted_deliverables_model->durationListByStatus('1');
            $data['chapterList'] = $this->submitted_deliverables_model->chapterListByStatus('1');
            $data['topicList'] = $this->submitted_deliverables_model->topicListByStatus('1');
                
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Deliverables Form';
            $this->loadViews("submitted_deliverables/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('submitted_deliverables.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/research/submittedDeliverables/list');
            }
            $data['deliverables'] = $this->submitted_deliverables_model->getDeliverables($id);
            $data['supervisor'] = $this->submitted_deliverables_model->getSupervisor($data['deliverables']->id_supervisor);
            $data['studentDetails'] = $this->submitted_deliverables_model->getStudentByStudentId($data['deliverables']->id_student);
            

            $data['organisationDetails'] = $this->submitted_deliverables_model->getOrganisation();
            $data['durationList'] = $this->submitted_deliverables_model->durationListByStatus('1');
            $data['chapterList'] = $this->submitted_deliverables_model->chapterListByStatus('1');
            $data['topicList'] = $this->submitted_deliverables_model->topicListByStatus('1');
                
            // echo "<Pre>"; print_r($data['studentDetails']);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Deliverables Form';
            $this->loadViews("submitted_deliverables/view", $this->global, $data, NULL);
        }
    }

    function getChapterByDuration($id_duration)
    {
        $results = $this->submitted_deliverables_model->getChapterByDuration($id_duration);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_chapter' id='id_chapter' class='form-control' onchange='getTopicByData()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getTopicByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->submitted_deliverables_model->getTopicByData($tempData);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_topic' id='id_topic' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->topic;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

