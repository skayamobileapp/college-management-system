<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Advisory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('advisory_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_advisory.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['advisoryList'] = $this->advisory_model->advisoryListSearch($formData);
            
            // echo "<Pre>"; print_r($data['advisoryList']);exit;
            
            $this->global['pageTitle'] = 'College Management System : Research Advisory List';
            $this->loadViews("advisory/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_advisory.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $course = $this->security->xss_clean($this->input->post('course'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'course' => $course,
                    'status' => $status
                );

                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->advisory_model->addNewAdvisory($data);
                redirect('/research/advisory/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Advisory';
            $this->loadViews("advisory/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_advisory.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/advisory/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $course = $this->security->xss_clean($this->input->post('course'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'course' => $course,
                    'status' => $status
                );

                $result = $this->advisory_model->editAdvisory($data,$id);
                redirect('/research/advisory/list');
            }
            $data['advisory'] = $this->advisory_model->getAdvisory($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Advisory';
            $this->loadViews("advisory/edit", $this->global, $data, NULL);
        }
    }
}
