<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Chapter extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('chapter_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_chapter.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['chapterList'] = $this->chapter_model->chapterListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Research Chapter List';
            $this->loadViews("chapter/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_chapter.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->chapter_model->addNewChapter($data);
                redirect('/research/chapter/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Chapter';
            $this->loadViews("chapter/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_chapter.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/chapter/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );

                $result = $this->chapter_model->editChapter($data,$id);
                redirect('/research/chapter/list');
            }
            $data['chapter'] = $this->chapter_model->getChapter($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Chapter';
            $this->loadViews("chapter/edit", $this->global, $data, NULL);
        }
    }
}
