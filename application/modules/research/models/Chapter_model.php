<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Chapter_model extends CI_Model
{
    function chapterList()
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function chapterListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getChapter($id)
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewChapter($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_chapter', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editChapter($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_chapter', $data);
        return TRUE;
    }
}

