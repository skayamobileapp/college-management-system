<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Readers_model extends CI_Model
{
    function readersList()
    {
        $this->db->select('*');
        $this->db->from('research_readers');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function readersListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_readers');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReaders($id)
    {
        $this->db->select('*');
        $this->db->from('research_readers');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewReaders($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_readers', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editReaders($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_readers', $data);
        return TRUE;
    }
}

