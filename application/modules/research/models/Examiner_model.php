<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Examiner_model extends CI_Model
{
    function examinerList()
    {
        $this->db->select('*');
        $this->db->from('research_examiner');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function examinerListSearch($data)
    {
        $this->db->select('re.*, s.name as staff_name, s.ic_no');
        $this->db->from('research_examiner as re');
        $this->db->join('staff as s', 're.id_staff = s.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(re.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where("re.type", $data['type']);
            // $likeCriteria = "(re.full_name  = '%" . $data['type'] . "%')";
            // $this->db->where($likeCriteria);
        }
        $this->db->order_by("re.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStaff($id_staff)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('id', $id_staff);
        $query = $this->db->get();
        return $query->row();
    }

    function getExaminer($id)
    {
        $this->db->select('*');
        $this->db->from('research_examiner');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExaminer($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_examiner', $data);
        return TRUE;
    }
}

