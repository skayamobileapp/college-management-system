<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Supervisor_model extends CI_Model
{
    function supervisorList()
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function specialisatoinListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_interest');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function supervisorListSearch($data)
    {
        $this->db->select('re.*, s.name as staff_name, s.ic_no');
        $this->db->from('research_supervisor as re');
        $this->db->join('staff as s', 're.id_staff = s.id','left');
        // $this->db->join('research_interest as ri', 're.id_specialisation = ri.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(re.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("re.id", "DESC");
         $query = $this->db->get();
         $results = $query->result();

        // echo "<Pre>"; print_r($results);exit;

         $details = array();
         foreach ($results as $result)
         {
            $id_supervisor = $result->id;
        // echo "<Pre>"; print_r($id_supervisor);exit;

            $result->count = 0;

            $number = $this->getStudentCountByIdSupervisor($id_supervisor);
        // echo "<Pre>"; print_r($number);exit;

            if($number > 0)
            {
                $result->count = $number;
            }

            array_push($details, $result);
         }
         return $details;
    }

    function getStudentCountByIdSupervisor($id_supervisor)
    {
        // echo "<Pre>"; print_r($id_supervisor);exit;
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_supervisor', $id_supervisor);
        $query = $this->db->get();
        $result = $query->num_rows();
         return $result;
    }

    function getStudentListByIdSupervisor($id_supervisor)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_supervisor', $id_supervisor);
        $query = $this->db->get();
        $result = $query->result();
         return $result;
    }

    function getSupervisor($id)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_supervisor', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSupervisor($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_supervisor', $data);
        return TRUE;
    }

    function getSalutationDetail($id)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStaffDetail($id)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function tempAddSpecialisationToSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_supervisor_has_specialization', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempSpecialisationToSupervisorBySession($id_session)
    {
        $this->db->select('tihp.*,ri.name as specialization');
        $this->db->from('temp_supervisor_has_specialization as tihp');
        $this->db->join('research_interest as ri', 'tihp.id_specialisation = ri.id');
        // $this->db->join('research_interest as p', 'tihp.id_staff = p.id');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempSpecialisationSupervisor($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_supervisor_has_specialization');
        return TRUE;
    }

    function moveTempToDetails($id_supervisor)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempSpecialisationToSupervisorBySession($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            unset($result->specialization);

            $result->id_supervisor = $id_supervisor;
            
            $this->addNewSpecialisationToSupervisor($result);
        }

        $deleted = $this->deleteTempSpecialisationToSupervisorBySessionId($id_session);
        return $deleted;
    }

    function addNewSpecialisationToSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('supervisor_has_specialization', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempSpecialisationToSupervisorBySessionId($id)
    {
        $this->db->where('id_session', $id);
        $this->db->delete('temp_supervisor_has_specialization');
        return TRUE;
    }

    function deleteSpecialisationSupervisor($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('supervisor_has_specialization');
        return TRUE;
    }

    function getSpecialisationToSupervisorByIdSupervisor($id_supervisor)
    {
        $this->db->select('tihp.*,ri.name as specialization');
        $this->db->from('supervisor_has_specialization as tihp');
        $this->db->join('research_interest as ri', 'tihp.id_specialisation = ri.id');
        // $this->db->join('research_interest as p', 'tihp.id_staff = p.id');
        $this->db->where('tihp.id_supervisor', $id_supervisor);
        $query = $this->db->get();
        return $query->result();
    }
}