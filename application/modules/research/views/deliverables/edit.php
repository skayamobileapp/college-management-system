<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Deliverables</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Deliverables Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Topic <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="topic" name="topic" value="<?php echo $deliverables->topic; ?>">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chapter <span class='error-text'>*</span> </label>
                        <select class="form-control" id="id_chapter" name="id_chapter">
                            <option value="">Select</option>
                            <?php
                            if (!empty($chapterList))
                            {
                                foreach ($chapterList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php if($deliverables->id_chapter == $record->id)
                                    {
                                        echo 'selected'; 
                                    }
                                    ?>>
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phd Duration <span class='error-text'>*</span> </label>
                        <select class="form-control" id="id_phd_duration" name="id_phd_duration">
                            <option value="">Select</option>
                            <?php
                            if (!empty($durationList))
                            {
                                foreach ($durationList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php if($deliverables->id_phd_duration == $record->id)
                                    {
                                        echo 'selected'; 
                                    }
                                    ?>>
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>






            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($deliverables->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($deliverables->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                chairman: {
                    required: true
                },
                id_supervisor: {
                    required: true
                },
                reader_one: {
                    required: true
                },
                reader_two: {
                    required: true
                }
            },
            messages: {
                chairman: {
                    required: "<p class='error-text'>Chairman Required</p>",
                },
                id_supervisor: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                reader_one: {
                    required: "<p class='error-text'>Select Reader One</p>",
                },
                reader_two: {
                    required: "<p class='error-text'>Select Reader Two</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>