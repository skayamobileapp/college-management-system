<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Research Articleship</h3>
            </div>

    <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Research Articleship Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getPreviousProgramSchemeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <span id='view_program_intake'></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student <span class='error-text'>*</span></label>
                        <span id='view_students_list'></span>
                    </div>
                </div>     

            </div>

            <br>
            
            <div id="view_student_details">
            </div>

            <br>


            <div class="row">

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester of Commencement <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_semester" name="id_semester">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                    </div>
                </div>


            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Require Assistance <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" name="assistance" id="assistance" value="0" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="assistance" id="assistance" value="1"><span class="check-radio"></span> Yes
                        </label>                 
                    </div>                         
                </div>


            </div>

        </div>



        <div class="form-container">
            <h4 class="form-group-title">Organisation Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_name" name="company_name">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="designation" name="designation">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_person" name="contact_person">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_country" name="id_country" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label> State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div>

            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>


            


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phone No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="phone_number" name="phone_number">
                    </div>
                </div>


            


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fax <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fax" name="fax">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Are You an employee here? <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" name="is_employee" id="is_employee" value="0" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_employee" id="is_employee" value="1"><span class="check-radio"></span> Yes
                        </label>                 
                    </div>                         
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year Of Service (Years)</label>
                        <select class="form-control" id="id_research_topic" name="id_research_topic">
                            <option value="">Select</option>
                            <option value="Below 1">Below 1</option>
                            <option value="1-4">1-4</option>
                            <option value="5-10">5-10</option>
                            <option value="Above 10">Above 10</option>
                        </select>
                    </div>
                </div>

            </div>

        </div>

        
    </form>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateProgram()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>






    <div class="form-container">
            <h4 class="form-group-title"> Articleship Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Supervisor Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Examiner Details</a>
                    </li>

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_supervisor" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Supervisor Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff" name="id_staff">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($staffList))
                                            {
                                                foreach ($staffList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff_role" name="id_staff_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($supervisorRoleList))
                                            {
                                                foreach ($supervisorRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="0"><span class="check-radio"></span> In-Active
                                        </label>                              
                                    </div>                         
                                </div> -->


                            </div>


                            <div class="row">
                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchArticleshipHasSupervisor()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_supervisor"></div>
                    </div>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_examiner" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Examiner Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner" name="id_examiner">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerList))
                                            {
                                                foreach ($examinerList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php
                                                    if($record->type == 1)
                                                    {
                                                        echo $record->ic_no . " - " . $record->staff_name;
                                                    }
                                                    else
                                                    {
                                                        echo "External - " . $record->full_name;
                                                    }
                                                    ?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner_role" name="id_examiner_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerRoleList))
                                            {
                                                foreach ($examinerRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="0"><span class="check-radio"></span> Inactive
                                        </label>                              
                                    </div>                         
                                </div> -->

                            </div>


                            <div class="row">

                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchArticleshipHasExaminer()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_examiner"></div>
                    </div>




                        </div> 
                    </div>




                </div>

            </div>
        

    </div>









    

         
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    function getPreviousProgramSchemeByProgramId(id)
    {
        $.get("/research/articleship/getIntakeByProgramme/"+id, function(data, status)
        {
            $("#view_program_intake").html(data);
            $("#view_program_intake").show();
        });
    }


    function getStudentByData()
    {
        // alert('sas');
        var id_programme = $("#id_programme").val();
        var id_intake = $("#id_intake").val();

         

        if(id_programme != '' && id_intake != '')
        {

        var tempPR = {};
       
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();


            $.ajax(
            {
                url: '/research/articleship/getStudentByData',
                type: 'POST',
                data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_students_list").html(result);
               }
            });
        }
    }



     function getStudentByStudentId(id)
     {

         $.get("/research/articleship/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }


    function getStateByCountry(id)
    {
        $.get("/research/articleship/getStateByCountry/"+id, function(data, status)
        {
            $("#view_state").html(data);
        });
    }


    function tempAddResearchArticleshipHasSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_staff_role'] = $("#id_staff_role").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/research/articleship/tempAddResearchArticleshipHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_supervisor").html(result);
               }
            });
        }
    }

    function deleteTempResearchArticleshipHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/articleship/deleteTempResearchArticleshipHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_supervisor").html(result);
               }
            });
    }



    function tempAddResearchArticleshipHasExaminer()
    {
        if($('#form_examiner').valid())
        {

        var tempPR = {};
        tempPR['id_examiner'] = $("#id_examiner").val();
        tempPR['id_examiner_role'] = $("#id_examiner_role").val();
        // tempPR['status'] = $("#examiner_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/research/articleship/tempAddResearchArticleshipHasExaminer',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_examiner").html(result);
               }
            });
        }
    }

    function deleteTempResearchArticleshipHasExaminer(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/articleship/deleteTempResearchArticleshipHasExaminer/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_examiner").html(result);
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_staff: {
                    required: true
                },
                id_staff_role: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_staff_role: {
                    required: "<p class='error-text'>Select Supervisor Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_examiner").validate({
            rules: {
                id_examiner: {
                    required: true
                },
                id_examiner_role: {
                    required: true
                }
            },
            messages: {
                id_examiner: {
                    required: "<p class='error-text'>Select Examiner</p>",
                },
                id_examiner_role: {
                    required: "<p class='error-text'>Select Examiner Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                id_intake: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                company_name: {
                    required: true
                },
                designation: {
                    required: true
                },
                contact_person: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                email: {
                    required: true
                },
                phone_number: {
                    required: true
                },
                fax: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Designation Required</p>",
                },
                contact_person: {
                    required: "<p class='error-text'>Contact Person</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                phone_number: {
                    required: "<p class='error-text'>Phone No. Required</p>",
                },
                fax: {
                    required: "<p class='error-text'>Fax. Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateProgram() {

    if($('#form_programme').valid())
      {
        console.log($("#view_supervisor").html());
        var dataSupervisor = $("#view_supervisor").html();
        var dataExaminer = $("#view_examiner").html();
        if(dataSupervisor=='')
        {
            alert("Add Supervisor To Research Articleship");
        }
        else if(dataExaminer=='')
        {
            alert("Add Examiner To Research Articleship");
        }else
        {
            $('#form_programme').submit();
        }
      }     
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>