
                    <h4>Postgraduate Admission</h4>
                    <ul>
                        <li><a href="/research/applicant/list">Application listing by programme structure</a></li>
                        <li><a href="/research/student/approvalList">Offer and Acceptance Status</a></li>
                        <li><a href="/research/studentRecord/list">Student Records</a></li>
                    </ul>
                    <!--<h4> Postgraduate Students Records by Programme  Structure (Structure I, II and III)</h4>
                    <ul>
                         <li><a href="/research/comingsoon/list">Application listing by programme structure</a></li>
                        <li><a href="/research/comingsoon/list">Offer and Acceptance Status</a></li> 
                    </ul> -->
                    <h4>Stage 1</h4>
                    <ul>
                        <li><a href="/research/advisorTagging/add">Tag Students to Academic Advisor</a></li>
                        <li><a href="/research/comingsoon/list">Registration List & Status</a></li>
                        <li><a href="/research/courseRegistration/list"></a></li>
                        <h5 style="background-color: #b5eff5;padding:5px;">Audit Course Registration</h5>
                        <!-- <li><a href="#" style="background-color: #b5eff5;padding:5px;">Colloquium Management</a> -->
                            <ul>
                                <li><a href="/research/courseRegistration/list">Registration</a></li>
                                <li><a href="/research/courseRegistration/addAttendance">Attendance</a></li>
                            </ul>
                        <!-- </li> -->
                        <h5 style="background-color: #b5eff5;padding:5px;">Colloquium Management</h5>
                        <!-- <li><a href="#" style="background-color: #b5eff5;padding:5px;">Colloquium Management</a> -->
                        <ul>
                            <li><a href="/research/colloquium/list">Colloquium List</a></li>
                            <li><a href="/research/colloquium/registrationList">Registration and attendance</a></li>
                        </ul>
                        <!-- </li> -->
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Research proposal</a>
                            <ul>
                                <li><a href="/research/comingsoon/list">Student Listing & Research Proposal Status</a></li>
                                 <li><a href="/research/comingsoon/list">Research Progress Report (Chapter 1 and 2)</a></li>
                            </ul>
                        </li>
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Proposal Defense</a>
                        <ul>
                            <li><a href="/research/comitee/list">Assign Proposal Defense Committee</a></li>
                            <li><a href="/research/comitee/updateStatusList">Student Listing and Status</a></li>
                            <li><a href="/research/comingsoon/list">Assign Proposal Defense Date</a></li>
                            <li><a href="/research/proposal/list">Proposal Defense Result update</a></li>
                        </ul>
                        </li>
                       
                       
                       
                    </ul>

                    <h4>Stage 2</h4>
                    <ul>
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Data Collection and Analysis Report</a>
                            <ul>
                        <li><a href="/research/comingsoon/list">Listing of students and their research progress report (Chapter 3)</a></li>
                    </ul>
                    </ul>

                    <h4>Stage 3</h4>
                    <ul>
                        <li><a href="/research/comingsoon/list">Thesis Writing Report</a></li>
                        <li><a href="/research/comingsoon/list"> Listing of students and their research progress report (Chapter 4)</a></li>
                    </ul>

                    <h4>Stage 4</h4>
                    <ul>
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Notice of Thesis submission </a>
                              <ul>
                                <li><a href="/research/comingsoon/list">Notice submission form</a></li>
                        <li><a href="/research/comingsoon/list">TOC (document upload)</a></li>

                        <li><a href="/research/comingsoon/list">Abstract (Document upload)</a></li>
                              </ul>
                         </li>                     
                        
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Examiners committee set-up</a>
                             <ul>
                        <li><a href="/research/comingsoon/list">Chairman</a></li>
                        <li><a href="/research/examiner/internalList">Internal examiner</a></li>
                        <li><a href="/research/examiner/internalList">External examiner</a></li>

                             </ul>
                         </li>
                            <li><a href="/research/comingsoon/list">Dissertation Examination Date set-up</a></li>
                            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Submission of Unbound Copies</a>
                                <ul>
                            <li><a href="/research/comingsoon/list">Submission status</a></li>
                            <li><a href="/research/comingsoon/list">Postgraduate School Clearance Update</a></li>

                                </ul>

                            </li>
 
                   
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Submission status checklist</a>
                            <ul>
                                <li><a href="/research/comingsoon/list">5 copies of hard bound submission</a></li>
                                <li><a href="/research/comingsoon/list">1 softcopy of final thesis in PDF</a></li>
                                <li><a href="/research/comingsoon/list">1 similarity report (Turnitin report)</a></li>
                                <li><a href="/research/comingsoon/list"> Powerpoint presentation</a></li>
                                <h5>Submission Receipt</h5>
                                <li>
                                    <ul>
                                        <li></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#" style="background-color: #b5eff5;padding:5px;">Thesis Examination</a>
                            <ul>
                                <li><a href="/research/comingsoon/list">Student List and Result Update</a></li>
                            </ul>
                        </li>

                         <li><a href="#" style="background-color: #b5eff5;padding:5px;">Thesis Resubmission</a>
                            <ul>
                                <li><a href="/research/comingsoon/list">Student List and Result Update</a></li>
                            </ul>
                        </li>

                    <!-- <h4>Setup</h4>
                    <ul>
                        <li><a href="/research/fieldOfInterest/list">Menu 1</a></li>
                        <li><a href="#">Menu 2</a></li>
                        <li>
                            <ul>
                                 <li><a href="#">Sub Menu 1</a></li>
                                 <li><a href="#">Sub Menu 2</a></li>
                            </ul>
                        </li>
                    </ul> -->


                    <!-- <h4>PhD Setup</h4>
                    <ul>
                        <li><a href="/research/stage/list">Stage</a></li>
                        <li><a href="/research/stage/overview">Overview</a></li>
                    </ul>
                    <h4>Supervisor</h4>
                    <ul>
                        <li><a href="/research/supervisorRole/list">Supervisor Role</a></li>
                    </ul>
                    
                    <h4>Registration</h4>
                    <ul>
                        <li><a href="/research/proposal/list">Proposal</a></li>
                        <li><a href="/research/articleship/list">Articleship</a></li>
                        <li><a href="/research/professionalpracricepaper/list">Professional Practice Paper</a></li>
                    </ul>
                    <h4>Approval</h4>
                    <ul>
                        <li><a href="/research/proposal/approvalList">Proposal Approval</a></li>
                        <li><a href="/research/articleship/approvalList">Articleship Approval</a></li>
                        <li><a href="/research/professionalpracricepaper/approvalList">PPP Approval</a></li>
                    </ul> -->