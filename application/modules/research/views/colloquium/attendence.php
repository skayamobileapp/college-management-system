<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Add Attendance For Colloquium </h3>
        </div>




            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Colloquium Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center" aria-controls="program_scheme" role="tab" data-toggle="tab">Student Registration List</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">













                        <form id="form_unit" action="" method="post">

                            <!-- <a class="btn btn-link">* Registration End Dated Colloquium Not Editable</a> -->
                            
                            <div class="form-container">
                                <h4 class="form-group-title">Colloquium Details</h4>


                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $colloquium->name; ?>" readonly>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $colloquium->description; ?>" readonly>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>URL <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="url" name="url" value="<?php echo $colloquium->url; ?>" readonly>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Start Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off" value="<?php if($colloquium->date_time){ echo date('d-m-Y', strtotime($colloquium->date_time)); } ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>End Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="to_date" name="to_date" autocomplete="off" value="<?php if($colloquium->to_date){ echo date('d-m-Y', strtotime($colloquium->to_date)); } ?>" readonly>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Mode <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="mode" id="mode" value="1" <?php if($colloquium->mode=='1') {
                                                 echo "checked=checked";
                                              };?> disabled><span class="check-radio"></span> Face To Face
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="mode" id="mode" value="0" <?php if($colloquium->mode=='0') {
                                                 echo "checked=checked";
                                              };?> disabled>
                                              <span class="check-radio"></span> Online
                                            </label>
                                        </div>
                                    </div>



                                </div>

                                <div class="row">



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Attendance <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="attendance" id="attendance" value="1" <?php if($colloquium->attendance=='1') {
                                                 echo "checked=checked";
                                              };?> disabled><span class="check-radio"></span> Compulsary
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="attendance" id="attendance" value="0" <?php if($colloquium->attendance=='0') {
                                                 echo "checked=checked";
                                              };?> disabled>
                                              <span class="check-radio"></span> Optional
                                            </label>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Start Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="registration_start_date" name="registration_start_date" autocomplete="off" value="<?php if($colloquium->registration_start_date){ echo date('d-m-Y', strtotime($colloquium->registration_start_date)); } ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>End Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="registration_end_date" name="registration_end_date" autocomplete="off" value="<?php if($colloquium->registration_end_date){ echo date('d-m-Y', strtotime($colloquium->registration_end_date)); } ?>" readonly>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Status <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="status" id="status" value="1" <?php if($colloquium->status=='1') {
                                                 echo "checked=checked";
                                              };?> disabled><span class="check-radio"></span> Active
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="status" id="status" value="0" <?php if($colloquium->status=='0') {
                                                 echo "checked=checked";
                                              };?> disabled>
                                              <span class="check-radio"></span> In-Active
                                            </label>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                                    <a href="../registrationList" class="btn btn-link">Back</a>
                                </div>
                            </div>


                            </form>




                            <br>


                            <?php
                            if(!empty($getColloquiumDetailsByColloquiumId))
                            {
                            ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Committee Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Programme</th>
                                                 <th>Intake</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getColloquiumDetailsByColloquiumId);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                
                                                <td><?php echo $getColloquiumDetailsByColloquiumId[$i]->programme_code ." - " .  $getColloquiumDetailsByColloquiumId[$i]->programme_name;?></td>

                                                <td><?php echo $getColloquiumDetailsByColloquiumId[$i]->intake_year . " - " .  $getColloquiumDetailsByColloquiumId[$i]->intake_name;?></td>
                                                <td>
                                                <a onclick="deleteColloquiumDetails(<?php echo $getColloquiumDetailsByColloquiumId[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

                            <?php
                            }
                            ?>


                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                      <form id="form_main" action="" method="post">


                        <?php
                            if(!empty($getColloquiumInterestedStudents))
                            {
                            ?>

                            <div class="form-container">
                                    <h4 class="form-group-title">Registration Student Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Student</th>
                                             <th>Student Email</th>
                                             <th>Programme</th>
                                             <th>Intake</th>
                                             <th>Registered On</th>
                                             <th>Attended Type</th>
                                             <th>Attendedance</th>
                                             <th>Comments</th>
                                             <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($getColloquiumInterestedStudents);$i++)
                                         { ?>
                                            <tr>
                                            <td><?php echo $i+1;?></td>
                                            
                                            <td><?php echo $getColloquiumInterestedStudents[$i]->student_name ." - " .  $getColloquiumInterestedStudents[$i]->nric;?></td>
                                            <td><?php echo $getColloquiumInterestedStudents[$i]->email_id;?></td>
                                            <td><?php echo $getColloquiumInterestedStudents[$i]->programme_code ." - " .  $getColloquiumInterestedStudents[$i]->programme_name;?></td>

                                            <td><?php echo $getColloquiumInterestedStudents[$i]->intake_year . " - " .  $getColloquiumInterestedStudents[$i]->intake_name;?></td>
                                            <td><?php
                                            if($getColloquiumInterestedStudents[$i]->date_time)
                                            {

                                              echo date('d-m-Y', strtotime($getColloquiumInterestedStudents[$i]->date_time));
                                            } 
                                            ?>    
                                            </td>
                                            <td>
                                              <select name="attenence_type[]" id="attenence_type[]" class="form-control" >
                                                <option value="">Select</option>
                                                <option value="Online">Online</option>
                                                <option value="Face To Face">Face To Face</option>
                                             </select>
                                            </td>
                                            <td>
                                              <div class="col-sm-3">
                                                  <div class="form-group">
                                                      <label class="radio-inline">
                                                        <input type="radio" name="is_attended[]" id="is_attended[]" value="1" <?php if($getColloquiumInterestedStudents[$i]->is_attended=='1' || $getColloquiumInterestedStudents[$i]->is_attended=='0') {
                                                           echo "checked=checked";
                                                        };?>><span class="check-radio"></span> Present
                                                      </label>
                                                      <br>
                                                      <label class="radio-inline">
                                                        <input type="radio" name="is_attended[]" id="is_attended[]" value="2" <?php if($getColloquiumInterestedStudents[$i]->is_attended=='2') {
                                                           echo "checked=checked";
                                                        };?>>
                                                        <span class="check-radio"></span> Absent
                                                      </label>
                                                  </div>
                                              </div>
                                            </td>
                                            <td>
                                              <div class="col-sm-4">
                                                  <div class="form-group">
                                                      <input type="text" class="form-control" id="comments[]" name="comments[]" value="<?php echo $getColloquiumInterestedStudents[$i]->comments; ?>">
                                                      <input type="hidden" class="form-control" id="id_interested[]" name="id_interested[]" value="<?php echo $getColloquiumInterestedStudents[$i]->id; ?>">
                                                  </div>
                                              </div>
                                              
                                            </td>
                                            <!-- <td>
                                            <a onclick="deleteColloquiumDetails(<?php echo $getColloquiumInterestedStudents[$i]->id; ?>)">Delete</a>
                                            </td> -->

                                             </tr>
                                          <?php 
                                      } 
                                      ?>
                                        </tbody>
                                    </table>
                                  </div>

                                </div>

                        <?php
                        }
                        ?>



                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                <a href="../registrationList" class="btn btn-link">Back</a>
                            </div>
                        </div>



                      </form>



                        </div>




                        


                        </div>
                    
                    </div>






                </div>


            </div>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>


<script type="text/javascript">
    $('select').select2();

    $(document).ready(function()
    {
        $("#form_main").validate(
        {
            rules:
            {
                attenence_type:
                {
                    required: true
                },
                is_attended:
                {
                    required: true
                },
                comments:
                {
                    required: true
                }
            },
            messages:
            {
                attenence_type:
                {
                    required: "<p class='error-text'>Select Attendance Type</p>",
                },
                is_attended:
                {
                    required: "<p class='error-text'>Attendance Required</p>",
                },
                comments:
                {
                    required: "<p class='error-text'>Comments Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>