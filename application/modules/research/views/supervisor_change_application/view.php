<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3> View Change Supervisor Application</h3>
            <a href="<?php echo '../list'; ?>" class="btn btn-link">Back</a>
        </div>    
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>






        <div class="form-container">
            <h4 class="form-group-title">Change Supervisor Applications</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Application</a>
                    </li>
                    <li role="presentation"><a href="#history" class="nav-link border rounded text-center"
                            aria-controls="history" role="tab" data-toggle="tab">Change History</a>
                    </li>
                    
                </ul>
                <br>

                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="mt-4">



                                    <?php
                                    if($supervisor)
                                    {
                                        ?>


                                         <div class="form-container">
                                            <h4 class="form-group-title">Current Supervisor Details</h4>
                                            <div class='data-list'>
                                                <div class='row'> 
                                                    <div class='col-sm-6'>
                                                        <dl>
                                                            <dt>Supervisor Name :</dt>
                                                            <dd><?php echo ucwords($supervisor->full_name);?></dd>
                                                        </dl>
                                                        <dl>
                                                            <dt>Supervisor Email :</dt>
                                                            <dd><?php echo $supervisor->email ?></dd>
                                                        </dl>                     
                                                    </div>        
                                                    
                                                    <div class='col-sm-6'>                           
                                                        <dl>
                                                            <dt>Supervisor Type :</dt>
                                                            <dd><?php 
                                                            if($supervisor->type == 0)
                                                            {
                                                                echo 'External';
                                                            }elseif($supervisor->type == 1)
                                                            {
                                                                echo 'Internal';
                                                            } ?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    <form id="form_unit" action="" method="post">

                                        <div class="form-container">
                                            <h4 class="form-group-title">Change Supervisor Application Details</h4>

                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Reason <span class='error-text'>*</span></label>
                                                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $supervisorChangeApplication->reason; ?>" readonly>
                                                    </div>
                                                </div>


                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Previous Supervisor <span class='error-text'>*</span></label>
                                                        <select name="id_supervisor_old" id="id_supervisor_old" class="form-control" disabled>
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($supervisorList))
                                                            {
                                                                foreach ($supervisorList as $record)
                                                                {?>
                                                             <option value="<?php echo $record->id;  ?>"
                                                                <?php if($record->id == $supervisorChangeApplication->id_supervisor_old)
                                                                {
                                                                    echo 'selected';
                                                                }?>

                                                                >
                                                                <?php if($record->type == 0)
                                                                {
                                                                    echo 'External';
                                                                }elseif($record->type == 1)
                                                                {
                                                                    echo 'Internal';
                                                                }
                                                                    echo " - " . $record->full_name;
                                                                ?>
                                                             </option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>




                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Changed Supervisor <span class='error-text'>*</span></label>
                                                        <select name="id_supervisor" id="id_supervisor" class="form-control" disabled>
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($supervisorList))
                                                            {
                                                                foreach ($supervisorList as $record)
                                                                {?>
                                                             <option value="<?php echo $record->id;  ?>"
                                                                <?php if($record->id == $supervisorChangeApplication->id_supervisor_new)
                                                                {
                                                                    echo 'selected';
                                                                }?>

                                                                >
                                                                <?php if($record->type == 0)
                                                                {
                                                                    echo 'External';
                                                                }elseif($record->type == 1)
                                                                {
                                                                    echo 'Internal';
                                                                }
                                                                    echo " - " . $record->full_name;
                                                                ?>
                                                             </option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                        <!-- <div class="button-block clearfix">
                                            <div class="bttn-group">
                                                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                                <a href="<?php echo '../edit/' . $supervisorChangeApplication->id; ?>" class="btn btn-link">Clear All Fields</a>
                                            </div>
                                        </div> -->


                                    </form>

                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                            <a class="btn btn-link">Supervisor Not Alloted Wait till the Supervisor Assigning</a>

                                        <?php

                                    }
                                    ?>



                            

                        </div> 
                    </div>












                    <div role="tabpanel" class="tab-pane" id="history">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Supervisor</th>
                                        <th>Supervisor Type</th>
                                        <th>Changed On</th>
                                        <th>Changed By</th>
                                        <th>Requested By Student</th>
                                        <!-- <th class="text-center">Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($supervisorHistoryList))
                                    {
                                        $i=1;
                                        foreach ($supervisorHistoryList as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->supervisor_name;
                                            ?></td>
                                            <td><?php 
                                            if($record->supervisor_type == 0)
                                            {
                                                echo 'External';
                                            }elseif($record->supervisor_type == 1)
                                            {
                                                echo 'Internal';
                                            } ?></td>
                                            <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                                            <td><?php echo $record->created_by; ?></td>
                                            <td><?php 
                                            if($record->is_change_application == 0)
                                            {
                                                echo 'No';
                                            }elseif($record->is_change_application <= 1)
                                            {
                                                echo 'Yes';
                                            } ?>
                                                
                                            </td>
                                            <!-- <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td> -->

                                            
                                            <!-- <td class="text-center">
                                                <a href="<?php echo 'view/' . $record->id; ?>" title="Preview">View</a>
                                            </td> -->
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>


                </div>
            </div>

        </div>
            

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>

<script>

    $('select').select2();

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                id_supervisor: {
                    required: true
                },
                email: {
                    required: true
                }
            },
            messages: {
                id_supervisor: {
                    required: "<p class='error-text'>Select New Suervisor</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

</script>