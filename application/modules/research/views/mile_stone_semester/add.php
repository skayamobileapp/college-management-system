<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Mile Stone Tag Semester</h3>
        </div>
        <form id="form_award" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Mile Stone Tag Semester Details</h4>  

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type <span class='error-text'>*</span></label>
                            <select name="type" id="type" class="form-control" onchange="getStageByType()">
                                <option value="">Select</option>
                                <option value="Part Time">Part Time</option>
                                <option value="Full Time">Full Time</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Stage <span class='error-text'>*</span></label>
                         <span id="view_stage">
                              <select class="form-control" id='id_stage' name='id_stage'>
                                <option value=''></option>
                              </select>

                         </span>
                      </div>
                    </div>


                    <!-- <div class="col-sm-4">
                      <div class="form-group">
                         <label>Stage <span class='error-text'>*</span></label>
                         <select name="id_stage" id="id_stage" class="form-control selitemIcon"  onchange="getSemesterByStage()">
                            <option value="">Select</option>
                            <?php
                               if (!empty($stageList))
                               {
                                   foreach ($stageList as $record)
                                   {?>
                            <option value="<?php echo $record->id;  ?>">
                               <?php echo $record->name;?>
                            </option>
                            <?php
                               }
                               }
                               ?>
                         </select>
                      </div>
                    </div> -->

                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Semester <span class='error-text'>*</span></label>
                         <span id="view_semester">
                              <select class="form-control" id='id_semester' name='id_semester'>
                                <option value=''></option>
                              </select>

                         </span>
                      </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Mile Stone <span class='error-text'>*</span></label>
                         <select name="id_mile_stone" id="id_mile_stone" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                               if (!empty($mileStoneList))
                               {
                                   foreach ($mileStoneList as $record)
                                   {?>
                            <option value="<?php echo $record->id;  ?>">
                               <?php echo $record->name;?>
                            </option>
                            <?php
                               }
                               }
                               ?>
                         </select>
                      </div>
                    </div>



                    <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div> -->
                
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function getStageByType()
    {
      if($("#type").val() != '')
        {
            var tempPR = {};
            tempPR['type'] = $("#type").val();

            $.ajax(
            {
               url: '/research/mileStoneToSemester/getStageByType',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_stage").html(result);                    
               }
            });
        }
    }

    function getSemesterByStage()
    {
        if($("#id_stage").val() != '' && $("#type").val() != '')
        {
            var tempPR = {};
            tempPR['id_stage'] = $("#id_stage").val();
            tempPR['type'] = $("#type").val();

            $.ajax(
            {
               url: '/research/mileStoneToSemester/getSemesterByStage',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_semester").html(result);                    
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                type: {
                    required: true
                },
                id_stage: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                
                id_mile_stone: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_stage: {
                    required: "<p class='error-text'>Select Stage</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_mile_stone: {
                    required: "<p class='error-text'>Select Mile Stone</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>