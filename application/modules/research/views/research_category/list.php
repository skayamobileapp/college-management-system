<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Research Category</h3>
      <a href="add" class="btn btn-primary">+ Add Research Category</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Research Category</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program</label>
                      <div class="col-sm-8">
                        <select name="id_program" id="id_program" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($programList)) {
                            foreach ($programList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_program'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->code . " - " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Program</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($researchCategoryList)) {
            $i=1;
            foreach ($researchCategoryList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo ($record->status=='1') ? "Active" : "In-Active";  ?></td>
                <td style="text-align: center;">
                  <a href="<?php echo 'edit/' . $record->id; ?>">Edit</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>