<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Topic</h3>
      <a href="add" class="btn btn-primary">+ Add Topic</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Topic</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Research Category</label>
                      <div class="col-sm-8">
                        <select name="id_topic_category" id="id_topic_category" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($researchCategoryList)) {
                            foreach ($researchCategoryList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_topic_category'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Max No Of Candidates</th>
            <th>Research Category</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($topicList)) {
            $i=1;
            foreach ($topicList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->max_candidates ?></td>
                <td><?php echo $record->topic_category_name ?></td>
                <td><?php echo ($record->status=='1') ? "Active" : "In-Active";  ?></td>
                <td style="text-align: center;">
                  <a href="<?php echo 'edit/' . $record->id; ?>">Edit</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>