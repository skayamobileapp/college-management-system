<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Topic</h3>
            </div>

    <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Topic Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Research Category <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_topic_category" name="id_topic_category">
                            <option value="">Select</option>
                            <?php
                            if (!empty($researchCategoryList))
                            {
                                foreach ($researchCategoryList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max No Of Candidates</label>
                        <input type="number" class="form-control" id="max_candidates" name="max_candidates">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assignment Requirement</label>
                        <input type="text" class="form-control" id="assignment_requirement" name="assignment_requirement">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>

            </div>

        </div>
    </form>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateProgram()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>


           
            
            

    <form id="form_programme_intake" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Topic Has Supervisor Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Supervisor <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_staff" name="id_staff">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>
            <div class="row">
                <div id="view"></div>
            </div>

        </div>
    </form>

         
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    function saveData()
    {
        if($('#form_programme_intake').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();

            $.ajax(
            {
               url: '/research/topic/tempAddTopicHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        }
    }

    function deleteTempTopicHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/topic/deleteTempTopicHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_staff: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                id_topic_category: {
                    required: true
                },
                status: {
                    required: true
                }
                // ,
                // max_candidates: {
                //     required: true
                // },
                // assignment_requirement: {
                //     required: true
                // }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_topic_category: {
                    required: "<p class='error-text'>Select Topic Category</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
                // ,
                // max_candidates: {
                //     required: "<p class='error-text'>Max. No Of Candidates Required</p>",
                // },
                // assignment_requirement: {
                //     required: "<p class='error-text'>Assignment Required</p>",
                // }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateProgram() {

    if($('#form_programme').valid())
      {
         console.log($("#view").html());
         var addedProgam = $("#view").html();
         if(addedProgam=='') {
            alert("Add Supervisor To Topic");
        }else {

         $('#form_programme').submit();
        }
      }     
    }

    $('select').select2();
</script>