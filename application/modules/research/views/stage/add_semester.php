<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Semester To Stage</h3>
        </div>

            <div class="form-container">
                <h4 class="form-group-title">Stage Details</h4>  

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $stage->name; ?>" readonly>
                        </div>
                    </div>

                   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $stage->description; ?>" readonly>
                        </div>
                    </div>

                

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type <span class='error-text'>*</span></label>
                            <select name="type" id="type" class="form-control" disabled>
                                <option value="">Select</option>
                                <option value="Part Time"
                                <?php
                                if('Part Time' == $stage->type)
                                {
                                    echo "selected";
                                } ?>
                                >Part Time</option>
                                <option value="Full Time"
                                <?php
                                if('Full Time' == $stage->type)
                                {
                                    echo "selected";
                                } ?>
                                >Full Time</option>
                            </select>
                        </div>
                    </div>

                </div>




                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="status" id="status" value="1" <?php if($stage->status=='1') {
                                echo "checked=checked";
                            };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="status" id="status" value="0" <?php if($stage->status=='0') {
                                echo "checked=checked";
                            };?>>
                            <span class="check-radio"></span> In-Active
                            </label>
                        </div>
                    </div>
                
                </div>
            </div>

            <br>

            <form id="form_award" action="" method="post">
                <div class="form-container">
                    <h4 class="form-group-title">Semester Details</h4>  

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Type <span class='error-text'>*</span></label>
                                <select name="semester_type" id="semester_type" class="form-control" onchange="showSemester(this.value)">
                                    <option value="">Select</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Range">Range</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" id="from_display">
                            <div class="form-group">
                                <label>Semester <span class='error-text'>*</span></label>
                                <select name="from_semester" id="from_semester" class="form-control">
                                    <option value="">Select</option>
                                    <?php

                                    for($i=1; $i<=12; $i++)
                                    {
                                    ?>
                                            <option value="<?php echo $i;?>"
                                            ><?php echo $i; ?>
                                            </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" id="to_display" style="display: none;">
                            <div class="form-group">
                                <label>To Semester <span class='error-text'>*</span></label>
                                <select name="to_semester" id="to_semester" class="form-control">
                                    <option value="">Select</option>
                                    <?php

                                    for($i=1; $i<=12; $i++)
                                    {
                                    ?>
                                            <option value="<?php echo $i;?>"
                                            ><?php echo $i; ?>
                                            </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>



                    <!-- <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="semester_status" id="semester_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="semester_status" id="semester_status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                        </div>
                    
                    </div> -->


                </div>

                <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                        <a href="../list" class="btn btn-link">Back</a>
                    </div>
                </div>
            </form>

            <br>


            <div class="form-container">
                <h4 class="form-group-title">Semester List</h4>


                <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Name</th>
                             <th>Type</th>
                             <th>Semester</th>
                             <th>To Semester</th>
                             <th class="text-center">Status</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                          for($i=0;$i<count($stageSemesterList);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $stageSemesterList[$i]->name;?></td>
                            <td><?php echo $stageSemesterList[$i]->type;?></td>
                            <td><?php echo $stageSemesterList[$i]->from;?></td>
                            <td><?php echo $stageSemesterList[$i]->to;?></td>
                            <td class="text-center">
                                <?php if($stageSemesterList[$i]->status == 1)
                                {
                                    echo "Active";
                                }
                                else
                                {
                                    echo "In-Active";
                                } 
                            ?>
                                    
                            </td>

                            <td class="text-center">
                                
                                <a class='btn btn-sm btn-edit' onclick='deleteSemsterStage(<?php echo $stageSemesterList[$i]->id; ?>)'>Delete</a>                                
                            </td>
                          <?php 
                         
                      }
                      ?>
                        </tbody>
                    </table>
                </div>
            </div>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function showSemester(type)
    {
        if(type == 'Regular')
        {
            $("#to_display").hide();
            $("#from_display").show();
        }
        else if(type == 'Range')
        {
            $("#from_display").show();
            $("#to_display").show();
        }
    }


    function deleteSemsterStage(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/research/stage/deleteSemsterStage/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                semester_type: {
                    required: true
                },
                from_semester: {
                    required: true
                },
                to_semester: {
                    required: true
                }
            },
            messages: {
                semester_type: {
                    required: "<p class='error-text'>Select Semester Type</p>",
                },
                from_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                to_semester: {
                    required: "<p class='error-text'>Select To Semester</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>