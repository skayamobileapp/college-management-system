<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approval List Research Proposal</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Research Proposal</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6"> 
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6"> 
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student NRIC</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                      </div>
                    </div>
                  </div>


                </div>

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Research Category</label>
                      <div class="col-sm-8">
                        <select name="id_research_category" id="id_research_category" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($researchCategoryList)) {
                            foreach ($researchCategoryList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_research_category'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Research Topic</label>
                      <div class="col-sm-8">
                        <select name="id_research_topic" id="id_research_topic" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($researchTopicList)) {
                            foreach ($researchTopicList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_research_topic'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                
                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Title</th>
            <th>Student</th>
            <th>Research Category</th>
            <th>Research Topic</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($researchProposalList)) {
            $i=1;
            foreach ($researchProposalList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->nric . " - " . $record->student_name ?></td>
                <td><?php echo $record->research_category ?></td>
                <td><?php echo $record->research_topic ?></td>
                <td style="text-align: center;"><?php
                 if($record->status == 0)
                  {
                      echo 'Pending';
                  }elseif($record->status == 1)
                  {
                      echo 'Approved';
                  }if($record->status == 2)
                  {
                      echo 'Rejected';
                  }
                 ?>
                </td>
                <td style="text-align: center;">
                  <a href="<?php echo 'approve/' . $record->id; ?>">Approve</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>