<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
	.highcharts-figure, .highcharts-data-table table {
    min-width: 360px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Students Count based on Intake </h3>
        </div>
        <form id="form_award" action="" method="post">

        <div class="form-container">

            <div class="row">
                 <div class="col-sm-4">

			<figure class="highcharts-figure">
			    <div id="container"></div>
			    <p class="highcharts-description">
			    </p>
			</figure>
        </div>

         <div class="col-sm-4">

            <figure class="highcharts-figure">
                <div id="container1"></div>
                <p class="highcharts-description">
                </p>
            </figure>
        </div>

         <div class="col-sm-4">

            <figure class="highcharts-figure">
                <div id="container2"></div>
                <p class="highcharts-description">
                </p>
            </figure>
        </div>



        </div>
		</div>
	</form>
</div>
</div>
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Intake From 2019 to 2020'
    },
    xAxis: {
        categories: ['Jan-2019', 'June-2019', 'Sept-2019', 'Jan-2020', 'June-2020']
    },
    yAxis: {
        title: {
            text: 'Students '
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    },
    series: [{
        name: 'Total Students',
        marker: {
            symbol: 'square'
        },
        data: [600,760,1000,1210,1190]

    }]
});
</script>

<script type="text/javascript">
Highcharts.chart('container1', {
    title: {
        text: 'Combination chart Applicant V/S Lead V/S Student By Intake'
    },
    xAxis: {
        categories: ['SEP-2019', 'JAN-2020', 'JUNE-2020', 'SEP-2020']
    },
    labels: {
        items: [{
            html: 'Total Applicant V/S Lead V/S Student',
            style: {
                left: '50px',
                top: '18px',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Applicant',
        data: [100, 160, 210, 230]
    }, {
        type: 'column',
        name: 'Lead',
        data: [78,105,190,176]
    }, {
        type: 'column',
        name: 'Student',
        data: [70,90,130,165]
    }, {
        type: 'pie',
        name: 'Total consumption',
        data: [{
            name: 'Applicant',
            y: 13,
            color: Highcharts.getOptions().colors[0] // Jane's color
        }, {
            name: 'Lead',
            y: 23,
            color: Highcharts.getOptions().colors[1] // John's color
        }, {
            name: 'Students',
            y: 23,
            color: Highcharts.getOptions().colors[2] // John's color
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});

</script>
<script type="text/javascript">
Highcharts.chart('container2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Course Wise Data  - For intake Jan2020'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Branch',
        colorByPoint: true,
        data: [{
            name: 'HONS',
            y: 61.41
        }, {
            name: 'BCA',
            y: 11.84
        }, {
            name: 'Bachelor in Accounts',
            y: 10.85
        }, {
            name: 'BTech',
            y: 4.67
        }, {
            name: 'Teaching',
            y: 4.18
        }]
    }]
});
</script>