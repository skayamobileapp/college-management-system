<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_withdraw_model extends CI_Model
{
    function courseWithdrawList()
    {
        $this->db->select('*');
        $this->db->from('course_withdraw');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseWithdrawListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('course_withdraw');
        if (!empty($search))
        {
            $likeCriteria = "(semester_type  LIKE '%" . $search . "%' or min_days  LIKE '%" . $search . "%' or effective_date_from  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseWithdraw($id)
    {
        $this->db->select('*');
        $this->db->from('course_withdraw');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCourseWithdraw($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_withdraw', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCourseWithdraw($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_withdraw', $data);
        return TRUE;
    }
}

