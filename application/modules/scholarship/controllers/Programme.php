<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Programme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('scholarship_programme.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['programmeList'] = $this->programme_model->programmeListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Program List';
            $this->loadViews("programme/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('scholarship_programme.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {                

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_award' => $id_award,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->programme_model->addNewProgrammeDetails($data);
                if ($inserted_id)
                {
                    $inserted_document_reuirements = $this->programme_model->addNewProgrammeDocumentRequirements($inserted_id);
                }
                redirect('/scholarship/programme/list');
            }
            $data['awardList'] = $this->programme_model->awardListByStatus('1');
            
            $this->global['pageTitle'] = 'Scholarship Management System : Add Programme';
            $this->loadViews("programme/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('scholarship_programme.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/programme/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_award' => $id_award,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->programme_model->editProgrammeDetails($data,$id);
                
                redirect('/scholarship/programme/list');
            }
            $data['id_programme'] = $id;
            // echo "<Pre>";print_r($data['programmeHasDeanList']);exit;
            $data['awardList'] = $this->programme_model->awardListByStatus('1');
            $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Programme';
            $this->loadViews("programme/edit", $this->global, $data, NULL);
        }
    }
}