<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AccreditationType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('accreditation_type_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('accreditation_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['accreditationTypeList'] = $this->accreditation_type_model->accreditationTypeListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Accreditation Type List';
            $this->loadViews("accreditation_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('accreditation_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->accreditation_type_model->addNewAccreditationType($data);
                redirect('/scholarship/accreditationType/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Accreditation Type';
            $this->loadViews("accreditation_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('accreditation_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/accreditationType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->accreditation_type_model->editAccreditationType($data,$id);
                redirect('/scholarship/accreditationType/list');
            }
            $data['accreditationType'] = $this->accreditation_type_model->getAccreditationType($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Accreditation Type';
            $this->loadViews("accreditation_type/edit", $this->global, $data, NULL);
        }
    }
}
