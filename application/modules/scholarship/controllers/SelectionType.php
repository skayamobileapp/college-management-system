<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SelectionType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('selection_type_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('selection_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['selectionTypeList'] = $this->selection_type_model->selectionTypeListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Selection Type List';
            $this->loadViews("selection_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('selection_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $calculation_method = $this->security->xss_clean($this->input->post('calculation_method'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'calculation_method' => $calculation_method,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->selection_type_model->addNewSelectionType($data);
                redirect('/scholarship/selectionType/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Selection Type';
            $this->loadViews("selection_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('selection_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/selectionType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $calculation_method = $this->security->xss_clean($this->input->post('calculation_method'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'calculation_method' => $calculation_method,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->selection_type_model->editSelectionType($data,$id);
                redirect('/scholarship/selectionType/list');
            }
            $data['selectionType'] = $this->selection_type_model->getSelectionType($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Selection Type';
            $this->loadViews("selection_type/edit", $this->global, $data, NULL);
        }
    }
}
