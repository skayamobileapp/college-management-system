<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Religion extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('religion_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('religion.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['religionList'] = $this->religion_model->religionListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Religion List';
            $this->loadViews("religion/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('religion.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->religion_model->addNewReligion($data);
                redirect('/scholarship/religion/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Religion';
            $this->loadViews("religion/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('religion.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/religion/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->religion_model->editReligion($data,$id);
                redirect('/scholarship/religion/list');
            }
            $data['religion'] = $this->religion_model->getReligion($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Religion';
            $this->loadViews("religion/edit", $this->global, $data, NULL);
        }
    }
}
