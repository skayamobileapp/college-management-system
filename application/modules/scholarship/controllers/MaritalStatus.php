<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MaritalStatus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('marital_status_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('marital_status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['maritalStatusList'] = $this->marital_status_model->maritalStatusListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Marital Status List';
            $this->loadViews("marital_status/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('marital_status.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->marital_status_model->addNewMaritalStatus($data);
                redirect('/scholarship/maritalStatus/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Marital Status';
            $this->loadViews("marital_status/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('marital_status.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/maritalStatus/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );

                $result = $this->marital_status_model->editMaritalStatus($data,$id);
                redirect('/scholarship/maritalStatus/list');
            }
            $data['maritalStatus'] = $this->marital_status_model->getMaritalStatus($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Marital Status';
            $this->loadViews("marital_status/edit", $this->global, $data, NULL);
        }
    }
}
