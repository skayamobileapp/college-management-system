<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css">

    
</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Scholarship Management System</a>
            </div>

            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <!--   <li><a href="/setup/user/list">Setup</a></li>
                    <li><a href="/admission/applicant/list">Admission</a></li>
                    <li><a href="/registration/examCenter/list">Registration</a></li>
                    <li><a href="/records/barringType/list">Records</a></li>
                    <li><a href="/examination/grade/list">Examination</a></li>
                    <li><a href="/graduation/award/list">Graduation</a></li>
                    <li><a href="/finance/amountCalculationType/list">Finance</a></li>
                    <li><a href="/sponser/sponser/list">Sponsor</a></li>
                    <li><a href="/timetable/timetable/list">Time Table</a></li>
                    <li><a href="/hostel/hostelRegistration/list">Hostel</a></li>
                    <li><a href="/internship/internship/list">Internship</a></li>
                    <li><a href="/allumini/alluminiList/list">Alumni</a></li>
                    <li><a href="/communication/template/list">Communication</a></li>
                    <li class="active"><a href="/scholarship/scheme/list">Scholarship</a></li>
                    <li><a href="/teaching_evaluation/welcome">Teaching Evaluation</a></li>
                    <li><a href="/student/profile/logout">logout</a></li> -->
                </ul>
                
            </nav>
        </div>
    </header>
</body>

     