<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Sub Thrust </h3>
        </div>

        <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">subThrust Details</h4>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thrust <span class='error-text'>*</span></label>
                        <select name="id_thrust" id="id_thrust" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($thrustList))
                            {
                                foreach ($thrustList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"  <?php if($subThrust->id_thrust==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Code</label>
                        <input type="text" class="form-control" id="scholarship_code" name="scholarship_code" value="<?php echo $subThrust->scholarship_code; ?>">

                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Name</label>
                        <input type="text" class="form-control" id="scholarship_name" name="scholarship_name" value="<?php echo $subThrust->scholarship_name; ?>">

                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Short Name</label>
                        <input type="text" class="form-control" id="scholarship_short_name" name="scholarship_short_name" value="<?php echo $subThrust->scholarship_short_name; ?>">

                    </div>
                </div>

    <!-- 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assesment Method <span class='error-text'>*</span></label>
                        <select name="detail_assesment_method" id="detail_assesment_method" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <option value="CGPA" <?php if($partnerProgramDetails->assesment_method=='CGPA'){ echo "selected"; } ?>>CGPA</option>
                            <option value="Grade" <?php if($partnerProgramDetails->assesment_method=='Grade'){ echo "selected"; } ?>>Grade</option>
                            <option value="Pass/Fail" <?php if($partnerProgramDetails->assesment_method=='Pass/Fail'){ echo "selected"; } ?>>Pass/Fail</option>
                            <option value="Attendence" <?php if($partnerProgramDetails->assesment_method=='Attendence'){ echo "selected"; } ?>>Attendence</option>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($subThrust->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($subThrust->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>                
                    </div>
                </div>

            </div>


        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

    </form>



        <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Scholarship Requirement</a>
                    </li>

                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Scholarship Program</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="col-12 mt-4">




                <form id="form_program_one" action="" method="post">

                <div class="form-container">
                    <h4 class="form-group-title"> Scholarship Requirements Details</h4>
                    

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Program <span class='error-text'>*</span></label>
                                <select name="program_requirement_id_program" id="program_requirement_id_program" class="form-control" style="width: 408px" multiple>
                                    <!-- <option value="">Select</option> -->
                                    <?php
                                    if (!empty($programList))
                                    {
                                        foreach ($programList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Status <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="program_requirement_status" id="program_requirement_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="program_requirement_status" id="program_requirement_status" value="0"><span class="check-radio"></span> Inactive
                                    </label>                              
                                </div>                         
                        </div>  

                        <div class="col-sm-4" style="align-content: left">
                            <div class="form-group">
                                <label>Entry Requirement <span class='error-text'>*</span></label>
                                <select name="program_requirement_entry" id="program_requirement_entry" class="form-control" style="width: 408px"  onchange="showRequirements(this.value)">
                                    <option value="">Select</option>
                                    <option value="Age">Age</option>
                                    <option value="Education">Education</option>
                                    <option value="Work Experience">Work Experience</option>
                                    <option value="Other Requirement">Other Requirement</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>



                <div class="form-container" style="display: none;" id="view_requirement_age">
                    <h4 class="form-group-title">Age Details</h4>


                    <div class="row">

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Min. (Years) <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="program_requirement_age_min" name="program_requirement_age_min" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Max. (Years) <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="program_requirement_age_max" name="program_requirement_age_max" autocomplete="off">
                            </div>
                        </div>

                    </div>

                </div>



                <div class="form-container" style="display: none;" id="view_requirement_education">
                    <h4 class="form-group-title">Education Details</h4>


                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Qualification <span class='error-text'>*</span></label>
                                <select name="program_requirement_education_id_qualification" id="program_requirement_education_id_qualification" class="form-control" style="width: 408px">
                                    <option value="">Select</option>
                                   <?php
                                    if (!empty($qualificationList))
                                    {
                                        foreach ($qualificationList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->short_name . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Description <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="program_requirement_education_descrption" name="program_requirement_education_descrption" autocomplete="off">
                            </div>
                        </div>

                    </div>

                </div>


                <div class="form-container" style="display: none;" id="view_requirement_experience">
                    <h4 class="form-group-title">Work Experience Details</h4>


                    <div class="row">

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Work Experience Min. (Years) <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="program_requirement_experience_min" name="program_requirement_experience_min" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Specialisation <span class='error-text'>*</span></label>
                                <select name="program_requirement_experience_id_specialisation" id="program_requirement_experience_id_specialisation" class="form-control" style="width: 408px">
                                    <option value="">Select</option>
                                    <!-- <option value="1">1</option> -->
                                   <?php
                                    if (!empty($workSpecialisationList))
                                    {
                                        foreach ($workSpecialisationList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Description <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="program_requirement_experience_descrption" name="program_requirement_experience_descrption" autocomplete="off">
                            </div>
                        </div>

                    </div>

                </div>


                <div class="form-container" style="display: none;" id="view_requirement_other">
                    <h4 class="form-group-title">Other Requirement Details</h4>


                    <div class="row">

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Description <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="program_requirement_other_descrption" name="program_requirement_other_descrption" autocomplete="off">
                            </div>
                        </div>

                    </div>

                </div>




                <div id="view">
                </div>



            </form>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="addProgramRequirement()">Add</button>
                </div>
            </div>


            <?php

                if(!empty($subThrustRequirementsList))
                {
                    ?>
                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">Scholarship Requirement List</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                     <th>Program Code</th>
                                     <th>Program Name</th>
                                     <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($subThrustRequirementsList);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $subThrustRequirementsList[$i]->program_code;?></td>
                                    <td><?php echo $subThrustRequirementsList[$i]->program_code;?></td>
                                    <!-- <td><?php echo $subThrustRequirementsList[$i]->program_name . " - " . $subThrustRequirementsList[$i]->min_duration_type;?></td>
                                    <td><?php echo date('d-m-Y', strtotime($partnerProgramStudyModeDetails[$i]->end_date));?></td> -->
                                    
                                    <td class="text-center">
                                    <a onclick="viewRequirementData(<?php echo $subThrustRequirementsList[$i]->id; ?>)">View</a>
                                    </td>

                                     </tr>
                                  <?php 
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>

                <?php
                
                }
                 ?>


                    
                                   



                </div> 
            
            </div>




            <div role="tabpanel" class="tab-pane" id="tab_two">
                <div class="mt-4">



            <form id="form_program_two" action="" method="post">

                    <div class="form-container">
                        <h4 class="form-group-title"> Scholarship Program Details</h4>
                        

                        <div class="row">
                        
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Program <span class='error-text'>*</span></label>
                                    <select name="program_id_program" id="program_id_program" class="form-control" style="width: 408px" onchange="getPartnerProgram(this.value)">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($programList))
                                        {
                                            foreach ($programList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>">
                                            <?php echo $record->code . " -  ". $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Partner <span class='error-text'>*</span></label>
                                    <span id='view_program'></span>
                                </div>
                            </div>

                        </div>


                    </div>








                </form>


                <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary btn-lg" onclick="addScholarshipProgram()">Add</button>
                    </div>
                </div>


                <?php

                    if(!empty($scholarshipProgramList))
                    {
                        ?>
                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Scholarship Program List</h4>

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                         <th>Program</th>
                                         <th>Program Partner</th>
                                         <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($scholarshipProgramList);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $scholarshipProgramList[$i]->program_code . " - " . $scholarshipProgramList[$i]->program_name;?></td>
                                        <td><?php echo $scholarshipProgramList[$i]->partner_code . " - " . $scholarshipProgramList[$i]->partner_name;?></td>
                                        
                                        <td class="text-center">
                                        <a onclick="deleteScholarshipProgram(<?php echo $scholarshipProgramList[$i]->id; ?>)">Delete</a>
                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>


                        


                </div>
            
            </div>









                </div>


            </div>




<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Program Requirement Details</h4>
      </div>
      <div class="modal-body">

            <div id="view_requirement_data">   
            </div>


      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" onclick="saveData()">Add</button> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script>

    function getPartnerProgram(id)
    {
        // $("#view_program").html(id);
        // alert(id);
        $.get("/scholarship/subThrust/getPartnerProgram/"+id,

            function(data, status)
            {
                $("#view_program").html(data);
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
        });
    }


    function showRequirements(requirement)
    {

    $("#view_requirement_age").hide();
    $("#view_requirement_education").hide();
    $("#view_requirement_experience").hide();
    $("#view_requirement_other").hide();

        if(requirement != '')
        {
            if(requirement == 'Age' )
            {
                $("#view_requirement_age").show();

            }
            if(requirement == 'Education' )
            {
                $("#view_requirement_education").show();

            }
            if(requirement == 'Work Experience' )
            {
                $("#view_requirement_experience").show();

            }
            if(requirement == 'Other Requirement' )
            {
                $("#view_requirement_other").show();

            }
        }
    }


    function saveAggrementData()
    {
        if($('#form_aggrement').valid())
        {

            $('#form_aggrement').submit();
        }
    }

    function validateUniversityData()
    {
        if($('#form_award').valid())
        {

            $('#form_award').submit();
        }
    }


    function addProgramRequirement()
    {

        if($('#form_program_one').valid())
        {

        var tempPR = {};
        tempPR['id_program'] = $("#program_requirement_id_program").val();
        tempPR['status'] = $("#program_requirement_status").val();
        tempPR['requirement_entry'] = $("#program_requirement_entry").val();
        tempPR['min_age'] = $("#program_requirement_age_min").val();
        tempPR['max_age'] = $("#program_requirement_age_max").val();
        tempPR['id_qualification'] = $("#program_requirement_education_id_qualification").val();
        tempPR['education_descrption'] = $("#program_requirement_education_descrption").val();
        tempPR['min_experience'] = $("#program_requirement_experience_min").val();
        tempPR['id_specialisation'] = $("#program_requirement_experience_id_specialisation").val();
        tempPR['experience_descrption'] = $("#program_requirement_experience_descrption").val();
        tempPR['other_descrption'] = $("#program_requirement_other_descrption").val();
        tempPR['id_sub_thrust'] = <?php echo $subThrust->id; ?>;

            $.ajax(
            {
               url: '/scholarship/subThrust/addProgramRequirement',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                 // $("#view").html(result);
                // alert(result);
                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function viewRequirementData(id_program)
    {
        

        var tempPR = {};
        tempPR['id_program'] = id_program;
        tempPR['id_sub_thrust'] = <?php echo $subThrust->id; ?>;

            $.ajax(
            {
               url: '/scholarship/subThrust/viewRequirementData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                $('#myModal').modal('show');
                $("#view_requirement_data").html(result);
                // $("#view").html(result);

                // location.reload();
                // window.location.reload();

               }
            });
    }

    function addScholarshipProgram()
    {
        if($('#form_program_two').valid())
        {

        var tempPR = {};
        tempPR['id_program'] = $("#program_id_program").val();
        tempPR['id_partner_university'] = $("#program_id_partner").val();
        tempPR['id_sub_thrust'] = <?php echo $subThrust->id; ?>;

            $.ajax(
            {
               url: '/scholarship/subThrust/addScholarshipProgram',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }


    function deleteScholarshipProgram(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/subThrust/deleteScholarshipProgram/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    window.location.reload();
               }
            });
    }









    function deleteAgeRequirement(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/subThrust/deleteAgeRequirement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }


    function deleteEducationRequirement(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/subThrust/deleteEducationRequirement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }


    function deleteWorkExperienceRequirement(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/subThrust/deleteWorkExperienceRequirement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }


    function deleteOtherRequirement(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/subThrust/deleteOtherRequirement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_thrust: {
                    required: true
                },
                scholarship_code: {
                    required: true
                },
                scholarship_name: {
                    required: true
                },
                scholarship_short_name : {
                    required : true
                },
                status: {
                    required: true
                }
            },
            messages: {
                id_thrust: {
                    required: "<p class='error-text'>Select Thrust</p>",
                },
                scholarship_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                scholarship_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                scholarship_short_name: {
                    required: "<p class='error-text'>Short Name Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_program_one").validate({
            rules: {
                program_requirement_id_program: {
                    required: true
                },
                program_requirement_status: {
                    required: true
                },
                program_requirement_entry: {
                    required: true
                },
                program_requirement_age_min: {
                    required: true
                },
                program_requirement_age_max: {
                    required: true
                },
                program_requirement_education_id_qualification: {
                    required: true
                },
                program_requirement_education_descrption: {
                    required: true
                },
                program_requirement_experience_min: {
                    required: true
                },
                program_requirement_experience_id_specialisation: {
                    required: true
                },
                program_requirement_experience_descrption: {
                    required: true
                },
                program_requirement_other_descrption: {
                    required: true
                }
            },
            messages: {
                program_requirement_id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                program_requirement_status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                program_requirement_entry: {
                    required: "<p class='error-text'>Select Requirement Entry</p>",
                },
                program_requirement_age_min: {
                    required: "<p class='error-text'>Min. Age Required</p>",
                },
                program_requirement_age_max: {
                    required: "<p class='error-text'>Min. Age Required</p>",
                },
                program_requirement_education_id_qualification: {
                    required: "<p class='error-text'>Select Qualification</p>",
                },
                program_requirement_education_descrption: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                program_requirement_experience_min: {
                    required: "<p class='error-text'>Min. Experience Required</p>",
                },
                program_requirement_experience_id_specialisation: {
                    required: "<p class='error-text'>Select Specialisatioin</p>",
                },
                program_requirement_experience_descrption: {
                    required: "<p class='error-text'>Descripotion Required</p>",
                },
                program_requirement_other_descrption: {
                    required: "<p class='error-text'>Descripotion Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_program_two").validate({
            rules: {
                program_id_program: {
                    required: true
                },
                program_id_partner: {
                    required: true
                }
            },
            messages: {
                program_id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                program_id_partner: {
                    required: "<p class='error-text'>Select Program Partner</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
<script type="text/javascript">
    $('select').select2();

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>