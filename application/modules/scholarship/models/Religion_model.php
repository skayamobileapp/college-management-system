<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Religion_model extends CI_Model
{
    function religionList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_religion_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function religionListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_religion_setup');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReligion($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_religion_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewReligion($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_religion_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editReligion($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_religion_setup', $data);
        return TRUE;
    }
}