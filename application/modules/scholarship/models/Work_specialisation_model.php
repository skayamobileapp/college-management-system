<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Work_specialisation_model extends CI_Model
{
    function workSpecialisationList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_work_specialisation');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function workSpecialisationListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_work_specialisation');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getWorkSpecialisation($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_work_specialisation');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewWorkSpecialisation($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_work_specialisation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editWorkSpecialisation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_work_specialisation', $data);
        return TRUE;
    }
}