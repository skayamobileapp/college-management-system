<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_syllabus_model extends CI_Model
{
    function programSyllabusList($data)
    {
        $this->db->select('fc.*, t.name as thrust_name, t.code as thrust_code, st.scholarship_name, st.scholarship_code, sp.code as program_code, sp.name as program_name');
        $this->db->from('scholarship_program_syllabus as fc');
        $this->db->join('scholarship_thrust as t', 'fc.id_thrust = t.id');
        $this->db->join('scholarship_sub_thrust as st', 'fc.id_sub_thrust = st.id');
        $this->db->join('scholarship_programme as sp', 'fc.id_program = sp.id');
        if($data['id_program'] != '')
        {
            $this->db->where('fc.id_program', $data['id_program']);
        }
        if($data['id_thrust'] != '')
        {
            $this->db->where('fc.id_thrust', $data['id_thrust']);
        }
        if($data['id_sub_thrust'] != '')
        {
            $this->db->where('fc.id_sub_thrust', $data['id_sub_thrust']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramSyllabus($id)
    {
        $this->db->select('fc.*, t.name as thrust_name, t.code as thrust_code, st.scholarship_name, st.scholarship_code, sp.code as program_code, sp.name as program_name');
        $this->db->from('scholarship_program_syllabus as fc');
        $this->db->join('scholarship_thrust as t', 'fc.id_thrust = t.id');
        $this->db->join('scholarship_sub_thrust as st', 'fc.id_sub_thrust = st.id');
        $this->db->join('scholarship_programme as sp', 'fc.id_program = sp.id');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramSyllabus($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_program_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editProgramSyllabus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_program_syllabus', $data);
        return TRUE;
    }

    function getSubThrustByThrustId($id_thrust)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('id_thrust', $id_thrust);
        $query = $this->db->get();
        return $query->result();
    }

    function thrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function subThrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("scholarship_name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function syllanusModuleList($id_program_syllabus)
    {
        $this->db->select('*');
        $this->db->from('scholarship_program_syllabus_has_module');
        $this->db->where('id_program_syllabus', $id_program_syllabus);
        $query = $this->db->get();
        return $query->result();
    }

    function addSyllabusModule($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_program_syllabus_has_module', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteSyllabusModule($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_program_syllabus_has_module');
        return TRUE;
    }
}