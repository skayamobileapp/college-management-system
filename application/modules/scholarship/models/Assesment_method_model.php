<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assesment_method_model extends CI_Model
{
    function assesmentMethodList()
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_assesment_method_setup as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function assesmentMethodListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_assesment_method_setup as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.type  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAssesmentMethod($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_assesment_method_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAssesmentMethod($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_assesment_method_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAssesmentMethod($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_assesment_method_setup', $data);
        return TRUE;
    }
}

