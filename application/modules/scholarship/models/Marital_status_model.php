<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Marital_status_model extends CI_Model
{
    function maritalStatusList()
    {
        $this->db->select('*');
        $this->db->from('marital_status_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function maritalStatusListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('marital_status_setup');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or sequence  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMaritalStatus($id)
    {
        $this->db->select('*');
        $this->db->from('marital_status_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewMaritalStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('marital_status_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMaritalStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('marital_status_setup', $data);
        return TRUE;
    }
}

