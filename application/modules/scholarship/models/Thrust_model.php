<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Thrust_model extends CI_Model
{
    function thrustList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function thrustListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getThrust($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewThrust($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_thrust', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editThrust($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_thrust', $data);
        return TRUE;
    }
}