<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Module_type_model extends CI_Model
{
    function moduleTypeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_module_type_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function moduleTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_module_type_setup');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getModuleType($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_module_type_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewModuleType($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_module_type_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editModuleType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_module_type_setup', $data);
        return TRUE;
    }
}