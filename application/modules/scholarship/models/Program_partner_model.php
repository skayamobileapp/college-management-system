<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_partner_model extends CI_Model
{
    function programPartnerList($data)
    {
        $this->db->select('fc.*, t.name as thrust_name, t.code as thrust_code, st.scholarship_name, st.scholarship_code, s.name as location, sp.code as program_code, sp.name as program_name, spu.code as partner_code, spu.name as partner_name');
        $this->db->from('scholarship_program_partner as fc');
        $this->db->join('scholarship_thrust as t', 'fc.id_thrust = t.id');
        $this->db->join('scholarship_sub_thrust as st', 'fc.id_sub_thrust = st.id');
        $this->db->join('scholarship_state as s', 'fc.id_location = s.id');
        $this->db->join('scholarship_programme as sp', 'fc.id_program = sp.id');
        $this->db->join('scholarship_partner_university as spu', 'fc.id_partner_university = spu.id');
        if($data['id_program'] != '')
        {
            $this->db->where('fc.id_program', $data['id_program']);
        }
        if($data['id_thrust'] != '')
        {
            $this->db->where('fc.id_thrust', $data['id_thrust']);
        }
        if($data['id_sub_thrust'] != '')
        {
            $this->db->where('fc.id_sub_thrust', $data['id_sub_thrust']);
        }
        if($data['id_partner_university'] != '')
        {
            $this->db->where('fc.id_partner_university', $data['id_partner_university']);
        }
        if($data['id_location'] != '')
        {
            $this->db->where('fc.id_location', $data['id_location']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramPartner($id)
    {
        $this->db->select('fc.*, t.name as thrust_name, t.code as thrust_code, st.scholarship_name, st.scholarship_code, s.name as location');
        $this->db->from('scholarship_program_partner as fc');
        $this->db->join('scholarship_thrust as t', 'fc.id_thrust = t.id');
        $this->db->join('scholarship_sub_thrust as st', 'fc.id_sub_thrust = st.id');
        $this->db->join('scholarship_state as s', 'fc.id_location = s.id');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramPartner($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_program_partner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editProgramPartner($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_program_partner', $data);
        return TRUE;
    }

    function getSubThrustByThrustId($id_thrust)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('id_thrust', $id_thrust);
        $query = $this->db->get();
        return $query->result();
    }

    function getPartnerProgram($id_program)
    {
        $this->db->select('DISTINCT(sst.id_partner_university) as id, sel.code as partner_code, sel.name as partner_name');
        $this->db->from('scholarship_partner_university_has_program as sst');
        $this->db->join('scholarship_partner_university as sel','sst.id_partner_university = sel.id');
        $this->db->where('sst.id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function thrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function subThrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("scholarship_name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('scholarship_state as s');
        $this->db->join('scholarship_country as c','s.id_country = c.id');
        $this->db->where('s.status', $status);
        $this->db->where('c.name', 'Malaysia');
        $this->db->order_by("s.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function partnerListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_partner_university');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
}