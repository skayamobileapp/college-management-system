<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_information_model extends CI_Model
{

    function getProgramme($id)
    {
        $this->db->select('p.*');
        $this->db->from('scholarship_programme as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function programmeLandscapeListSearch($formData)
    {

        $this->db->select('pl.*, p.name as programme, i.name as intake');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->order_by("pl.id", "desc");
        if($formData['id_programme']) {
            $likeCriteria = "(pl.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['name']) {
            $likeCriteria = "(pl.name  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }

         if($formData['id_intake']) {
            $likeCriteria = "(pl.id_intake  LIKE '%" . $formData['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function programmeApplicantDocsByProgramId($id_programme,$formData)
    {
        $this->db->select('pl.*,p.code as programme_code, p.name as programme');
        $this->db->from('scholarship_documents_for_applicant as pl');
        $this->db->join('scholarship_programme as p', 'pl.id_scholarship_program = p.id');
        
        if($formData['id_scholarship_program'])
        {
            $likeCriteria = "(pl.id_scholarship_program  LIKE '%" . $formData['id_scholarship_program'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        if($formData['name'])
        {
            $likeCriteria = "(pl.name  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->where('pl.id_scholarship_program', $id_programme);
        $this->db->order_by("pl.name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        // echo "<pre>";print_r($result);exit;
        return $result;
    }

    function addCourseToProgramLandscape($data)
    {
        $this->db->trans_start();
        $this->db->insert('add_course_to_program_landscape', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCompulsoryCourse($id)
    {
        $ctype ="Compulsory";
        $this->db->select('cpl.*, c.name as coursename, s.name as semesterName, pl.name as programName, i.name as intake');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('semester as s', 'cpl.id_semester = s.id','left');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getMajorCourse($id)
    {
        $ctype ="Major";
        $this->db->select('cpl.*, c.name as coursename, s.name as semesterName, pl.name as programName, i.name as intake');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('semester as s', 'cpl.id_semester = s.id','left');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getMinorCourse($id)
    {
        $ctype ="Minor";
        $this->db->select('cpl.*, c.name as coursename, s.name as semesterName, pl.name as programName, i.name as intake');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('semester as s', 'cpl.id_semester = s.id','left');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getNotCompulsoryCourse($id)
    {
        $ctype ="Not-Compulsory";
        $this->db->select('cpl.*, c.name as coursename, s.name as semesterName, pl.name as programName, i.name as intake');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('semester as s', 'cpl.id_semester = s.id','left');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function getProgrammeLandscapeDetails($id)
    {
        $this->db->select('pl.*, p.name as programme, i.name as intake');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->where('pl.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgrammeLandscapeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_landscape', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeLandscapeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_landscape', $data);
        return TRUE;
    }

    function deleteCourseFromProgramLandscape($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('add_course_to_program_landscape');
         return TRUE;
    }

    function programmeLandscapeDuplicationCheck($id_programme,$id_intake)
    {
        $this->db->select('pl.*');
        $this->db->from('programme_landscape as pl');
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->where('pl.id_intake', $id_intake);
         $query = $this->db->get();
         
         $result = $query->row();

         // echo "<Pre>";print_r($result);exit;
         if($result)
         {
            return 1;
         }
         else
         {
             return 0;
         }
    }

    function programmeLandscapeCourseDuplicationCheck($id_landscape, $id_course)
    {
        $this->db->select('acpl.*');
        $this->db->from('add_course_to_program_landscape as acpl');
        $this->db->where('acpl.id_program_landscape', $id_landscape);
        $this->db->where('acpl.id_course', $id_course);
         $query = $this->db->get();
         
         $result = $query->row();

         // echo "<Pre>";print_r($result);exit;
         if($result)
         {
            return 1;
         }
         else
         {
             return 0;
         }
    }

    function intakeListForLandscape($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getIsPersonelDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholarship_applicant_personel_details as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsQualificationDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholarship_applicant_examination_details as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsFamilyDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholar_applicant_family_details as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsEmploymentDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholar_applicant_employment_status as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }


    function updateProgram($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_programme', $data);
        return TRUE;
    }

    function updateProgramIsPersonelDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('is_scholarship_applicant_personel_details', $data);
        return TRUE;
    }

    function updateProgramIsQualificationDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('is_scholarship_applicant_examination_details', $data);
        return TRUE;
    }

    function updateProgramIsFamilyDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('is_scholar_applicant_family_details', $data);
        return TRUE;
    }

    function updateProgramIsEmploymentDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('is_scholar_applicant_employment_status', $data);
        return TRUE;
    }

}

