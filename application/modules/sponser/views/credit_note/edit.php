<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Receipt</h3>
        </div>
        <form id="form_credit_note" action="" method="post">

            <div class="row">
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Receipt Number *</label>
                        <input type="text" class="form-control" id="credit_note_number" name="credit_note_number" value="<?php echo $creditNote->credit_note_number;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Receipt Amount *</label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount" value="<?php echo $creditNote->total_amount;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks *</label>
                        <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $creditNote->remarks;?>">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Approval Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="approval_status" id="approval_status" value="1" <?php if($creditNote->approval_status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="approval_status" id="approval_status" value="0" <?php if($creditNote->approval_status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_credit_note").validate({
            rules: {
                credit_note_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                remarks: {
                    required: true
                },
                approval_status: {
                    required: true
                }
            },
            messages: {
                credit_note_number: {
                    required: "Enter Receipt Number",
                },
                total_amount: {
                    required: "Enter Amount",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Select Approval Status",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
