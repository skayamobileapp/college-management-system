<?php $this->load->helper("form"); ?>
<form id="form_credit_note" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Sponsor Credit Note</h3>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label> Credit Note Number *</label>
                        <input type="text" class="form-control" id="credit_note_number" name="credit_note_number">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Note Amount *</label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks *</label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Approval Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="approval_status" id="approval_status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="approval_status" id="approval_status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>

            <h3>Sponsor Credit Note Details</h3><button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>
            <div class="row">
                <div id="view"></div>
            </div>

           



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sponsor Credit Note Details</h4>
      </div>
      <div class="modal-body">
         <h4></h4>


            <input type="text" class="form-control" id="id" name="id">
             <div class="row">

                    <div class="col-sm-3">
                    <div class="form-group">
                        <label>Sponsor Main Invoice *</label>
                        <select name="id_sponser_main_invoice" id="id_sponser_main_invoice" class="form-control">
                            <option value="">Select</option>
                                
                                <?php
                                if (!empty($mainInvoiceList))
                                {
                                    foreach ($mainInvoiceList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->invoice_number;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>

                        </select>
                    </div>
                </div>

                     <div class="col-sm-3">
                    <div class="form-group">
                        <label>Invoice Amount *</label>
                        <input type="number" class="form-control" id="invoice_amount" name="invoice_amount">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Credit Note Amount *</label>
                        <input type="number" class="form-control" id="credit_note_amount" name="credit_note_amount">
                    </div>
                </div>

                </div>

             <div id="view">     
             <div>


      </div>
      <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="saveData()">Add</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</form>
<script>

    function opendialog()
    {
        $("#id_sponser_main_invoice").val('');
        $("#invoice_amount").val('');
        $("#credit_note_amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData() {


        var tempPR = {};
        tempPR['id_sponser_main_invoice'] = $("#id_sponser_main_invoice").val();
        tempPR['invoice_amount'] = $("#invoice_amount").val();
        tempPR['credit_note_amount'] = $("#credit_note_amount").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/sponser/creditNote/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                $('#myModal').modal('hide');
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/sponser/creditNote/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/procurement/prEntry/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }

    $(document).ready(function() {
        $("#form_credit_note").validate({
            rules: {
                credit_note_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                remarks: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                credit_note_number: {
                    required: "Enter Credit Note Number",
                },
                total_amount: {
                    required: "Enter Total Amount",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Select status",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
