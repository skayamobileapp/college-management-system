<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Sponsor</h3>
        </div>
        <form id="form_sponser" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Sponsor Details</h4>         
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $sponserDetails->code;?>" readonly>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $sponserDetails->name;?>">
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>         
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $sponserDetails->address;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 2 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $sponserDetails->address2;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $sponserDetails->mobile_number;?>">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Country <span class='error-text'>*</span></label>
                            <select name="id_country" id="id_country" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $sponserDetails->id_country)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select State <span class='error-text'>*</span></label>
                            <select name="id_state" id="id_state" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $sponserDetails->id_state)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Location <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="location" name="location"value="<?php echo $sponserDetails->location;?>">
                        </div>
                    </div>


                </div>

                <div class="row">


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="fax" name="fax"value="<?php echo $sponserDetails->fax;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email" name="email"value="<?php echo $sponserDetails->email;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>ZIP Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="zip_code" name="zip_code" value="<?php echo $sponserDetails->zip_code;?>">
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>


        <br>


        <form id="form_detail" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Convocation Fee Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Code <span class='error-text'>*</span></label>
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeList))
                                {
                                    foreach ($feeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>">
                                            <?php echo  $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Frequency Mode <span class='error-text'>*</span></label>
                            <select name="id_frequency_mode" id="id_frequency_mode" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($frequencyModeList))
                                {
                                    foreach ($frequencyModeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>">
                                            <?php echo  $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                   
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Calculation Mode <span class='error-text'>*</span></label>
                            <select name="calculation_mode" id="calculation_mode" class="form-control">
                                <option value="">Select</option>
                                <option value="Amount">Amount</option>
                                <option value="Percentage">Percentage</option>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" name="fee_amount" id="fee_amount" autocomplete="off" value="">
                            <!-- <span id='view_centers'></span> -->
                        </div>
                    </div>


                     <div class="col-sm-4">
                            <div class="form-group">
                                <p>Repeat <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="repeat" id="repeat" value="0" checked="checked"><span class="check-radio"></span> No
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="repeat" id="repeat" value="1"><span class="check-radio"></span> Yes
                                </label>                              
                            </div>                         
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Max Repeat <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" name="max_repeat" id="max_repeat" autocomplete="off">
                            <!-- <span id='view_centers'></span> -->
                        </div>
                    </div>


                </div>

                <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="saveFeeDetailData()">Add</button>
                    <!-- <a href="list" class="btn btn-link">Cancel</a> -->
                </div>
            </div>

            </div>

        </form>


        <?php
        if(!empty($sponserFeeDetails))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Sponsor Fee Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Fee Item</th>
                            <th>Frequency Mode</th>
                            <th>Calculation Mode</th>
                            <th>Is Repeat</th>
                            <th>Max. Repeat</th>
                            <th>Fee Amount</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($sponserFeeDetails);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $sponserFeeDetails[$i]->fee_name . " - " . $sponserFeeDetails[$i]->fee_code; ?></td>
                            <td><?php echo $sponserFeeDetails[$i]->frequency_name . " - " . $sponserFeeDetails[$i]->frequency_code; ?></td>
                            <td><?php echo $sponserFeeDetails[$i]->calculation_mode; ?></td>
                            <td>
                            <?php if($sponserFeeDetails[$i]->repeat == 1)
                            {
                                echo "Yes";
                            }elseif($sponserFeeDetails[$i]->repeat == 0)
                            {
                                echo "No";
                            }; ?>
                               
                            </td>
                            <td><?php echo $sponserFeeDetails[$i]->max_repeat; ?></td>
                            <td><?php echo $sponserFeeDetails[$i]->amount; ?></td>
                            <td class="text-center">
                                <a onclick="deleteFeeDetailData(<?php echo $sponserFeeDetails[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>





    <br>



    <form id="form_detail_coordinator" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Convocation Fee Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="coordinator_name" id="coordinator_name" autocomplete="off">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" name="coordinator_email" id="coordinator_email" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phone <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="coordinator_phone" id="coordinator_phone" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="coordinator_mobile" id="coordinator_mobile" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fax <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="coordinator_fax" id="coordinator_fax" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="coordinator_status" id="coordinator_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="coordinator_status" id="coordinator_status" value="0"><span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>

            </div>



            <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="saveCoordinatorDetailData()">Add</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

        </div>

    </form>



    <?php
        if(!empty($sponserCoordinatorDetails))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Sponser Co-ordinator Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Mobile</th>
                            <th>Fax</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($sponserCoordinatorDetails);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $sponserCoordinatorDetails[$i]->name; ?></td>
                            <td><?php echo $sponserCoordinatorDetails[$i]->email; ?></td>
                            <td><?php echo $sponserCoordinatorDetails[$i]->phone; ?></td>
                            <td><?php echo $sponserCoordinatorDetails[$i]->mobile; ?></td>
                            <td><?php echo $sponserCoordinatorDetails[$i]->fax; ?></td>
                            <td>
                                <?php if($sponserCoordinatorDetails[$i]->status == 1)
                            {
                                echo "Active";
                            }elseif($sponserCoordinatorDetails[$i]->status == 0)
                            {
                                echo "In-Active";
                            }; ?>
                                
                            </td>
                            <td class="text-center">
                                <a onclick="deleteCoordinatorDetailData(<?php echo $sponserCoordinatorDetails[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>






        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    $('select').select2();



     function saveFeeDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['id_frequency_mode'] = $("#id_frequency_mode").val();
        tempPR['calculation_mode'] = $("#calculation_mode").val();
        tempPR['repeat'] = $("#repeat").val();
        tempPR['max_repeat'] = $("#max_repeat").val();
        tempPR['amount'] = $("#fee_amount").val();
        tempPR['id_sponser'] = <?php echo $sponserDetails->id; ?>;

            $.ajax(
            {
               url: '/sponser/sponser/saveFeeDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view_temp_details").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }


     function saveCoordinatorDetailData()
    {
        if($('#form_detail_coordinator').valid())
        {

        var tempPR = {};

        tempPR['name'] = $("#coordinator_name").val();
        tempPR['email'] = $("#coordinator_email").val();
        tempPR['phone'] = $("#coordinator_phone").val();
        tempPR['mobile'] = $("#coordinator_mobile").val();
        tempPR['fax'] = $("#coordinator_fax").val();
        tempPR['status'] = $("#coordinator_status").val();
        tempPR['id_sponser'] = <?php echo $sponserDetails->id; ?>;

            $.ajax(
            {
               url: '/sponser/sponser/saveCoordinatorDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view_temp_details").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function deleteFeeDetailData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/sponser/sponser/deleteFeeDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view_temp_details").html(result);
                    window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }

    function deleteCoordinatorDetailData(id)
    {
         $.ajax(
            {
               url: '/sponser/sponser/deleteCoordinatorDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view_temp_details").html(result);
                    window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                id_frequency_mode: {
                    required: true
                },
                calculation_mode: {
                    required: true
                },
                repeat: {
                    required: true
                },
                max_repeat: {
                    required: true
                },
                fee_amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                calculation_mode: {
                    required: "<p class='error-text'>Select Calculation Type</p>",
                },
                repeat: {
                    required: "<p class='error-text'>Select Repeat</p>",
                },
                max_repeat: {
                    required: "<p class='error-text'>Max. Repeat Reuired</p>",
                },
                fee_amount: {
                    required: "<p class='error-text'>Fee Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_detail_coordinator").validate({
            rules: {
                coordinator_name: {
                    required: true
                },
                coordinator_email: {
                    required: true
                },
                coordinator_phone: {
                    required: true
                },
                coordinator_mobile: {
                    required: true
                },
                coordinator_fax: {
                    required: true
                },
                coordinator_status: {
                    required: true
                }
            },
            messages: {
                coordinator_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                coordinator_email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                coordinator_phone: {
                    required: "<p class='error-text'>Phone Required</p>",
                },
                coordinator_mobile: {
                    required: "<p class='error-text'>Mobile Required</p>",
                },
                coordinator_fax: {
                    required: "<p class='error-text'>Fax Required</p>",
                },
                coordinator_status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });










    
    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                address: {
                    required: true
                },
                mobile_number: {
                    required: true
                },
                id_state: {
                    required: true
                },
                id_city: {
                    required: true
                },
                zip_code: {
                    required: true
                },
                status: {
                    required: true
                },
                address2: {
                    required: true
                },
                location: {
                    required: true
                },
                fax: {
                    required: true
                },
                email: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Enter Name</p>",
                },
                address: {
                    required: "<p class='error-text'>Enter Address</p>",
                },
                mobile_number: {
                    required: "<p class='error-text'>Enter Mobile Number</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                id_city: {
                    required: "<p class='error-text'>Select City</p>",
                },
                zip_code: {
                    required: "<p class='error-text'>Enter Zip Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                },
                address2: {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                location: {
                    required: "<p class='error-text'>Location Required</p>",
                },
                fax: {
                    required: "<p class='error-text'>Fax Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
