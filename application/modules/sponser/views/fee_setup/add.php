<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Fee Setup</h3>
        </div>
        <form id="form_fee_setup" action="" method="post">

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Name In Malay <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Category <span class='error-text'>*</span></label>
                        <select name="id_fee_category" id="id_fee_category" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($feeCategoryList))
                            {
                                foreach ($feeCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount Calculation Type <span class='error-text'>*</span></label>
                        <select name="id_amount_calculation_type" id="id_amount_calculation_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($amountCalculationTypeList))
                            {
                                foreach ($amountCalculationTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Frequency Mode <span class='error-text'>*</span></label>
                        <select name="id_frequency_mode" id="id_frequency_mode" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($frequencyModeList))
                            {
                                foreach ($frequencyModeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                                    <option value="<?php echo $record->code;?>"
                                    ><?php echo $record->code;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GST Tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="gst_tax" name="gst_tax">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="effective_date" name="effective_date">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_fee_setup").validate({
            rules: {
                name: {
                    required: true
                },
                name_optional_language: {
                    required: true
                },
                code: {
                    required: true
                },
                id_fee_category: {
                    required: true
                },
                id_amount_calculation_type: {
                    required: true
                },
                id_frequency_mode:{
                    required: true
                },
                account_code: {
                    required: true
                },
                effective_date: {
                    required: true
                },
                gst_tax: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Name Required",
                },
                name_optional_language: {
                    required: "Name 2 Required",
                },
                code: {
                    required: "Fee Code Required",
                },
                id_fee_category: {
                    required: "Select Fee Category",
                },
                id_amount_calculation_type: {   
                    required: "Select Amount Calculation Type",
                },
                id_frequency_mode: {
                    required: "Select Frequency Mode",
                },
                account_code: {
                    required: "Select Account Code",
                },
                effective_date: {
                    required: "Effective Date Required",
                },
                gst_tax: {
                    required: "Enter GST Tax",
                },
                status: {
                    required: "Status required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
