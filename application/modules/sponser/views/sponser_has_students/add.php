<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sponsership To Students</h3>
        </div>
        <form id="form_sponser_has_students" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Sponsor Details</h4>
            <div class='data-list'>
                <div class='row'> 
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Sponsor Name :</dt>
                            <dd><?php echo ucwords($sponser->name);?></dd>
                        </dl>
                        <dl>
                            <dt>Sponsor Mobile :</dt>
                            <dd><?php echo $sponser->mobile_number ?></dd>
                        </dl>
                                           
                    </div>        
                    
                    <div class='col-sm-6'>  
                        <dl>
                            <dt>Sponsor Code :</dt>
                            <dd><?php echo $sponser->code ?></dd>
                        </dl>                           
                        <dl>
                            <dt>Sponsor Email :</dt>
                            <dd><?php echo $sponser->email; ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>




        <br>
        <!-- <h3 style="text-align: center;">Search Student For Sporser Tagging</h3></td> -->



        <div class="form-container">
            <h4 class="form-group-title">Select Student For Sponsor</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Student <span class='error-text'>*</span></label>
                        <select name="id_student" id="id_student" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->nric .". ".$record->full_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="false">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="false">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Aggrement No. <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="aggrement_no" name="aggrement_no" autocomplete="false">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="amount" name="amount" autocomplete="false">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Item <span class='error-text'>*</span></label>
                        <select name="id_fee_item" id="id_fee_item" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($feeList))
                            {
                                foreach ($feeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo  $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Calculation Mode <span class='error-text'>*</span></label>
                        <select name="calculation_mode" id="calculation_mode" class="form-control">
                            <option value="">Select</option>
                            <option value="Amount">Amount</option>
                            <option value="Percentage">Percentage</option>
                        </select>
                    </div>
                </div>



            </div>

        </div>


        <!-- <div class="page-title clearfix">
            <h4>Search Student For Sporser Tagging</h4>
        </div> -->


            <div>
      <!--           <table border="0px" style="width: 100%">
                    <tr>
                        <td style="text-align: right;" colspan="6">
                            <button type="button" id="btn_add_detail" onclick="showstudentlist()" class="btn btn-primary btn-light btn-lg">Search</button>
                        </td>
                    </tr>

                </table>
                <br>
            </div> -->

        </div>

        <div class="form-container" id="view_visible" style="display: none;">
            <h4 class="form-group-title">Student Details</h4>

            <div id="view">     
            </div>

        </div>




     


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>


        <?php
        if(!empty($sponserHasStudentsList))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Student List</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Student</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Aggrement No.</th>
                            <th>Fee Item</th>
                            <th>Calculation Mode</th>
                            <th>Amount</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($sponserHasStudentsList);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $sponserHasStudentsList[$i]->nric . " - " . $sponserHasStudentsList[$i]->student; ?></td>
                            <td><?php echo date('d-m-Y', strtotime($sponserHasStudentsList[$i]->start_date)); ?></td>
                            <td><?php echo date('d-m-Y', strtotime($sponserHasStudentsList[$i]->end_date)); ?></td>
                            <td><?php echo $sponserHasStudentsList[$i]->aggrement_no; ?></td>
                            <td><?php echo $sponserHasStudentsList[$i]->fee_code . " - " . $sponserHasStudentsList[$i]->fee_name; ?></td>
                            <td><?php echo $sponserHasStudentsList[$i]->calculation_mode; ?></td>
                            <td><?php echo $sponserHasStudentsList[$i]->amount; ?></td>
                            <td class="text-center">
                                <a onclick="deleteStudentFromSponser(<?php echo $sponserHasStudentsList[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

</div>
<script>

    $('select').select2();
    

 // function searchStudent() {


 //        var tempPR = {};
 //        tempPR['id_programme'] = $("#id_programme");
 //        tempPR['id_intake'] = $("#id_intake");
 //        tempPR['student'] = $("#student");
 //            $.ajax(
 //            {
 //               url: '/sponser/sponserHasStudents/getStudentsSearch',
 //                type: 'POST',
 //               data:
 //               {
 //                tempData: tempPR
 //               },
 //               error: function()
 //               {
 //                alert('Something is wrong');
 //               },
 //               success: function(result)
 //               {
 //                $("#view_student_details").html(result);
 //                // var ta = $("#inv-total-amount").val();
 //                // $("#total_amount").val(ta);
 //               }
 //            });
        
 //    }

    function showstudentlist()
    // $("button").click(function()
    {
        // alert("asdf");
      var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['student'] = $("#student").val();

        $.ajax(
        {
           url: '/sponser/sponserHasStudents/getStudentsSearch',
           type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert(data);
            alert('Something is wrong');
           },
           success: function(result)
           {
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }

    function deleteStudentFromSponser(ii) {
         $.ajax(
            {
               url: '/sponser/sponserHasStudents/deleteStudentFromSponser/'+ii,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(ii);
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }


    $(function ()
    {
         // alert("asdf");
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });


    $(document).ready(function() {
        $("#form_sponser_has_students").validate({
            rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                id_fee_item: {
                    required: true
                },
                id_student: {
                    required: true
                },
                calculation_mode: {
                    required: true
                },
                amount: {
                    required: true
                },
                aggrement_no: {
                    required: true
                }
            },
            messages: {
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                calculation_mode: {
                    required: "<p class='error-text'>Select Calculation Mode</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                aggrement_no: {
                    required: "<p class='error-text'>Aggrement No. Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );



</script>
