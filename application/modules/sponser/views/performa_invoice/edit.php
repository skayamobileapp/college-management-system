<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Performa Invoice</h3>
        </div>
        <form id="form_performa_invoice" action="" method="post">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type Of Invoice *</label>
                        <select name="type_of_invoice" id="type_of_invoice" class="form-control">
                            <option value="<?php echo $performaInvoice->type_of_invoice;  ?>"
                                <?php 
                                if ($performaInvoice->type_of_invoice == 'Student')
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo "Student";  ?>
                            </option>

                            <option value="<?php echo $performaInvoice->type_of_invoice;?>"
                                <?php 
                                if ($performaInvoice->type_of_invoice == 'Applicant')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Applicant";  ?>
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Performa Number *</label>
                        <input type="text" class="form-control" id="performa_number" name="performa_number" value="<?php echo $performaInvoice->performa_number;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount *</label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount" value="<?php echo $performaInvoice->total_amount;?>">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student *</label>
                        <select name="id_student" id="id_student" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $performaInvoice->id_student)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Application *</label>
                        <select name="id_application" id="id_application" class="form-control">
                            <option value="<?php echo $performaInvoice->id_application;  ?>"
                                <?php 
                                if ($performaInvoice->id_application == '1')
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo "Application1";  ?>
                            </option>

                            <option value="<?php echo $performaInvoice->id_application;  ?>"
                                <?php 
                                if ($performaInvoice->id_application == '2')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Application2";  ?>
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks *</label>
                        <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $performaInvoice->remarks;?>">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($performaInvoice->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($performaInvoice->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_performa_invoice").validate({
            rules: {
                type_of_invoice: {
                    required: true
                },
                performa_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_application: {
                    required: true
                },
                remarks: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                type_of_invoice: {
                    required: "Select Type Of Invoice",
                },
                performa_number: {
                    required: "Enter Performa Number",
                },
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                id_application: {
                    required: "Select Application",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Status required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
