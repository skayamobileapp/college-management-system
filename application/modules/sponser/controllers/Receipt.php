<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Receipt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('receipt_model');
        $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('receipt.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['receiptList'] = $this->receipt_model->receiptList();
            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("receipt/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('receipt.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;

                $receipt_number = $this->security->xss_clean($this->input->post('receipt_number'));
                $receipt_amount = $this->security->xss_clean($this->input->post('receipt_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $approval_status = $this->security->xss_clean($this->input->post('approval_status'));

                $data = array(
                    'receipt_number' => $receipt_number,
                    'receipt_amount' => $receipt_amount,
                    'remarks' => $remarks,
                    'approval_status' => $approval_status
                );
                $inserted_id = $this->receipt_model->addNewReceipt($data);

                // $id_sponser_main_invoice = $this->security->xss_clean($this->input->post('id_sponser_main_invoice'));
                // $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
                // $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));

                $temp_details = $this->receipt_model->getTempReceiptDetails($id_session);
                 for($i=0;$i<count($temp_details);$i++)
                 {
                    $id_sponser_main_invoice = $temp_details[$i]->id_sponser_main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $paid_amount = $temp_details[$i]->paid_amount;

                     $detailsData = array(
                        'id_sponser_receipt' => $inserted_id,
                        'id_sponser_main_invoice' => $id_sponser_main_invoice,
                        'invoice_amount' => $invoice_amount,
                        'paid_amount' => $paid_amount,
                    );
                    //print_r($details);exit;
                    $result = $this->receipt_model->addNewReceiptDetails($detailsData);
                 }

                $this->receipt_model->deleteTempDataBySession($id_session);
                redirect('/sponser/receipt/list');
            }

            $data['mainInvoiceList'] = $this->main_invoice_model->mainInvoiceList();
            $this->global['pageTitle'] = 'Campus Management System : Add Receipt';
            $this->loadViews("receipt/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('receipt.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/sponser/receipt/list');
            }
            if($this->input->post())
            {
               $receipt_number = $this->security->xss_clean($this->input->post('receipt_number'));
                $receipt_amount = $this->security->xss_clean($this->input->post('receipt_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $approval_status = $this->security->xss_clean($this->input->post('approval_status'));

                $data = array(
                    'receipt_number' => $receipt_number,
                    'receipt_amount' => $receipt_amount,
                    'remarks' => $remarks,
                    'approval_status' => $approval_status
                );
                //print_r($data);exit;
                $result = $this->receipt_model->editReceipt($data,$id);
                redirect('/sponser/receipt/list');
            }
            // $data['studentList'] = $this->receipt_model->studentList();
            $data['receipt'] = $this->receipt_model->getReceipt($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Receipt';
            $this->loadViews("receipt/edit", $this->global, $data, NULL);
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->receipt_model->updateTempDetails($tempData,$id);
        }
        else
        {

            unset($tempData['id']);
            $inserted_id = $this->receipt_model->addTempDetails($tempData);
        }
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->receipt_model->getTempReceiptDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Sponser Invoice No.</th>
                    <th>Invoice Amount</th>
                    <th>Paid Amount</th>
                    <th>Action</th>
                </tr>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $sponser_main_invoice = $temp_details[$i]->sponser_main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $paid_amount = $temp_details[$i]->paid_amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$sponser_main_invoice</td>
                            <td>$invoice_amount</td>                            
                            <td>$paid_amount</td>                            
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</table>";
        return $table;
    }

    function tempDetailsDataAdd()
    {
        
        $id_session = $this->session->my_session_id;

        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));
             
        // echo "<Pre>";  print_r($effective_start_date);exit;
        $data = array(
            'id_session' => $id_session,
            'id_staff' => $id_staff,
            'effective_start_date' => $effective_start_date
        );   
        $inserted_id = $this->receipt_model->addNewTempProgrammeHasDean($data);

        $temp_details = array(
                'id' => $inserted_id,
                'id_staff' => $id_staff,
                'effective_start_date' => $effective_start_date
            );

        $temp_details = $this->receipt_model->getTempProgrammeHasDean($id_session);
        // echo "<Pre>";  print_r($temp_details);exit;

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . ". " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;

                    $table .= "
                <tr>
                    <td>$i+1</td>
                    <td>$staff</td>
                    <td>$effective_start_date</td>
                    <td>
                        <button onclick='deleteid($id)' type='button'>Delete</button>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->receipt_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempDetailsDataAdd1()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_sponser_main_invoice = $this->security->xss_clean($this->input->post('id_sponser_main_invoice'));
        $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
        $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));

        $data = array(
                'id_session' => $id_session,
                'id_sponser_main_invoice' => $id_sponser_main_invoice,
                'invoice_amount' => $invoice_amount,
                'paid_amount' => $paid_amount
            );
        $inserted_id = $this->receipt_model->addNewTempReceiptDetails($data);
        // echo "<Pre>";  print_r($data);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'id_sponser_main_invoice' => $id_sponser_main_invoice,
                'invoice_amount' => $invoice_amount,
                'paid_amount' => $paid_amount
            );
        $temp_details = $this->receipt_model->getTempReceiptDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Main Invoice</th>
                    <th>Invoice Amount</th>
                    <th>Paid Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $main_invoice = $temp_details[$i]->sponser_main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $paid_amount = $temp_details[$i]->paid_amount;

                    $table .= "
                <tr>
                    <td>$main_invoice</td>
                    <td>$invoice_amount</td>
                    <td>$paid_amount</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }
}
