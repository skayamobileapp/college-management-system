<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CreditNote extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('credit_note_model');
        $this->load->model('payment_type_model');
        $this->load->model('main_invoice_model');
        $this->load->model('receipt_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('credit_note.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['creditNoteList'] = $this->credit_note_model->creditNoteList();
            $this->global['pageTitle'] = 'Campus Management System : Receipt Paid Details List';
            $this->loadViews("credit_note/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('credit_note.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;

                $credit_note_number = $this->security->xss_clean($this->input->post('credit_note_number'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $approval_status = $this->security->xss_clean($this->input->post('approval_status'));

                $data = array(
                    'credit_note_number' => $credit_note_number,
                    'total_amount' => $total_amount,
                    'remarks' => $remarks,
                    'approval_status' => $approval_status
                );
                $inserted_id = $this->credit_note_model->addNewCreditNote($data);
                $temp_details = $this->credit_note_model->getTempCreditNoteDetails($id_session);
                 for($i=0;$i<count($temp_details);$i++)
                 {
                    $id_sponser_main_invoice = $temp_details[$i]->id_sponser_main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $credit_note_amount = $temp_details[$i]->credit_note_amount;

                     $detailsData = array(
                        'id_sponser_credit_note' => $inserted_id,
                        'id_sponser_main_invoice' => $id_sponser_main_invoice,
                        'invoice_amount' => $invoice_amount,
                        'credit_note_amount' => $credit_note_amount,
                    );
                    //print_r($details);exit;
                    $result = $this->credit_note_model->addNewCreditNoteDetails($detailsData);
                 }

                $this->credit_note_model->deleteTempDataBySession($id_session);

                redirect('/sponser/creditNote/list');
            }
            $data['receiptList'] = $this->receipt_model->receiptList();
            $data['mainInvoiceList'] = $this->main_invoice_model->mainInvoiceList();
            $data['paymentTypeList'] = $this->payment_type_model->paymentTypeList();
            $this->global['pageTitle'] = 'Campus Management System : Add Receipt Paid Details';
            $this->loadViews("credit_note/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('credit_note.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/sponser/creditNote/list');
            }
            if($this->input->post())
            {
                $credit_note_number = $this->security->xss_clean($this->input->post('credit_note_number'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $approval_status = $this->security->xss_clean($this->input->post('approval_status'));

                $data = array(
                    'credit_note_number' => $credit_note_number,
                    'total_amount' => $total_amount,
                    'remarks' => $remarks,
                    'approval_status' => $approval_status
                );
                //print_r($data);exit;
                $result = $this->credit_note_model->editCreditNote($data,$id);
                redirect('/sponser/creditNote/list');
            }
            // $data['studentList'] = $this->credit_note_model->studentList();
            $data['creditNote'] = $this->credit_note_model->getCreditNote($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Receipt Paid Details';
            $this->loadViews("credit_note/edit", $this->global, $data, NULL);
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->credit_note_model->updateTempDetails($tempData,$id);
        }
        else
        {

            unset($tempData['id']);
            $inserted_id = $this->credit_note_model->addTempDetails($tempData);
        }
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->credit_note_model->getTempCreditNoteDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Sponser Main Invoice</th>
                    <th>Invoice Amount</th>
                    <th>Paid Amount</th>
                    <th>Action</th>
                </tr>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $sponser_main_invoice = $temp_details[$i]->sponser_main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $credit_note_amount = $temp_details[$i]->credit_note_amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$sponser_main_invoice</td>
                            <td>$invoice_amount</td>                            
                            <td>$credit_note_amount</td>                            
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</table>";
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->credit_note_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 


    function tempDetailsDataAdd()
    {
        // echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_sponser_main_invoice = $this->security->xss_clean($this->input->post('id_sponser_main_invoice'));
        $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
        $credit_note_amount = $this->security->xss_clean($this->input->post('credit_note_amount'));

        $data = array(
                'id_session' => $id_session,
                'id_sponser_main_invoice' => $id_sponser_main_invoice,
                'invoice_amount' => $invoice_amount,
                'credit_note_amount' => $credit_note_amount
            );
        $inserted_id = $this->credit_note_model->addNewTempCreditNoteDetails($data);
        // echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'id_sponser_main_invoice' => $id_sponser_main_invoice,
                'invoice_amount' => $invoice_amount,
                'credit_note_amount' => $credit_note_amount
            );
        $temp_details = $this->credit_note_model->getTempCreditNoteDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Main Invoice</th>
                    <th>Invoice Amount</th>
                    <th>Paid Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $sponser_main_invoice = $temp_details[$i]->sponser_main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $credit_note_amount = $temp_details[$i]->credit_note_amount;

                    $table .= "
                <tr>
                    <td>$sponser_main_invoice</td>
                    <td>$invoice_amount</td>
                    <td>$credit_note_amount</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }
}
