<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Receipt_model extends CI_Model
{
    function receiptList()
    {
        $this->db->select('r.*');
        $this->db->from('sponser_receipt as r');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReceipt($id)
    {
        $this->db->select('r.*');
        $this->db->from('sponser_receipt as r');
        $this->db->where('r.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editReceipt($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sponser_receipt', $data);
        return TRUE;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewTempReceiptDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;
        $this->db->trans_start();
        $this->db->insert('temp_sponser_receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
// echo "<Pre>";  print_r($db);exit;
        return $insert_id;
    }

    function getTempReceiptDetails($id_session)
    {
         $this->db->select('trd.*, mi.invoice_number as sponser_main_invoice');
        $this->db->from('temp_sponser_receipt_details as trd');
        $this->db->join('sponser_main_invoice as mi', 'trd.id_sponser_main_invoice = mi.id');        
        $this->db->where('trd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_sponser_receipt_details');
    }

     function deleteTempData($id)
    { 
       $this->db->where('id', $id);
       $this->db->delete('temp_sponser_receipt_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_sponser_receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_sponser_receipt_details', $data);
        return TRUE;
    }
}

