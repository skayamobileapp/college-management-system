<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add PR Entry</h3>
            </div>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Entry Type <span class='error-text'>*</span></label>
                        <select name='type_of_pr' id='type_of_pr' class='form-control'>
                            <option value=''>Select</option>
                            <option value='PO'>PO</option>
                            <option value='Non-PO'>Non-PO</option>
                            <option value='Warrant'>Warrant</option>
                            <option value='Tender'>Tender</option>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <select name="id_vendor" id="id_vendor" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($vendorList))
                            {
                                foreach ($vendorList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                  

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control" onchange="getCategory(this.value)">
                            <option value="">Select</option>
                            <option value="Asset">Asset</option>
                            <option value="Procurement">Procurement</option>
                        </select>
                    </div>
                </div> 


               
            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  
               


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  
            
            </div>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" readonly="readonly" value="0">
                    </div>
               </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

            </div>

        </form>

            <h3>PR Details</h3><button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>
            <br>
            <br>
            <div class="row">
                <div id="view"></div>
            </div>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<form id="form_pr_entry_details" action="" method="post">

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PR Details</h4>
      </div>
      <div class="modal-body">
         <h4></h4>


             <div class="row">

                <input type="hidden" class="form-control" id="id" name="id">
                


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <span id='view_category'></span>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Sub Category <span class='error-text'>*</span></label>
                        <span id='view_sub_category'></span>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Item <span class='error-text'>*</span></label>
                        <span id='view_item'></span>
                    </div>
                </div>  

            </div>


            <h4>Debit Budget GL</h4>

             <div class="row">   


                <div class="col-sm-3" id="view_debit_budget_fund_span" style="display: none">
                    <div class="form-group">
                        <label>Dt Fund Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_fund"></span>
                    </div>
                </div>

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Dt Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="dt_department" name="dt_department" readonly>
                    </div>
                </div>

                <div class="col-sm-3" id="view_debit_budget_activity_span" style="display: none">
                    <div class="form-group">
                        <label>Dt Activity Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_activity"></span>
                    </div>
                </div>

                <div class="col-sm-3" id="view_debit_budget_account_span" style="display: none">
                    <div class="form-group">
                        <label>Dt Account Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_account"></span>
                    </div>
                </div>
            </div>


            <div class="row">   
                <div class="col-sm-3" id="view_debit_span" style="display: none">
                    <div class="form-group">
                        <label>GL Debit Balance Amount <span class='error-text'>*</span></label>
                        <span id="view_debit_amount"></span>
                    </div>
            </div>

<!-- 



                <div class="col-sm-3">

                    <div class="col-sm-3">
                    <div class="form-group">
                        <label>Dt Fund <span class='error-text'>*</span></label>
                        <select name="dt_fund" id="dt_fund" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                    <div class="form-group">
                        <label>Dt Department <span class='error-text'>*</span></label>
                        <select name="dt_department" id="dt_department" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Dt Activity <span class='error-text'>*</span></label>
                        <select name="dt_activity" id="dt_activity" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Dt Account <span class='error-text'>*</span></label>
                        <select name="dt_account" id="dt_account" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   -->




            </div>



            <h4>Credit Budget GL</h4>


            <div class="row">


                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Fund <span class='error-text'>*</span></label>
                        <select name="cr_fund" id="cr_fund" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   
                

                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Department <span class='error-text'>*</span></label>
                        <select name="cr_department" id="cr_department" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 



                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Activity <span class='error-text'>*</span></label>
                        <select name="cr_activity" id="cr_activity" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Account <span class='error-text'>*</span></label>
                        <select name="cr_account" id="cr_account" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


            </div>

           

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Quantity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="quantity" name="quantity">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="price" name="price">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tax Code <span class='error-text'>*</span></label>
                        <select name="id_tax" id="id_tax" class="form-control" style="width: 196px;" onchange="getTaxCalculated()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($taxList))
                            {
                                foreach ($taxList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name . " - " . $record->percentage . " %";?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tax Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax_price" name="tax_price" readonly="readonly">
                    </div>
               </div>
                
            </div>
            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tax Percentage(%) <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax_percentage" name="tax_percentage" readonly="readonly">
                    </div>
               </div>


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_final" name="total_final" readonly="readonly">
                    </div>
               </div>

               <div class="col-sm-3">
                    <div class="form-group">
                        <span id='view_dummy' style="display: none;"></span>
                        <!-- <span id='view_dummy'></span> -->
                    </div>
                </div>  



            </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="saveData()">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</form>
<script>

    function opendialog()
    {
         if($('#form_pr_entry').valid())
        {
            // $("#dt_fund").val('');
            // // $("#dt_department").val('');
            // $("#dt_activity").val('');
            // $("#dt_account").val('');
            // $("#cr_fund").val('');
            // $("#cr_department").val('');
            // $("#cr_activity").val('');
            // $("#cr_account").val('');
            // $("#id_category").val('');
            // $("#id_sub_category").val('');
            // $("#id_item").val('');
            // $("#quantity").val('');
            // $("#price").val('');
            // $("#id_tax").val('');
            // $("#tax_price").val('');
            // $("#total_final").val('');
            // $("#tax_percentage").val('');
            // $("#id").val('0');
            $('#myModal').modal('show');
        }
    }

    function getDebitBudget()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;

            $.ajax(
            {
               url: '/procurement/prEntry/getDebitBudget',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_debit_budget_fund").html(result);
                    $("#view_debit_budget_fund_span").show();


                    // document.getElementById('dt_department').value = document.getElementById('department_code').value;

                    // $("#view_debit_budget_department_span").show();
                    $("#dt_department").val(id_dt_department);

                }
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
        }
    }

    function getActivityDebitCodes()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();
        var dt_fund_code = $("#dt_fund").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;

            $.ajax(
            {
               url: '/procurement/prEntry/getActivityDebitCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    
                    $("#view_debit_budget_activity").html(result);
                    $("#view_debit_budget_activity_span").show();
                }
               }
            });
        }
    }


    function getAccountDebitCodes()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();
        var dt_fund_code = $("#dt_fund").val();
        var dt_activity_code = $("#dt_activity").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '' && dt_activity_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;
        tempPR['dt_activity_code'] = dt_activity_code;

            $.ajax(
            {
               url: '/procurement/prEntry/getAccountDebitCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_debit_budget_account").html(result);
                    $("#view_debit_budget_account_span").show();
                }
               }
            });
        }
    }

    function getBudgetAllocationByCodes()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();
        var dt_fund_code = $("#dt_fund").val();
        var dt_activity_code = $("#dt_activity").val();
        var dt_account_code = $("#dt_account").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '' && dt_activity_code != '' && dt_account_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;
        tempPR['dt_activity_code'] = dt_activity_code;
        tempPR['dt_account_code'] = dt_account_code;

            $.ajax(
            {
               url: '/procurement/prEntry/getBudgetAllocationByCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_debit_amount").html(result);
                    $("#view_debit_span").show();
                }
               }
            });
        }
    }



    function saveData()
    {
        if($('#form_pr_entry_details').valid())
        {
            $('#myModal').modal('hide');
            document.getElementById("type").disabled = false;

            var tempPR = {};
            tempPR['dt_fund'] = $("#dt_fund").val();
            tempPR['dt_department'] = $("#dt_department").val();
            tempPR['dt_activity'] = $("#dt_activity").val();
            tempPR['dt_account'] = $("#dt_account").val();
            tempPR['cr_fund'] = $("#cr_fund").val();
            tempPR['cr_department'] = $("#cr_department").val();
            tempPR['cr_activity'] = $("#cr_activity").val();
            tempPR['cr_account'] = $("#cr_account").val();
            tempPR['type'] = $("#type").val();
            tempPR['id_category'] = $("#id_category").val();
            tempPR['id_sub_category'] = $("#id_sub_category").val();
            tempPR['id_item'] = $("#id_item").val();
            tempPR['quantity'] = $("#quantity").val();
            tempPR['price'] = $("#price").val();
            tempPR['id_tax'] = $("#id_tax").val();
            tempPR['tax_price'] = $("#tax_price").val();
            tempPR['total_final'] = $("#total_final").val();
            tempPR['id_budget_allocation'] = $("#id_budget_allocation").val();
        
            $.ajax(
            {
               url: '/procurement/prEntry/tempadd',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                document.getElementById("type").disabled = true;

                // $('#myModal').modal('hide');
                $("#view").html(result);
                var ta = $("#total_detail").val();
                // alert(ta);
                $("#amount").val(ta);
                if($result== '')
                {
                    $("#amount").val('0');
                }
                if(ta == '')
                {
                    $("#amount").val('0');
                }
               }
            });
        }
    }


    function getCategory(type)
     {
        $("#type").val(type);
        // alert(type);
         $.get("/procurement/prEntry/getCategory/"+type, function(data, status)
         {
            // alert(data);
            $("#view_category").html(data);
            $("#view_sub_category").html('');
            $("#view_item").html('');
            document.getElementById("type").disabled = true;
            // $("#view_student_details").show();
        });
     }


    function getSubCategoryByCategoryId()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_category'] = $("#id_category").val();
        $.ajax(
            {
               url: '/procurement/prEntry/getSubCategoryByCategoryId',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_sub_category").html(result);
                $("#view_item").html('');
                // var ta = $("#total_detail").val();
                // // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }


    function getItemBySubCategoryId()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_category'] = $("#id_category").val();
        tempPR['id_sub_category'] = $("#id_sub_category").val();
        $.ajax(
            {
               url: '/procurement/prEntry/getItemBySubCategoryId',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_item").html(result);
                // var ta = $("#total_detail").val();
                // // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }


    function deleteTempData(id)
    {

        document.getElementById("type").disabled = false;
        var type = $("#type").val();



         $.ajax(
            {
               url: '/procurement/prEntry/tempDelete/'+id+'/'+type,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
                    var ta = $("#total_detail").val();
                    // alert(ta);
                    if(ta == '')
                    {
                        document.getElementById("type").disabled = false;
                        $("#amount").val('0');
                    }
                    else
                    {
                        document.getElementById("type").disabled = true;
                        $("#amount").val(ta);
                    }
                    // if($result== '')
                    // {
                    // }
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/procurement/prEntry/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#dt_activity").val(result['dt_activity']);
                    $("#dt_account").val(result['dt_account']);
                    $("#cr_fund").val(result['cr_fund']);
                    $("#cr_department").val(result['cr_department']);
                    $("#cr_activity").val(result['cr_activity']);
                    $("#cr_account").val(result['cr_account']);
                    $("#type").val(result['type']);
                    $("#id_category").val(result['id_category']);
                    $("#id_sub_category").val(result['id_sub_category']);
                    $("#id_item").val(result['id_item']);
                    $("#quantity").val(result['quantity']);
                    $("#price").val(result['price']);
                    $("#id_tax").val(result['id_tax']);
                    $("#tax_price").val(result['tax_price']);
                    $("#total_final").val(result['total_final']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    function getTaxCalculated()
    {
        var tempPR = {};
        tempPR['id_tax'] = $("#id_tax").val();
        tempPR['quantity'] = $("#quantity").val();
        tempPR['price'] = $("#price").val();

                // alert($tempPR['id_tax']);
        // if ($tempPR['id_tax'] != '' && $tempPR['quantity'] != '' && $tempPR['price'] != '')
        // {

            $.ajax(
            {
               url: '/procurement/prEntry/getTaxCalculated',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 $("#view_dummy").html(result);
                
                var tax_amount = $("#tax_amount").val();
                var tax_per = $("#tax_per").val();
                var total_amount = $("#total_amount").val();

// alert(total_amount);
                $("#tax_price").val(tax_amount);
                $("#tax_percentage").val(tax_per);
                $("#total_final").val(total_amount);

                // var ta = $("#total_detail").val();
                // // alert(ta);
                // $("#amount").val(ta);
               
               }
            });
        // }        
    }

    function validateDetailsData()
    {
        if($('#form_pr_entry').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add PR Details");
            }
            else
            {
                $('#type').prop('disabled', false);
                $('#form_pr_entry').submit();
            }
        }    
    }


     $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                type_of_pr: {
                    required: true
                },
                 description: {
                    required: true
                },
                 pr_entry_date: {
                    required: true
                },
                 type: {
                    required: true
                },
                 id_vendor: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 id_financial_year: {
                    required: true
                },
                 id_budget_year: {
                    required: true
                },
                 amount: {
                    required: true
                }
            },
            messages: {
                type_of_pr: {
                    required: "<p class='error-text'>Select Type Of PR",
                },
                description: {
                    required: "<p class='error-text'>Enter Description",
                },
                pr_entry_date: {
                    required: "<p class='error-text'>Select PR Entry Date</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_vendor: {
                    required: "<p class='error-text'>Select Vendor</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                amount: {
                    required: "<p class='error-text'>Enter PR Details For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_pr_entry_details").validate({
            rules: {
                id_category: {
                    required: true
                },
                 id_sub_category: {
                    required: true
                },
                 id_item: {
                    required: true
                },
                 dt_account: {
                    required: true
                },
                 dt_activity: {
                    required: true
                },
                 dt_department: {
                    required: true
                },
                 dt_fund: {
                    required: true
                },
                 cr_account: {
                    required: true
                },
                 cr_activity: {
                    required: true
                },
                 cr_department: {
                    required: true
                },
                 cr_fund: {
                    required: true
                },
                 quantity: {
                    required: true
                },
                 price: {
                    required: true
                },
                 id_tax: {
                    required: true
                },
                total_final: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category",
                },
                id_sub_category: {
                    required: "<p class='error-text'>Select Sub Category",
                },
                id_item: {
                    required: "<p class='error-text'>Select Item</p>",
                },
                dt_account: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                dt_activity: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                dt_department: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                dt_fund: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                cr_account: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                cr_activity: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                cr_department: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                cr_fund: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                quantity: {
                    required: "<p class='error-text'>Enter Quantity</p>",
                },
                price: {
                    required: "<p class='error-text'>Enter Price Per Item</p>",
                },
                id_tax: {
                    required: "<p class='error-text'>Select Tax Code</p>",
                },
                total_final: {
                    required: "<p class='error-text'>Wait Till Total Calculation</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>