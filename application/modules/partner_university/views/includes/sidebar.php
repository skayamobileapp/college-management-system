            <div class="container-fluid page-wrapper">
            <div class="sidebar">
                <div class="user-profile clearfix">

                    <a href="/applicant" class="user-profile-link">
                        <span>
                            <img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg">
                        </span>
                        <?php echo $partner_university_name; ?>
                        
                    </a>
                        
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/partner_university/partnerUniversity/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Profile</h4>
                    <ul>
                        <li><a href="/partner_university/partnerUniversity/view">Profile</a></li>
                        <!-- <li><a href="/applicant/applicant/changePasssword">Change Password</a></li> -->
                        <!-- <li><a href="/applicant/applicant">Upload Documents</a></li> -->
                    </ul>
                    <h4>Student</h4>
                    <ul>
                        <li><a href="/partner_university/student/list">Student Registration</a></li>
                        <li><a href="/partner_university/studentRecord/list">Student Records</a></li>
                        <li><a href="/partner_university/studentProfile/list">Student Profile</a></li>
                    </ul>
                    <h4>Accounts</h4>
                    <ul>
                        <li><a href="/partner_university/invoice/add">Student Invoice Billing</a></li>
                        <li><a href="/partner_university/invoice/list">Invoice Listing</a></li>
                        <li><a href="/partner_university/receipt/list">Receipt</a></li>
                    </ul>
                </div>
            </div>