<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>

/* Chatbot CSS starts Here */
.chat-bot-container {
  width: 400px;
  border: 1px solid #ccc;
  padding: 10px;
  border-radius: 15px 15px 0px 0px;
  position: fixed;
  bottom: 0px;
  right: 0px;
  background-color: #fff;
  z-index: 9999;
  box-shadow: 0px 0px 5px rgba(0,0,0,0.2);
  max-height: 500px;
  overflow-y: auto;
  padding-top: 55px;
}
.cb-header {
  background-color: #b5eff5;
  padding: 10px 15px;
  color: #3639a4;
  border-radius: 10px;
  font-size: 16px;
}
.chat-bot-container strong {
  font-family: "rubikbold";  
}
.cb-help {
  list-style: none;
  padding-left: 0px;
  margin-top: 10px;
}
.cb-help li {
  background-color: #f5f5fa;
  padding: 5px 10px;
  margin-bottom: 5px;
  border-radius: 5px;
}
.cb-answer {
  background-color: #3639a4;
  max-width: 65%;
  padding: 10px 15px;
  border-radius: 0px 10px 10px 10px;
  color: #fff;
  font-size: 14px;
}
.cb-help-text {
  float: right;
  max-width: 65%;
  padding: 10px 15px;
  border-radius: 10px 0px 10px 10px;  
  background-color: #f5f5fa;
}
.cb-form {
  position: relative;
}
.cb-form .form-control {
  height: 45px;
  border-radius: 30px;
  padding: 6px 20px;  
}
.cb-form .btn-primary {
  position: absolute;
  right: 7px;
  top: 7px;
}
.chat-bot-container h4 {
  margin-top: 0px;
  color: #3639a4;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  width: 365px;
  background: #fff;
  padding: 10px;
  margin-top: -55px;
}
.chat-hide .chat-box{
  display: none;
}
/* Chatbot CSS ends Here */
</style>

</head>

<body>
   
           

            <div class="main-container clearfix">
                <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd><?php echo ucwords($applicantDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $applicantDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $applicantDetails->program_scheme; ?></dd>
                            </dl>
                                                       
                        </div>        
                        
                        <div class='col-sm-6'>  
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $applicantDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $applicantDetails->nationality ?></dd>
                            </dl> 
                            
                        </div>
                    </div>
                </div>
            </div>

                <!-- Chatbot HTML Start Here-->
                <div class="chat-bot-container">
                    <h4>Chatbox <button class="btn btn-default toggle-chat"><span class="glyphicon glyphicon-minus"></span></button></h4>
                    <div class="chat-box">
                        <div class="cb-header">
                            Hi, <strong>Mr. kiran as kumar</strong> <br />We are happy to help you,
                        </div>
                        <ul class="cb-help">
                            <li>Type '<strong>1</strong>' : For Application Status</li>
                            <li>Type '<strong>2</strong>' : For Fee Amount</li>
                            <li>Type '<strong>3</strong>' : For Offer Letter</li>
                            <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
                            <li>Type '<strong>9</strong>' : To get a call from the representative</li>
                        </ul>
                        <p class="cb-answer">Congratulation - your Application Approved</p>
                        <div class="clearfix">
                            <p class="cb-help-text">For more information Type '<strong>0</strong>' :For main menu</p>
                        </div>
                        <p class="cb-answer">Fee for the program will be 1500RM</p>
                        <div class="clearfix">
                            <p class="cb-help-text">For more information Type '<strong>0</strong>' :For main menu</p>
                        </div> 
                        <div class="form-group cb-form">
                            <input type="text" class="form-control" placeholder="Type here...">
                            <button class="btn btn-primary" type="button">Start</button>
                          </div><!-- /input-group -->                                       
                    </div>
                </div>
                <!-- Chatbot HTML Ends Here-->

                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
                
            </div>        
        </div>
    </div>      
    
    <script>
        $(document).ready(function() {
            $('.toggle-chat').on('click', function() {
                $(this).parents('.chat-bot-container').toggleClass('chat-hide');
            });
        });
    </script>
</body>

</html>