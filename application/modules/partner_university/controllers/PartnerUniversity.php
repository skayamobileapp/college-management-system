<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerUniversity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_model');
        $this->isPartnerUniversityLoggedIn();
    }


    function view()
    {
        $id_partner_university = $this->session->id_partner_university;
        $id = $id_partner_university;
        
        if($this->input->post())
        {
         
        }
        $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
        $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
        $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
        $data['stateList'] = $this->partner_university_model->stateListByStatus('1');
        $data['staffList'] = $this->partner_university_model->staffListByStatus('1');
        $data['programList'] = $this->partner_university_model->programListByStatus('1');
        $data['moduleTypeList'] = $this->partner_university_model->moduleTypeListByStatus('1');
        $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');




        $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
        $data['comiteeList'] = $this->partner_university_model->comiteeList($id);


        $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);
        $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);
        $data['partnerProgramDetails'] = $this->partner_university_model->partnerProgramDetails($id);

        if($data['partnerProgramDetails'])
        {
            $data['partnerProgramStudyModeDetails'] = $this->partner_university_model->partnerProgramStudyModeDetails($id,$data['partnerProgramDetails']->id);
            $data['partnerProgramApprenticeshipDetails'] = $this->partner_university_model->partnerProgramApprenticeshipDetails($id,$data['partnerProgramDetails']->id);
            $data['partnerProgramInternshipDetails'] = $this->partner_university_model->partnerProgramInternshipDetails($id,$data['partnerProgramDetails']->id);
            $data['partnerProgramSyllabusDetails'] = $this->partner_university_model->partnerProgramSyllabusDetails($id,$data['partnerProgramDetails']->id);
        }

        $this->global['pageTitle'] = 'Partner University Portal : View Partner University Profile';
        $this->loadViews("profile/view", $this->global, $data, NULL);
    }


    function edit($id = NULL)
    {
      
        $id_partner_university = $this->session->id_partner_university;

        $id = $id_partner_university;
        
        if($this->input->post())
        {
         
        }
        $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
        $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
        $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
        $data['stateList'] = $this->partner_university_model->stateListByStatus('1');
        $data['staffList'] = $this->partner_university_model->staffListByStatus('1');
        $data['programList'] = $this->partner_university_model->programListByStatus('1');
        $data['moduleTypeList'] = $this->partner_university_model->moduleTypeListByStatus('1');
        $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');




        $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
        $data['comiteeList'] = $this->partner_university_model->comiteeList($id);


        $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);
        $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);
        $data['partnerProgramDetails'] = $this->partner_university_model->partnerProgramDetails($id);

        if($data['partnerProgramDetails'])
        {
            $data['partnerProgramStudyModeDetails'] = $this->partner_university_model->partnerProgramStudyModeDetails($id,$data['partnerProgramDetails']->id);
            $data['partnerProgramApprenticeshipDetails'] = $this->partner_university_model->partnerProgramApprenticeshipDetails($id,$data['partnerProgramDetails']->id);
            $data['partnerProgramInternshipDetails'] = $this->partner_university_model->partnerProgramInternshipDetails($id,$data['partnerProgramDetails']->id);
            $data['partnerProgramSyllabusDetails'] = $this->partner_university_model->partnerProgramSyllabusDetails($id,$data['partnerProgramDetails']->id);
        }

        $this->global['pageTitle'] = 'Partner University Portal : View Partner University Profile';
        $this->loadViews("profile/edit", $this->global, $data, NULL);
    }


    function addComitee()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addComitee($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteComitee($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteComitee($id);
        
        echo "Success"; 
    }

    function getPartnerUniversity()
    {
        $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));

        $partner_category = $this->partner_university_model->getPartnerUniversityCategory($id_partner_category);

        $name = $partner_category->name;
        $code = $partner_category->code;
        $value = 0;

        if($name == 'Franchise')
        {
            $value= 1;
        }
        echo $value;exit();
    }

    function getStateByCountry($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    // Copied From Scholarship Partner University


    function getStateByCountryForTraining($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_training_state' id='id_training_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }



    function deleteMoaAggrement($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteMoaAggrement($id);
        
        echo "Success"; 
    }

     function addTrainingCenter()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addTrainingCenter($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteTrainingCenter($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteTrainingCenter($id);
        
        echo "Success"; 
    }

    function saveProgramDetailsData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $id = $tempData['id'];
        unset($tempData['id']);

        if($id > 0)
        {
            $inserted_id = $this->partner_university_model->editPartnerUniversityProgram($tempData,$id);
        }
        else
        {
            $inserted_id = $this->partner_university_model->addNewPartnerUniversityProgram($tempData);
        }

        if($inserted_id)
        {
            echo "success";
        }
        // echo "<Pre>"; print_r($tempData);exit();
    }


    function addProgramStudyModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        $inserted_id = $this->partner_university_model->addNewPartnerProgramStudyMode($tempData);
        // echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramInternshipData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        $inserted_id = $this->partner_university_model->addNewPartnerProgramInternship($tempData);
        // echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramApprenticeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addNewPartnerProgramApprenticeship($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addNewPartnerProgramSyllabus($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteProgramModeOfStudy($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramStudyMode($id);
        echo "Success";
    }

    function deleteProgramInternship($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramInternship($id);
        echo "Success";
    }

    function deleteProgramApprenticeship($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramApprenticeship($id);
        echo "Success";
    }

    function deleteProgramSyllabus($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramSyllabus($id);
        echo "Success";
    }

    function logout()
    {
        $sessionArray = array('id_partner_university'=> '',                    
                    'partner_university_name'=> '',
                    'partner_university_login_id' => '',
                    'partner_university_code'=> '',
                    'partner_university_last_login'=> '',
                    'isPartnerUniversityLoggedIn' => FALSE
            );
     $this->session->set_userdata($sessionArray);
     $this->isPartnerUniversityLoggedIn();
    }
}
