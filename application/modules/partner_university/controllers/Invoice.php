<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Invoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_model');
        $this->isPartnerUniversityLoggedIn();
    }

    function list()
    {
        $id_partner_university = $this->session->id_partner_university;

        $partner_university_name = $this->session->partner_university_name;
        $partner_university_code = $this->session->partner_university_code;

        $data['partner_university_name'] = $partner_university_name;
        $data['partner_university_code'] = $partner_university_code;
    

        $data['intakeList'] = $this->invoice_model->intakeListByStatus('1');
        $data['programList'] = $this->invoice_model->programmeListByStatus('1');


        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
        $formData['type'] = 'Partner University';
        $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
        $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
        $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
        $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));
        $formData['id_partner_university'] = $id_partner_university;

        $data['searchParam'] = $formData;

        $data['mainInvoiceList'] = $this->invoice_model->getMainInvoiceListSearch($formData);


        // echo "<Pre>";print_r($data['mainInvoiceList']);exit;


        $this->global['pageTitle'] = 'Partner University Portal : Invoice';
        $this->loadViews("invoice/list", $this->global, $data, NULL);
    }

    function add()
    {

        $id_partner_university = $this->session->id_partner_university;
        $partner_university_name = $this->session->partner_university_name;
        $partner_university_code = $this->session->partner_university_code;


        // $data['partner_university_name'] = $partner_university_name;
        // $data['partner_university_code'] = $partner_university_code;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;

            $formData = $this->input->post();

            // echo "<Pre>"; print_r($formData);exit;
            // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));


            $id_student = $this->security->xss_clean($this->input->post('id_student'));

            $student_quantity = count($id_student);

            $invoice_number = $this->invoice_model->generateMainInvoiceNumber();

            // $total_after_multiply = $student_quantity * $total_amount;
            $total_after_multiply = 0;

            $data = array(
                'invoice_number' => $invoice_number,
                'total_amount' => $total_after_multiply,
                'invoice_total' => $total_after_multiply,
                'balance_amount' => $total_after_multiply,
                'paid_amount' => 0,
                'id_student' => $id_partner_university,
                'id_program' => 0,
                'no_count' => $student_quantity,
                'applicant_partner_fee' => $student_quantity,
                'type' => 'Partner University',
                'remarks' => 'Partner Applicant Bulk Registration On : ' . date('d-m-Y'),
                'currency' => 'USD',
                'status' => 1,
                'created_by' => 1
            );

            $inserted_id = $this->invoice_model->addNewMainInvoice($data);

            $total_amount = 0;

            if($inserted_id)
            {
                for($i=0;$i<count($formData['id_student']);$i++)
                {
                    $id_student = $formData['id_student'][$i];

                    if($id_student > 0 && $inserted_id > 0)
                    {
                        $student_invoice_data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_partner_university' => $id_partner_university,
                        'id_student'=> $id_student,
                        'amount'=> 0,
                        'status'=> 1

                        );

                    // echo "<Pre>"; print_r($student_invoice_data);exit;
                        $id_pu_invoice_student_details = $this->invoice_model->addPartnerStudentInvoiceDetails($student_invoice_data);
                        
                        $invoice_amount = 0;

                        if($id_pu_invoice_student_details)
                        {
                            $invoice_amount = $this->invoice_model->createNewMainInvoiceForStudent($id_student,$id_pu_invoice_student_details,$inserted_id);
                        }

                        $partner_university_invoice_student_details['amount'] = $invoice_amount;

                        $this->invoice_model->editPartnerStudentInvoiceDetails($partner_university_invoice_student_details, $id_pu_invoice_student_details);
                    }

                    $total_amount = $total_amount + $invoice_amount;
                }

            $invoice_update['total_amount'] = $total_amount;
            $invoice_update['balance_amount'] = $total_amount;
            $invoice_update['invoice_total'] = $total_amount;
            $invoice_update['total_discount'] = 0;
            $invoice_update['paid_amount'] = 0;

            $updated_invoice = $this->invoice_model->editMainInvoice($invoice_update,$inserted_id);


            }

            redirect('/partner_university/invoice/list');
        }


        $partnerUniversity = $this->invoice_model->getPartnerUniversity($id_partner_university);
        $data['partnerUniversity'] = $partnerUniversity;
        
        // echo "<Pre>";print_r($partnerUniversity);exit;

        $this->global['pageTitle'] = 'Partner University Portal : Add Applicant Invoice';
        $this->loadViews("invoice/add", $this->global, $data, NULL);
    }


    function view($id)
    {

        // echo "<Pre>";print_r($id);exit;

        $id_partner_university = $this->session->id_partner_university;

        $partner_university_name = $this->session->partner_university_name;
        $partner_university_code = $this->session->partner_university_code;

        $data['partner_university_name'] = $partner_university_name;
        $data['partner_university_code'] = $partner_university_code;

        if ($id == null)
        {
            redirect('/finance/partnerUniversityInvoice/list');
        }
        if($this->input->post())
        {
            redirect('/finance/partnerUniversityInvoice/list');
        }

        $data['mainInvoice'] = $this->invoice_model->getMainInvoice($id);
        $data['mainInvoiceDetailsList'] = $this->invoice_model->getMainInvoiceDetails($id);
        $data['mainInvoiceDiscountDetailsList'] = $this->invoice_model->getMainInvoiceDiscountDetails($id);
        $data['mainInvoiceStudentDetails'] = $this->invoice_model->getMainInvoiceStudentDetails($id,$data['mainInvoice']->applicant_partner_fee);
        


        if($data['mainInvoice']->type == 'Partner University')
        {
            $data['invoiceFor'] = $this->invoice_model->getMainInvoicePartnerUniversityData($data['mainInvoice']->id_student);
            // $data['studentDetails'] = $this->invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
        }

        // echo "<Pre>";  print_r($data['mainInvoiceStudentDetails']);exit;

        $this->global['pageTitle'] = 'Partner University Portal : View Partner University Invoice';
        $this->loadViews("invoice/view", $this->global, $data, NULL);
    }


    function searchStudents()
    {
        $id_partner_university = $this->session->id_partner_university;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_partner_university'] = $id_partner_university;
        $student_data = $this->invoice_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "";


         $table .= "

         <h4> Select Applicant For Billing</h4>
         <br>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Nationality</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification</th>
                    <th>Submitted On</th>
                    <th>Fee Structure</th>
                    <th style='text-align: center;'>Action</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $nationality = $student_data[$i]->nationality;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $qualification_code = $student_data[$i]->qualification_code;
                $qualification_name = $student_data[$i]->qualification_name;
                $fee_structure_name = $student_data[$i]->fee_structure_name;
                $fee_structure_code = $student_data[$i]->fee_structure_code;
                $id_fee_structure = $student_data[$i]->id_fee_structure;
                $submitted_date = $student_data[$i]->submitted_date;


                if($submitted_date)
                {
                    $submitted_date = date('d-m-Y', strtotime($submitted_date));
                }

            

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name - $nric</td>
                    <td>$email_id</td>
                    <td>$phone</td>
                    <td>$nationality</td>
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                           
                    <td>$qualification_name</td>        
                    <td>$submitted_date</td>                      
                    <td>$fee_structure_code - $fee_structure_name</td>  
                    <td>
                        <a onclick='viewFeeStructureByApplicant($id)' title='View Fee Details'>View</a>
                    </td>
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }
                        // <a onclick='viewFeeStructureDetails($id)' title='View Fee Details'>View</a>

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }

    function viewFeeStructureByApplicant($id_applicant)
    {
        $data = $this->invoice_model->getfeeStructureMasterByApplicant($id_applicant);
        
        // echo "<Pre>"; print_r($data);exit;

        if(!empty($data))
        {


         $table = "";


         $table .= "
         <br>
         <h4> Fee Structure (Billable)</h4>

         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Trigger On</th>
                    <th>Frequency Mode</th>
                    <th>Currency</th>
                    <th>Installment(if)</th>
                    <th>Type</th>
                    <th>Amount</th>
                </tr>";

                $total = 0;
            for($i=0;$i<count($data);$i++)
            {
                $fee_structure_code = $data[$i]->fee_structure_code;
                $fee_structure = $data[$i]->fee_structure;
                $trigger_name = $data[$i]->trigger_name;
                $frequency_mode = $data[$i]->frequency_mode;
                $currency = $data[$i]->currency;
                $currency_name = $data[$i]->currency_name;
                $is_installment = $data[$i]->is_installment;
                $installments = $data[$i]->installments;
                $amount = $data[$i]->amount;

                if($currency_name != '')
                {
                    $currency = $currency_name;
                }

                if($is_installment == 0)
                {
                    $type = 'Installment';
                }
                else
                {
                    $type = 'Per Semester';
                }
            

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$fee_structure_code - $fee_structure</td>                           
                    <td>$trigger_name</td>                        
                    <td>$frequency_mode</td>                           
                    <td>$currency</td>                           
                    <td>$installments</td>                      
                    <td>$type</td>                      
                    <td>$amount</td>               
                </tr>";

                $total = $total + $amount;

            }
                $table .= "
                <tr>
                    <td colspan='6'></td>                      
                    <td>Total : </td>               
                    <td>$total</td>               
                </tr>";


                        // <a onclick='viewFeeStructureDetails($id)' title='View Fee Details'>View</a>

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Fee Structure Data Found, Add Fee Structure</h4>";
        }
        echo $table;exit;
    }

    function viewStudentFeeBulkPartnerDetails($id_pu_invoice_student_details)
    {
        $data = $this->invoice_model->viewStudentFeeBulkPartnerDetails($id_pu_invoice_student_details);
        
        // echo "<Pre>"; print_r($data);exit;

        if(!empty($data))
        {


         $table = "";


         $table .= "
         <br>
         <h4> Fee Structure (Billable)</h4>

         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Frequency Mode</th>
                    <th>Amount Calculation Type</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>";

                $total = 0;
            for($i=0;$i<count($data);$i++)
            {
                $fee_setup_name = $data[$i]->fee_setup_name;
                $fee_steup_code = $data[$i]->fee_steup_code;
                $frequency_mode = $data[$i]->frequency_mode;
                $amount_calculation_type = $data[$i]->amount_calculation_type;
                $quantity = $data[$i]->quantity;
                $amount = $data[$i]->amount;

                // if($currency_name != '')
                // {
                //     $currency = $currency_name;
                // }

                // if($is_installment == 0)
                // {
                //     $type = 'Installment';
                // }
                // else
                // {
                //     $type = 'Per Semester';
                // }
            

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$fee_steup_code - $fee_setup_name</td>                           
                    <td>$frequency_mode</td>                           
                    <td>$amount_calculation_type</td>                        
                    <td>$quantity</td>             
                    <td>$amount</td>               
                </tr>";

                $total = $total + $amount;

            }
                $table .= "
                <tr>
                    <td colspan='4'></td>                      
                    <td>Total : </td>               
                    <td>$total</td>               
                </tr>";


                        // <a onclick='viewFeeStructureDetails($id)' title='View Fee Details'>View</a>

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Fee Structure Data Found, Add Fee Structure</h4>";
        }
        echo $table;exit;
    }
}
