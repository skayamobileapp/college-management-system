<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Bulk_withdraw_model extends CI_Model
{
    function bulkWithdrawList($applicantList)
    {
        $this->db->select('cr.*, in.name as intake_name, c.name as course_name, c.code as course_code, p.name as programme_name, p.code as programme_code');
        $this->db->from('bulk_withdraw_master as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('programme as p', 'cr.id_programme = p.id');
        // $this->db->join('exam_center as exc', 'cr.id_exam_center = exc.id');
        // if($applicantList['first_name']) {
        //     $likeCriteria = "(stu.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        //  if($applicantList['email_id']) {
        //     $likeCriteria = "(stu.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        //  if($applicantList['nric']) {
        //     $likeCriteria = "(stu.nric  LIKE '%" . $applicantList['nric'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
         if($applicantList['id_program'])
        {
         $this->db->where('cr.id_programme', $data['id_program']);
        }
        if($applicantList['id_course'])
        {
         $this->db->where('cr.id_course', $data['id_course']);
        }
         if($applicantList['id_intake'])
        {
          $this->db->where('cr.id_intake', $data['id_intake']);
        }
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }


    function bulkWithdrawListByIntakeProgrammeStudent($id)
    {
        $this->db->select('cr.*, s.nric, s.full_name, s.email_id, s.phone, s.gender, sem.code as semester_code, sem.name as semester_name');
        $this->db->from('bulk_withdraw as cr');
        $this->db->join('student as s', 'cr.id_student = s.id');
        $this->db->join('semester as sem', 'cr.id_semester = sem.id');
        $this->db->where('cr.id_bulk_withdraw_master', $id);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }


    function assignedStudentList()
    {
        $this->db->select('a.*, s.first_name, s.last_name');
        $this->db->from('course_registration as a');
        $this->db->join('student as s', 'a.id_student = s.id');
                $this->db->where('a.status=0');

        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function examCenterListSearch($search)
    {
        // $date = 
        $this->db->select('*');
        $this->db->from('exam_center');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseList()
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getWithdrawMaster($id)
    {
        $this->db->select('cr.*, in.name as intake_name, c.name as course_name, c.code as course_code, c.credit_hours, p.name as programme_name, p.code as programme_code');
        $this->db->from('bulk_withdraw_master as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('programme as p', 'cr.id_programme = p.id');
        $this->db->where_in('cr.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         // print_r($result);exit();     
         return $result;
    }

    function editStudentExamCenterStatus($array){
      $status = ['status'=>'1'];

        $this->db->where_in('id', $array);
      $this->db->update('course_registration', $status);
  }

    function getCourseFromExamRegister($id_intake,$id_programme,$id_student)
    {
      $status = '0';
        $this->db->select('cr.id as id, in.name as intake_name, c.code as course_code, c.name as course_name, exc.name as exam_center_name, exc.address as exam_center_address, exc.city, ee.name as location, ee.from_dt, ee.from_tm');
        $this->db->from('exam_registration as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('exam_event as ee', 'cr.id_exam_event = ee.id');
        $this->db->join('exam_center as exc', 'ee.id_exam_center = exc.id');
        $this->db->where('cr.id_intake', $id_intake);
        $this->db->where('cr.id_student', $id_student);
         $this->db->where('cr.id_programme', $id_programme);
         $this->db->where('cr.is_bulk_withdraw', $status);
         $this->db->where('cr.is_semester_result', $status);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function getCourseFromCourseRegistration($id_intake,$id_programme,$id_student)
    {
      $status = '0';
        $this->db->select('cr.id as id, sem.name as semester_name, sem.code as semester_code, c.code as course_code, c.name as course_name, c.credit_hours');
        $this->db->from('course_registration as cr');
        $this->db->join('semester as sem', 'cr.id_semester = sem.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->where('cr.id_intake', $id_intake);
        $this->db->where('cr.id_student', $id_student);
         $this->db->where('cr.id_programme', $id_programme);
         $this->db->where('cr.is_bulk_withdraw', $status);
         $this->db->where('cr.is_exam_registered', $status);
         $this->db->where('cr.is_result_announced', $status);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function addBulkWithdrawMaster($data)
    {
        $this->db->trans_start();
        $this->db->insert('bulk_withdraw_master', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addBulkWithdraw($data)
    {
        $this->db->trans_start();
        $this->db->insert('bulk_withdraw', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function getExamRegistration($id)
    {
        $this->db->select('*');
        $this->db->from('exam_registration');
         $this->db->where('id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function updateExamRegistration($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('exam_registration', $data);
      return TRUE;
    }

    function updateCourseRegistration($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('course_registration', $data);
      return TRUE;
    }

    function getCourseRegistration($id)
    {
      $this->db->select('*');
      $this->db->from('course_registration');
      $this->db->where('id', $id);
      $query = $this->db->get();
      $result = $query->row();
      
      return $result; 
    }





    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoice($data,$id_apply_change_status)
    {
        $user_id = $this->session->userId;


        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);

        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;

        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('COURSE WITHDRAW','Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('COURSE WITHDRAW','Approval Level',$id_program);
        }

        if($fee_structure_data)
        {

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';
                $invoice_amount = $fee_structure_data->amount_local;
            }
            elseif($nationality == 'Other')
            {
                $currency = 'USD';
                $invoice_amount = $fee_structure_data->amount_international;
            }


        

            $invoice_number = $this->generateMainInvoiceNumber();


            $invoice['invoice_number'] = $invoice_number;
            $invoice['type'] = 'Student';
            $invoice['remarks'] = 'Student Course Withdraw';
            $invoice['id_application'] = '0';
            $invoice['id_program'] = $id_program;
            $invoice['id_intake'] = $id_intake;
            $invoice['id_student'] = $id_student;
            $invoice['currency'] = $currency;
            $invoice['total_amount'] = $invoice_amount;
            $invoice['invoice_total'] = $invoice_amount;
            $invoice['balance_amount'] = $invoice_amount;
            $invoice['paid_amount'] = '0';
            $invoice['status'] = '1';
            $invoice['created_by'] = $user_id;

            // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

            
            // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
            
            $inserted_id = $this->addNewMainInvoice($invoice);

            if($inserted_id)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure_data->id_fee_setup,
                        'amount' => $invoice_amount,
                        'status' => 1,
                        'quantity' => 1,
                        'price' => $invoice_amount,
                        'id_reference' => $id_apply_change_status,
                        'description' => 'COURSE WITHDRAW',
                        'created_by' => $user_id
                    );

                $this->addNewMainInvoiceDetails($data);
            }

        }
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }   

    function getCourseLandscapeByByProgNIntake($data)
    {
        $this->db->select('DISTINCT(s.id_course) as id_course, s.id as id_course_registered_landscape');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.id_program', $data['id_programme']);
        $query = $this->db->get();
        $results = $query->result(); 


        $details = array();

        foreach ($results as $result)
        {
           $id_course = $result->id_course;

           $course = $this->getCourse($id_course);


           if($course)
           {

            $course->id_course_registered_landscape = $result->id_course_registered_landscape;
            array_push($details, $course);
           }
        }

        // echo "<Pre>";print_r($details);exit;
        
        return $details;
    }

    function getCourse($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $course_data = $query->row();

        return $course_data;
    }


    function getCourseRegisteredLandscape($id)
    {
        $this->db->select('c.*, s.course_type');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->join('course as c', 's.id_course = c.id'); 
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getStudentListByCourseRegistrationData($data)
    {
        $this->db->select('s.*, c.id as id_course_registered, sem.code as semester_code, sem.name as semester_name');
        $this->db->from('course_registration as c');
        $this->db->join('student as s', 'c.id_student = s.id','left'); 
        $this->db->join('semester as sem', 'c.id_semester = sem.id','left');
        $this->db->where('c.id_intake', $data['id_intake']);
        $this->db->where('c.id_programme', $data['id_programme']);
        $this->db->where('c.is_bulk_withdraw', 0);
        $this->db->where('c.is_exam_registered', 0);
        $this->db->where('c.id_course_registered_landscape', $data['id_course_registered_landscape']);
        // $this->db->where('a.is_exam_registered !=', 0);
        // $this->db->where('a.is_bulk_withdraw', 0);

        if($data['name'])
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'])
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['email_id'])
        {
            $likeCriteria = "(s.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }

        // $likeCriteria = "s.id NOT IN (SELECT a.id_student FROM course_registration as a WHERE a.id_programme =" . $data['id_programme'] . " AND a.id_intake =" . $data['id_intake'] ." AND a.id_course_registered_landscape = " . $data['id_course_registered_landscape'] . ")";
        
        // $this->db->where($likeCriteria);  

         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }
}