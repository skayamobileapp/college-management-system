<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_event_model extends CI_Model
{
    function getCentersByLocatioin($id_location)
    {
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_center as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
            $this->db->where('a.id_location', $id_location);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function examEventListSearch($data)
    {
        // $date = 
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_event as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        $this->db->join('exam_center as ec', 'a.id_exam_center = ec.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_location'] != '')
        {
            $this->db->where('a.id_location', $data['id_location']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('a.id_exam_center', $data['id_exam_center']);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  

         $list = array();
         foreach ($result as $value)
         {
            $data = $this->getExamCenterNLocationByCenterId($value->id_exam_center);
            $value->exam_center = $data['exam_center'];
            $value->location = $data['location'];
            
            array_push($list, $value);
         }
         return $list;
    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamEvent($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_event', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamEvent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_event', $data);
        return TRUE;
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function examCenterLocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterLocationList()
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterList()
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }
}