<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_model extends CI_Model
{
   function applicantList($applicantList)
    {
        $this->db->select('a.*');
        $this->db->from('applicant as a');

        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $applicantList['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        // $this->db->where('a.is_submitted', '1');
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDetails($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        if($sd->sibbling_discount== "Yes" && $sd->employee_discount== "No"){

            $this->db->select('a.*, sd.sibbling_name, sd.sibbling_nric');
            $this->db->from('applicant as a');
            $this->db->join('applicant_has_sibbling_discount as sd', 'a.id = sd.id_applicant');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // $d = $query->row();
            // print_r($d);exit();
            return $query->row();
        }
        else
        if($sd->employee_discount== "Yes" && $sd->sibbling_discount== "No"){

            $this->db->select('a.*, ed.employee_name, ed.employee_nric, ed.employee_designation');
            $this->db->from('applicant as a');
            $this->db->join('applicant_has_employee_discount as ed', 'a.id = ed.id_applicant');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // print_r($query);exit();
            return $query->row();
        }
        else
         if($sd->employee_discount== "Yes" && $sd->sibbling_discount== "Yes"){

            $this->db->select('a.*, sd.sibbling_name, sd.sibbling_nric, ed.employee_name, ed.employee_nric, ed.employee_designation');
            $this->db->from('applicant as a');
            $this->db->join('applicant_has_sibbling_discount as sd', 'a.id = sd.id_applicant');
            $this->db->join('applicant_has_employee_discount as ed', 'a.id = ed.id_applicant');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // print_r($query);exit();
            return $query->row();
        }
        else{
            $this->db->select('a.*');
            $this->db->from('applicant as a');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            return $query->row();
        }
    }
    
    function addNewApplicant($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSibblingDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_sibbling_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewEmployeeDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_employee_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }

    function editSibblingDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_sibbling_discount', $data);
        return TRUE;
    }

    function editEmployeeDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_employee_discount', $data);
        return TRUE;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


   

    function checkFeeStructure($data)
    {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('id_programme', $data['id_program']);
        $this->db->where('status', '1');
        $query = $this->db->get();
        // print_r($query->row());exit();
        return $query->row();
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

