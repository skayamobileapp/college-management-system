<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assign_student_to_exam_center_model extends CI_Model
{
    function assignedStudentList()
    {
        $this->db->select('a.*, e.name as examName, s.first_name, s.last_name');
        $this->db->from('assign_student_to_exam_center as a');
        $this->db->join('exam_center as e', 'a.id_exam_center = e.id');
        $this->db->join('student as s', 'a.id_student = s.id');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function examCenterListSearch($search)
    {
        // $date = 
        $this->db->select('*');
        $this->db->from('exam_center');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getAssignedStudentList($id)
    {
        $this->db->select('*');
        $this->db->from('assign_student_to_exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addStudentToExamCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('assign_student_to_exam_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStudentToExamCenter($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('assign_student_to_exam_center', $data);
        return TRUE;
    }
}