<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Credit Transfer</h3>
        </div>
    <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Institution Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Institution Type <span class='error-text'>*</span></label>
                        <select name="institution_type" id="institution_type" class="form-control" onchange="getInstitutionType(this.value)" >
                            <option value="">Select</option>
                            <option value="Internal">Internal</option>
                            <option value="External">External</option>
                        </select>
                    </div>
                </div>

            </div>

        </div>

        <div class="form-container">
            <h4 class="form-group-title">Student Information</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student ID / Name <span class='error-text'>*</span></label>

                        <select name="id_student" id="id_student" class="form-control" onchange="getStudentById(this.value)">
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($studentList))
                                foreach ($studentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->nric . " - " . $record->full_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>


                       <!--  <input type="text" class="form-control" id="student_name" name="student_name">
                        <br>
                        <label class="radio-inline">
                          <input type="radio" name="student_search" id="student_search" value="1" checked="checked"><span class="check-radio"></span> Student ID
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="student_search" id="student_search" value="0"><span class="check-radio"></span> Name
                        </label>    --> 

                    </div>
                </div>

            </div>

        </div>


        <div class="form-container" id="view_student_data" style="display: none">
            <h4 class="form-group-title">Student Details</h4>

            <div class="row">


                <div id="view_student">   

                </div>
                
            </div>

        </div>

        <div class="form-container">
            <h4 class="form-group-title">Application Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Type <span class='error-text'>*</span></label>
                        <select name="application_type" id="application_type" class="form-control" 
                        >
                        <!-- onchange="getInstitutionType(this.value)"  -->
                            <option value="">Select</option>
                            <option value="CREDIT TRANSFER">CREDIT TRANSFER</option>
                            <option value="CREDIT EXCEMPTION">CREDIT EXCEMPTION</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" value="<?php echo date('d-m-Y'); ?>" autocomplete="off" readonly>
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


    </form>


    <form id="form_detail" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Add Course Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Institution Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="institution_type_detail" id="institution_type_detail" autocomplete="off" value="" readonly>
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


                <div class="col-sm-4" id="view_subject_course_external" style="display: none">
                    <div class="form-group">
                        <label>Subject <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="subject_course" id="subject_course" autocomplete="off">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4" id="view_subject_course_external" style="display: none">
                    <div class="form-group">
                        <label>Credit Hours <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="credit_hours" id="credit_hours" autocomplete="off" value="" readonly>
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


                <div class="col-sm-4" id="view_subject_course_internal" style="display: none">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($courseList))
                                foreach ($courseList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Equivalent <span class='error-text'>*</span></label>
                        <select name="id_equivalent_course" id="id_equivalent_course" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($equivalentCourseList))
                                foreach ($equivalentCourseList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <!-- 
            </div>


            <div class="row"> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade <span class='error-text'>*</span></label>
                        <select name="id_grade" id="id_grade" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($gradeList))
                                foreach ($gradeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name . " - " . $record->description;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>

            </div>

            <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="saveDetailData()">Add</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

        </div>

    </form>



        <div id="view_temp_details">   

        </div>





        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    
    $('select').select2();


    function getInstitutionType(type)
    {
        if(type != '')
        {
            // var t = $("#institution_type").val();
            $("#institution_type_detail").val(type);

            
            if(type == 'Internal')
            {
                $("#view_subject_course_internal").show();
                $("#view_subject_course_external").hide();
            }
            else if(type == 'External')
            {
                 $("#view_subject_course_external").show();
                 $("#view_subject_course_internal").hide();
            }
        }

        // $.get("/registration/examEvent/getCentersByLocatioin/"+id, function(data, status){
       
        //     $("#view_centers").html(data);
        //     // $("#view_programme_details").html(data);
        //     // $("#view_programme_details").show();
        // });
    }


    function saveDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['subject_course'] = $("#subject_course").val();
        tempPR['institution_type'] = $("#institution_type_detail").val();
        tempPR['id_course'] = $("#id_course").val();
        tempPR['id_equivalent_course'] = $("#id_equivalent_course").val();
        tempPR['id_grade'] = $("#id_grade").val();
        tempPR['remarks'] = $("#remarks").val();

            $.ajax(
            {
               url: '/registration/creditTransfer/saveTempDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                $("#view_temp_details").html(result);

                // location.reload();
                // window.location.reload();

               }
            });
        }
    }

    function deleteTempData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/registration/creditTransfer/deleteTempData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_temp_details").html(result);
                    // window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }

    function getStudentById(id)
    {
        if(id != '')
        {

        $.ajax(
            {
               url: '/registration/creditTransfer/getStudentById/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {

                    $("#view_student_data").show();
                    $("#view_student_data").html(result);
                    // window.location.reload();
                    // alert(result);
                    // window.location.reload();
                }
               }
            });
        }
    }

    function validateDetailsData()
    {
        if($('#form_main').valid())
        {
            console.log($("#view_temp_details").html());
            var addedProgam = $("#view_temp_details").html();
            if(addedProgam=='')
            {
                alert("Add Credit Transfer Details");
            }
            else
            {
                $('#form_main').submit();
            }
        }    
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                institution_type: {
                    required: true
                },
                id_student: {
                    required: true
                },
                application_type: {
                    required: true
                },
                 id_semester: {
                    required: true
                }
            },
            messages: {
                institution_type: {
                    required: "<p class='error-text'>Select Institution Type</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                application_type: {
                    required: "<p class='error-text'>Select Application Type</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                subject_course: {
                    required: true
                },
                institution_type_detail: {
                    required: true
                },
                id_course: {
                    required: true
                },
                 id_equivalent_course: {
                    required: true
                },
                 id_grade: {
                    required: true
                },
                 remarks: {
                    required: true
                }
            },
            messages: {
                subject_course: {
                    required: "<p class='error-text'>Subject Required</p>",
                },
                institution_type_detail: {
                    required: "<p class='error-text'>Select Institution Type In Institute Section</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_equivalent_course: {
                    required: "<p class='error-text'>Select Equivalent Course</p>",
                },
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remarks Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $('select').select2();

$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );


</script>