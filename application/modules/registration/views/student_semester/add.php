<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Student Semester Promotion Tagging</h3>
            </div>

    <form id="form_pr_entry" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Search Student For Semester Promotion</h4>
            <h4 >Search Student For Semester Promotion</h4>


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Program </label>
                        <select name="id_program" id="id_program" class="form-control" >
                            <option value="">-- All --</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Intake </label>
                        <select name="id_intake" id="id_intake" class="form-control" >
                            <option value="">-- All --</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

              <!--   <div class="col-sm-3">
                    <div class="form-group">
                        <label>Qualification Level </label>
                        <select name="id_qualification" id="id_qualification" class="form-control" >
                            <option value="">-- All --</option>
                            <?php
                            if (!empty($qualificationList))
                            {
                                foreach ($qualificationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  -->

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Phd Duration </label>
                        <select name="current_semester" id="current_semester" class="form-control" >
                            <option value="">-- All --</option>
                            <?php
                                for ($i=1; $i < 9; $i++)
                                {
                                ?>
                             <option value="<?php echo $i;  ?>">
                                <?php
                                    echo $i;
                                ?>
                             </option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

               <!--  <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tagging Status </label>
                        <select name="tagging_status" id="tagging_status" class="form-control" >
                            <option value="">Select</option>
                            <option value="1">Tagged Student</option>
                            <option value="0">Not Tagged Student</option>
                        </select>
                    </div>
                </div>   -->

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Student Name</label>
                        <input type="text" class="form-control" id="full_name" name="full_name">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Student Email</label>
                        <input type="text" class="form-control" id="email_id" name="email_id">
                    </div>
                </div>               
                
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
              </div>

        </div>


        <div class="form-container" style="display: none;" id="view_student_display">
            <h4 class="form-group-title">Semester Promotion For Student</h4>


            <div  id='view_student'>
            </div>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Upgrade</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

    </form>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script>
    
    $('select').select2();

    $(function(){
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    function searchStudents()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['current_semester'] = $("#current_semester").val();
        tempPR['full_name'] = $("#full_name").val();
        tempPR['email_id'] = $("#email_id").val();
            $.ajax(
            {
               url: '/registration/studentSemester/searchStudents',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_student: {
                    required: true
                }
            },
            messages: {
                id_student: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>