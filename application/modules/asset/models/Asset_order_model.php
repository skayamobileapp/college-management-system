<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_order_model extends CI_Model
{

    function getAssetOrderList($data)
    {
        $this->db->select('ao.*, po.po_number, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('grn as ao');
        $this->db->join('po_entry as po','ao.id_po = po.id');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.po_number  LIKE '%" . $data['name'] . "%' or ao.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_vendor']!='')
        {
            $this->db->where('ao.id_vendor', $data['id_vendor']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('ao.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('ao.id_budget_year', $data['id_budget_year']);
        }
        if ($data['department_code']!='')
        {
            $this->db->where('ao.department_code', $data['department_code']);
        }
        if ($data['status']!='')
        {
            $this->db->where('ao.status', $data['status']);
        }
        
        // $this->db->join('procurement_category as pc', 'ao.id_category = pc.id');
        // $this->db->join('procurement_sub_category as psc', 'ao.id_sub_category = psc.id');
        // $this->db->join('procurement_item as pi', 'ao.id_item = pi.id');
        $query = $this->db->get();
        return $query->result();
    }

    function poListByStatus($status) {
        $type = 'Asset';
        $balance = '0';
        $this->db->select('DISTINCT(po.id) as id, po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, pr.pr_number, bty.name as budget_year');
        $this->db->from('po_entry as po');
        $this->db->join('po_detail as pod','po.id = pod.id_po_entry');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('pr_entry as pr','po.id_pr = pr.id');
        $this->db->where('po.status', $status);
        $this->db->where('pod.type', $type);
        $likeCriteria = "(pod.balance_qty  > '" . $balance . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();
    }

    function getPoMaster($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('po_entry as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = fy.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        $this->db->where('po.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function getPoDetailById($id)
     {
         $this->db->select('tpe.*');
        // $this->db->from('pr_detail');
        $this->db->from('po_detail as tpe');
        $this->db->where('tpe.id', $id);
        // $this->db->where('id_pr', $id);
        $query = $this->db->get();
        return $query->row();
    }



     function getPoDetails($id)
     {
        $type = 'Asset';
        $balance = '0';
         $this->db->select('tpe.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code,  pi.description as item_name, pi.code as item_code');
        // $this->db->from('pr_detail');
        $this->db->from('po_detail as tpe');
        $this->db->join('asset_category as pc', 'tpe.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'tpe.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'tpe.id_item = pi.id');
        $this->db->where('tpe.id_po_entry', $id);
        $this->db->where('tpe.type', $type);
        $likeCriteria = "(tpe.balance_qty  > '" . $balance . "')";
        $this->db->where($likeCriteria);
        // $this->db->where('id_pr', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function updatePoDetailBalance($id_po_detail, $entered_qty)
    {
        $this->db->select('*');
        $this->db->from('po_detail');
        $this->db->where('id', $id_po_detail);
        $query = $this->db->get();
        $po_detaiil_row = $query->row();

        $quantity = $po_detaiil_row->quantity;
        $balance_qty = $po_detaiil_row->balance_qty;
        $received_qty = $po_detaiil_row->received_qty;

        $new_received = $received_qty + $entered_qty;
        $new_balance = $balance_qty - $entered_qty;

        $data['received_qty'] = $new_received;
        $data['balance_qty'] = $new_balance;


         $this->db->where('id', $id_po_detail);
        $this->db->update('po_detail', $data);
        return TRUE;

    }

    function addNewAssetOrder($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_order', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function getAssetOrderById($id)
    {
        $this->db->select('ao.*, po.po_number, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code');
        $this->db->from('asset_order as ao');
        $this->db->join('po_entry as po','ao.id_po = po.id');
        $this->db->join('po_detail as pod','ao.id_po_detail = pod.id');
        $this->db->join('asset_category as pc', 'ao.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'ao.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'ao.id_item = pi.id');
        // $this->db->join('procurement_category as pc', 'ao.id_category = pc.id');
        // $this->db->join('procurement_sub_category as psc', 'ao.id_sub_category = psc.id');
        // $this->db->join('procurement_item as pi', 'ao.id_item = pi.id');
        $this->db->where('ao.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function generateAssetOrderNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('asset_order');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $generate_number = $number = "AO" .(sprintf("%'06d", $count)). "/" . $Year;
           return $generate_number;
    }

    function generateGRNNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('grn');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $generate_number = $number = "GR" .(sprintf("%'06d", $count)). "/" . $Year;
           return $generate_number;
    }

    function addNewGRN($data)
    {
        $this->db->trans_start();
        $this->db->insert('grn', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateGrn($data,$id)
    {
         $this->db->where('id', $id);
        $this->db->update('grn', $data);
    }

    function getGrn($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, apo.po_number, pr.pr_number, bty.name as budget_year');
        $this->db->from('grn as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('po_entry as apo','po.id_po = apo.id');
        $this->db->join('pr_entry as pr','apo.id = pr.is_po');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        $this->db->where('po.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function getAssetOrderByGRNId($id)
    {
        $this->db->select('DISTINCT(ao.id) as id, ao.*, po.po_number, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code');
        $this->db->from('asset_order as ao');
        $this->db->join('po_entry as po','ao.id_po = po.id');
        $this->db->join('po_detail as pod','ao.id_po_detail = pod.id');
        $this->db->join('asset_category as pc', 'ao.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'ao.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'ao.id_item = pi.id');
        $this->db->where('ao.id_grn', $id);
        $query = $this->db->get();
        return $query->result();
    }

     function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function vendorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('vendor_details');
        $this->db->where('vendor_status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function postToGL($id)
    {
        $user_id = $this->session->userId;

        $grn_data = $this->getGrn($id);
        $asset_order_datas = $this->getAssetOrderByGRNId($id);

            // echo "<Pre>";print_r($asset_order_datas);exit();


        $po_detail_datas = $this->getPoDetails($grn_data->id_po);


        $ledger_data['ledger_number'] = $this->generateLedgerNumber();
        $ledger_data['id_financial_year'] = $grn_data->id_financial_year;
        $ledger_data['id_budget_year'] = $grn_data->id_budget_year;
        $ledger_data['id_journal'] = $grn_data->id;
        $ledger_data['total_amount'] = $grn_data->total_amount * 2;
        $ledger_data['description'] = "GRN";
        $ledger_data['status'] = 1;
        $ledger_data['created_by'] = $user_id;
        $ledger_data['approval_status'] = 1;
        $ledger_data['approved_by'] = $user_id;

        $id_ledger = $this->addNewLedger($ledger_data);


        // $ledger_details = array();

        foreach ($asset_order_datas as $asset_order_data)
        {
            $po_detail_data = $this->getPoDetailByDetailId($asset_order_data->id_po_detail);

            $detail_data = array();
             $detail_data = array(
                'id_ledger' => $id_ledger,
                'journal_type' => "Debit",
                'account_code' => $po_detail_data->dt_account,
                'activity_code' => $po_detail_data->dt_activity,
                'department_code' => $po_detail_data->dt_department,
                'fund_code' => $po_detail_data->dt_fund,
                'credit_amount' => 0,
                'debit_amount' => $asset_order_data->amount
                );
            $id_ledger_details = $this->addNewLedgerDetails($detail_data);
            // array_push($ledger_details, $detail_data);

            $detail_data = array();
            $detail_data = array(
                'id_ledger' => $id_ledger,
                'journal_type' => "Credit",
                'account_code' => $po_detail_data->cr_account,
                'activity_code' => $po_detail_data->cr_activity,
                'department_code' => $po_detail_data->cr_department,
                'fund_code' => $po_detail_data->cr_fund,
                'credit_amount' => $asset_order_data->amount,
                'debit_amount' => 0
                );
        $id_ledger_details = $this->addNewLedgerDetails($detail_data);

                 // array_push($ledger_details, $detail_data);

        }

            // echo "<Pre>";print_r($ledger_details);exit();

        // $id_ledger_details = $this->addNewLedgerDetails($ledger_details);

        return TRUE;



        // if($id_ledger)
        // {
        //     $ledger_details = array();
        //     foreach ($po_detail_datas as $po_detail_data)
        //     {
        //          $detail_data = array(
        //         'id_ledger' => $id_ledger,
        //         'journal_type' => "Debit",
        //         'id_account_code' => $po_detail_data->dt_account,
        //         'id_activity_code' => $po_detail_data->dt_activity,
        //         'id_department_code' => $po_detail_data->dt_department,
        //         'id_fund_code' => $po_detail_data->dt_fund,
        //         'credit_amount' => 0,
        //         'debit_amount' => $po_detail_data->total_final
        //         );
        //          array_push($ledger_details, $detail_data);
        //     }

        //     foreach ($po_detail_datas as $po_detail_data)
        //     {
        //          $detail_data = array(
        //         'id_ledger' => $id_ledger,
        //         'journal_type' => "Credit",
        //         'id_account_code' => $po_detail_data->cr_account,
        //         'id_activity_code' => $po_detail_data->cr_activity,
        //         'id_department_code' => $po_detail_data->cr_department,
        //         'id_fund_code' => $po_detail_data->cr_fund,
        //         'credit_amount' => $po_detail_data->total_final,
        //         'debit_amount' => 0
        //         );
        //          array_push($ledger_details, $detail_data);
        //     }

        //     echo "<Pre>";print_r($ledger_details);exit();
        // }

    }

    function generateLedgerNumber()
    {

        $year = date('y');
        $Year = date('Y');

            $this->db->select('j.*');
            $this->db->from('ledger as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "LD" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewLedger($data)
    {
        $this->db->trans_start();
        $this->db->insert('ledger', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewLedgerDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('ledger_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getPoDetailByDetailId($id)
    {
        $this->db->select('tpe.*');
        $this->db->from('po_detail as tpe');
        $this->db->where('tpe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}
