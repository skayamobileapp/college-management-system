<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Disposal_type_model extends CI_Model
{
    function disposalTypeList()
    {
        $this->db->select('*');
        $this->db->from('disposal_type');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function disposalTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('disposal_type');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getDisposalType($id)
    {
        $this->db->select('*');
        $this->db->from('disposal_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDisposalType($data)
    {
        $this->db->trans_start();
        $this->db->insert('disposal_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDisposalType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('disposal_type', $data);
        return TRUE;
    }
}

