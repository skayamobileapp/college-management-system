<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_registration_model extends CI_Model
{

    function assetRegistrationListSearch($data)
    {
        // echo "<Pre>";print_r($tempData);exit();


        $this->db->select('ar.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code');
        $this->db->from('asset_registration as ar');
        $this->db->join('asset_category as pc', 'ar.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'ar.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'ar.id_item = pi.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(ar.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['code']!='')
        {
            $likeCriteria = "(ar.code  LIKE '%" . $data['code'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['brand']!='')
        {
            $likeCriteria = "(ar.brand  LIKE '%" . $data['brand'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['company']!='')
        {
            $likeCriteria = "(ar.company  LIKE '%" . $data['company'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category']!='')
        {
            $this->db->where('ar.id_category', $data['id_category']);
        }
        if ($data['id_sub_category']!='')
        {
            $this->db->where('ar.id_sub_category', $data['id_sub_category']);
        }
        if ($data['id_item']!='')
        {
            $this->db->where('ar.id_item', $data['id_item']);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function assetCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('asset_category');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }    


    function assetSubCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('asset_sub_category');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }    

    function assetItemListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('asset_item');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function geAssetRegistration($id)
    {
        $this->db->select('ar.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code');
        $this->db->from('asset_registration as ar');
        $this->db->join('asset_category as pc', 'ar.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'ar.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'ar.id_item = pi.id');
        $this->db->where('ar.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function updateAssetRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('asset_registration', $data);
        return TRUE;
    }
}