<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_disposal_model extends CI_Model
{
    
    function disposalTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('disposal_type');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }
    
    function departmentListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

     function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
    
    function staffListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function assetDisposalListSearch($data)
    {
        $this->db->select('ad.*, dt.name as disposal_type_name, dt.code as disposal_type_code');
        $this->db->from('asset_disposal as ad');
        $this->db->join('disposal_type as dt','ad.id_disposal_type = dt.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(ad.reason  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_disposal_type'] !='')
        {
            $this->db->where('ad.id_disposal_type', $data['id_disposal_type']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ad.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function assetRegistrationListSearchForDispose($data)
    {
        // echo "<Pre>";print_r($tempData);exit();


        $this->db->select('ar.*');
        $this->db->from('asset_registration as ar');
        if ($data['name']!='')
        {
            $likeCriteria = "(ar.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['code']!='')
        {
            $likeCriteria = "(ar.code  LIKE '%" . $data['code'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['brand']!='')
        {
            $likeCriteria = "(ar.brand  LIKE '%" . $data['brand'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['company']!='')
        {
            $likeCriteria = "(ar.company  LIKE '%" . $data['company'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('ar.status', '1');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewAssetDisposalDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_disposal_detail', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateAssetDisposal($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('asset_disposal', $data);
        return TRUE;
    }

    function editAssetDisposalDetail($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('asset_disposal_detail', $data);
        return TRUE;
    }

    function checkAssetDisposalDetail($id_asset_disposal)
    {
        $this->db->select('asdd.*');
        $this->db->from('asset_disposal_detail as asdd');
        $this->db->where('asdd.status', '0');
        $this->db->where('asdd.id_asset_disposal', $id_asset_disposal);
        $query = $this->db->get();
        $data = $query->row();

        if($data)
        {

        }
        else
        {
            $update['status'] = 1;
            $this->db->where('id', $id_asset_disposal);
            $this->db->update('asset_disposal', $update);
            return TRUE;
        }
    }

    function addAssetDisposal($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_disposal', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addAssetDisposalDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_disposal_detail', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        if($insert_id)
        {
            $this->updateAssetRegistrationForDisposalAdd($data['id_asset'],'3');
            return $insert_id;
        }
    }

    function updateAssetRegistrationForDisposalAdd($id,$status) {
        // Here 3 is For added To Disposal
        $data['status'] = $status;
        $this->db->where('id', $id);
        $this->db->update('asset_registration', $data);
        return TRUE;
    }


    function getAssetDisposal($id)
    {
        $this->db->select('ad.*, dt.name as disposal_type_name, dt.code as disposal_type_code, dep.code as department_code, dep.name as department_name, st.ic_no, st.name as staff_name');
        $this->db->from('asset_disposal as ad');
        $this->db->join('disposal_type as dt','ad.id_disposal_type = dt.id');
        $this->db->join('department_code as dep','ad.department_code = dep.code');
        $this->db->join('staff as st','ad.id_staff = st.id');
        $this->db->where('ad.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }


    function getAssetDisposalDetailsByMaster($id_asset_disposal)
    {
        $this->db->select('asdd.*, ar.name, ar.code, ar.company, ar.brand, ar.price, ar.depriciation_value, depriciation_code');
        $this->db->from('asset_disposal_detail as asdd');
        $this->db->join('asset_registration as ar','asdd.id_asset = ar.id');
        $this->db->where('asdd.id_asset_disposal', $id_asset_disposal);
        $query = $this->db->get();
        return $query->result();
    }

    function getAssetDisposalDetail($id)
    {
        $this->db->select('asdd.*');
        $this->db->from('asset_disposal_detail as asdd');
        $this->db->where('asdd.id', $id);
        $query = $this->db->get();
        $data = $query->row();
        $id_asset = $data->id_asset;
        $this->updateAssetRegistrationForDisposalAdd($id_asset,'4');
        return $id_asset;
    }




    function getTempDetails($id){
        $this->db->select('*');
        $this->db->from('temp_asset_disposal_detail');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function assetDisposalDetailsListForApprove($status)
    {
        $this->db->select('asdd.*, ar.name, ar.code, ar.company, ar.brand, ar.price, ar.depriciation_value, depriciation_code, ad.reason, dt.name as disposal_type_name, dt.code as disposal_type_code');
        $this->db->from('asset_disposal_detail as asdd');
        $this->db->join('asset_registration as ar','asdd.id_asset = ar.id');
        $this->db->join('asset_disposal as ad','asdd.id_asset_disposal = ad.id');
        $this->db->join('disposal_type as dt','ad.id_disposal_type = dt.id');
        $this->db->where('asdd.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function approveAssetDisposalList($array)
    {
      $status = ['status'=>'1'];
      $this->db->where_in('id', $array);
      $this->db->update('pr_entry', $status);
      foreach ($array as $value)
      {
        $this->updateAssetRegistrationForDisposalAdd($value,'2');
      }
    }
}