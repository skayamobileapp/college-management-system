<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_item_model extends CI_Model
{
    function assetItemList()
    {
        $this->db->select('ai.*, ac.description as category_description, ac.code as category_code, assc.description as sub_category_description, assc.code as sub_category_code,');
        $this->db->from('asset_item as ai');
        $this->db->join('asset_category as ac', 'ai.id_asset_category = ac.id');
        $this->db->join('asset_sub_category as assc', 'ai.id_asset_sub_category = assc.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function assetItemListSearch($formData)
    {
        $this->db->select('ai.*, ac.description as asset_category_description, ac.code as asset_category_code, assc.description as asset_sub_category_description, assc.code as asset_sub_category_code,');
        $this->db->from('asset_item as ai');
        $this->db->join('asset_category as ac', 'ai.id_asset_category = ac.id');
        $this->db->join('asset_sub_category as assc', 'ai.id_asset_sub_category = assc.id');
        if($formData['id_asset_category']) {
            $likeCriteria = "(ai.id_asset_category  LIKE '%" . $formData['id_asset_category'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($formData['id_asset_sub_category']) {
            $likeCriteria = "(ai.id_asset_sub_category  LIKE '%" . $formData['id_asset_sub_category'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($formData['name']) {
            $likeCriteria = "(ai.description  LIKE '%" . $formData['name'] . "%' or ai.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getAssetItem($id)
    {
        $this->db->select('*');
        $this->db->from('asset_item');
        $this->db->where('id', $id);
        $query = $this->db->get();
         // print_r($query->row());exit();     
        return $query->row();
    }
    
    function addNewAssetItem($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_item', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAssetItem($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('asset_item', $data);
        return TRUE;
    }
}

