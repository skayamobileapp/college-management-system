<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Asset_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('asset_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_sub_category'] = $this->security->xss_clean($this->input->post('id_sub_category'));
            $formData['id_item'] = $this->security->xss_clean($this->input->post('id_item'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['code'] = $this->security->xss_clean($this->input->post('code'));
            $formData['brand'] = $this->security->xss_clean($this->input->post('brand'));
            $formData['company'] = $this->security->xss_clean($this->input->post('company'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['assetRegistrationList'] = $this->Asset_registration_model->assetRegistrationListSearch($formData);


            $data['assetCategoryList'] = $this->Asset_registration_model->assetCategoryListByStatus('1');
            $data['assetSubCategoryList'] = $this->Asset_registration_model->assetSubCategoryListByStatus('1');
            $data['assetItemList'] = $this->Asset_registration_model->assetItemListByStatus('1');

            $this->global['pageTitle'] = 'FIMS : List Asset Registration';
            $this->loadViews("asset_registration/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('asset_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/asset/assetRegistration/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                // redirect('/procurement/procurementCategory/list');

            $name = $this->security->xss_clean($this->input->post('name'));
            $brand = $this->security->xss_clean($this->input->post('brand'));
            $company = $this->security->xss_clean($this->input->post('company'));
            $price = $this->security->xss_clean($this->input->post('price'));
            $tax = $this->security->xss_clean($this->input->post('tax'));
            $depriciation_code = $this->security->xss_clean($this->input->post('depriciation_code'));
            $depriciation_value = $this->security->xss_clean($this->input->post('depriciation_value'));

                $data = array(
                    'name' => $name,
                    'brand' => $brand,
                    'company' => $company,
                    'price' => $price,
                    'tax' => $tax,
                    'depriciation_code' => $depriciation_code,
                    'depriciation_value' => $depriciation_value,
                    'status' => '1',
                    'updated_by' => $user_id
                    );

                    $inserted_id = $this->Asset_registration_model->updateAssetRegistration($data,$id);
                    redirect('/asset/assetRegistration/list');
            }
            
            $data['assetRegistration'] = $this->Asset_registration_model->geAssetRegistration($id);
            // echo "<Pre>";print_r($data);exit;

            $this->global['pageTitle'] = 'FIMS : Edit Asset Registration';
            // $this->loadViews("asset_registration/edit", $this->global, $data, NULL);
            $this->loadViews("asset_registration/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('asset_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/asset/assetRegistration/list');
            }
            if($this->input->post())
            {
                    redirect('/asset/assetRegistration/list');
            }
            
            $data['assetRegistration'] = $this->Asset_registration_model->geAssetRegistration($id);
            // echo "<Pre>";print_r($data);exit;

            $this->global['pageTitle'] = 'FIMS : View Asset Registration';
            // $this->loadViews("asset_registration/edit", $this->global, $data, NULL);
            $this->loadViews("asset_registration/view", $this->global, $data, NULL);
        }
    }
}
