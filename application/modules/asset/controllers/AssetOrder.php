<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetOrder extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Asset_order_model');
        $this->load->model('Asset_registration_model');

        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('asset_order.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['assetCategoryList'] = $this->Asset_registration_model->assetCategoryListByStatus('1');
            $data['assetSubCategoryList'] = $this->Asset_registration_model->assetSubCategoryListByStatus('1');
            $data['assetItemList'] = $this->Asset_registration_model->assetItemListByStatus('1');
            $data['departmentCodeList'] = $this->Asset_order_model->getDepartmentCodeList();
            $data['financialYearList'] = $this->Asset_order_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Asset_order_model->budgetYearListByStatus('1');
            $data['vendorList'] = $this->Asset_order_model->vendorListByStatus('Approved');

            

            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['asset_order_number'] = $this->security->xss_clean($this->input->post('asset_order_number'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;


            $data['grnList'] = $this->Asset_order_model->getAssetOrderList($formData);

            // echo "<Pre>";print_r($data['grnList']);exit();

            $this->global['pageTitle'] = 'FIMS : List GRN';
            //print_r($subjectDetails);exit;
            $this->loadViews("asset_order/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('asset_order.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {


                $id_po = $this->security->xss_clean($this->input->post('id_po'));
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;


                $expire_date = $this->security->xss_clean($this->input->post('expire_date'));
                $id_vendor = $this->security->xss_clean($this->input->post('id_vendor'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_department = $this->security->xss_clean($this->input->post('id_department'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $rating = $this->security->xss_clean($this->input->post('rating'));
                $id_pre_register = $this->security->xss_clean($this->input->post('id_pre_register'));


                $generated_grn_number = $this->Asset_order_model->generateGRNNumber();

                 $poMaster = $this->Asset_order_model->getPoMaster($id_po);

                $grnData = array(
                    'id_po' => $id_po,
                    'reference_number' => $generated_grn_number,
                    'date_time' => date('Y-m-d'),
                    'expire_date' => date('Y-m-d',strtotime($expire_date)),
                    'id_financial_year' => $poMaster->id_financial_year,
                    'id_budget_year' => $poMaster->id_budget_year,
                    'id_vendor' => $poMaster->id_vendor,
                    'description' => $description,
                    'id_department' => $poMaster->id_department,
                    'department_code' => $poMaster->department_code,
                    'total_amount' => '0',
                    'rating' => $rating,
                    'id_pre_register' => '0',
                    'status' => '0',
                    'created_by' => $user_id
                );

                $inserted_grn_id = $this->Asset_order_model->addNewGRN($grnData);




                $formData = $this->input->post();

                $total_amount=0;
                for($i=0;$i<count($formData['id_po_detail']);$i++)
                 {

                    $id_po_detail = $formData['id_po_detail'][$i];
                    $amount_per_each_item = $formData['amount_per_each_item'][$i];
                    $quantity = $formData['quantity'][$i];
                    $balance_qty = $formData['balance_qty'][$i];
                    $entered_qty = $formData['entered_qty'][$i];
                    $id_category = $formData['id_category'][$i];
                    $id_sub_category = $formData['id_sub_category'][$i];
                    $total_price_wot_tax = $formData['total_price_wot_tax'][$i];
                    $price_per_item_wot_tax = $formData['price_per_item_wot_tax'][$i];
                    $id_item = $formData['id_item'][$i];
                    $id_tax = $formData['id_tax'][$i];
                    $tax_per_each_item = $formData['tax_per_each_item'][$i];



                    if($entered_qty > 0)
	                {
                // echo "<Pre>"; print_r($formData);exit;
                        $amount = $amount_per_each_item * $entered_qty;

                        
                        $generated_number = $this->Asset_order_model->generateAssetOrderNumber();
	                    $poDetail = $this->Asset_order_model->getPoDetailById($id_po_detail);

	                    if($inserted_grn_id)
	                    {
		                        $detailsData = array(
                                'id_po' => $id_po,
		                        'id_grn' => $inserted_grn_id,
                                'id_po_detail' => $id_po_detail,
		                        'asset_order_number' => $generated_number,
		                        'id_category' => $id_category,
		                        'id_sub_category' => $id_sub_category,
                                'id_item' => $id_item,
		                        'id_tax' => $id_tax,
		                        'ordered_qty' => $quantity,
		                        'balance_qty' => $quantity - $entered_qty,                 
                                'received_qty' => $entered_qty,
                                'amount_per_item' => $amount_per_each_item,
		                        'amount' => $amount,
                                'cr_account' => $poDetail->cr_account,
                                'cr_activity' => $poDetail->cr_activity,
                                'cr_department' => $poDetail->cr_department,
                                'cr_fund' => $poDetail->cr_fund,
                                'dt_account' => $poDetail->dt_account,
                                'dt_activity' => $poDetail->dt_activity,
                                'dt_department' => $poDetail->dt_department,
                                'dt_fund' => $poDetail->dt_fund,
                                'price_per_item_wot_tax' => $price_per_item_wot_tax,
                                'tax_per_each_item' => $tax_per_each_item,
		                        'status' => '1',
		                        'created_by' => $user_id
		                        );
                // echo "<Pre>"; print_r($detailsData);exit;
		                        $inserted_id = $this->Asset_order_model->addNewAssetOrder($detailsData);
                                if($inserted_id)
                                {
                                    $updated = $this->Asset_order_model->updatePoDetailBalance($id_po_detail, $entered_qty);
                                }
		                        // print_r($inserted_id);exit;
		                }
	                }
                    $total_amount = $total_amount + $amount;
                }

                $update_grn_amount['total_amount'] = $total_amount;
                $updated_grn = $this->Asset_order_model->updateGrn($update_grn_amount,$inserted_grn_id);

                redirect('/asset/assetOrder/list');
            }

            $data['poEntryList'] = $this->Asset_order_model->poListByStatus('1');
            $this->global['pageTitle'] = 'FIMS : Add GRN';
            // echo "<Pre>";print_r($data['poEntryList']);exit;
            $this->loadViews("asset_order/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('receipt.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/assetOrder/list');
            }
            if($this->input->post())
            {
                redirect('/asset/assetOrder/list');
            }
            // $data['studentList'] = $this->receipt_model->studentList();
            $data['grn'] = $this->Asset_order_model->getGrn($id);
            $data['grnDetail'] = $this->Asset_order_model->getAssetOrderByGRNId($id);
            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'FIMS : View GRN';
            $this->loadViews("asset_order/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {

        if ($this->checkAccess('asset_order.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['assetCategoryList'] = $this->Asset_registration_model->assetCategoryListByStatus('1');
            $data['assetSubCategoryList'] = $this->Asset_registration_model->assetSubCategoryListByStatus('1');
            $data['assetItemList'] = $this->Asset_registration_model->assetItemListByStatus('1');
            $data['departmentCodeList'] = $this->Asset_order_model->getDepartmentCodeList();
            $data['financialYearList'] = $this->Asset_order_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Asset_order_model->budgetYearListByStatus('1');
            $data['vendorList'] = $this->Asset_order_model->vendorListByStatus('Approved');

            

            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['asset_order_number'] = $this->security->xss_clean($this->input->post('asset_order_number'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;


            $data['grnList'] = $this->Asset_order_model->getAssetOrderList($formData);

            // echo "<Pre>";print_r($data['grnList']);exit();

            $this->global['pageTitle'] = 'FIMS : Approval List GRN';
            //print_r($subjectDetails);exit;
            $this->loadViews("asset_order/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('receipt.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/assetOrder/approvalList');
            }
            if($this->input->post())
            {

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                
                 $result = $this->Asset_order_model->updateGrn($data,$id);
                 if($status == '1')
                 {
                    $detailsDatas = $this->Asset_order_model->postToGL($id);
                 }
                redirect('/asset/assetOrder/approvalList');
            }
            $data['grn'] = $this->Asset_order_model->getGrn($id);
            $data['grnDetail'] = $this->Asset_order_model->getAssetOrderByGRNId($id);
            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'FIMS : View GRN';
            $this->loadViews("asset_order/view", $this->global, $data, NULL);
        }
    }

    function getData($id)
    {
        
        $poDetails = $this->Asset_order_model->getPoDetails($id);
        $poMaster = $this->Asset_order_model->getPoMaster($id);
	// echo "<Pre>";print_r($poDetails);exit;


		$table = "

        <script type='text/javascript'>

            $( function()
            {
                $( '.datepicker' ).datepicker({
                    changeYear: true,
                    changeMonth: true,
                });
             } );


            $('select').select2();
        </script>


        <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$poMaster->po_number'>
                        </div>
                    </div>

                   <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Type <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$poMaster->type'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='$poMaster->po_entry_date'>
                        </div>
                    </div>

                </div>

                <div class='row'>
                   
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Financial Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$poMaster->financial_year' readonly='readonly'>
                        </div>
                    </div>

                    

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Budget Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$poMaster->budget_year' readonly='readonly'>
                        </div>
                    </div>



                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Department <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$poMaster->department_code - $poMaster->department_name' readonly='readonly'>
                        </div>
                    </div>

                    

                </div>

                <div class='row'>

                <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Description <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='d'  value='$poMaster->description'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Vendor <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$poMaster->vendor_code - $poMaster->vendor_name' readonly='readonly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Total Amount <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$poMaster->amount' readonly='readonly'>
                        </div>
                    </div>

                </div>


                <h4> GRN </h4>

                <div class='row'>

                 <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Expire Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' name='expire_date' id='expire_date' autocomplete='off'>
                        </div>
                    </div>
                    

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>GRN Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' name='description' id='description'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>GRN Rating <span class='error-text'>*</span></label>
                            <select class='form-control' name='rating' id='rating'>
                            <option value=''>Select</option>
                            <option value='A'>A</option>
                            <option value='B'>B</option>
                            <option value='C'>C</option>
                            </select>
                        </div>
                    </div>



                </div>



		<hr>
        <br>
        <h3>PO Details For GRN </h3>
        

        <div class='custom-table'>

		<table  class='table' id='list-table'>
              <tr>
                <th>Sl. No</th>
                <th>Item Name</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Total Qty</th>
                <th>Balance Qty</th>
                <th>Enter Qty</th>
            </tr>";
             // <th>Amount/Item(Inc. Tax)</th>

            $total_amount = 0;
            for($i=0;$i<count($poDetails);$i++)
            {

	            $id = $poDetails[$i]->id;
	            $id_tax = $poDetails[$i]->id_tax;
	            $id_item = $poDetails[$i]->id_item;
	            $item_code = $poDetails[$i]->item_code;
	            $item_name = $poDetails[$i]->item_name;
	            $id_category = $poDetails[$i]->id_category;
	            $sub_category_code = $poDetails[$i]->sub_category_code;
	            $sub_category_name = $poDetails[$i]->sub_category_name;
	            $id_sub_category = $poDetails[$i]->id_sub_category;
	            $category_code = $poDetails[$i]->category_code;
	            $category_name = $poDetails[$i]->category_name;
	            $quantity = $poDetails[$i]->quantity;
	            $balance_qty = $poDetails[$i]->balance_qty;
                $received_qty = $poDetails[$i]->received_qty;
                $total_final = $poDetails[$i]->total_final;
                $price_per_item_wot_tax = $poDetails[$i]->price;
                $total_tax_amount = $poDetails[$i]->tax_price;

                $total_price_wot_tax = $received_qty * $price_per_item_wot_tax;

                $amount_per_each_item = $total_final/$quantity; // Including Tac
                $tax_per_each_item = $total_tax_amount/$quantity; // Including Tac
                // print_r($amount_per_each_item);exit();
            	$j = $i+1;
                
                $table .= "
                <tr>
                    <td>$j. <input type='number' hidden='hidden' readonly='readonly' id='id_po_detail[]' name='id_po_detail[]' value='$id'></td>
                    <td>
	                    <input type='number' hidden='hidden' readonly='readonly' id='id_item[]' name='id_item[]' value='$id_item'>
	                    $item_code - item_name
                    </td>
                    <td>
                    	<input type='number' hidden='hidden' readonly='readonly' id='id_category[]' name='id_category[]' value='$id_category'>
                    	$category_code - $category_name
                    </td>
                    <td>

                        
                    	<input type='number' hidden='hidden' readonly='readonly' id='id_sub_category[]' name='id_sub_category[]' value='$id_sub_category'>
                    	$sub_category_code - $sub_category_name
                    </td>                    
                    <td>
                    <input type='number' readonly='readonly' id='quantity[]' name='quantity[]' value='$quantity'>
                    <input type='number' hidden='hidden' readonly='readonly' id='amount_per_each_item[]' name='amount_per_each_item[]' value='$amount_per_each_item'>
                    <input type='number' hidden='hidden' readonly='readonly' id='total_price_wot_tax[]' name='total_price_wot_tax[]' value='$total_price_wot_tax'>
                    <input type='number' hidden='hidden' readonly='readonly' id='price_per_item_wot_tax[]' name='price_per_item_wot_tax[]' value='$price_per_item_wot_tax'>
                    <input type='number' hidden='hidden' readonly='readonly' id='id_tax[]' name='id_tax[]' value='$id_tax'>
                    <input type='number' hidden='hidden' readonly='readonly' id='tax_per_each_item[]' name='tax_per_each_item[]' value='$tax_per_each_item'>


                    </td> 
                    <td>
                    <input type='number' readonly='readonly' id='balance_qty[]' name='balance_qty[]' value='$balance_qty'>
                    </td>                      
                    <td>
                        <input type='number' id='entered_qty[]' name='entered_qty[]' value=''>
                    </td>
                </tr>";
			}

        $table.= "
        </table>
        </div>";

       echo $table;
       // exit();

    }
}