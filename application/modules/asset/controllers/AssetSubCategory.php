<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetSubCategory extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('asset_sub_category_model');
            $this->load->model('asset_category_model');
            $this->isLoggedIn();
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function list()
    {

        if ($this->checkAccess('asset_sub_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r("dasda");exit;
            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryList();
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_asset_category'] = $this->security->xss_clean($this->input->post('id_asset_category'));

            $data['searchParameters'] = $formData; 

            $data['assetSubCategoryList'] = $this->asset_sub_category_model->assetSubCategoryListSearch($formData);
            $this->global['pageTitle'] = 'FIMS : List Asset Sub Category';
            $this->loadViews("asset_sub_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('asset_sub_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_asset_category = $this->security->xss_clean($this->input->post('id_asset_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_asset_category' => $id_asset_category,
                    'status' => $status
                );
            
                $result = $this->asset_sub_category_model->addNewAssetSubCategory($data);
                redirect('/asset/assetSubCategory/list');
            }
            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryList();
            $this->global['pageTitle'] = 'FIMS : Add Asset Sub Category';
            $this->loadViews("asset_sub_category/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('asset_sub_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/asset_sub_category/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_asset_category = $this->security->xss_clean($this->input->post('id_asset_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_asset_category' => $id_asset_category,
                    'status' => $status
                );
                
                $result = $this->asset_sub_category_model->editAssetSubCategory($data,$id);
                redirect('/asset/assetSubCategory/list');
            }
            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryList();
            $data['assetSubCategory'] = $this->asset_sub_category_model->getAssetSubCategory($id);
            $this->global['pageTitle'] = 'FIMS : Edit Asset Sub Category';
            $this->loadViews("asset_sub_category/edit", $this->global, $data, NULL);
        }
    }
}
