<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetItem extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('asset_sub_category_model');
            $this->load->model('asset_category_model');
            $this->load->model('asset_item_model');
            $this->isLoggedIn();
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function list()
    {

        if ($this->checkAccess('asset_sub_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryList();
            $data['assetSubCategoryList'] = $this->asset_sub_category_model->assetSubCategoryList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_asset_category'] = $this->security->xss_clean($this->input->post('id_asset_category'));
            $formData['id_asset_sub_category'] = $this->security->xss_clean($this->input->post('id_asset_sub_category'));

            $data['searchParameters'] = $formData; 
            // print_r($formData);exit;

            $data['assetItemList'] = $this->asset_item_model->assetItemListSearch($formData);
            $this->global['pageTitle'] = 'FIMS : List Asset Item';
            $this->loadViews("asset_item/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('asset_item.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_asset_category = $this->security->xss_clean($this->input->post('id_asset_category'));
                $id_asset_sub_category = $this->security->xss_clean($this->input->post('id_asset_sub_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_asset_category' => $id_asset_category,
                    'id_asset_sub_category' => $id_asset_sub_category,
                    'status' => $status,
                    'created_by' => $id_user

                );
            
                $result = $this->asset_item_model->addNewAssetItem($data);
                redirect('/asset/assetItem/list');
            }
            
            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryList();
            $data['assetSubCategoryList'] = $this->asset_sub_category_model->assetSubCategoryList();
            $this->global['pageTitle'] = 'FIMS : Add Asset Item';
            $this->loadViews("asset_item/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('asset_item.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/asset_item/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_asset_category = $this->security->xss_clean($this->input->post('id_asset_category'));
                $id_asset_sub_category = $this->security->xss_clean($this->input->post('id_asset_sub_category'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'id_asset_category' => $id_asset_category,
                    'id_asset_sub_category' => $id_asset_sub_category,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                
                $result = $this->asset_item_model->editAssetItem($data,$id);
                redirect('/asset/assetItem/list');
            }
            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryList();
            $data['assetSubCategoryList'] = $this->asset_sub_category_model->assetSubCategoryList();
            $data['assetItem'] = $this->asset_item_model->getAssetItem($id);
            $this->global['pageTitle'] = 'FIMS : Edit Asset Item';
            $this->loadViews("asset_item/edit", $this->global, $data, NULL);
        }
    }

    function getSubcategory($id)
     {       
            // print_r("dasda");exit;
            $results = $this->asset_sub_category_model->assetSubCategoryListByAssetId($id);
            // echo "<Pre>"; print_r($results);exit;
            $table="

             <script type='text/javascript'>
                $('select').select2();
            </script>

            
            <select name='id_asset_sub_category' id='id_asset_sub_category' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_asset_category;
            $id = $results[$i]->id;
            $categoryname = $results[$i]->code;
            $table.='<option value="'. $id.'">'.$categoryname.'</option>';

            }
            $table.="</select>";
            echo $table;
            exit;
    }
}
