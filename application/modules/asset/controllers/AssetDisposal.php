<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetDisposal extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Asset_disposal_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('asset_disposal.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['id_disposal_type'] = $this->security->xss_clean($this->input->post('id_disposal_type'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['assetDisposalList'] = $this->Asset_disposal_model->assetDisposalListSearch($formData);


            $data['disposalTypeList'] = $this->Asset_disposal_model->disposalTypeListByStatus('1');
            $data['departmentList'] = $this->Asset_disposal_model->departmentListByStatus('1');
            $data['staffList'] = $this->Asset_disposal_model->staffListByStatus('1');

            $this->global['pageTitle'] = 'FIMS : List Asset Disposal';
            $this->loadViews("asset_disposal/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('asset_disposal.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                
               
                $id_disposal_type = $this->security->xss_clean($this->input->post('id_disposal_type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));

                $data = array(
                    'id_disposal_type' =>$id_disposal_type,
                    'reason' => $reason,
                    'id_staff' => $id_staff,
                    'department_code' => $department_code,
                    'status'=>'0',
                    'created_by' => $user_id
                );
                $inserted_id = $this->Asset_disposal_model->addAssetDisposal($data);


                

                for($i=0;$i<count($formData['id_asset']);$i++)
                 {
                    $id_asset = $formData['id_asset'][$i];
                    $asset_code = $formData['asset_code'][$i];

                    if($id_asset > 0)
                    {
                        if($inserted_id)
                        {
                            $detailsData = array(
                                'id_asset_disposal'=>$inserted_id,
                                'id_asset'=>$id_asset,
                                'asset_code'=>$asset_code,
                                'status'=>'0',
                                'created_by'=>$user_id
                            );
                    // echo "<Pre>"; print_r($detailsData);exit;
                        $inserted_detail_id = $this->Asset_disposal_model->addAssetDisposalDetail($detailsData);
                       
                        }
                    }
                }
                $no_of_assets = $i;
                $update_master['no_of_assets'] = $no_of_assets;
                $inserted_detail_id = $this->Asset_disposal_model->updateAssetDisposal($update_master,$inserted_id);

                redirect('/asset/assetDisposal/list');
            }

            $data['disposalTypeList'] = $this->Asset_disposal_model->disposalTypeListByStatus('1');
            $data['departmentList'] = $this->Asset_disposal_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Asset_disposal_model->getDepartmentCodeList();
            $data['staffList'] = $this->Asset_disposal_model->staffListByStatus('1');
            // echo "<Pre>";print_r($data['getActivityCodeList']);exit;
            $this->global['pageTitle'] = 'FIMS : Add Asset Disposal';
            $this->loadViews("asset_disposal/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('asset_disposal.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/asset/assetDisposal/list');
            }
            if($this->input->post())
            {
                // redirect('/procurement/procurementCategory/list');
            }
            
            $data['assetDisposal'] = $this->Asset_disposal_model->getAssetDisposal($id);
            $data['assetDisposalDetails'] = $this->Asset_disposal_model->getAssetDisposalDetailsByMaster($id);

        // echo "<Pre>";print_r($data);exit();

            
            $this->global['pageTitle'] = 'FIMS : View Asset Disposal';
            // $this->loadViews("asset_disposal/edit", $this->global, $data, NULL);
            $this->loadViews("asset_disposal/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('asset_disposal.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/asset/assetDisposal/list');
            }
            if($this->input->post())
            {
                $meeting_date = $this->security->xss_clean($this->input->post('meeting_date'));
                $meeting_reference_no = $this->security->xss_clean($this->input->post('meeting_reference_no'));

                $formData = $this->input->post();

                for($i=0;$i<count($formData['approval']);$i++)
                 {
                    // echo "<Pre>"; print_r($formData);exit;
                    $id_detail = $formData['approval'][$i];
                    // $id_asset = $formData['id_asset'][$i];

                    if($id_detail > 0)
                    {
                            $detailsData = array(
                                'status'=>1
                            );
                        $inserted_detail_id = $this->Asset_disposal_model->editAssetDisposalDetail($detailsData,$id_detail);
                        $id_asset = $this->Asset_disposal_model->getAssetDisposalDetail($id_detail);
                    // echo "<Pre>"; print_r($id_asset);exit;
                    }

                    $master_data = array(
                                'meeting_date'=> date('Y-m-d', strtotime($meeting_date)),
                                'meeting_reference_no'=>$meeting_reference_no
                            );

                    $updated_master = $this->Asset_disposal_model->updateAssetDisposal($master_data,$id);
                }

                $check_for_remaining = $this->Asset_disposal_model->checkAssetDisposalDetail($id);
                redirect('/asset/assetDisposal/approvalList');
            }
            
            $data['assetDisposal'] = $this->Asset_disposal_model->getAssetDisposal($id);
            $data['assetDisposalDetails'] = $this->Asset_disposal_model->getAssetDisposalDetailsByMaster($id);
            

            $this->global['pageTitle'] = 'FIMS : View Asset Disposal';
            // $this->loadViews("asset_disposal/edit", $this->global, $data, NULL);
            $this->loadViews("asset_disposal/view", $this->global, $data, NULL);
        }
    }


    function approvalList()
    {
        if ($this->checkAccess('asset_disposal.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
           $formData['id_disposal_type'] = $this->security->xss_clean($this->input->post('id_disposal_type'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['status'] = '0';
 
           $data['searchParam'] = $formData;

            $data['disposalTypeList'] = $this->Asset_disposal_model->disposalTypeListByStatus('1');
            $data['departmentList'] = $this->Asset_disposal_model->departmentListByStatus('1');
            $data['staffList'] = $this->Asset_disposal_model->staffListByStatus('1');

            $data['assetDisposalList'] = $this->Asset_disposal_model->assetDisposalListSearch($formData);
            // redirect($_SERVER['HTTP_REFERER']);


            $this->global['pageTitle'] = 'FIMS : Approve Asset Disposal';
            $this->loadViews("asset_disposal/approval_list", $this->global, $data, NULL);
        }
    }


    function searchAsset()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $asset_data = $this->Asset_disposal_model->assetRegistrationListSearchForDispose($tempData);

        if(!empty($asset_data))
        {

        // echo "<Pre>";print_r($asset_data);exit();

         $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Asset Name</th>
                    <th>Asset Code</th>
                    <th>Comapny</th>
                    <th>Brand</th>
                    <th>Price</th>
                    <th>Depriciation Code</th>
                    <th>Depriciation Value</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($asset_data);$i++)
            {
                $id = $asset_data[$i]->id;
                $name = $asset_data[$i]->name;
                $code = $asset_data[$i]->code;
                $company = $asset_data[$i]->company;
                $brand = $asset_data[$i]->brand;
                $price = $asset_data[$i]->price;
                $depriciation_code = $asset_data[$i]->depriciation_code;
                $depriciation_value = $asset_data[$i]->depriciation_value;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$name</td>
                    <td>
                        <input type='text' hidden='hidden' readonly='readonly' id='asset_code[]' name='asset_code[]' value='$code'>
                        $code
                    </td>                           
                    <td>$company</td>                           
                    <td>$brand</td>                           
                    <td>$price</td>                      
                    <td>$depriciation_code</td>                      
                    <td>$depriciation_value</td>                      
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_asset[]' name='id_asset[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "";
        }
        echo $table;
    }
}