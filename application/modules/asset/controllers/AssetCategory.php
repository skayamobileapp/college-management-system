<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('asset_category_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('asset_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['assetCategoryList'] = $this->asset_category_model->assetCategoryListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Asset Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("asset_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('asset_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
            
                $result = $this->asset_category_model->addNewAssetCategory($data);
                redirect('/asset/assetCategory/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Asset Category';
            $this->loadViews("asset_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('asset_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/asset_category/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->asset_category_model->editAssetCategory($data,$id);
                redirect('/asset/assetCategory/list');
            }
            $data['assetCategory'] = $this->asset_category_model->getAssetCategoryDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Asset Category';
            $this->loadViews("asset_category/edit", $this->global, $data, NULL);
        }
    }
}
