<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Asset Pre Registration </h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Asset Pre-Registration</h4>



            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="balance_qty" name="balance_qty" value="<?php echo $assetPreRegistration->po_number;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="balance_qty" name="balance_qty" value="<?php echo $assetPreRegistration->reference_number;?>" readonly="readonly">
                    </div>
                </div>

            </div>
            <br>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetPreRegistration->category_name . ' - ' . $assetPreRegistration->category_code;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub-Category <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetPreRegistration->sub_category_code . ' - ' . $assetPreRegistration->sub_category_name;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Item <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetPreRegistration->item_code . ' - ' . $assetPreRegistration->item_name;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Asset Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetPreRegistration->name;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Brand <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetPreRegistration->brand;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo $assetPreRegistration->company;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="ordered_qty" name="ordered_qty" value="<?php echo $assetPreRegistration->price;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="received_qty" name="received_qty" value="<?php echo $assetPreRegistration->tax; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Depreciation Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="balance_qty" name="balance_qty" value="<?php echo $assetPreRegistration->depriciation_code;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Depreciation Value <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="balance_qty" name="balance_qty" value="<?php echo $assetPreRegistration->depriciation_value;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="status" name="status" value="<?php
                        if($assetPreRegistration->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($assetPreRegistration->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($assetPreRegistration->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            </div>

        </div>


            <hr>


        <!-- <table width="100%">
            <thead>
                 <tr>
                     <th>Fund</th>
                     <th>Department</th>
                     <th>Activity</th>
                     <th>Account</th>
                     <th>Fund</th>
                     <th>Department</th>
                     <th>Activity</th>
                     <th>Account</th>
                     <th>Category</th>
                     <th>Sub Category</th>
                     <th>Item</th>
                     <th>Tax</th>
                     <th>Qty</th>
                     <th>Price</th>
                     <th>Total</th>
                     <th>Final Total</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 

                 for($i=0;$i<count($poDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $poDetails[$i]->cr_account;?></td>
                    <td><?php echo $poDetails[$i]->cr_activity;?></td>
                    <td><?php echo $poDetails[$i]->cr_department;?></td>
                    <td><?php echo $poDetails[$i]->cr_fund;?></td>
                    <td><?php echo $poDetails[$i]->dt_account;?></td>
                    <td><?php echo $poDetails[$i]->dt_activity;?></td>
                    <td><?php echo $poDetails[$i]->dt_department;?></td>
                    <td><?php echo $poDetails[$i]->dt_fund;?></td>
                    <td><?php echo $poDetails[$i]->category_name;?></td>
                    <td><?php echo $poDetails[$i]->category_code . " - " . $poDetails[$i]->sub_category_name;?></td>
                    <td><?php echo $poDetails[$i]->sub_category_code . " - " . $poDetails[$i]->item_name;?></td>
                    <td><?php echo $poDetails[$i]->item_code . " - " . $poDetails[$i]->quantity;?></td>
                    <td><?php echo $poDetails[$i]->price;?></td>
                    <td><?php echo $poDetails[$i]->id_tax;?></td>
                    <td><?php echo $poDetails[$i]->tax_price;?></td>
                    <td><?php echo $poDetails[$i]->total_final;?></td>

                     </tr>
                  <?php } ?>

            </tbody>
        </table> -->


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>