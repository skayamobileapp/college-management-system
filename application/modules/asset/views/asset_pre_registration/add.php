<?php $this->load->helper("form"); ?>
<form id="form_po_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Asset Pre-Registration</h3>
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Asset Pre-Registration</h4>

         <div class="row">
            <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select GRN Number <span class='error-text'>*</span></label>
                        <select name='id_grn' id='id_grn' class='form-control' onChange="getDetails(this.value)">
                            <option value=''>Select</option>

                            <?php for($i=0;$i<count($assetOrderList);$i++)
                            {
                                ?>
                                <option value="<?php echo $assetOrderList[$i]->id;?>">
                                <?php echo $assetOrderList[$i]->reference_number . " - " . $assetOrderList[$i]->description;?>  
                                </option>
                                <?php
                            } ?> 
                        </select>
                    </div>
            </div>
        </div>

      </div>

      <div style="display: none;" class="form-container" id="view_asset_order_display">
            <h4 class="form-group-title">GRN Details</h4>

        <div id="view_asset_order">
        </div>

      </div>

      
      <div style="display: none;" class="form-container" id="view_pre_register_display">
          <h4 class="form-group-title">Asset Pre-Registration Details</h4>

          <div  id="view_pre_register">
          </div>

      </div>

      <div class="button-block clearfix">
          <div class="bttn-group">
              <button type="submit" class="btn btn-primary btn-lg">Save</button>
              <a href="list" class="btn btn-link">Cancel</a>
          </div>
      </div>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


</form>
<script>

    function getDetails(id)
    {
       // alert(id);
            $.ajax(
            {
               url: '/asset/assetPreRegistration/getData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_asset_order_display").show();
                    $("#view_asset_order").html(result);
               }
            });   
    }

    function showPreRegister(id)
    {
        $.ajax(
            {
               url: '/asset/assetPreRegistration/showPreRegister/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_pre_register_display").show();
                    $("#view_pre_register").html(result);
               }
            });   
    }


   $(document).ready(function() {
        $("#form_po_entry").validate({
            rules: {
                id_grn: {
                    required: true
                }
            },
            messages: {
                id_grn: {
                    required: "<p class='error-text'>Select PO",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>

<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>