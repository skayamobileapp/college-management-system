<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Asset Disposal</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Asset Disposal</h4>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <select name="id_staff" id="id_staff" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->ic_no . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Disposal Type <span class='error-text'>*</span></label>
                        <select name="id_disposal_type" id="id_disposal_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($disposalTypeList))
                            {
                                foreach ($disposalTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Disposal Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" readonly="readonly" value="<?php echo date('d-m-Y'); ?>">
                    </div>
                </div>
            
            </div>
        </div>


        <div class="form-container">
            <h4 class="form-group-title">Search Asset For Disposal</h4>
            <h4 >Search Asset For Disposal</h4>


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Asset name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Asset Code</label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Brand</label>
                        <input type="text" class="form-control" id="brand" name="brand">
                    </div>
               </div>

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Company</label>
                        <input type="text" class="form-control" id="company" name="company">
                    </div>
               </div>
                
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchAsset()">Search</button>
            </div>

        </div>


        <div class="form-container" style="display: none;" id="view_assets_display">
            <h4 class="form-group-title">Assets For Disposal</h4>


            <div  id='view_assets'>
            </div>

        </div>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



</form>
<script>

    function opendialog(){
         $("#checkval").val('');
        $("#code").val('');
        $("#name").val('');
        $('#myModal').modal('show');
    }

    function searchAsset()
    {
        var tempPR = {};
        tempPR['name'] = $("#name").val();
        tempPR['code'] = $("#code").val();
        tempPR['brand'] = $("#brand").val();
        tempPR['company'] = $("#company").val();
            $.ajax(
            {
               url: '/asset/assetDisposal/searchAsset',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_assets_display").show();
                $("#view_assets").html(result);
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }


    function saveData() {

        var tempPR = {};
        tempPR['asset_code'] = $("#asset_code").val();
        tempPR['id_asset'] = $("#id_asset").val();

        // alert(ta);


        var tempPR = {};
        
            $.ajax(
            {
               url: '/asset/assetDisposal/tempadd',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $('#myModal').modal('hide');
                $("#view").html(result);
                var ta = $("#total_detail").val();
                // alert(ta);
                $("#amount").val(ta);
               }
            });
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/asset/assetDisposal/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_disposal_type: {
                    required: true
                },
                 reason: {
                    required: true
                },
                id_staff: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 date_time: {
                    required: true
                }
            },
            messages: {
                id_disposal_type: {
                    required: "<p class='error-text'>Select Disposal Type</p>",
                },
                reason: {
                    required: "<p class='error-text'>Enter Reason</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>