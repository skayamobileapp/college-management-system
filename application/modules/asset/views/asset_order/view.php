<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Approve GRN</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">GRN Entry Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->reference_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo date('d-m-Y',strtotime($grn->date_time));?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Expire Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="expire_date" name="expire_date" value="<?php echo date('d-m-Y',strtotime($grn->expire_date));?>" readonly="readonly">
                    </div>
                </div>

           

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->po_number;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->vendor_code . ' - ' . $grn->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->financial_year;?>" readonly="readonly">
                    </div>
                </div>
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->budget_year;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->department_code . ' - ' . $grn->department_name;?>" readonly="readonly">
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->total_amount;?>" readonly="readonly">
                    </div>
                </div> -->

            </div>


             <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($grn->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($grn->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($grn->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

             <?php    
            if($grn->status == '2')
            {
             ?>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $grn->reason;?>" readonly="readonly">
                    </div>
                </div>

             <?php
            }
            ?>

             </div>



       <?php    
        if($grn->status == '0')
        {
         ?>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

            </div>

         <?php
        }
        ?>

            <div class="row">

                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>
            </div>


        </div>


            <hr>

            <h3>GRN Details</h3>

        <div class="form-container">
        <h4 class="form-group-title">GRN  Details</h4>

            <div class="custom-table">
                <table class="table">
                    <thead>
                         <tr>
                             <th>Sl. No</th>
                             <th>PO Number</th>
                             <!-- <th>PR Number</th> -->
                             <th>Item</th>
                             <th>Category</th>
                             <th>Sub Category</th>
                             <th>Quantity Received</th>
                             <!-- <th>Amount / Item</th>
                             <th>Amount</th> -->
                         </tr>
                    </thead>
                    <tbody>
                         <?php 
                         $total = 0;
                         for($i=0;$i<count($grnDetail);$i++)
                            { 
                            // echo "<Pre>";print_r($grnDetail[$i]);exit();

                                ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $grnDetail[$i]->po_number;?></td>
                            <!-- <td><?php echo $grnDetail[$i]->pr_number;?></td> -->
                            <td><?php echo $grnDetail[$i]->item_code . " - " . $grnDetail[$i]->item_name;?></td>
                            <td><?php echo $grnDetail[$i]->category_code . " - " . $grnDetail[$i]->category_name; ?></td>
                            <td><?php echo $grnDetail[$i]->sub_category_code . " - " . $grnDetail[$i]->sub_category_name;?></td>
                            <td><?php echo $grnDetail[$i]->received_qty;?></td>
                            <!-- <td><?php echo $grnDetail[$i]->amount_per_item;?></td>
                            <td><?php echo $grnDetail[$i]->amount;?></td> -->
                             </tr>
                          <?php
                          $total = $total + $grnDetail[$i]->amount;

                        }
                          $total = number_format($total, 2, '.', ',');
                          ?>

                         <!--  <tr>
                            <td colspan="4" bgcolor=""></td>
                            <td bgcolor=""><b> Total : </b></td>
                            <td bgcolor=""><b><?php echo $total; ?></b></td>
                        </tr> -->

                    </tbody>
                </table>

            </div>
        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">

         <?php    
        if($grn->status == '0')
        {
         ?>
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

        <?php
        }
        ?>
                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</form>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    
    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>