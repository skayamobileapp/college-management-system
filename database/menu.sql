-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 24, 2020 at 12:25 AM
-- Server version: 5.7.32-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `college`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int(20) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1),
(4, 'Salutation', 'Setup', 'General Setup', NULL, 'salutation', 'list', 1),
(5, 'Country', 'Setup', 'country', NULL, 'country', 'list', NULL),
(6, 'State', 'Setup', 'state', NULL, 'state', 'list', NULL),
(7, 'Education Level', 'Setup', 'General Setup', NULL, 'educationLevel', 'list', NULL),
(8, 'Academic Year', 'Setup', 'Operation Setup', 2, 'academicYear', 'list', 6),
(9, 'Award', 'Setup', 'General Setup', NULL, 'award', 'list', NULL),
(10, 'Race', 'Setup', 'Demographic', 1, 'race', 'list', 2),
(11, 'Religion', 'Setup', 'Demographic', 2, 'religion', 'list', 2),
(12, 'Nationality', 'Setup', 'Demographic', 3, 'nationality', 'list', 2),
(13, 'Department', 'Setup', 'Organisation Setup', 3, 'department', 'list', 3),
(14, 'Organisation', 'Setup', 'Organisation Setup', 1, 'organisation', 'edit', 3),
(15, 'Partner Category', 'pm', 'Setup', 1, 'partnerCategory', 'list', 1),
(16, 'Partner University', 'pm', 'Setup', 2, 'partnerUniversity', 'list', 1),
(17, 'Faculty Program', 'Setup', 'Organisation Setup', 2, 'facultyProgram', 'list', 6),
(18, 'Course Major / Minor', 'Setup', NULL, NULL, NULL, 'list', NULL),
(19, 'Course Type', 'cm', 'Course Setup', 1, 'courseType', 'list', 1),
(20, 'Course Offered', 'Setup', 'Operation Setup', 6, 'courseOffered', 'list', 6),
(21, 'Course Main', 'cm', 'Course Setup', 2, 'course', 'list', 1),
(22, 'Course Requisite', 'cm', 'Course Setup', 3, 'courseMaster', 'list', 1),
(23, 'Course Equivalent', 'cm', 'Course Setup', 4, 'equivalentCourse', 'list', 1),
(24, 'Scheme', 'Setup', 'Operation Setup', 1, 'scheme', 'list', 6),
(25, 'Activity', 'Setup', 'Operation Setup', 5, 'activityDetails', 'list', 6),
(26, 'Semester', 'Setup', 'Operation Setup', 4, 'semester', 'list', 6),
(27, 'Intake', 'Setup', 'Operation Setup', 3, 'intake', 'list', 6),
(28, 'Landscape Course Type', 'Setup', NULL, NULL, NULL, 'list', NULL),
(29, 'Program Type', 'cm', 'Program Setup', 1, 'programType', 'list', 2),
(30, 'Program Main', 'cm', 'Program Setup', 2, 'programme', 'list', 2),
(31, 'Program Landscape', 'cm', 'Program Setup', 4, 'programmeLandscape', 'list', 2),
(32, 'Program Entry Requirement', 'cm', 'Program Setup', 3, 'individualEntryRequirement', 'list', 2),
(33, 'Documents Required', 'Setup', 'Document', 1, 'documents', 'list', 5),
(34, 'Staff Master', 'Setup', 'Staff', 1, 'staff', 'list', 4),
(35, 'Course Withdraw', 'Setup', NULL, NULL, NULL, 'list', NULL),
(36, 'Late Registration', 'Setup', NULL, NULL, NULL, 'list', NULL),
(37, 'Report Students by Intake', 'Setup', NULL, NULL, NULL, 'list', NULL),
(38, 'Report Applicant V/S Lead V/S Student', 'Setup', NULL, NULL, NULL, 'list', NULL),
(39, 'Report Course Wise data', 'Setup', NULL, NULL, NULL, 'list', NULL),
(40, 'Group Setup', 'Admission', NULL, NULL, NULL, 'list', NULL),
(41, 'Specialization', 'Admission', NULL, NULL, NULL, 'list', NULL),
(42, 'Application List', 'Admission', 'Online Application and Applicant Portal', 1, 'applicant', 'list', 1),
(43, 'APEL Approval', 'Admission', 'Online Application and Applicant Portal', 2, 'apelApproval', 'list', 1),
(44, 'Sibbling Discount Approval', 'Admission', 'Online Application and Applicant Portal', 3, 'sibblingDiscountApproval', 'list', 1),
(45, 'Employee Discount Approval', 'Admission', 'Online Application and Applicant Portal\n', 4, 'employeeDiscountApproval', 'list', 1),
(46, 'Alumni Discount Approval', 'Admission', 'Online Application and Applicant Portal\n', 5, 'alumniDiscountApproval', 'list', 1),
(47, 'Application Approval', 'Admission', 'Online Application and Applicant Portal\n', 6, 'applicantApproval', 'list', 1),
(48, 'Student Registration', 'Registration', 'Registration', 1, 'student', 'approvalList', 1),
(49, 'Academic Advisor Tagging', 'Registration', 'Registration', 2, 'advisorTagging', 'add', 1),
(50, 'Subject Registration', 'Registration', 'Registration', 3, 'courseRegistration', 'list', 1),
(51, 'Individual and bulk Withdrawal', 'Registration', 'Registration', 4, 'bulkWithdraw', 'list', 1),
(52, 'Credit Transfer / Excemption Application', 'Registration', 'Credit Transfer / Excemption', 1, 'creditTransfer', 'list', 2),
(53, 'Credit Transfer / Excemption Approval', 'Registration', 'Credit Transfer / Excemption', 2, 'creditTransfer', 'approvalList', 2),
(54, 'Attendence Setup', 'Registration', NULL, NULL, NULL, 'list', NULL),
(55, 'Report Course Count', 'Registration', NULL, NULL, NULL, 'list', NULL),
(56, 'Barring Type', 'Records', 'Barring', 1, 'barringType', 'list', 2),
(57, 'Barring', 'Records', 'Barring', 2, 'barring', 'list', 2),
(58, 'Release', 'Records', 'Barring', 3, 'release', 'list', 2),
(59, 'Student Profile', 'Records', 'Records Info', 2, 'studentRecord', 'list', 1),
(60, 'Student Records', 'Records', 'Records Info', 1, 'student', 'list', 1),
(61, 'Change Status Setup', 'Records', 'Change Status', 1, 'changeStatus', 'list', 3),
(62, 'Apply Change Status', 'Records', 'Change Status', 2, 'applyChangeStatus', 'list', 3),
(63, 'Apply Change Status Approval', 'Records', 'Change Status', 3, 'applyChangeStatus', 'approval_list', 3),
(64, 'Visa Details', 'Records', 'Visa Management', 1, 'visaDetails', 'list', 7),
(65, 'Apply Change Program', 'Records', 'Change Programme', 1, 'applyChangeProgramme', 'list', 4),
(66, 'Apply Change Program Approval', 'Records', 'Change Programme', 2, 'applyChangeProgramme', 'approval_list', 4),
(67, 'Apply Change Scheme', 'Records', NULL, NULL, NULL, 'list', NULL),
(68, 'Apply Change Scheme Approval', 'Records', NULL, NULL, NULL, 'list', NULL),
(69, 'Change Branch', 'Records', 'Change Branch/Learning Centre', 1, 'changeBranch', 'list', 6),
(70, 'Change Branch Approval', 'Records', 'Change Branch/Learning Centre', 2, 'changeBranch', 'approval_list', 6),
(71, 'Exam Configuration', 'Examination', 'Setup', 1, 'examConfiguration', 'edit', 1),
(72, 'Exam Calender', 'Examination', NULL, 1, NULL, 'list', NULL),
(73, 'GPA/CGPA Setup', 'Examination', 'Setup', 2, 'gpaCgpaSetup', 'list', NULL),
(74, 'Assesment Type', 'Examination', 'Setup', 3, 'assesmentType', 'list', NULL),
(75, 'Grade Setup', 'Examination', 'Setup', 4, 'gradeSetup', 'list', NULL),
(76, 'Course Grade', 'Examination', NULL, NULL, NULL, 'list', NULL),
(77, 'Publish Exam Result', 'Examination', NULL, NULL, NULL, 'list', NULL),
(78, 'Exam Locaction Setup', 'Examination', NULL, NULL, NULL, 'list', NULL),
(79, 'Exam Center Setup', 'Examination', NULL, NULL, NULL, 'list', NULL),
(80, 'Exam Event', 'Examination', NULL, NULL, NULL, 'list', NULL),
(81, 'Exam Registration List', 'Examination', 'Exam Registration', 1, 'examRegistration', 'list', 2),
(82, 'Tag Student To Exam Registration', 'Examination', 'Exam Registration', 5, 'examRegistration', 'tagList', 2),
(83, 'Release Exam Slip', 'Examination', NULL, NULL, NULL, 'list', NULL),
(84, 'Publish Exam Result Schedule', 'Examination', NULL, NULL, NULL, 'list', NULL),
(85, 'Publish Assesment Result Schedule', 'Examination', NULL, NULL, NULL, 'list', NULL),
(86, 'Mark Distribution', 'Examination', 'Mark Distribution Setup', 1, 'markDistribution', 'list', 3),
(87, 'Mark Distribution Approval', 'Examination', 'Mark Distribution Setup', 2, 'markDistribution', 'approvalList', 3),
(88, 'Student Marks Entry Approval', 'Examination', 'Marks Entry', 2, 'studentMarksEntry', 'marksEntryApprovalList', 4),
(89, 'Student Marks Entry Summary', 'Examination', 'Marks Entry', 3, 'studentMarksEntry', 'summaryList', 4),
(90, 'Remarking Application', 'Examination', 'Remarking', 1, 'remarkingApplication', 'remarkingList', 5),
(91, 'Remarking Application Approval', 'Examination', 'Remarking', 2, 'remarkingApplication', 'marksEntryApprovalList', 5),
(92, 'Remarking List', 'Examination', 'Remarking', 3, 'remarkingApplication', 'summaryList', 5),
(93, 'Transcript', 'Examination', 'Remarking', 4, 'transcript', 'list', 5),
(94, 'Result Slip', 'Examination', NULL, 5, NULL, 'list', NULL),
(95, 'Award Level', 'Graduation', 'Setup', 1, 'award', 'list', 1),
(96, 'Convocation Setup', 'Graduation', 'Graduation', 1, 'convocation', 'list', 2),
(97, 'Guest', 'Graduation', 'Graduation', 2, 'guest', 'list', 2),
(98, 'Graduation List', 'Graduation', 'Graduation', 3, 'graduationList', 'list', NULL),
(99, 'Approve Graduation Students', 'Graduation', 'Graduation', 4, 'graduationList', 'approvalList', NULL),
(100, 'Currency Setup', 'Finance', 'Finance Setup', 1, 'currency', 'list', 1),
(101, 'Currency Rate Setup', 'Finance', 'Finance Setup', 2, 'currencyRateSetup', 'list', 1),
(102, 'Credit Note Type', 'Finance', 'Finance Setup', 3, 'creditNoteType', 'list', 1),
(103, 'Fee Structure Activity', 'Finance', 'Fee Structure', 3, 'feeStructureActivity', 'list', 2),
(104, 'Account Code', 'Finance', 'Finance Setup', 5, 'accountCode', 'list', 1),
(105, 'Bank Registration', 'Finance', 'Finance Setup', 4, 'bankRegistration', 'list', 1),
(106, 'Amount Calculation Type', 'Finance', 'Finance Setup', 7, 'amountCalculationType', 'list', 1),
(107, 'Frequency Mode', 'Finance', 'Finance Setup', 6, 'frequencyMode', 'list', 1),
(108, 'Payment Type', 'Finance', 'Finance Setup', 9, 'paymentType', 'list', 1),
(109, 'Fee Category', 'Finance', 'Finance Setup', 8, 'feeCategory', 'list', 1),
(110, 'Fee Setup', 'Finance', 'Fee Structure', 1, 'feeSetup', 'list', 2),
(111, 'Fee Structure', 'Finance', 'Fee Structure', 2, 'feeStructure', 'list', 2),
(112, 'Invoicing', 'pm', 'Finance', 1, 'partnerUniversityInvoice', 'list', 2),
(113, 'Payment & Receipt', 'pm', 'Finance', 2, 'receipt', 'list', 2),
(114, 'Employee Discount', 'Finance', 'Discount Setup', 2, 'employeeDiscount', 'list', 3),
(115, 'Alumni Discount', 'Finance', 'Discount Setup', 3, 'alumniDiscount', 'list', 3),
(116, 'Statement Of Accounts', 'Finance', 'Refund', 1, 'StatementOfAccount', 'list', 8),
(117, 'Invoice Generation', 'Finance', 'Invoice', 1, 'mainInvoice', 'list', 4),
(118, 'Wrong Invoice Generated', 'Invoice', 'Invoice', 2, 'mainInvoice', 'approvalList', 4),
(119, 'Partner Invoice Generation', 'Finance', NULL, NULL, NULL, 'list', NULL),
(120, 'Receipt', 'Finance', 'Receivables', 1, 'receipt', 'list', 5),
(121, 'Receipt Approval', 'Finance', 'Receivables', 2, 'receipt', 'approvalList', 5),
(122, 'Sponsor Invoice Generation', 'Finance', NULL, NULL, NULL, 'list', NULL),
(123, 'Sponsor Invoice Cancellation', 'Finance', NULL, NULL, NULL, 'list', NULL),
(124, 'Advance Payment', 'Finance', NULL, NULL, NULL, 'list', NULL),
(125, 'Credit Note Entry', 'Finance', 'Credit Note', 1, 'creditNote', 'list', 6),
(126, 'Debit Note Entry', 'Finance', 'Debit Note', 1, 'debitNote', 'list', 7),
(127, 'Refund Entry', 'Finance', NULL, NULL, NULL, 'list', NULL),
(128, 'Sponsor', 'Sponsor', NULL, NULL, NULL, 'list', NULL),
(129, 'Sponsor Has Students', 'Sponsor', NULL, NULL, NULL, 'list', NULL),
(130, 'Room Type', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(131, 'Inventory', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(132, 'Hostel Registration', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(133, 'Apartment Setup', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(134, 'Unit Setup', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(135, 'Room Setup', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(136, 'Room Allotment', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(137, 'Room Allotment Summary', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(138, 'Room Vacancy', 'Hostel', NULL, NULL, NULL, 'list', NULL),
(139, 'Equipments', 'Facility', NULL, NULL, NULL, 'list', NULL),
(140, 'Room Type', 'Facility', NULL, NULL, NULL, 'list', NULL),
(141, 'Building', 'Facility', NULL, NULL, NULL, 'list', NULL),
(142, 'Room Setup', 'Facility', NULL, NULL, NULL, 'list', NULL),
(143, 'Apply For Booking', 'Facility', NULL, NULL, NULL, 'list', NULL),
(144, 'Booking Summary', 'Facility', NULL, NULL, NULL, 'list', NULL),
(145, 'Max. Internship Limit', 'Internship', NULL, NULL, NULL, 'list', NULL),
(146, 'Company Type', 'Internship', NULL, NULL, NULL, 'list', NULL),
(147, 'Company Registration', 'Internship', NULL, NULL, NULL, 'list', NULL),
(148, 'Application', 'Internship', NULL, NULL, NULL, 'list', NULL),
(149, 'Application Approval', 'Internship', NULL, NULL, NULL, 'list', NULL),
(150, 'Reporting', 'Internship', NULL, NULL, NULL, 'list', NULL),
(151, 'Placement Form', 'Internship', NULL, NULL, NULL, 'list', NULL),
(152, 'Alumni List', 'Alumni', NULL, NULL, NULL, 'list', NULL),
(153, 'Templates', 'Communication', 'Communication', 1, 'template', 'list', 1),
(154, 'Group', 'Communication', 'Communication', 2, 'group', 'list', 1),
(155, 'Group Recepients', 'Communication', 'Communication', 3, 'group', 'recepientList', 1),
(156, 'Messaging Templates', 'Communication', 'Messaging', 1, 'templateMessage', 'list', 2),
(157, 'Messaging Group', 'Communication', 'Messaging', 2, 'groupMessage', 'list', 2),
(158, 'Group Recepients (Messaging)', 'Communication', 'Messaging', 3, 'groupMessage', 'recepientList', 2),
(159, 'IPS Topics', 'IPS', NULL, NULL, NULL, 'list', NULL),
(160, 'Research Status', 'IPS', NULL, NULL, NULL, 'list', NULL),
(161, 'Field Of Interest', 'IPS', NULL, NULL, NULL, 'list', NULL),
(162, 'Reason For Applying', 'IPS', NULL, NULL, NULL, 'list', NULL),
(163, 'Research Category', 'IPS', NULL, NULL, NULL, 'list', NULL),
(164, 'Colloquium Setup', 'IPS', NULL, NULL, NULL, 'list', NULL),
(165, 'Advisory', 'IPS', NULL, NULL, NULL, 'list', NULL),
(166, 'Readers', 'IPS', NULL, NULL, NULL, 'list', NULL),
(167, 'Proposal Defense Comitee', 'IPS', NULL, NULL, NULL, 'list', NULL),
(168, 'Chapter', 'IPS', NULL, NULL, NULL, 'list', NULL),
(169, 'Deliverables', 'IPS', NULL, NULL, NULL, 'list', NULL),
(170, 'Submitted Deliverables', 'IPS', NULL, NULL, NULL, 'list', NULL),
(171, 'Phd Duration Tagging', 'IPS', NULL, NULL, NULL, 'list', NULL),
(172, 'Stage', 'IPS', NULL, NULL, NULL, 'list', NULL),
(173, 'Mile Stone', 'IPS', NULL, NULL, NULL, 'list', NULL),
(174, 'Tag Mile Stone To Semester', 'IPS', NULL, NULL, NULL, 'list', NULL),
(175, 'Overview', 'IPS', NULL, NULL, NULL, 'list', NULL),
(176, 'Supervisor Role', 'IPS', NULL, NULL, NULL, 'list', NULL),
(177, 'Supervisor', 'IPS', NULL, NULL, NULL, 'list', NULL),
(178, 'Supervisor Tagging', 'IPS', NULL, NULL, NULL, 'list', NULL),
(179, 'Examiner Role', 'IPS', NULL, NULL, NULL, 'list', NULL),
(180, 'Examiner', 'IPS', NULL, NULL, NULL, 'list', NULL),
(181, 'Proposal', 'IPS', NULL, NULL, NULL, 'list', NULL),
(182, 'Articleship', 'IPS', NULL, NULL, NULL, 'list', NULL),
(183, 'Professional Practice Paper', 'IPS', NULL, NULL, NULL, 'list', NULL),
(184, 'Proposal Approval', 'IPS', NULL, NULL, NULL, 'list', NULL),
(185, 'Articleship Approval', 'IPS', NULL, NULL, NULL, 'list', NULL),
(186, 'PPP Approval', 'IPS', NULL, NULL, NULL, 'list', NULL),
(187, 'Student Marks Entry', 'Examination', 'Marks Entry', 1, 'studentMarksEntry', 'liststudentList', 4),
(188, 'Supervisor Change Application', 'IPS', NULL, NULL, NULL, 'list', NULL),
(189, 'Student IPS Progress', 'Examination', NULL, NULL, NULL, 'list', NULL),
(190, 'Payment Rate', 'af', 'Setup', 1, 'paymentRate', 'list', 1),
(191, 'Academic Facilitator profile', 'af', 'Setup', 2, 'staff', 'list', 1),
(192, 'Change Of Status', 'af', 'Records Management', 1, 'staff', 'changeStatusList', 2),
(193, 'Online claim', 'af', 'Records Management', 2, 'onlineClaim', 'list', 2),
(194, 'Statement of account', 'af', 'Records Management', 3, 'statementOfAccount', 'list', 2),
(195, 'Sibbling Discount', 'Finance', 'Discount Setup', 2, 'sibblingDiscount', 'list', 3),
(196, 'Change Learning Mode Application', 'Records', 'Change Learning Mode', 1, 'applyChangeLearningMode', 'list', 5),
(197, 'Change Learning Mode Approval', 'Records', 'Change Learning Mode', 2, 'applyChangeLearningMode', 'approval_list', 5),
(198, 'Schedule Publish Final Exam Result', 'Examination', 'Setup', 5, 'publishExamResult', 'list', 1),
(199, 'Student IP List', 'Examination', 'Exam Registration', 2, 'ipsProgress', 'list', 2),
(200, 'Exam Registration Slip', 'Examination', 'Exam Registration', 3, 'examAttendence', 'comingSoon', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
