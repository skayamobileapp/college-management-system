
-- AP


TRUNCATE TABLE `bill_registration`;
TRUNCATE TABLE `bill_registration_details`;
TRUNCATE TABLE `temp_bill_registration_details`;

TRUNCATE TABLE `payment_voucher`;
TRUNCATE TABLE `payment_voucher_details`;



TRUNCATE TABLE `petty_cash_allocation`;
TRUNCATE TABLE `petty_cash_entry`;
TRUNCATE TABLE `emergency_fund_allocation`;
TRUNCATE TABLE `emergency_fund_entry`;


TRUNCATE TABLE `etf_voucher`;
TRUNCATE TABLE `etf_voucher_details`;
TRUNCATE TABLE `etf_batch`;
TRUNCATE TABLE `etf_batch_details`;





-- Assets

TRUNCATE TABLE `grn`;
TRUNCATE TABLE `asset_order`;
TRUNCATE TABLE `asset_pre_registration`;
TRUNCATE TABLE `asset_registration`;
TRUNCATE TABLE `disposal_type`;
TRUNCATE TABLE `asset_disposal`;
TRUNCATE TABLE `asset_disposal_detail`;
TRUNCATE TABLE `temp_asset_disposal_detail`;


TRUNCATE Table `asset_category`;
TRUNCATE Table `asset_sub_category`;
TRUNCATE Table `asset_item`;



-- Budget



TRUNCATE TABLE `budget_amount`;
TRUNCATE TABLE `budget_allocation`;
TRUNCATE TABLE `temp_budget_allocation`;
TRUNCATE TABLE `budget_transaction`;
TRUNCATE TABLE `budget_transaction_details`;
TRUNCATE TABLE `temp_budget_transaction_details`;




-- investment

TRUNCATE TABLE `investment_type`;
TRUNCATE TABLE `investment_institution`;
TRUNCATE TABLE `investment_application`;
TRUNCATE TABLE `investment_application_details`;
TRUNCATE TABLE `temp_investment_application_details`;
TRUNCATE TABLE `investment_registration`;
TRUNCATE TABLE `investment_registration_withdraw`;




-- GLJournal








TRUNCATE TABLE `journal`;
TRUNCATE TABLE `journal_details`;
TRUNCATE TABLE `temp_journal_details`;
TRUNCATE TABLE `ledger`;
TRUNCATE TABLE `ledger_details`;





TRUNCATE TABLE `bank_registration`;
TRUNCATE TABLE `financial_year`;
TRUNCATE TABLE `budget_year`;
TRUNCATE TABLE `fund_code`;
TRUNCATE TABLE `department_code`;
TRUNCATE TABLE `activity_code`;
TRUNCATE TABLE `account_code`;
TRUNCATE TABLE `tax`;






-- PR

TRUNCATE TABLE `pr_entry`;
TRUNCATE TABLE `pr_entry_details`;
TRUNCATE TABLE `temp_pr_entry`;

TRUNCATE TABLE `po_entry`;
TRUNCATE TABLE `po_detail`;

TRUNCATE TABLE `nonpo_entry`;
TRUNCATE TABLE `nonpo_detail`;


TRUNCATE Table `tender_quotation`;
TRUNCATE Table `tender_quotation_detail`;
TRUNCATE Table `tender_commitee`;
TRUNCATE Table `temp_tender_commitee`;
TRUNCATE Table `tender_remarks`;
TRUNCATE Table `temp_tender_remarks`;



TRUNCATE Table `vendor_details`;
TRUNCATE Table `company_details`;
TRUNCATE Table `temp_bank_details`;
TRUNCATE Table `temp_billing_details`;
TRUNCATE Table `bank_details`;
TRUNCATE Table `personal_billing_details`;




TRUNCATE Table `procurement_category`;
TRUNCATE Table `procurement_sub_category`;
TRUNCATE Table `procurement_item`;
TRUNCATE Table `procurement_limit`;

TRUNCATE Table `mode_of_category`;

















-- INSERT INTO `fund_code` ( `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`) VALUES
-- ( 'Fund1', '', '01', 1, NULL, '2020-03-09 02:02:03', NULL),
-- ( 'FUND2', '', '02', 1, NULL, '2020-05-01 15:04:46', NULL),
-- ( 'FUND3', '', '03', 1, NULL, '2020-05-05 22:50:05', NULL),
-- ( 'FUND4', '', '04', 1, NULL, '2020-05-05 22:50:05', NULL),
-- ( 'FUND5', '', '05', 1, NULL, '2020-05-05 22:50:05', NULL),
-- ( 'FUND6', '', '06', 1, NULL, '2020-05-05 22:50:05', NULL)






ALTER TABLE `asset_registration` CHANGE `id_po` `id_grn` INT(20) NULL DEFAULT NULL;





ALTER TABLE `asset_pre_registration` CHANGE `id_po` `id_grn` INT(20) NULL DEFAULT NULL;




ALTER TABLE `asset_order` ADD `amount_per_item` FLOAT(20,2) NULL DEFAULT '0' AFTER `balance_qty`;




ALTER TABLE `asset_order` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `balance_qty`;





ALTER TABLE `asset_order` ADD `id_grn` INT(20) NULL DEFAULT '0' AFTER `id`;





CREATE TABLE `grn`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_financial_year` int(20) NULL DEFAULT '0',
  `id_po` int(20) NULL DEFAULT '0',
  `reference_number` varchar(50) NULL DEFAULT '',
  `date_time` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `expire_date` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `id_vendor` int(20) NULL DEFAULT '0',
  `description` varchar(500) NULL DEFAULT '',
  `id_department` int(20) NULL DEFAULT '0',
  `total_amount` float(20,2) NULL DEFAULT '0',
  `rating` varchar(20) NULL DEFAULT '',
  `id_pre_register` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `asset_disposal` ADD `no_of_assets` INT(20) NULL DEFAULT '0' AFTER `reason`;




ALTER TABLE `asset_disposal` ADD `id_staff` INT(20) NULL DEFAULT '0' AFTER `date_time`, ADD `id_department` INT(20) NULL DEFAULT '0' AFTER `id_staff`;





ALTER TABLE `asset_disposal` ADD `meeting_reference_no` VARCHAR(50) NULL AFTER `no_of_assets`, ADD `meeting_date` DATETIME NULL DEFAULT NULL AFTER `meeting_reference_no`;



ALTER TABLE `temp_journal_details` CHANGE `id_account_code` `account_code` VARCHAR(50) NULL DEFAULT '', CHANGE `id_activity_code` `activity_code` VARCHAR(50) NULL DEFAULT '', CHANGE `id_department_code` `department_code` VARCHAR(50) NULL DEFAULT '', CHANGE `id_fund_code` `fund_code` VARCHAR(50) NULL DEFAULT '';




ALTER TABLE `temp_journal_details` CHANGE `id_session` `id_session` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '';



ALTER TABLE `temp_journal_details` CHANGE `credit_amount` `credit_amount` FLOAT(20,2) NULL DEFAULT '0';


ALTER TABLE `temp_journal_details` CHANGE `updated_dt_tm` `updated_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;


ALTER TABLE `temp_journal_details` CHANGE `debit_amount` `debit_amount` FLOAT(20,2) NULL DEFAULT '0';


ALTER TABLE `journal_details` CHANGE `id_account_code` `account_code` VARCHAR(50) NULL DEFAULT '', CHANGE `id_activity_code` `activity_code` VARCHAR(50) NULL DEFAULT '', CHANGE `id_department_code` `department_code` VARCHAR(50) NULL DEFAULT '', CHANGE `id_fund_code` `fund_code` VARCHAR(50) NULL DEFAULT '';


ALTER TABLE `journal` CHANGE `total_amount` `total_amount` FLOAT(20,2) NULL DEFAULT NULL;




ALTER TABLE `journal` ADD `approved_by` INT(2) NULL DEFAULT '0' AFTER `status`;


ALTER TABLE `journal` CHANGE `updated_dt_tm` `updated_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;


ALTER TABLE `journal_details` CHANGE `updated_dt_tm` `updated_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;





CREATE TABLE `ledger` ( `id` int(20) NOT NULL AUTO_INCREMENT, `ledger_number` varchar(20) DEFAULT NULL, `date` datetime DEFAULT CURRENT_TIMESTAMP, `reason` varchar(520) DEFAULT NULL, `total_amount` float(20,2) DEFAULT NULL, `id_financial_year` int(10) DEFAULT NULL, `status` int(2) DEFAULT '1', `approved_by` int(2) DEFAULT '0', `approval_status` int(10) DEFAULT '0', `created_by` int(20) DEFAULT NULL, `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP, `updated_by` int(20) DEFAULT NULL, `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `ledger_details` ( `id` int(20) NOT NULL AUTO_INCREMENT, `id_ledger` int(20) DEFAULT NULL, `journal_type` varchar(50) DEFAULT '', `credit_amount` float(20,2) DEFAULT '0.00', `debit_amount` float(20,2) DEFAULT '0.00', `account_code` varchar(50) DEFAULT '', `activity_code` varchar(50) DEFAULT '', `department_code` varchar(50) DEFAULT '', `fund_code` varchar(50) DEFAULT '', `status` int(2) DEFAULT '1', `created_by` int(20) DEFAULT NULL, `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP, `updated_by` int(20) DEFAULT NULL, `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `ledger` ADD `id_journal` INT(20) NULL DEFAULT '0' AFTER `ledger_number`;




ALTER TABLE `activity_code` ADD `level` INT(2) NULL DEFAULT '0' AFTER `code`, ADD `id_parent` INT(20) NULL DEFAULT '0' AFTER `level`;





CREATE TABLE `budget_year` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT '',
  `from_year` varchar(20) DEFAULT '',
  `to_year` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `budget_amount` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_budget_year` int(20) DEFAULT '0',
  `id_financial_year` int(20) DEFAULT '0',
  `id_department` int(20) DEFAULT '0',
  `department_code` VARCHAR(50) NULL DEFAULT ''
  `amount` float(20,2) DEFAULT '0',
  `reason` varchar(500) DEFAULT '',
  `status` int(20) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `budget_amount` ADD `is_detail_added` INT(2) NULL DEFAULT '0' AFTER `amount`;



CREATE TABLE `budget_allocation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_budget_amount` int(20) DEFAULT NULL,
  `id_budget_year` int(20) DEFAULT NULL,
  `id_department` int(20) DEFAULT NULL,
  `fund_code` varchar(50) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `account_code` varchar(50) DEFAULT '',
  `budget_amount` float(20,2) DEFAULT '0',
  `allocated_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `used_amount` float(20,2) DEFAULT '0',
  `increment_amount` float(20,2) DEFAULT '0',
  `decrement_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `budget_allocation` ADD `id_transaction_detail` INT(20) NULL DEFAULT '0' AFTER `decrement_amount`;




ALTER TABLE `temp_budget_allocation` ADD `id_transaction_detail` INT(20) NULL DEFAULT '0' AFTER `decrement_amount`;







CREATE TABLE `temp_budget_allocation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(100) DEFAULT '',
  `id_budget_year` int(20) DEFAULT NULL,
  `id_department` int(20) DEFAULT NULL,
  `fund_code` varchar(50) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `account_code` varchar(50) DEFAULT '',
  `budget_amount` float(20,2) DEFAULT '0',
  `allocated_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `used_amount` float(20,2) DEFAULT '0',
  `increment_amount` float(20,2) DEFAULT '0',
  `decrement_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




-- OLD


CREATE TABLE `budget_transaction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_credit_budget` int(20) DEFAULT '0',
  `id_debit_budget` int(20) DEFAULT '0',
  `total_amount` float(20,2) DEFAULT '0',
  `reason` varchar(500) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `budget_transaction_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_budget_transaction` int(20) DEFAULT '0',
  `id_budget_allocation` int(20) DEFAULT '0',
  `amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





DROP TABLE `budget_transaction`;

DROP TABLE `budget_transaction_details`;



-- New




CREATE TABLE `budget_transaction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_budget_allocation` int(20) DEFAULT '0',
  `id_budget_amount` int(20) DEFAULT '0',
  `id_budget_year` int(20) DEFAULT '0',
  `id_financial_year` int(20) DEFAULT '0',
  `department_code` varchar(50) DEFAULT '',
  `amount` float(20,2) DEFAULT '0',
  `reason` varchar(500) DEFAULT '',
  `status` int(20) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `budget_amount` ADD `increment_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount`, ADD `decrement_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `increment_amount`;
ALTER TABLE `budget_amount` ADD `balance_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount`, ADD `decrement_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `increment_amount`;







CREATE TABLE `budget_transaction_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_budget_transaction` int(20) DEFAULT NULL,
  `id_budget_year` int(20) DEFAULT NULL,
  `department_code` varchar(50) DEFAULT '',
  `fund_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `account_code` varchar(50) DEFAULT '',
  `budget_amount` float(20,2) DEFAULT '0',
  `allocated_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `used_amount` float(20,2) DEFAULT '0',
  `increment_amount` float(20,2) DEFAULT '0',
  `decrement_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_budget_transaction_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(100) DEFAULT '',
  `id_budget_year` int(20) DEFAULT NULL,
  `department_code` varchar(50) DEFAULT '',
  `fund_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `account_code` varchar(50) DEFAULT '',
  `budget_amount` float(20,2) DEFAULT '0',
  `allocated_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `used_amount` float(20,2) DEFAULT '0',
  `increment_amount` float(20,2) DEFAULT '0',
  `decrement_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `budget_allocation` ADD `pending_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `decrement_amount`;


ALTER TABLE `temp_budget_allocation` ADD `pending_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `decrement_amount`;








ALTER TABLE `pr_entry` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`, ADD `department_code` VARCHAR(50) NULL DEFAULT '' AFTER `id_budget_year`;



ALTER TABLE `pr_entry_details` ADD `id_budget_allocation` INT(20) NULL DEFAULT '0' AFTER `type`;


ALTER TABLE `temp_pr_entry` ADD `id_budget_allocation` INT(20) NULL DEFAULT '0' AFTER `type`;





ALTER TABLE `po_entry` ADD `department_code` VARCHAR(50) NULL DEFAULT '' AFTER `id_department`;


ALTER TABLE `nonpo_entry` ADD `department_code` VARCHAR(50) NULL DEFAULT '' AFTER `id_department`;



ALTER TABLE `tender_quotation` ADD `department_code` VARCHAR(50) NULL DEFAULT '' AFTER `id_department`;




ALTER TABLE `po_entry` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;



ALTER TABLE `nonpo_entry` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;



ALTER TABLE `tender_quotation` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;





ALTER TABLE `budget_amount` ADD `allocation_status` INT(2) NULL DEFAULT '0' AFTER `status`;




ALTER TABLE `budget_amount` ADD `allocation_reason` VARCHAR(500) NULL DEFAULT '' AFTER `allocation_status`;






ALTER TABLE `grn` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;





ALTER TABLE `grn` ADD `department_code` VARCHAR(50) NULL DEFAULT '' AFTER `id_department`;






ALTER TABLE `asset_disposal` ADD `department_code` VARCHAR(50) NULL DEFAULT '' AFTER `id_department`;




ALTER TABLE `grn` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `status`;



ALTER TABLE `ledger` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;




ALTER TABLE `journal` ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;




ALTER TABLE `grn` ADD `is_bill_registered` INT(20) NULL DEFAULT '0' AFTER `id_pre_register`;




ALTER TABLE `grn` ADD `ap_status` varchar(20) NULL DEFAULT '0' AFTER `is_bill_registered`;




Alter Table asset_order Add `cr_account` varchar(20) DEFAULT '' AFTER id_item;
Alter Table asset_order Add  `cr_activity` varchar(20) DEFAULT '' AFTER cr_account;
 Alter Table asset_order Add `cr_department` varchar(20) DEFAULT '' AFTER cr_activity;
 Alter Table asset_order Add `cr_fund` varchar(20) DEFAULT '' AFTER cr_department; 
 Alter Table asset_order Add `dt_account` varchar(20) DEFAULT '' AFTER cr_account;
 Alter Table asset_order Add `dt_activity` varchar(20) DEFAULT '' AFTER dt_account;
 Alter Table asset_order Add `dt_department` varchar(20) DEFAULT '' AFTER dt_activity;
 Alter Table asset_order Add `dt_fund` varchar(20) DEFAULT '' AFTER dt_department;











 CREATE TABLE `emergency_fund_allocation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(500) DEFAULT '',
  `id_financial_year` int(20) DEFAULT '0',
  `id_budget_year` int(20) DEFAULT NULL,
  `fund_code` varchar(50) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `account_code` varchar(50) DEFAULT '',
  `contribution_amount` float(20,2) DEFAULT '0',
  `used_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `id_staff` int(20) DEFAULT '0',
  `id_student` int(20) DEFAULT '0',
  `description` varchar(500) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;












  CREATE TABLE `emergency_fund_entry` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_emergency_fund_allocation` int(20) DEFAULT '0',
  `reference_number` varchar(500) DEFAULT '',
  `id_staff` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_budget_year` int(20) DEFAULT NULL,
  `id_financial_year` int(20) DEFAULT '0',
  `fund_code` varchar(50) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `account_code` varchar(50) DEFAULT '',
  `requested_amount` float(20,2) DEFAULT '0',
  `type` varchar(50) DEFAULT '',
  `ap_status` varchar(20) NULL DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `emergency_fund_allocation` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `description`;






ALTER TABLE `emergency_fund_entry` ADD `description` VARCHAR(500) NULL DEFAULT '' AFTER `requested_amount`;





ALTER TABLE `emergency_fund_entry` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `requested_amount`;






 CREATE TABLE `petty_cash_allocation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `reference_number` varchar(50) DEFAULT '',
  `id_financial_year` int(20) DEFAULT '0',
  `id_budget_year` int(20) DEFAULT '0',
  `payment_mode` varchar(100) DEFAULT '',
  `min_amount` float(20,2) DEFAULT '0',
  `max_amount` float(20,2) DEFAULT '0',
  `min_reimbursement_amount` float(20,2) DEFAULT '0',
  `max_reimbursement_amount` float(20,2) DEFAULT '0',
  `min_reimbursement_percentage` float(20,2) DEFAULT '0',
  `max_reimbursement_percentage` float(20,2) DEFAULT '0',
  `amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;








  CREATE TABLE `petty_cash_entry` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_petty_cash_allocation` int(20) DEFAULT '0',
  `reference_number` varchar(500) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `transaction_type` varchar(100) DEFAULT '',
  `id_budget_year` int(20) DEFAULT '0',
  `id_financial_year` int(20) DEFAULT '0',
  `paid_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `reimbursement` int(20) DEFAULT NULL,
  `ap_status` varchar(20) NULL DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `petty_cash_entry` ADD `id_staff` INT(20) NULL DEFAULT '0' AFTER `id_petty_cash_allocation`;








ALTER TABLE petty_cash_allocation add `account_code` varchar(50) NULL DEFAULT '' After amount;
ALTER TABLE petty_cash_allocation add   `activity_code` varchar(50) NULL DEFAULT '' AFTER account_code;
ALTER TABLE petty_cash_allocation add   `department_code` varchar(50) NULL DEFAULT '' AFTER activity_code;
ALTER TABLE petty_cash_allocation add   `fund_code` varchar(50) NULL DEFAULT '' AFTER department_code; 







ALTER TABLE petty_cash_entry add `account_code` varchar(50) NULL DEFAULT '' After description;
ALTER TABLE petty_cash_entry add   `activity_code` varchar(50) NULL DEFAULT '' AFTER account_code;
ALTER TABLE petty_cash_entry add   `fund_code` varchar(50) NULL DEFAULT '' AFTER department_code; 




ALTER TABLE `petty_cash_allocation` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `amount`;




ALTER TABLE `petty_cash_allocation` ADD `used_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount`, ADD `balance_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `used_amount`;




ALTER TABLE `petty_cash_entry` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `ap_status`;




ALTER TABLE `petty_cash_entry` ADD `is_bill_registered` INT(20) NULL DEFAULT '0' AFTER `reimbursement`;




ALTER TABLE `emergency_fund_entry` ADD `is_bill_registered` INT(20) NULL DEFAULT '0' AFTER `type`;











CREATE TABLE `bill_registration`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
 `reference_number` varchar(50) NULL DEFAULT '',
 `description` varchar(500) NULL DEFAULT '',
 `type` varchar(50) NULL DEFAULT '',
 `date_time` DATETIME  DEFAULT CURRENT_TIMESTAMP,
 `invoice_date` DATETIME  DEFAULT CURRENT_TIMESTAMP,
 `invoice_rec_date` DATETIME  DEFAULT CURRENT_TIMESTAMP,
 `contact_person_one` varchar(500) NULL DEFAULT '',
 `contact_person_two` varchar(500) NULL DEFAULT '',
 `contact_person_three` varchar(500) NULL DEFAULT '',
 `email` varchar(500) NULL DEFAULT '',
 `id_vendor` int(20) NULL DEFAULT '0',
 `id_constomer` int(20) NULL DEFAULT '0',
 `bank_acc_no` varchar(50) NULL DEFAULT '0',
 `total_amount` float(20,2) NULL DEFAULT '0',
 `paid_amount` float(20,2) NULL DEFAULT '0',
 `balance_amount` float(20,2) NULL DEFAULT '0',
  `is_payment_voucher` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `bill_registration_details`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_bill_registration` int(20) NULL DEFAULT '0',
 `type` varchar(50) NULL DEFAULT '',
  `id_tax_code` int(20) NULL DEFAULT '0',
  `tax_amount` float(20,2) NULL DEFAULT '0',
  `total_inc_tax` float(20,2) NULL DEFAULT '0',
  `date_time` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `quantity` int(20) NULL DEFAULT '0',
  `unit` int(20) NULL DEFAULT '0',
  `price` float(20,2) NULL DEFAULT '0',
  `amount` float(20,2) NULL DEFAULT '0',
  `percentage` float(20,2) NULL DEFAULT '0',
  `account_code` varchar(50) NULL DEFAULT '',
  `activity_code` varchar(50) NULL DEFAULT '',
  `department_code` varchar(50) NULL DEFAULT '',
  `fund_code` varchar(50) NULL DEFAULT '',
  `budget_account_code` varchar(50) NULL DEFAULT '',
  `budget_activity_code` varchar(50) NULL DEFAULT '',
  `budget_department_code` varchar(50) NULL DEFAULT '',
  `budget_fund_code` varchar(50) NULL DEFAULT '',
  `description` varchar(500) NULL DEFAULT '',
  `is_payment_voucher` int(20) NULL DEFAULT '0',
  `id_referal` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `temp_bill_registration_details`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(200) NULL DEFAULT '',
 `type` varchar(50) NULL DEFAULT '',
  `id_tax_code` int(20) NULL DEFAULT '0',
  `tax_amount` float(20,2) NULL DEFAULT '0',
  `total_inc_tax` float(20,2) NULL DEFAULT '0',
  `date_time` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `quantity` int(20) NULL DEFAULT '0',
  `unit` int(20) NULL DEFAULT '0',
  `price` float(20,2) NULL DEFAULT '0',
  `amount` float(20,2) NULL DEFAULT '0',
  `percentage` float(20,2) NULL DEFAULT '0',
  `account_code` varchar(50) NULL DEFAULT '',
  `activity_code` varchar(50) NULL DEFAULT '',
  `department_code` varchar(50) NULL DEFAULT '',
  `fund_code` varchar(50) NULL DEFAULT '',
  `budget_account_code` varchar(50) NULL DEFAULT '',
  `budget_activity_code` varchar(50) NULL DEFAULT '',
  `budget_department_code` varchar(50) NULL DEFAULT '',
  `budget_fund_code` varchar(50) NULL DEFAULT '',
  `description` varchar(500) NULL DEFAULT '',
  `is_payment_voucher` int(20) NULL DEFAULT '0',
  `id_referal` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;











ALTER TABLE `bill_registration` ADD `id_from` INT(20) NULL DEFAULT '0' AFTER `type`;




ALTER TABLE `bill_registration` ADD `id_bank` INT(20) NULL DEFAULT '0' AFTER `id_vendor`;




ALTER TABLE `bill_registration` DROP `id_constomer`;










 Alter Table bill_registration_details Add `cr_account` varchar(20) DEFAULT '' AFTER id_referal;
Alter Table bill_registration_details Add  `cr_activity` varchar(20) DEFAULT '' AFTER cr_account;
 Alter Table bill_registration_details Add `cr_department` varchar(20) DEFAULT '' AFTER cr_activity;
 Alter Table bill_registration_details Add `cr_fund` varchar(20) DEFAULT '' AFTER cr_department; 
 Alter Table bill_registration_details Add `dt_account` varchar(20) DEFAULT '' AFTER cr_account;
 Alter Table bill_registration_details Add `dt_activity` varchar(20) DEFAULT '' AFTER dt_account;
 Alter Table bill_registration_details Add `dt_department` varchar(20) DEFAULT '' AFTER dt_activity;
 Alter Table bill_registration_details Add `dt_fund` varchar(20) DEFAULT '' AFTER dt_department;





 ALTER TABLE `bill_registration` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `status`;




 ALTER TABLE `bill_registration` ADD `customer_name` VARCHAR(500) NULL DEFAULT '' AFTER `type`;



ALTER TABLE `temp_bill_registration_details` ADD `tax_price` FLOAT(20,2) NULL DEFAULT '0' AFTER `tax_amount`;


ALTER TABLE `bill_registration_details` ADD `tax_price` FLOAT(20,2) NULL DEFAULT '0' AFTER `tax_amount`;









 Alter Table temp_bill_registration_details Add `cr_account` varchar(20) DEFAULT '' AFTER type;
Alter Table temp_bill_registration_details Add  `cr_activity` varchar(20) DEFAULT '' AFTER cr_account;
 Alter Table temp_bill_registration_details Add `cr_department` varchar(20) DEFAULT '' AFTER cr_activity;
 Alter Table temp_bill_registration_details Add `cr_fund` varchar(20) DEFAULT '' AFTER cr_department; 
 Alter Table temp_bill_registration_details Add `dt_account` varchar(20) DEFAULT '' AFTER cr_account;
 Alter Table temp_bill_registration_details Add `dt_activity` varchar(20) DEFAULT '' AFTER dt_account;
 Alter Table temp_bill_registration_details Add `dt_department` varchar(20) DEFAULT '' AFTER dt_activity;
 Alter Table temp_bill_registration_details Add `dt_fund` varchar(20) DEFAULT '' AFTER dt_department;



ALTER TABLE `temp_bill_registration_details` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `cr_fund`, ADD `id_sub_category` INT(20) NULL DEFAULT '0' AFTER `id_category`, ADD `id_item` INT(20) NULL DEFAULT '0' AFTER `id_sub_category`;





ALTER TABLE `bill_registration_details` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `cr_fund`, ADD `id_sub_category` INT(20) NULL DEFAULT '0' AFTER `id_category`, ADD `id_item` INT(20) NULL DEFAULT '0' AFTER `id_sub_category`;




ALTER TABLE `temp_bill_registration_details` ADD `id_tax` INT(20) NULL DEFAULT '0' AFTER `id_tax_code`;




ALTER TABLE `bill_registration_details` ADD `id_tax` INT(20) NULL DEFAULT '0' AFTER `id_tax_code`;





ALTER TABLE `temp_bill_registration_details` ADD `total_final` FLOAT(20,2) NULL DEFAULT '0' AFTER `tax_price`;





ALTER TABLE `bill_registration_details` ADD `total_final` FLOAT(20,2) NULL DEFAULT '0' AFTER `tax_price`;






ALTER TABLE `temp_bill_registration_details` ADD `id_budget_allocation` int(20) NULL DEFAULT '0' AFTER `tax_price`;





ALTER TABLE `bill_registration_details` ADD `id_budget_allocation` int(20) NULL DEFAULT '0' AFTER `tax_price`;




ALTER TABLE `bill_registration` ADD `id_financial_year` INT(20) NULL DEFAULT '0' AFTER `reference_number`;




ALTER TABLE `bill_registration`  ADD `id_budget_year` INT(20) NULL DEFAULT '0'  AFTER `id_financial_year`;




ALTER TABLE `bill_registration` ADD `department_code` VARCHAR(50) NULL DEFAULT '0' AFTER `id_budget_year`;






ALTER TABLE `bill_registration` ADD `id_staff` INT(20) NULL DEFAULT '0' AFTER `id_bank`;




ALTER TABLE `po_detail` CHANGE `id_tax` `id_tax` INT(20) NULL DEFAULT NULL;


ALTER TABLE `asset_order` ADD `price_per_item_wot_tax` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_item`, ADD `id_tax` INT(20) NULL DEFAULT '0' AFTER `price_per_item_wot_tax`;




ALTER TABLE `asset_order` ADD `tax_per_each_item` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_tax`;




ALTER TABLE `bill_registration` ADD `ef_for` VARCHAR(50) NULL DEFAULT '0' AFTER `type`, ADD `ef_for_id` INT(20) NULL DEFAULT '0' AFTER `ef_for`;




CREATE TABLE `payment_voucher`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
 `type` varchar(50) NULL DEFAULT '',
 `reference_number` varchar(50) NULL DEFAULT '',
 `payment_mode` varchar(50) NULL DEFAULT '',
 `description` varchar(500) NULL DEFAULT '',
 `payment_reference_number` varchar(500) NULL DEFAULT '',
 `payment_date` DATETIME  DEFAULT CURRENT_TIMESTAMP,
 `id_bank` int(20) NULL DEFAULT '0',
 `account_number` varchar(50) NULL DEFAULT '',
 `total_amount` float(20,2) NULL DEFAULT '0',
`is_sab` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `payment_voucher` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `is_sab`;





ALTER TABLE `payment_voucher` ADD `entry` VARCHAR(20) NULL DEFAULT '' AFTER `type`;




ALTER TABLE `payment_voucher` ADD `payee_name` VARCHAR(500) NULL DEFAULT '' AFTER `description`;





ALTER TABLE `investment_registration` ADD `is_payment_voucher` INT(20) NULL DEFAULT '0' AFTER `investment_status`, ADD `ap_status` VARCHAR(20) NULL DEFAULT '0' AFTER `is_payment_voucher`;




ALTER TABLE `bill_registration` ADD `ap_status` VARCHAR(20) NULL DEFAULT '0' AFTER `is_payment_voucher`;






CREATE TABLE `payment_voucher_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_payment_voucher` int(20) DEFAULT NULL,
  `cr_account` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_fund` varchar(50) DEFAULT '',
  `dt_account` varchar(50) DEFAULT '',
  `dt_activity` varchar(50) DEFAULT '',
  `dt_department` varchar(50) DEFAULT '',
  `dt_fund` varchar(50) DEFAULT '',
  `type` varchar(20) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `id_referal` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `etf_voucher`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
 `batch_id` varchar(50) NULL DEFAULT '',
 `id_bank` int(20) NULL DEFAULT '0',
 `reference_number` varchar(50) NULL DEFAULT '',
 `payment_mode` varchar(50) NULL DEFAULT '',
 `description` varchar(500) NULL DEFAULT '',
 `reason` varchar(500) NULL DEFAULT '',
 `letter_reference_number` varchar(50) NULL DEFAULT '',
 `payment_date` DATETIME  DEFAULT CURRENT_TIMESTAMP,
 `total_amount` float(20,2) NULL DEFAULT '0',
`is_sab_group` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `etf_voucher_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_etf_voucher` int(20) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `id_referal` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;









CREATE TABLE `etf_batch`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
 `batch_id` varchar(50) NULL DEFAULT '',
 `reference_number` varchar(50) NULL DEFAULT '',
 `description` varchar(500) NULL DEFAULT '',
 `bank_reference_number` varchar(50) NULL DEFAULT '',
 `total_amount` float(20,2) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `etf_batch_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_etf_batch` int(20) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `id_referal` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `payment_voucher` ADD `id_staff` INT(20) NULL DEFAULT '0' AFTER `id_bank`;







ALTER TABLE `payment_voucher` ADD `id_financial_year` INT(20) NULL DEFAULT '0' AFTER `reference_number`, ADD `id_budget_year` INT(20) NULL DEFAULT '0' AFTER `id_financial_year`;





ALTER TABLE `petty_cash_entry` ADD `is_payment_voucher` INT(20) NULL DEFAULT '0' AFTER `is_bill_registered`;



ALTER TABLE `payment_voucher_details` ADD `journal_type` VARCHAR(50) NULL DEFAULT 'Debit' AFTER `type`;



ALTER TABLE `asset_pre_registration` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `depriciation_value`;




ALTER TABLE `journal` ADD `description` VARCHAR(500) NULL DEFAULT '' AFTER `date`;





ALTER TABLE `ledger` ADD `description` VARCHAR(500) NULL DEFAULT '' AFTER `date`;




ALTER TABLE `bill_registration_details`
  DROP `account_code`,
  DROP `activity_code`,
  DROP `department_code`,
  DROP `fund_code`,
  DROP `budget_account_code`,
  DROP `budget_activity_code`,
  DROP `budget_department_code`,
  DROP `budget_fund_code`;





  ALTER TABLE `temp_bill_registration_details`
  DROP `account_code`,
  DROP `activity_code`,
  DROP `department_code`,
  DROP `fund_code`,
  DROP `budget_account_code`,
  DROP `budget_activity_code`,
  DROP `budget_department_code`,
  DROP `budget_fund_code`;




ALTER TABLE `vendor_details` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `vendor_status`;
  
  

ALTER TABLE `vendor_details` ADD `expire_date` DATETIME NULL AFTER `date_of_birth`;



ALTER TABLE `personal_billing_details` CHANGE `updated_dt_tm` `updated_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;



ALTER TABLE `activity_code` ADD `short_code` VARCHAR(50) NULL DEFAULT '' AFTER `name_optional_language`;




ALTER TABLE `account_code` ADD `short_code` VARCHAR(50) NULL DEFAULT '' AFTER `name_optional_language`;


------ Update FIMS From Here ------
