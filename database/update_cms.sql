-- Records

TRUNCATE TABLE `course_registration`;
TRUNCATE TABLE `exam_registration`;
TRUNCATE TABLE `bulk_withdraw`;
TRUNCATE TABLE `exam_center`;



-- Finance


TRUNCATE TABLE `main_invoice`;
TRUNCATE TABLE `main_invoice_details`;
TRUNCATE TABLE `temp_main_invoice_details`;
TRUNCATE TABLE `receipt`;
TRUNCATE TABLE `receipt_details`;
TRUNCATE TABLE `receipt_paid_details`;
TRUNCATE TABLE `temp_receipt_details`;
TRUNCATE TABLE `add_temp_receipt_paid_amount`;





TRUNCATE TABLE `apply_change_programme`;
TRUNCATE TABLE `intake_has_programme`;
TRUNCATE TABLE `courses_from_programme_landscape`;
TRUNCATE TABLE `programme`;
TRUNCATE TABLE `programme_has_course`;
TRUNCATE TABLE `programme_has_dean`;
TRUNCATE TABLE `programme_has_fee_structure`;
TRUNCATE TABLE `programme_landscape`;
TRUNCATE TABLE `temp_intake_has_programme`;
TRUNCATE TABLE `temp_programme_has_course`;
TRUNCATE TABLE `temp_programme_has_dean`;
TRUNCATE TABLE `fee_structure`;
TRUNCATE TABLE `fee_structure_details`;
TRUNCATE TABLE `programme_has_fee_structure`;
TRUNCATE TABLE `temp_fee_structure_details`;
TRUNCATE TABLE `fee_category`;
TRUNCATE TABLE `fee_setup`;
TRUNCATE TABLE `intake`;


TRUNCATE TABLE `add_course_to_program_landscape`;
TRUNCATE TABLE `course`;
TRUNCATE TABLE `courses_from_programme_landscape`;
TRUNCATE TABLE `course_grade`;
TRUNCATE TABLE `course_registration`;
TRUNCATE TABLE `course_requisite`;
TRUNCATE TABLE `course_withdraw`;
TRUNCATE TABLE `equivalent_course`;
TRUNCATE TABLE `equivalent_course_details`;
TRUNCATE TABLE `staff_has_course`;
TRUNCATE TABLE `temp_equivalent_course_details`;
TRUNCATE TABLE `temp_staff_has_course`;
TRUNCATE TABLE `staff`;




TRUNCATE TABLE `applicant`;
TRUNCATE TABLE `sibbling_discount`;
TRUNCATE TABLE `applicant_has_sibbling_discount`;
TRUNCATE TABLE `applicant_has_employee_discount`;
TRUNCATE TABLE `employee_discount`;
-- TRUNCATE TABLE `assign_student_to_exam_center`;
TRUNCATE TABLE `sponser_has_students`;
TRUNCATE TABLE `student`;
TRUNCATE TABLE `student_semester_course_details`;
TRUNCATE TABLE `student_semester_result`;





-- TRUNCATE TABLE `exam_center_to_semester`;

TRUNCATE TABLE `semester`;
TRUNCATE TABLE `semester_has_activity`;
TRUNCATE TABLE `scheme`;





TRUNCATE TABLE `award`;
TRUNCATE TABLE `award_level`;
TRUNCATE TABLE `activity_code`;
TRUNCATE TABLE `activity_details`;
TRUNCATE TABLE `bank_registration`;
TRUNCATE TABLE `late_registration`;
TRUNCATE TABLE `unit`;
TRUNCATE TABLE `department`;
TRUNCATE TABLE `barring`;
TRUNCATE TABLE `barring_type`;
TRUNCATE TABLE `apply_change_status`;
TRUNCATE TABLE `change_status`;
TRUNCATE TABLE `convocation`;
TRUNCATE TABLE `amount_calculation_type`;
TRUNCATE TABLE `add_temp_receipt_paid_amount`;
TRUNCATE TABLE `frequency_mode`;
TRUNCATE TABLE `payment_type`;
TRUNCATE TABLE `credit_note`;
TRUNCATE TABLE `credit_note_details`;
TRUNCATE TABLE `sponser_credit_note`;
TRUNCATE TABLE `sponser_credit_note_details`;
TRUNCATE TABLE `temp_credit_note_details`;
TRUNCATE TABLE `temp_sponser_credit_note_details`;
TRUNCATE TABLE `sponser`;
TRUNCATE TABLE `sponser_main_invoice`;
TRUNCATE TABLE `sponser_main_invoice_details`;
TRUNCATE TABLE `temp_sponser_main_invoice_details`;
TRUNCATE TABLE `sponser_receipt`;
TRUNCATE TABLE `sponser_receipt_details`;
TRUNCATE TABLE `sponser_receipt_paid_details`;
TRUNCATE TABLE `temp_sponser_receipt_details`;





TRUNCATE TABLE `academic_year`;
-- TRUNCATE TABLE `company_details`;
TRUNCATE TABLE `class_details`;


TRUNCATE TABLE `grade`;


-- Complete 

TRUNCATE TABLE `applicant`;
TRUNCATE TABLE `sibbling_discount`;
TRUNCATE TABLE `applicant_has_sibbling_discount`;
TRUNCATE TABLE `applicant_has_employee_discount`;
TRUNCATE TABLE `employee_discount`;
TRUNCATE TABLE `assign_student_to_exam_center`;
TRUNCATE TABLE `sponser_has_students`;
TRUNCATE TABLE `student`;
TRUNCATE TABLE `student_semester_course_details`;
TRUNCATE TABLE `student_semester_result`;
TRUNCATE TABLE `exam_center_to_semester`;
TRUNCATE TABLE `semester`;
TRUNCATE TABLE `semester_has_activity`;
TRUNCATE TABLE `scheme`;
TRUNCATE TABLE `award`;
TRUNCATE TABLE `award_level`;
TRUNCATE TABLE `activity_code`;
TRUNCATE TABLE `activity_details`;
TRUNCATE TABLE `bank_registration`;
TRUNCATE TABLE `late_registration`;
TRUNCATE TABLE `unit`;
TRUNCATE TABLE `department`;
TRUNCATE TABLE `barring`;
TRUNCATE TABLE `barring_type`;
TRUNCATE TABLE `apply_change_status`;
TRUNCATE TABLE `change_status`;
TRUNCATE TABLE `convocation`;
TRUNCATE TABLE `amount_calculation_type`;
TRUNCATE TABLE `add_temp_receipt_paid_amount`;
TRUNCATE TABLE `frequency_mode`;
TRUNCATE TABLE `payment_type`;
TRUNCATE TABLE `credit_note`;
TRUNCATE TABLE `credit_note_details`;
TRUNCATE TABLE `sponser_credit_note`;
TRUNCATE TABLE `sponser_credit_note_details`;
TRUNCATE TABLE `temp_credit_note_details`;
TRUNCATE TABLE `temp_sponser_credit_note_details`;
TRUNCATE TABLE `sponser`;
TRUNCATE TABLE `sponser_main_invoice`;
TRUNCATE TABLE `sponser_main_invoice_details`;
TRUNCATE TABLE `temp_sponser_main_invoice_details`;
TRUNCATE TABLE `sponser_receipt`;
TRUNCATE TABLE `sponser_receipt_details`;
TRUNCATE TABLE `sponser_receipt_paid_details`;
TRUNCATE TABLE `temp_sponser_receipt_details`;
TRUNCATE TABLE `academic_year`;
TRUNCATE TABLE `class_details`;
TRUNCATE TABLE `grade`;
TRUNCATE TABLE `add_course_to_program_landscape`;




TRUNCATE TABLE `course_grade`;
TRUNCATE TABLE `course_registration`;
TRUNCATE TABLE `course_requisite`;
TRUNCATE TABLE `course_withdraw`;
TRUNCATE TABLE `equivalent_course`;
TRUNCATE TABLE `staff_has_course`;

TRUNCATE TABLE `grade`;
TRUNCATE TABLE `grade`;



TRUNCATE TABLE `country`;


ALTER TABLE `applicant_has_sibbling_discount` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `sibbling_status`;


ALTER TABLE `applicant_has_employee_discount` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `employee_status`;



ALTER TABLE `applicant_has_employee_discount` ADD `rejected_by` INT(20) NULL DEFAULT '0' AFTER `status`;



ALTER TABLE `applicant_has_sibbling_discount` ADD `rejected_by` INT(20) NULL DEFAULT '0' AFTER `status`;



ALTER TABLE `applicant` ADD `approved_by` INT(20) NULL DEFAULT '0' AFTER `status`;


ALTER TABLE `applicant_has_employee_discount` ADD `rejected_on` DATETIME NULL DEFAULT NULL AFTER `rejected_by`;



ALTER TABLE `applicant_has_sibbling_discount` ADD `rejected_on` DATETIME NULL DEFAULT NULL AFTER `rejected_by`;


ALTER TABLE `activity_code` CHANGE `status` `status` INT(2) NULL DEFAULT '1';




ALTER TABLE `activity_code` ADD `level` INT(2) NULL DEFAULT '0' AFTER `code`, ADD `id_parent` INT(20) NULL DEFAULT '0' AFTER `level`;




ALTER TABLE `account_code` ADD `level` INT(2) NULL DEFAULT '0' AFTER `code`, ADD `id_parent` INT(20) NULL DEFAULT '0' AFTER `level`;




ALTER TABLE `bank_registration` ADD `cr_fund` VARCHAR(50) NULL DEFAULT '' AFTER `zipcode`, ADD `cr_department` VARCHAR(50) NULL DEFAULT '' AFTER `cr_fund`, ADD `cr_activity` VARCHAR(50) NULL DEFAULT '' AFTER `cr_department`, ADD `cr_account` VARCHAR(50) NULL DEFAULT '' AFTER `cr_activity`;





ALTER TABLE `student` ADD `passport_number` VARCHAR(50) NULL DEFAULT '' AFTER `applicant_status`;


ALTER TABLE `student` ADD `id_type` VARCHAR(50) NULL DEFAULT '' AFTER `password`;





CREATE TABLE `organisation`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NULL DEFAULT '',
  `short_name` varchar(500) NULL DEFAULT '',
  `name_in_malay` varchar(500) NULL DEFAULT '',
  `url` varchar(500) NULL DEFAULT '',
  `id_country` int(20) NULL DEFAULT '0',
  `id_registrar` int(20) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `organisation` ADD `date_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `id_registrar`;





ALTER TABLE `organisation` ADD `contact_number` INT(20) NULL DEFAULT '0' AFTER `date_time`, ADD `permanent_address` VARCHAR(980) NULL DEFAULT '' AFTER `contact_number`, ADD `correspondence_address` VARCHAR(980) NULL DEFAULT '' AFTER `permanent_address`;



ALTER TABLE `organisation` ADD `email` VARCHAR(200) NULL DEFAULT '' AFTER `contact_number`;


INSERT INTO `organisation` (`id`, `name`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `permanent_address`, `correspondence_address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'CMS', 'CMS', 'CMS', 'camsedu.com', '1', '1', current_timestamp(), '0', 'camsedu.edu.my', 'Malaysia', 'Malaysia', '1', '1', current_timestamp(), NULL, current_timestamp());




CREATE TABLE `partner_university`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NULL DEFAULT '',
  `short_name` varchar(500) NULL DEFAULT '',
  `name_in_malay` varchar(500) NULL DEFAULT '',
  `url` varchar(500) NULL DEFAULT '',
  `id_country` int(20) NULL DEFAULT '0',
  `id_registrar` int(20) NULL DEFAULT '0',
  `date_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `contact_number` INT(20) NULL DEFAULT '0',
  `email` VARCHAR(200) NULL DEFAULT '',
  `permanent_address` VARCHAR(980) NULL DEFAULT '',
  `correspondence_address` VARCHAR(980) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




ALTER TABLE `main_invoice_discount_details` ADD `id_student` INT(20) NULL DEFAULT '0' AFTER `amount`;



ALTER TABLE `student_semester_result` ADD `reason` VARCHAR(500) NULL DEFAULT '' AFTER `status`;





ALTER TABLE `course_registration` CHANGE `created_by` `created_by` INT(20) NULL DEFAULT '0';



CREATE TABLE `internship_company_type`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NULL DEFAULT '',
  `short_name` varchar(500) NULL DEFAULT '',
  `name_in_malay` varchar(500) NULL DEFAULT '',
  `code` varchar(500) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `internship_company_registration`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NULL DEFAULT '',
  `registration_no` varchar(50) NULL DEFAULT '',
  `person_in_charge` varchar(500) NULL DEFAULT '',
  `email` varchar(500) NULL DEFAULT '',
  `address` varchar(500) NULL DEFAULT '',
  `contact_number` bigint(20) NULL DEFAULT 0,
  `id_company_type` bigint(20) NULL DEFAULT 0,
  `id_country` bigint(20) NULL DEFAULT 0,
  `id_state` bigint(20) NULL DEFAULT 0,
  `city` varchar(500) NULL DEFAULT '',
  `zipcode` bigint(20) NULL DEFAULT 0,
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `internship_student_limit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `max_limit` bigint(20) NULL DEFAULT 0,
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `internship_student_limit` (`id`, `max_limit`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '3', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);






CREATE TABLE `internship_application`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `application_number` VARCHAR(50) NULL DEFAULT '',
  `id_company_type` bigint(20) NULL DEFAULT 0,
  `id_company` bigint(20) NULL DEFAULT 0,
  `id_student` bigint(20) NULL DEFAULT 0,
  `id_intake` bigint(20) NULL DEFAULT 0,
  `id_program` bigint(20) NULL DEFAULT 0,
  `description` varchar(500) NULL DEFAULT '',
  `from_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `to_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `duration` varchar(50) NULL DEFAULT '',
  `reason` varchar(50) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_internship_company_has_program`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(200) NULL DEFAULT '',
  `id_company_type` bigint(20) NULL DEFAULT 0,
  `id_intake` bigint(20) NULL DEFAULT 0,
  `id_program` bigint(20) NULL DEFAULT 0,
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `internship_company_has_program`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_company` bigint(20) NULL DEFAULT 0,
  `id_company_type` bigint(20) NULL DEFAULT 0,
  `id_intake` bigint(20) NULL DEFAULT 0,
  `id_program` bigint(20) NULL DEFAULT 0,
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `applicant_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE `applicant` ADD `email_verified` INT(2) NULL DEFAULT '0' AFTER `is_employee_discount`,
 ADD `is_updated` INT(2) NULL DEFAULT '0' AFTER `email_verified`;




-- Not Using This Table

CREATE TABLE `applicant_registration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(1024) NULL DEFAULT '',
  `salutation` varchar(500) NULL DEFAULT '',
  `first_name` varchar(500) NULL DEFAULT '',
  `last_name` varchar(500) NULL DEFAULT '',
  `phone` int(20) NULL DEFAULT '0',
  `email_id` varchar(1024) NULL DEFAULT '',
  `password` varchar(1024) NULL DEFAULT '',
  `confirm_password` varchar(1024) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL DEFAULT '0',
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL DEFAULT '0',
  `updated_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `applicant` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `is_updated`;


ALTER TABLE `student` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `status`;




ALTER TABLE `applicant` ADD `is_submitted` INT(2) NULL DEFAULT '0' AFTER `email_verified`;



CREATE TABLE `documents` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) NULL DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `documents_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_document` int(20) NULL DEFAULT 0,
  `description` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_documents_program_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(520) NULL DEFAULT '',
  `id_program` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `documents_program_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_documents_program` int(20) NULL DEFAULT 0,
  `id_program` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `hostel_registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `type` varchar(50) DEFAULT '',
  `id_staff` int(20) DEFAULT 0,
  `contact_number` int(20) DEFAULT 0,
  `max_capacity` int(20) DEFAULT 0,
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `hostel_room` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT '0',
  `id_parent` int(20) DEFAULT '0',
  `id_hostel` int(20) DEFAULT '0',
  `max_capacity` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `hostel_room_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `hostel_inventory_registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `hostel_item_registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `inventory_allotment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `level` int(20) DEFAULT '0',
  `id_room` INT(20) NULL DEFAULT '0',
  `id_inventory` INT(20) NULL DEFAULT '0',
  `quantity` INT(20) NULL DEFAULT '0',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `room_allotment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student` int(20) DEFAULT '0',
  `id_hostel` int(20) DEFAULT '0',
  `id_room` INT(20) NULL DEFAULT '0',
  `from_dt` date DEFAULT NULL,
  `to_dt` date DEFAULT NULL,
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;









ALTER TABLE `profile_details` ADD `nric` VARCHAR(50) NULL DEFAULT '' AFTER `permanent_zipcode`;



ALTER TABLE `receipt` ADD `type` VARCHAR(50) NULL DEFAULT '' AFTER `id`;



ALTER TABLE `main_invoice` ADD `type` VARCHAR(50) NULL DEFAULT '' AFTER `id`;


ALTER TABLE `main_invoice` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `type`, ADD `id_intake` INT(20) NULL DEFAULT '0' AFTER `id_program`;









CREATE TABLE `program_entry_requirement` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_program` int(20) DEFAULT '0',
  `description` varchar(2048) DEFAULT '',
  `from_dt` date DEFAULT NULL,
  `to_dt` date DEFAULT NULL,
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `group_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `qualification_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `specialization_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;











CREATE TABLE `entry_requirement_requirements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_requirement_entry` int(20) NULL DEFAULT 0,
  `id_group` int(20) NULL DEFAULT 0,
  `id_qualification` int(20) NULL DEFAULT 0,
  `id_specialization` int(20) NULL DEFAULT 0,
  `validity` int(20) NULL DEFAULT 0,
  `result_item` varchar(2048) NULL DEFAULT '',
  `condition` varchar(2048) NULL DEFAULT '',
  `result_value` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_entry_requirement_requirements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(520) NULL DEFAULT '',
  `id_group` int(20) NULL DEFAULT 0,
  `id_qualification` int(20) NULL DEFAULT 0,
  `id_specialization` int(20) NULL DEFAULT 0,
  `validity` int(20) NULL DEFAULT 0,
  `result_item` varchar(2048) NULL DEFAULT '',
  `condition` varchar(2048) NULL DEFAULT '',
  `result_value` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `entry_requirement_other_requirements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_requirement_entry` int(20) NULL DEFAULT 0,
  `id_group` int(20) NULL DEFAULT 0,
  `condition` varchar(2048) NULL DEFAULT '',
  `type` varchar(2048) NULL DEFAULT '',
  `result_value` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_entry_requirement_other_requirements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(520) NULL DEFAULT '',
  `id_group` int(20) NULL DEFAULT 0,
  `condition` varchar(2048) NULL DEFAULT '',
  `type` varchar(2048) NULL DEFAULT '',
  `result_value` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `program_entry_requirement` CHANGE `from_dt` `from_dt` DATETIME NULL DEFAULT NULL, CHANGE `to_dt` `to_dt` DATETIME NULL DEFAULT NULL;









CREATE TABLE `question_pool` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `question` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_pool` int(20) NULL DEFAULT 0,
  `question` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;









CREATE TABLE `set_form` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(2048) DEFAULT '',
  `id_pool` int(20) NULL DEFAULT 0,
  `id_branch` int(20) NULL DEFAULT 0,
  `id_semester` int(20) NULL DEFAULT 0,
  `id_program` int(20) NULL DEFAULT 0,
  `id_program_scheme` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `set_form_has_question_pool` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_set_form` int(20) NULL DEFAULT 0,
  `id_pool` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






ALTER TABLE `set_form` ADD `is_publish` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;



ALTER TABLE `set_form` ADD `published_by` INT(20) NULL DEFAULT '0' AFTER `is_publish`;





ALTER TABLE `hostel_room` ADD `id_room_type` INT(20) NULL DEFAULT '0' AFTER `id_hostel`;




ALTER TABLE `main_invoice` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `paid_amount`;




ALTER TABLE `receipt` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `remarks`, ADD `id_intake` INT(20) NULL DEFAULT '0' AFTER `id_program`;


ALTER TABLE `receipt` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `status`;




ALTER TABLE `room_allotment` ADD `id_room_type` INT(20) NULL DEFAULT '0' AFTER `id_room`;


ALTER TABLE `applicant` ADD `is_hostel` INT(20) NULL DEFAULT '0' AFTER `nationality`, ADD `id_degree_type` INT(20) NULL DEFAULT '0' AFTER `is_hostel`;



ALTER TABLE `student` ADD `is_hostel` INT(20) NULL DEFAULT '0' AFTER `nationality`, ADD `id_degree_type` INT(20) NULL DEFAULT '0' AFTER `is_hostel`;



ALTER TABLE `student` ADD `id_applicant` INT(20) NULL DEFAULT '0' AFTER `passport_number`;







CREATE TABLE `project_report_submission` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(2048) DEFAULT '',
  `description` varchar(8088) DEFAULT '',
  `id_program` int(20) NULL DEFAULT 0,
  `id_qualification` int(20) NULL DEFAULT 0,
  `id_intake` int(20) NULL DEFAULT 0,
  `id_student` int(20) NULL DEFAULT 0,
  `from_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `to_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `duration` varchar(50) NULL DEFAULT '',
  `reason` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `graduation_list` ADD `message` VARCHAR(2048) NULL DEFAULT '' AFTER `id_convocation`;




CREATE TABLE `graduation_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_graduation` int(20) NULL DEFAULT 0,
  `id_student` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `graduation_list` ADD `date_tm` DATETIME NULL AFTER `id_convocation`;


ALTER TABLE `graduation_list` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `message`;





CREATE TABLE `student_placement`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student` bigint(20) NULL DEFAULT 0,
  `company_name` VARCHAR(1024) NULL DEFAULT '',
  `company_address` VARCHAR(1024) NULL DEFAULT '',
  `phone` bigint(20) NULL DEFAULT 0,
  `email` varchar(1024) NULL DEFAULT '',
  `id_country` bigint(20) NULL DEFAULT 0,
  `id_state` bigint(20) NULL DEFAULT 0,
  `designation` varchar(500) NULL DEFAULT '',
  `joining_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `reason` varchar(50) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




ALTER TABLE `student_placement` ADD `city` VARCHAR(1024) NULL DEFAULT '' AFTER `id_state`, ADD `zipcode` INT(20) NULL DEFAULT NULL AFTER `city`;




ALTER TABLE `student_placement` ADD `id_intake` INT(20) NULL DEFAULT '0' AFTER `id_student`, ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `id_intake`, ADD `id_qualification` INT(20) NULL DEFAULT '0' AFTER `id_program`;





ALTER TABLE `main_invoice` ADD `invoice_total` FLOAT(20,2) NULL DEFAULT '0' AFTER `total_amount`, ADD `total_discount` FLOAT(20,2) NULL DEFAULT '0' AFTER `invoice_total`;







CREATE TABLE `communication_template`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(1024) NULL DEFAULT 0,
  `message` VARCHAR(8194) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `race_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `scheme_has_program`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_scholarship_scheme` bigint(20) NULL DEFAULT 0,
  `id_program` bigint(20) NULL DEFAULT 0,
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_scheme_has_program`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(500) NULL DEFAULT '',
  `id_program` bigint(20) NULL DEFAULT 0,
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_scheme` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1048) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `percentage` int(10) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;








CREATE TABLE `scholarship_application`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student` bigint(20) NULL DEFAULT 0,
  `id_program` bigint(20) NULL DEFAULT 0,
  `id_intake` bigint(20) NULL DEFAULT 0,
  `id_qualification` bigint(20) NULL DEFAULT 0,
  `application_number` VARCHAR(80) NULL DEFAULT '',
  `father_name` VARCHAR(1024) NULL DEFAULT '',
  `mother_name` VARCHAR(1024) NULL DEFAULT '',
  `father_deceased` bigint(20) NULL DEFAULT 0,
  `father_occupation` varchar(1024) NULL DEFAULT '',
  `no_siblings` bigint(20) NULL DEFAULT 0,
  `year` bigint(20) NULL DEFAULT 0,
  `result_item` varchar(500) NULL DEFAULT '',
  `max_marks` bigint(20) NULL DEFAULT 0,
  `obtained_marks` bigint(20) NULL DEFAULT 0,
  `percentage` bigint(20) NULL DEFAULT 0,
  `reason` varchar(50) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







ALTER TABLE `scholarship_application` CHANGE `percentage` `percentage` FLOAT(20,2) NULL DEFAULT '0';


ALTER TABLE `scholarship_application` ADD `id_scholarship_scheme` INT(20) NULL DEFAULT '0' AFTER `application_number`;


ALTER TABLE `scholarship_application` ADD `est_fee` FLOAT(20,2) NULL DEFAULT '0' AFTER `result_item`;



ALTER TABLE `internship_application` ADD `is_reporting` INT(2) NULL DEFAULT '0' AFTER `duration`;


ALTER TABLE `internship_application` CHANGE `to_dt` `to_dt` DATETIME NULL;


ALTER TABLE `add_course_to_program_landscape` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `id_course`, ADD `id_qualification` INT(20) NULL DEFAULT '0' AFTER `id_program`;


ALTER TABLE `communication_template` ADD `subject` VARCHAR(8088) NULL DEFAULT '' AFTER `name`;





CREATE TABLE `communication_template`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(1024) NULL DEFAULT 0,
  `message` VARCHAR(8194) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `scholar` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(1024) NOT NULL COMMENT 'login email',
  `password` varchar(1024) NOT NULL COMMENT 'hashed login password',
  `name` varchar(1024) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `id_role` tinyint(4) NOT NULL,
  `is_ddeleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


ALTER TABLE `scholar` CHANGE `is_ddeleted` `is_deleted` TINYINT(4) NOT NULL DEFAULT '0';




CREATE TABLE `scholar_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `scholar_role_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholar_roles` (
  `id` tinyint(4) NOT NULL COMMENT 'role id' AUTO_INCREMENT PRIMARY KEY,
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;






CREATE TABLE `scholarship_race_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_religion_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `scholarship_scheme` ADD `from_dt` DATE NULL DEFAULT NULL AFTER `percentage`, ADD `to_dt` DATE NULL DEFAULT NULL AFTER `from_dt`;




CREATE TABLE `scholar_applicant_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scholar_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `scholar_applicant` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT '3',
  `is_employee_discount` int(20) DEFAULT '3',
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `scholarship_programme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(250) DEFAULT '',
  `id_award` int(10) DEFAULT '0',
  `code` varchar(50) DEFAULT '',
  `total_cr_hrs` varchar(20) DEFAULT '',
  `graduate_studies` varchar(100) DEFAULT '',
  `foundation` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;











CREATE TABLE `scholarship_documents` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) NULL DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `scholarship_documents_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_document` int(20) NULL DEFAULT 0,
  `description` varchar(2048) NULL DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_scholarship_documents_program_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(520) NULL DEFAULT '',
  `id_program` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_documents_program_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_documents_program` int(20) NULL DEFAULT 0,
  `id_program` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_country` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(2) DEFAULT '0',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(2) DEFAULT '0',
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `scholar_applicant` ADD `id_type` VARCHAR(888) NULL AFTER `is_employee_discount`, ADD `passport_number` VARCHAR(1024) NULL AFTER `id_type`;



CREATE TABLE `scholarship_examination_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scholar_applicant` int(20) DEFAULT NULL,
  `qualification_level` varchar(120) DEFAULT '',
  `degree_awarded` varchar(120) DEFAULT '',
  `specialization` varchar(120) DEFAULT '',
  `class_degree` varchar(120) DEFAULT '',
  `result` varchar(120) DEFAULT '',
  `year` varchar(20) DEFAULT '',
  `medium` varchar(120) DEFAULT '',
  `college_country` varchar(120) DEFAULT '',
  `college_name` varchar(120) DEFAULT '',
  `certificate` varchar(520) DEFAULT '',
  `transcript` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholar_applicant_family_details`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_scholar_applicant` bigint(20) NULL DEFAULT 0,
  `father_name` VARCHAR(1024) NULL DEFAULT '',
  `mother_name` VARCHAR(1024) NULL DEFAULT '',
  `father_deceased` bigint(20) NULL DEFAULT 0,
  `father_occupation` varchar(1024) NULL DEFAULT '',
  `no_siblings` bigint(20) NULL DEFAULT 0,
  `est_fee` bigint(20) NULL DEFAULT 0,
  `family_annual_income` FLOAT(20,2) NOT NULL DEFAULT 0,
  `reason` varchar(50) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;








CREATE TABLE `scholar_applicant_employment_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scholar_applicant` int(10) DEFAULT NULL,
  `company_name` varchar(120) DEFAULT '',
  `company_address` varchar(120) DEFAULT '',
  `telephone` varchar(120) DEFAULT '',
  `fax_num` varchar(120) DEFAULT '',
  `designation` varchar(120) DEFAULT '',
  `position` varchar(120) DEFAULT '',
  `service_year` varchar(120) DEFAULT '',
  `industry` varchar(120) DEFAULT '',
  `job_desc` varchar(520) DEFAULT '',
  `employment_letter` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_applicant` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scholarship_scheme` int(11) NULL DEFAULT 0,
  `id_scholar_applicant` int(11) NULL DEFAULT 0,
  `year` int(11) NULL DEFAULT 0,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT '3',
  `is_employee_discount` int(20) DEFAULT '3',
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `scholarship_applicant` ADD `id_type` VARCHAR(888) NULL AFTER `is_employee_discount`, ADD `passport_number` VARCHAR(1024) NULL AFTER `id_type`;

ALTER TABLE `scholarship_application` ADD `id_scholar_applicant` INT(20) NULL DEFAULT '0' AFTER `id_scholarship_scheme`;


ALTER TABLE `scholarship_examination_details` ADD `id_scholarship_scheme` INT(20) NULL DEFAULT '0' AFTER `id_scholar_applicant`;


ALTER TABLE `scholar_applicant_family_details` ADD `id_scholarship_scheme` INT(20) NULL DEFAULT '0' AFTER `id_scholar_applicant`, ADD `year` INT(20) NULL DEFAULT '0' AFTER `id_scholarship_scheme`;




ALTER TABLE `scholar_applicant_employment_status` ADD `id_scholarship_scheme` INT(20) NULL DEFAULT '0' AFTER `id_scholar_applicant`, ADD `year` INT(20) NULL DEFAULT '0' AFTER `id_scholarship_scheme`, ADD `id_application` INT(20) NULL DEFAULT '0' AFTER `year`;


ALTER TABLE `scholar_applicant_family_details` ADD `id_application` INT(20) NULL DEFAULT '0' AFTER `id_scholar_applicant`;


ALTER TABLE `scholarship_examination_details` ADD `id_application` INT(20) NULL DEFAULT '0' AFTER `id_scholarship_scheme`;


ALTER TABLE `scholarship_applicant` ADD `id_application` INT(20) NULL DEFAULT '0' AFTER `id_scholar_applicant`;



CREATE TABLE `scholarship_amount_calculation_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_bank_registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_frequency_mode` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `scholarship_frequency_mode` 
(`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Yearly', 'Y', 'Y', 1, NULL, '2020-04-16 23:39:42', NULL, '2020-04-16 23:39:42'),
(2, 'Semesterly', 'S', 'S', 1, NULL, '2020-04-16 23:39:57', NULL, '2020-04-16 23:39:57');



CREATE TABLE `scholarship_payment_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `scholarship_payment_type` (`id`, `name`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Cash', 'CASH', '', 1, NULL, '2020-04-16 23:38:14', NULL, '2020-04-16 23:38:14');



CREATE TABLE `scholarship_documents_for_applicant` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) NULL DEFAULT '',
  `name_in_malay` varchar(1024) DEFAULT '',
  `code` varchar(50) NULL DEFAULT '',
  `id_scholarship_program` int(20) DEFAULT NULL,
  `status` int(2) NULL DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1




ALTER TABLE `scholarship_programme` 
ADD `is_personel_details` INT NULL DEFAULT 1 AFTER `foundation`,
 ADD `is_education_details` INT NULL DEFAULT 1 AFTER `is_personel_details`,
  ADD `is_family_details` INT NULL DEFAULT 1 AFTER `is_education_details`,
   ADD `is_financial_recoverable_details` INT NULL DEFAULT 1 AFTER `is_family_details`,
    ADD `is_file_upload_details` INT NULL DEFAULT 1 AFTER `is_financial_recoverable_details`,
     ADD `is_employment_details` INT NULL DEFAULT 1 AFTER `is_file_upload_details`;






CREATE TABLE `is_scholarship_applicant_personel_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT primary key,
  `id_scholarship_program` int(20) DEFAULT '0',
  `is_year` int(20) DEFAULT 1,
  `is_id_intake` int(20) DEFAULT 1,
  `is_id_program` int(20) DEFAULT 1,
  `is_full_name` int(20) DEFAULT 1,
  `is_salutation` int(20) DEFAULT 1,
  `is_first_name` int(20) DEFAULT 1,
  `is_last_name` int(20) DEFAULT 1,
  `is_nric` int(20) DEFAULT 1,
  `is_passport` int(20) DEFAULT 1,
  `is_phone` int(20) DEFAULT 1,
  `is_email_id` int(20) DEFAULT 1,
  `is_password` int(20) DEFAULT 1,
  `is_passport_expiry_date` int(20) DEFAULT 1,
  `is_gender` int(20) DEFAULT 1,
  `is_date_of_birth` int(20) DEFAULT 1,
  `is_martial_status` int(20) DEFAULT 1,
  `is_religion` int(20) DEFAULT 1,
  `is_nationality` int(20) DEFAULT 1,
  `is_is_hostel` int(20) DEFAULT 1,
  `is_id_degree_type` int(20) DEFAULT 1,
  `is_id_race` int(20) DEFAULT 1,
  `is_mail_address1` int(20) DEFAULT 1,
  `is_mail_address2` int(20) DEFAULT 1,
  `is_mailing_country` int(20) DEFAULT 1,
  `is_mailing_state` int(20) DEFAULT 1,
  `is_mailing_city` int(20) DEFAULT 1,
  `is_mailing_zipcode` int(20) DEFAULT 1,
  `is_permanent_address1` int(20) DEFAULT 1,
  `is_permanent_address2` int(20) DEFAULT 1,
  `is_permanent_country` int(20) DEFAULT 1,
  `is_permanent_state` int(20) DEFAULT 1,
  `is_permanent_city` int(20) DEFAULT 1,
  `is_permanent_zipcode` int(20) DEFAULT 1,
  `is_sibbling_discount` int(20) DEFAULT 1,
  `is_employee_discount` int(20) DEFAULT 1,
  `is_applicant_status` int(20) DEFAULT 1,
  `is_is_sibbling_discount` int(20) DEFAULT 1,
  `is_is_employee_discount` int(20) DEFAULT 1,
  `is_id_type` int(20) DEFAULT 1,
  `is_passport_number` int(20) DEFAULT 1,
  `email_verified` int(2) DEFAULT '0',
  `is_submitted` int(2) DEFAULT '0',
  `is_updated` int(2) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `is_scholarship_applicant_examination_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_qualification_level` int(20) DEFAULT 1,
  `is_degree_awarded` int(20) DEFAULT 1,
  `is_specialization` int(20) DEFAULT 1,
  `is_class_degree` int(20) DEFAULT 1,
  `is_result` int(20) DEFAULT 1,
  `is_year` int(20) DEFAULT 1,
  `is_medium` int(20) DEFAULT 1,
  `is_college_country` int(20) DEFAULT 1,
  `is_college_name` int(20) DEFAULT 1,
  `is_certificate` int(20) DEFAULT 1,
  `is_transcript` int(20) DEFAULT 1,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `is_scholar_applicant_family_details`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_scholarship_program`  int(20) DEFAULT 0,
  `is_father_name`  int(20) DEFAULT 1,
  `is_mother_name`  int(20) DEFAULT 1,
  `is_father_deceased`  int(20) DEFAULT 1,
  `is_father_occupation`  int(20) DEFAULT 1,
  `is_no_siblings`  int(20) DEFAULT 1,
  `is_est_fee`  int(20) DEFAULT 1,
  `is_family_annual_income` int(20) DEFAULT 1,
  `reason` varchar(50) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;








CREATE TABLE `is_scholar_applicant_employment_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scholarship_program` int(10) DEFAULT 0,
  `is_company_name` int(20) DEFAULT 1,
  `is_company_address` int(20) DEFAULT 1,
  `is_telephone` int(20) DEFAULT 1,
  `is_fax_num` int(20) DEFAULT 1,
  `is_designation` int(20) DEFAULT 1,
  `is_position` int(20) DEFAULT 1,
  `is_service_year` int(20) DEFAULT 1,
  `is_industry` int(20) DEFAULT 1,
  `is_job_desc` int(20) DEFAULT 1,
  `is_employment_letter` int(20) DEFAULT 1,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `exam_center_location` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `exam_center` ADD `id_location` INT(20) NULL DEFAULT '0' AFTER `city`;




CREATE TABLE `exam_event` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_location` int(20) DEFAULT 0,
  `id_exam_center` int(20) DEFAULT 0,
  `max_count` int(20) DEFAULT 0,
  `name` varchar(250) DEFAULT '',
  `type` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `from_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `to_dt` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `from_tm` DATETIME  DEFAULT NULL,
  `to_tm` DATETIME  DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




ALTER TABLE `exam_event` CHANGE `from_tm` `from_tm` VARCHAR(50) NULL DEFAULT '', CHANGE `to_tm` `to_tm` VARCHAR(50) NULL DEFAULT '';


ALTER TABLE `exam_registration` ADD `id_location` INT(20) NULL DEFAULT '0' AFTER `id_programme`;


ALTER TABLE `exam_registration` ADD `id_exam` INT(20) NULL DEFAULT '0' AFTER `id`;





CREATE TABLE `exam_register` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_intake` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `status` int(5) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `exam_register` ADD `id_location` INT(2) NULL DEFAULT '0' AFTER `id_course`;

ALTER TABLE `exam_register` ADD `is_attended` INT(2) NULL DEFAULT '0' AFTER `id_student`;

ALTER TABLE `exam_register` ADD `is_marks_entered` INT(2) NULL DEFAULT '0' AFTER `id_student`;







CREATE TABLE `exam_attendence` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_intake` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT NULL,
  `id_exam_event` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `status` int(5) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `exam_attendence_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_exam_attendence` int(20) DEFAULT NULL,
  `id_exam_register` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_exam_event` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `status` int(5) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `exam_attendence` ADD `total` INT(20) NULL DEFAULT '0' AFTER `id_semester`, ADD `present` INT(20) NULL DEFAULT '0' AFTER `total`, ADD `absent` INT(20) NULL DEFAULT '0' AFTER `present`;


ALTER TABLE `exam_attendence` ADD `room` VARCHAR(1024) NULL DEFAULT '' AFTER `id_intake`;







CREATE TABLE `temp_programme_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(500) DEFAULT '',
  `id_scheme` int(20) DEFAULT 0,
  `status` int(5) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `programme_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_program` int(20) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `status` int(5) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;











CREATE TABLE `temp_inventory_allotment` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `level` int(20) DEFAULT '0',
  `id_session` varchar(1024) DEFAULT '',
  `id_inventory` int(20) DEFAULT '0',
  `quantity` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT '0',
  `id_parent` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `bulk_withdraw` CHANGE `id_exam_center` `id_exam_center` INT(20) NULL DEFAULT '0' COMMENT 'it\'s id_exam_event not changed due to change in flow';




CREATE TABLE `faculty_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `staff` ADD `id_faculty_program` INT(20) NULL DEFAULT '0' COMMENT 'as similar to department' AFTER `id_department`;





CREATE TABLE `program_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `partner_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `programme` ADD `id_scheme` INT(20) NULL DEFAULT '0' AFTER `id_award`;


ALTER TABLE `programme` ADD `internal_external` VARCHAR(50) NULL DEFAULT '' AFTER `id_scheme`;







CREATE TABLE `program_has_major_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `program_has_minor_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `program_has_concurrent_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `id_concurrent_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `program_has_acceredation_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `category` varchar(1024) DEFAULT '',
  `type` varchar(50) DEFAULT '',
  `acceredation_dt` datetime DEFAULT NULL,
  `acceredation_number` varchar(50) DEFAULT '',
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `approval_dt` datetime DEFAULT NULL,
  `acceredation_reference` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `programme_has_scheme` CHANGE `id_scheme` `id_program_type` INT(20) NULL DEFAULT '0';



ALTER TABLE `programme_has_scheme` ADD `mode_of_program` VARCHAR(100) NULL DEFAULT '' AFTER `id_program_type`, ADD `mode_of_study` VARCHAR(100) NULL DEFAULT '' AFTER `mode_of_program`, ADD `min_duration` INT(20) NULL DEFAULT '0' AFTER `mode_of_study`, ADD `max_duration` INT(20) NULL DEFAULT '0' AFTER `min_duration`;



ALTER TABLE `programme` ADD `id_partner_category` INT(20) NULL DEFAULT '0' AFTER `id_scheme`, ADD `id_partner_university` INT(20) NULL DEFAULT '0' AFTER `id_partner_category`;



ALTER TABLE `programme` ADD `start_date` DATETIME NULL DEFAULT NULL AFTER `foundation`, ADD `end_date` DATETIME NULL DEFAULT NULL AFTER `start_date`, ADD `certificate` DATETIME NULL DEFAULT NULL AFTER `end_date`;



ALTER TABLE `programme` CHANGE `certificate` `certificate` VARCHAR(200) NULL DEFAULT '';




CREATE TABLE `organisation_comitee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;








CREATE TABLE `partner_university_comitee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `add_course_to_program_landscape` ADD `id_program_major` INT(20) NULL DEFAULT '0' AFTER `id_intake`, ADD `id_program_minor` INT(20) NULL DEFAULT '0' AFTER `id_program_major`;


ALTER TABLE `award` ADD `level` INT(20) NULL DEFAULT '0' AFTER `name`


ALTER TABLE `semester` ADD `name_optional_language` VARCHAR(200) NULL DEFAULT '' AFTER `code`;


ALTER TABLE `semester` ADD `is_countable` INT(2) NULL DEFAULT '0' AFTER `semester_type`;


ALTER TABLE `partner_university` ADD `id_partner_category` INT(20) NULL DEFAULT '0' AFTER `name`, ADD `id_partner_university` INT(20) NULL DEFAULT '0' AFTER `id_partner_category`;



ALTER TABLE `partner_university` ADD `start_date` DATETIME NULL DEFAULT NULL AFTER `id_partner_university`, ADD `end_date` DATETIME NULL DEFAULT NULL AFTER `start_date`, ADD `certificate` DATETIME NULL DEFAULT NULL AFTER `end_date`;



ALTER TABLE `partner_university` CHANGE `certificate` `certificate` VARCHAR(200) NULL DEFAULT '';



ALTER TABLE `organisation` CHANGE `permanent_address` `address1` VARCHAR(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '', CHANGE `correspondence_address` `address2` VARCHAR(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '';

ALTER TABLE `partner_university` CHANGE `permanent_address` `address1` VARCHAR(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '', CHANGE `correspondence_address` `address2` VARCHAR(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '';

ALTER TABLE `partner_university` ADD `id_state` INT(20) NULL DEFAULT '0' AFTER `address2`, ADD `city` VARCHAR(512) NULL DEFAULT '' AFTER `id_state`, ADD `zipcode` INT(20) NULL DEFAULT '0' AFTER `city`;


ALTER TABLE `organisation` ADD `id_state` INT(20) NULL DEFAULT '0' AFTER `address2`, ADD `city` VARCHAR(512) NULL DEFAULT '' AFTER `id_state`, ADD `zipcode` INT(20) NULL DEFAULT '0' AFTER `city`;





CREATE TABLE `grade_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `based_on` varchar(200) DEFAULT '',
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_award` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `grade_setup_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_grade_setup` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `description_optional_language` varchar(1024) DEFAULT '',
  `point` int(20) DEFAULT 0,
  `min_marks` int(20) DEFAULT 0,
  `max_marks` int(20) DEFAULT 0,
  `pass` varchar(20) DEFAULT '',
  `rank` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_grade_setup_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(200) DEFAULT '',
  `id_grade` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `description_optional_language` varchar(1024) DEFAULT '',
  `point` int(20) DEFAULT 0,
  `min_marks` int(20) DEFAULT 0,
  `max_marks` int(20) DEFAULT 0,
  `pass` varchar(20) DEFAULT '',
  `rank` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `course_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `course` ADD `id_faculty_program` INT(20) NULL DEFAULT '0' AFTER `id_department`;


ALTER TABLE `course` ADD `id_course_type` INT(20) NULL DEFAULT '0' AFTER `id_faculty_program`;


ALTER TABLE `course` ADD `class_total_hr` INT(20) NULL DEFAULT '0' AFTER `id_course_type`, ADD `class_recurrence` VARCHAR(50) NULL DEFAULT '' AFTER `class_total_hr`, ADD `class_recurrence_period` VARCHAR(50) NULL DEFAULT '' AFTER `class_recurrence`, ADD `tutorial_total_hr` INT(20) NULL DEFAULT '0' AFTER `class_recurrence_period`, ADD `tutorial_recurrence` VARCHAR(50) NULL DEFAULT '' AFTER `tutorial_total_hr`, ADD `tutorial_recurrence_period` VARCHAR(50) NULL DEFAULT '' AFTER `tutorial_recurrence`, ADD `lab_total_hr` INT(20) NULL DEFAULT '0' AFTER `tutorial_recurrence_period`, ADD `lab_recurrence` VARCHAR(50) NULL DEFAULT '' AFTER `lab_total_hr`, ADD `lab_recurrence_period` VARCHAR(50) NULL DEFAULT '' AFTER `lab_recurrence`;

ALTER TABLE `course` ADD `exam_total_hr` INT(20) NULL DEFAULT '0' AFTER `lab_recurrence_period`;



CREATE TABLE `program_landscape_requisite_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_compulsary_course` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_master_course` int(20) DEFAULT 0,
  `requisite_type` varchar(50) DEFAULT '',
  `min_pass_grade` int(20) DEFAULT 0,
  `min_total_credit` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;








CREATE TABLE `landscape_course_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;









CREATE TABLE `temp_program_landscape_semester_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(250) DEFAULT '',
  `semester_type` varchar(50) DEFAULT '',
  `registration_rule` varchar(50) DEFAULT '',
  `minimum` int(20) DEFAULT NULL,
  `maximum` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `program_landscape_semester_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` int(20) DEFAULT NULL,
  `semester_type` varchar(50) DEFAULT '',
  `registration_rule` varchar(50) DEFAULT '',
  `minimum` int(20) DEFAULT NULL,
  `maximum` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `programme_landscape` ADD `program_scheme` VARCHAR(50) NULL DEFAULT '' AFTER `total_semester`;






CREATE TABLE `program_landscape_requirement_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` int(20) DEFAULT NULL,
  `id_landscape_course_type` int(20) DEFAULT NULL,
  `requirement_type` varchar(50) DEFAULT '',
  `minimum_credit` int(20) DEFAULT NULL,
  `compulsary` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `temp_entry_requirement_requirements` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `result_item`;

ALTER TABLE `entry_requirement_requirements` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `result_item`;


ALTER TABLE `documents_program_details` ADD `id_document` INT(20) NULL DEFAULT '0' AFTER `id_documents_program`;

ALTER TABLE `applicant` ADD `contact_email` VARCHAR(500) NULL DEFAULT '' AFTER `email_id`;

ALTER TABLE `student` ADD `contact_email` VARCHAR(500) NULL DEFAULT '' AFTER `email_id`;


INSERT INTO `scholarship_country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (1, 'Malaysia', '1', '0', current_timestamp(), '0', current_timestamp());

INSERT INTO `scholarship_state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES ('', 'KL', '1', '1', '1', NULL, NULL, current_timestamp());




CREATE TABLE `gpa_cgpa_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `setup_by` varchar(50) DEFAULT '',
  `id_semester` int(20) DEFAULT NULL,
  `id_award` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT NULL,
  `academic_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `roles` ADD `status` INT(20) NULL DEFAULT '0' AFTER `role`;




CREATE TABLE `scholarship_salutation_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `marital_status_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_organisation_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT '0',
  `id_registrar` int(20) DEFAULT '0',
  `date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `contact_number` int(20) DEFAULT '0',
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT '0',
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `scholarship_organisation_setup` (`id`, `name`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'CMS  1', 'CMS 1', 'Exam Center', 'CMS.com 1 ', 1, 1, '2020-06-01 00:00:00', 2147483647, 'email', 'Raajajinagara', '9-2-353/1, Ramanagar, Near Maheshwari School', 3, 'sadsad', 8888, 1, 1, '2020-06-08 20:52:06', NULL, '2020-06-08 20:52:06');



CREATE TABLE `scholarship_department` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `scholarship_organisation_setup_comitee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_organisation_has_department` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_department_has_staff` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_department` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `effective_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `scholarship_organisation_setup` ADD `code` VARCHAR(200) NULL DEFAULT '' AFTER `name`;



CREATE TABLE `scholarship_partner_university` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `name_optional_language` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_registrar` int(20) DEFAULT '0',
  `id_contact_person` int(20) DEFAULT '0',
  `date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `contact_number` int(20) DEFAULT '0',
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `login_id` varchar(1048) DEFAULT '',
  `password` varchar(1048) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_country` int(20) DEFAULT '0',
  `id_state` int(20) DEFAULT '0',
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_partner_university_has_aggrement` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT '0',
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `file` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_partner_university_has_training_center` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `complete_code` varchar(500) DEFAULT '',
  `id_contact_person` int(20) DEFAULT '0',
  `contact_number` int(20) DEFAULT '0',
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_country` int(20) DEFAULT '0',
  `id_state` int(20) DEFAULT '0',
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_award_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `level` int(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `scholarship_assesment_method_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `type` float(20,2) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `scholarship_programme` ADD `start_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `id_award`, ADD `end_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `start_date`;





CREATE TABLE `scholarship_partner_university_has_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `completion_criteria` int(20) DEFAULT NULL,
  `completion_criteria_type` varchar(1024) DEFAULT '',
  `assesment_method` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `scholarship_partner_university_program_has_study_mode` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `mode_of_study` varchar(200) DEFAULT '',
  `min_duration` int(20) DEFAULT NULL,
  `max_duration` int(20) DEFAULT NULL,
  `min_duration_type` varchar(200) DEFAULT '',
  `max_duration_type` varchar(200) DEFAULT '',
  `id_training_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `scholarship_partner_university_program_has_syllabus` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `credit` int(20) DEFAULT NULL,
  `credit_type` varchar(200) DEFAULT '',
  `id_module_type` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `scholarship_partner_university_program_has_internship` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `duration` int(20) DEFAULT NULL,
  `duration_type` varchar(200) DEFAULT '',
  `id_internship_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;









CREATE TABLE `scholarship_partner_university_program_has_apprenticeship` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `duration` int(20) DEFAULT NULL,
  `duration_type` varchar(200) DEFAULT '',
  `id_apprenticeship_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_module_type_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `partner_university` ADD `location` VARCHAR(512) NULL DEFAULT '' AFTER `address2`;

ALTER TABLE `scholarship_partner_university_has_training_center` ADD `location` VARCHAR(512) NULL DEFAULT '' AFTER `address2`;


ALTER TABLE `scholarship_partner_university` ADD `location` VARCHAR(512) NULL DEFAULT '' AFTER `address2`;





CREATE TABLE `scholarship_education_level` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `short_name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_selection_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `calculation_method` varchar(200) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_thrust` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `scholarship_sub_thrust` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_thrust` int(20) DEFAULT 0,
  `id_scholarship_manager` int(20) DEFAULT 0, 
  `scholarship_name` varchar(1024) DEFAULT '',
  `scholarship_short_name` varchar(1024) NULL DEFAULT '',
  `scholarship_code` varchar(200) DEFAULT '',
  `scholarship_name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `scholarship_sub_thrust_has_requirement` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `requirement_entry` VARCHAR(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `scholarship_sub_thrust_requirement_has_age` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `min_age` int(20) DEFAULT NULL,
  `max_age` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_sub_thrust_requirement_has_education` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_qualification` int(20) DEFAULT NULL,
  `education_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_sub_thrust_requirement_has_work_experience` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `min_experience` int(20) DEFAULT NULL,
  `id_specialisation` int(20) DEFAULT NULL,
  `experience_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_sub_thrust_requirement_has_other` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `other_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--  Not Required 
CREATE TABLE `scholarship_permissions_to_roles` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_role` int(20) DEFAULT NULL,
  `id_permission` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_work_specialisation` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_sub_thrust_has_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `scholar_roles` ADD `status` INT(2) NULL DEFAULT '1' AFTER `role`;


ALTER TABLE `scholar_permissions` ADD `status` INT(2) NULL DEFAULT '1' AFTER `description`;



CREATE TABLE `scholarship_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `scholar` ADD `status` INT(20) NULL DEFAULT '0' AFTER `id_role`;



CREATE TABLE `scholarship_program_partner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_thrust` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT NULL,
  `internship` int(20) DEFAULT NULL,
  `apprenticeship` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_program_syllabus` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_thrust` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholarship_program_syllabus_has_module` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_syllabus` int(20) DEFAULT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `credit_hours` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_program_accreditation` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_accreditation_category` int(20) DEFAULT NULL,
  `id_accreditation_type` int(20) DEFAULT NULL,
  `date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `valid_from` datetime DEFAULT CURRENT_TIMESTAMP,
  `valid_to` datetime DEFAULT CURRENT_TIMESTAMP,
  `approval_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `accreditation_number` varchar(2048) DEFAULT '',
  `reference_number` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `scholarship_accreditation_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `scholarship_accreditation_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `scholarship_individual_entry_requirement` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `age` int(20) DEFAULT 0,
  `education` int(20) DEFAULT 0,
  `work_experience` int(20) DEFAULT 0,
  `other` int(20) DEFAULT 0,
  `min_age` int(20) DEFAULT 0,
  `max_age` int(20) DEFAULT 0,
  `id_education_qualification` int(20) DEFAULT 0,
  `education_description` varchar(2048) DEFAULT '',
  `min_work_experience` int(20) DEFAULT 0,
  `id_work_specialisation` int(20) DEFAULT 0,
  `work_description` varchar(2048) DEFAULT '',
  `other_description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `scholar` ADD `first_name` VARCHAR(1024) NULL DEFAULT '' AFTER `name`, ADD `last_name` VARCHAR(1024) NULL DEFAULT '' AFTER `first_name`;


ALTER TABLE `scholar` CHANGE `created_dt_tm` `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `scholar` CHANGE `updated_dt_tm` `updated_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `scholar` ADD `salutation` VARCHAR(50) NULL DEFAULT '' AFTER `password`;


CREATE TABLE `scholarship_sub_thrust_has_entry_requirement` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sub_thrust` int(20) DEFAULT 0,
  `nationality` varchar(200) DEFAULT '',
  `id_race` int(20) DEFAULT 0,
  `age` int(20) DEFAULT 0,
  `education` int(20) DEFAULT 0,
  `income` int(20) DEFAULT 0,
  `other` int(20) DEFAULT 0,
  `min_age` int(20) DEFAULT 0,
  `max_age` int(20) DEFAULT 0,
  `id_education_qualification` int(20) DEFAULT 0,
  `education_description` varchar(2048) DEFAULT '',
  `min_income` int(20) DEFAULT 0,
  `max_income` int(20) DEFAULT 0,
  `income_type` varchar(2048) DEFAULT '',
  `other_description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `temp_scheme_has_program` ADD `id_sub_thrust` INT(20) NULL DEFAULT '0' AFTER `id_program`;

ALTER TABLE `scheme_has_program` ADD `id_sub_thrust` INT(20) NULL DEFAULT '0' AFTER `id_program`;


ALTER TABLE `temp_scheme_has_program` ADD `quota` INT(20) NULL DEFAULT '0' AFTER `id_sub_thrust`;

ALTER TABLE `scheme_has_program` ADD `quota` INT(20) NULL DEFAULT '0' AFTER `id_sub_thrust`;



ALTER TABLE `scholarship_organisation_setup` ADD `fax_number` VARCHAR(50) NULL DEFAULT '' AFTER `email`;


ALTER TABLE `applicant` ADD `approved_dt_tm` DATETIME NULL DEFAULT NULL AFTER `approved_by`;

ALTER TABLE `student` ADD `approved_by` INT(20) NULL DEFAULT '0' AFTER `reason`, ADD `approved_dt_tm` DATETIME NULL AFTER `approved_by`;




CREATE TABLE `credit_transfer` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `institution_type` varchar(2048) DEFAULT '',
  `application_id` varchar(200) DEFAULT '',
  `application_type` varchar(200) DEFAULT '',
  `id_semester` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `applied_by` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `credit_transfer_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_credit_transfer` int(20) DEFAULT 0,
  `id_institution` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `institution_type` varchar(200) DEFAULT '',
  `subject_course` varchar(2048) DEFAULT '',
  `id_equivalent_course` int(20) DEFAULT 0,
  `credit_hours` int(20) DEFAULT 0,
  `remarks` varchar(2048) DEFAULT '',
  `processing_fee` int(20) DEFAULT 0,
  `document_upload` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `temp_credit_transfer_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(500) DEFAULT '',
  `id_institution` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `institution_type` varchar(200) DEFAULT '',
  `subject_course` varchar(2048) DEFAULT '',
  `id_equivalent_course` int(20) DEFAULT 0,
  `credit_hours` int(20) DEFAULT 0,
  `remarks` varchar(2048) DEFAULT '',
  `processing_fee` int(20) DEFAULT 0,
  `document_upload` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






ALTER TABLE `credit_transfer` ADD `reason` VARCHAR(1024) NULL DEFAULT '' AFTER `status`;

ALTER TABLE `applicant` ADD `program_scheme` VARCHAR(200) NULL DEFAULT '' AFTER `mailing_city`;

ALTER TABLE `student` ADD `program_scheme` VARCHAR(200) NULL DEFAULT '' AFTER `mailing_city`;

ALTER TABLE `intake` ADD `is_sibbling_discount` INT(2) NULL DEFAULT '0' AFTER `year`, ADD `is_employee_discount` INT(2) NULL DEFAULT '0' AFTER `is_sibbling_discount`;




CREATE TABLE `attendence_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date_time` datetime DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `program_scheme` varchar(200) DEFAULT '',
  `registration_item` varchar(200) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `min_requirement` int(20) DEFAULT 0,
  `is_barring_applicable` int(2) DEFAULT 0,
  `min_warning` int(20) DEFAULT 0,
  `max_warning` int(20) DEFAULT 0,
  `min_barring` int(20) DEFAULT 0,
  `max_barring` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




ALTER TABLE `visa_details` ADD `visa_number` VARCHAR(200) NULL DEFAULT '' AFTER `malaysian_visa`;


CREATE TABLE `student_visa_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `passport_no` varchar(200) DEFAULT '',
  `visa_type` varchar(200) DEFAULT '',
  `special_pass_type` varchar(200) DEFAULT '',
  `special_pass_remarks` varchar(2048) DEFAULT '',
  `visa_number` varchar(200) DEFAULT '',
  `date_issue` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `special_pass_date_issue` datetime DEFAULT NULL,
  `special_pass_expiry_date` datetime DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `insurance_coverage` varchar(200) DEFAULT '',
  `reminder_type` varchar(200) DEFAULT '',
  `reminder_months_expiry` int(20) DEFAULT 0,
  `reminder_template` varchar(200) DEFAULT '',
  `reminder_remarks` varchar(2048) DEFAULT '',
  `image_visa_1` varchar(200) DEFAULT '',
  `image_visa_2` varchar(200) DEFAULT '',
  `special_pass_number` varchar(200) DEFAULT '',
  `special_pass_image` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `student_insurance_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `insurance_id_visa` int(20) DEFAULT 0,
  `insurance_insurance_type` varchar(200) DEFAULT '',
  `insurance_reference_no` varchar(200) DEFAULT '',
  `insurance_date_issue` datetime DEFAULT NULL,
  `insurance_expiry_date` datetime DEFAULT NULL,
  `insurance_reminder_type` varchar(200) DEFAULT '',
  `insurance_reminder_months_expiry` int(20) DEFAULT 0,
  `insurance_reminder_template` varchar(200) DEFAULT 0,
  `insurance_reminder_remarks` varchar(2048) DEFAULT 0,
  `insurance_cover_letter` varchar(200) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `student_passport_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `passport_number` varchar(200) DEFAULT '',
  `passport_name` varchar(2048) DEFAULT '',
  `passport_country_of_issue` int(20) DEFAULT 0,
  `passport_date_issue` datetime DEFAULT NULL,
  `passport_expiry_date` datetime DEFAULT NULL,
  `passport_reminder_type` varchar(200) DEFAULT '',
  `passport_reminder_months_expiry` int(20) DEFAULT 0,
  `passport_reminder_template` varchar(200) DEFAULT 0,
  `passport_cover_letter` VARCHAR(200) NULL DEFAULT '',
  `passport_reminder_remarks` varchar(2048) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `student` ADD `id_advisor` INT(20) NULL DEFAULT '0' AFTER `id_race`;






CREATE TABLE `advisor_tagging` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_advisor` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `exam_calender` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_semester` int(20) DEFAULT 0,
  `activity` varchar(200) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





DROP TABLE gpa_cgpa_setup_details;
DROP TABLE temp_gpa_cgpa_setup_details;



CREATE TABLE `gpa_cgpa_setup_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_gpa_cgpa` int(20) DEFAULT NULL,
  `min_grade_point` int(20) DEFAULT NULL,
  `max_grade_point` int(20) DEFAULT NULL,
  `description` varchar(5096) DEFAULT '',
  `status_optional_language` varchar(5096) DEFAULT '',
  `probation` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_gpa_cgpa_setup_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(500) DEFAULT '',
  `min_grade_point` int(20) DEFAULT NULL,
  `max_grade_point` int(20) DEFAULT NULL,
  `description` varchar(5096) DEFAULT '',
  `status_optional_language` varchar(5096) DEFAULT '',
  `probation` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `examination_assesment_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type_id` varchar(500) DEFAULT '',
  `type` varchar(500) DEFAULT '',
  `exam` varchar(500) DEFAULT '',
  `id_semester` int(20) DEFAULT NULL,
  `order` int(50) DEFAULT 0,
  `description` varchar(5096) DEFAULT '',
  `description_optional_language` varchar(5096) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `examination_components` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `examination_components` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Internals', 'Int', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);



INSERT INTO `examination_components` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Practicals', 'Pra', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
INSERT INTO `examination_components` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Thorey Examination', 'The', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);









DROP TABLE mark_destribution;
DROP TABLE temp_mark_destribution_details;
DROP TABLE mark_destribution_details;


CREATE TABLE `mark_distribution` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_course_registered` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `reason` VARCHAR(1024) NULL DEFAULT ''
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `temp_mark_distribution_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(500) DEFAULT '',
  `id_component` int(20) DEFAULT NULL,
  `is_pass_compulsary` int(2) DEFAULT NULL,
  `pass_marks` int(20) DEFAULT NULL,
  `max_marks` int(20) DEFAULT NULL,
  `weightage` int(20) DEFAULT NULL,
  `attendance_status` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `mark_distribution_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_mark_distribution` int(20) DEFAULT NULL,
  `id_component` int(20) DEFAULT NULL,
  `is_pass_compulsary` int(2) DEFAULT NULL,
  `pass_marks` int(20) DEFAULT NULL,
  `max_marks` int(20) DEFAULT NULL,
  `weightage` int(20) DEFAULT NULL,
  `attendance_status` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `course_registration` ADD `id_course_registered_landscape` INT(20) NULL DEFAULT '0' AFTER `id_course`;








CREATE TABLE `student_marks_entry`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student` int(20) NULL DEFAULT '0',
  `id_program` int(20) NULL DEFAULT '0',
  `id_intake` int(20) NULL DEFAULT '0',
  `id_course_registered_landscape` int(20) NULL DEFAULT '0',
  `id_course_registration` int(20) NULL DEFAULT '0',
  `id_mark_distribution` int(20) NULL DEFAULT '0',
  `result` varchar(500) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `student_marks_entry` ADD `total_course_marks` INT(20) NULL DEFAULT '0' AFTER `id_mark_distribution`, ADD `total_obtained_marks` INT(20) NULL DEFAULT '0' AFTER `total_course_marks`, ADD `total_min_pass_marks` INT(20) NULL DEFAULT '0' AFTER `total_obtained_marks`;



ALTER TABLE `student_marks_entry` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `status`;



CREATE TABLE `student_marks_entry_details`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student_marks_entry` int(20) NULL DEFAULT '0',
  `id_component` int(20) NULL DEFAULT '0',
  `id_marks_distribution_details` int(20) NULL DEFAULT '0',
  `max_marks` int(20) NULL DEFAULT '0',
  `obtained_marks` int(20) NULL DEFAULT '0',
  `pass_marks` int(20) NULL DEFAULT '0',
  `result` varchar(500) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `student_marks_entry_history`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `total_course_marks` int(20) NULL DEFAULT '0',
  `total_obtained_marks` int(20) NULL DEFAULT '0',
  `total_min_pass_marks` int(20) NULL DEFAULT '0',
  `id_course_registration` int(20) NULL DEFAULT '0',
  `id_student` int(20) NULL DEFAULT '0',
  `id_student_marks_entry` int(20) NULL DEFAULT '0',
  `total_result` varchar(500) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `course_registration` ADD `total_course_marks` INT(20) NULL DEFAULT '0' AFTER `by_student`, ADD `total_obtained_marks` INT(20) NULL DEFAULT '0' AFTER `total_course_marks`, ADD `total_min_pass_marks` INT(20) NULL DEFAULT '0' AFTER `total_obtained_marks`, ADD `total_result` VARCHAR(200) NULL DEFAULT '' AFTER `total_min_pass_marks`;


ALTER TABLE `financial_account_code` ADD `type` VARCHAR(200) NULL DEFAULT '' AFTER `code`;


ALTER TABLE `convocation` ADD `year` INT(20) NULL DEFAULT '0' AFTER `number_of_guest`;



CREATE TABLE `convocation_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_convocation` int(20) DEFAULT 0,
  `fee_item` varchar(1048) DEFAULT '',
  `fee_amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `alumni_discount` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `amount` varchar(520) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp,
  `end_date` datetime DEFAULT current_timestamp,
  `sibbling_status` varchar(50) DEFAULT 'Pending',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `sponser` ADD `code` VARCHAR(50) NULL DEFAULT '' AFTER `mobile_number`, ADD `address2` VARCHAR(1024) NULL DEFAULT '' AFTER `code`, ADD `location` VARCHAR(1024) NULL DEFAULT '' AFTER `address2`;


ALTER TABLE `sponser` ADD `email` VARCHAR(500) NULL DEFAULT '' AFTER `mobile_number`, ADD `fax` VARCHAR(20) NULL DEFAULT '' AFTER `email`;




CREATE TABLE `sponser_fee_info_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sponser` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `id_frequency_mode` int(20) DEFAULT 0,
  `id_calculation_mode` int(20) DEFAULT 0,
  `repeat` int(20) DEFAULT 0,
  `max_repeat` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `sponser_coordinator_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sponser` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `phone` int(20) DEFAULT 0,
  `mobile` int(20) DEFAULT 0,
  `fax` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `sponser_fee_info_details` CHANGE `id_calculation_mode` `calculation_mode` VARCHAR(200) NULL DEFAULT '';


ALTER TABLE `sponser_has_students` ADD `start_date` DATETIME NULL DEFAULT NULL AFTER `amount`, ADD `end_date` DATETIME NULL DEFAULT NULL AFTER `start_date`, ADD `aggrement_no` VARCHAR(200) NULL DEFAULT '' AFTER `end_date`, ADD `id_fee_item` INT(20) NULL DEFAULT '0' AFTER `aggrement_no`, ADD `calculation_mode` VARCHAR(200) NULL DEFAULT '' AFTER `id_fee_item`;


ALTER TABLE `main_invoice` ADD `id_sponser` INT(20) NULL DEFAULT '0' AFTER `id_application`;



ALTER TABLE `receipt` ADD `id_sponser` INT(20) NULL DEFAULT '0' AFTER `id_student`;


CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




ALTER TABLE `course_registration` ADD `is_result_announced` INT(2) NULL DEFAULT '0' AFTER `is_exam_registered`;

ALTER TABLE `exam_registration` ADD `total_capacity` INT(20) NULL DEFAULT '0' AFTER `id_semester`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `total_capacity`, ADD `updated_by` INT(20) NULL DEFAULT '0' AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL DEFAULT NULL AFTER `updated_by`, ADD `id_exam_event` INT(20) NULL DEFAULT '0' AFTER `updated_dt_tm`;


ALTER TABLE `exam_registration` ADD `id_course_registered_landscape` INT(20) NULL DEFAULT '0' AFTER `is_bulk_withdraw`, ADD `id_course_registered` INT(20) NULL DEFAULT '0' AFTER `id_course_registered_landscape`;


CREATE TABLE `examination` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_exam_event` int(20) DEFAULT 0,
  `id_course_registered_landscape` int(20) DEFAULT 0,
  `total_capacity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `bulk_withdraw` ADD `id_course_registered` INT(20) NULL DEFAULT '0' AFTER `id_exam_register`;

ALTER TABLE `mark_distribution` CHANGE `id_course_registered` `id_course_registered` INT(20) NULL DEFAULT NULL COMMENT 'its a id_program_landscape_course typed wrongly';


ALTER TABLE `applicant` ADD `alumni_discount` VARCHAR(50) NULL DEFAULT '' AFTER `employee_discount`;


ALTER TABLE `applicant` ADD `is_alumni_discount` INT(2) NULL DEFAULT '3' AFTER `is_employee_discount`;



CREATE TABLE `applicant_has_alumni_discount` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_applicant` int(20) DEFAULT NULL,
  `alumni_name` varchar(520) DEFAULT '',
  `alumni_nric` varchar(520) DEFAULT '',
  `alumni_email` varchar(520) DEFAULT '',
  `alumni_status` varchar(50) DEFAULT 'Pending',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `rejected_by` int(20) DEFAULT 0,
  `rejected_on` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `intake` ADD `is_alumni_discount` INT(2) NULL DEFAULT '0' AFTER `is_employee_discount`;

ALTER TABLE `intake` ADD `is_temp_offer_letter` INT(2) NULL DEFAULT '1' AFTER `is_alumni_discount`;



CREATE TABLE `credit_note` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(520) DEFAULT '',
  `id_invoice` int(20) DEFAULT NULL,
  `reference_number` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_sponser` int(20) DEFAULT 0,
  `id_type` int(20) DEFAULT 0,
  `ratio` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `debit_note` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(520) DEFAULT '',
  `id_invoice` int(20) DEFAULT NULL,
  `reference_number` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_sponser` int(20) DEFAULT 0,
  `id_type` int(20) DEFAULT 0,
  `ratio` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `department_has_staff` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_department` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `faculty_program_has_staff` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_faculty_program` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `department_has_staff` ADD `id_staff` INT(20) NULL DEFAULT '0' AFTER `id`
ALTER TABLE `faculty_program_has_staff` ADD `id_staff` INT(20) NULL DEFAULT '0' AFTER `id_faculty_program`;


CREATE TABLE `credit_note_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `fee_structure_activity` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_program` int(20) DEFAULT 0,
  `id_activity` int(20) DEFAULT 0,
  `id_fee_setup` int(20) DEFAULT 0,
  `trigger` varchar(1024) DEFAULT '',
  `performa` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `staff` ADD `first_name` VARCHAR(2048) NULL DEFAULT '' AFTER `name`, ADD `last_name` VARCHAR(2048) NULL DEFAULT '' AFTER `first_name`;




CREATE TABLE `student_note` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student` int(20) DEFAULT 0,
  `note` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `student` ADD `alumni_discount` VARCHAR(50) NULL DEFAULT '' AFTER `employee_discount`;

ALTER TABLE `barring` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `id_barring_type`, ADD `id_intake` INT(20) NULL DEFAULT '0' AFTER `id_program`;

ALTER TABLE `barring` ADD `barring_to_date` DATETIME NULL DEFAULT NULL AFTER `barring_date`;

ALTER TABLE `partner_university` ADD `code` VARCHAR(200) NULL DEFAULT '' AFTER `name`;


CREATE TABLE `student_emergency_contact_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student` int(20) DEFAULT 0,
  `relationship` varchar(10240) DEFAULT '',
  `relative_name` varchar(10240) DEFAULT '',
  `relative_address` varchar(10240) DEFAULT '',
  `relative_mobile` varchar(20) DEFAULT '',
  `relative_home_phone` varchar(20) DEFAULT '',
  `relative_office_phone` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `student_marks_entry` ADD `grade` VARCHAR(512) NULL DEFAULT '' AFTER `result`;


ALTER TABLE `course_registration` ADD `grade` VARCHAR(512) NULL DEFAULT '' AFTER `total_result`;

ALTER TABLE `student_marks_entry_history` ADD `grade` VARCHAR(512) NULL DEFAULT '' AFTER `total_result`;


ALTER TABLE `semester` ADD `id_scheme` INT(20) NULL DEFAULT '0' AFTER `id_academic_year`;

ALTER TABLE `semester` ADD `special_semester` INT(2) NULL DEFAULT '0' AFTER `is_countable`;

ALTER TABLE `faculty_program` ADD `type` VARCHAR(200) NULL DEFAULT '' AFTER `description`, ADD `id_partner_university` INT(20) NULL DEFAULT '0' AFTER `type`;



CREATE TABLE `publish_exam_result_date` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `publish_assesment_result_date` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1



DROP TABLE activity_details;


CREATE TABLE `activity_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `fee_structure_activity` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `performa`;


ALTER TABLE `student_marks_entry` ADD `is_remarking` INT(20) NULL DEFAULT '0' AFTER `grade`;



CREATE TABLE `exam_configuration` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(120) DEFAULT '',
  `max_count` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `exam_configuration` (`id`, `name`, `max_count`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Highest', '2', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);











CREATE TABLE `student_remarks_entry`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student_marks_entry` int(20) NULL DEFAULT '0',
  `id_student` int(20) NULL DEFAULT '0',
  `id_program` int(20) NULL DEFAULT '0',
  `id_intake` int(20) NULL DEFAULT '0',
  `id_course_registered_landscape` int(20) NULL DEFAULT '0',
  `id_course_registration` int(20) NULL DEFAULT '0',
  `id_mark_distribution` int(20) NULL DEFAULT '0',
  `result` varchar(500) NULL DEFAULT '',
  `grade` varchar(500) NULL DEFAULT '',
  `announced` int(2) NULL DEFAULT '0',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `student_remarks_entry` ADD `total_course_marks` INT(20) NULL DEFAULT '0' AFTER `id_mark_distribution`, ADD `total_obtained_marks` INT(20) NULL DEFAULT '0' AFTER `total_course_marks`, ADD `total_min_pass_marks` INT(20) NULL DEFAULT '0' AFTER `total_obtained_marks`;



ALTER TABLE `student_remarks_entry` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `status`;






CREATE TABLE `student_remarks_entry_details`
(
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_student_remarks_entry` int(20) NULL DEFAULT '0',
  `id_component` int(20) NULL DEFAULT '0',
  `id_marks_distribution_details` int(20) NULL DEFAULT '0',
  `max_marks` int(20) NULL DEFAULT '0',
  `obtained_marks` int(20) NULL DEFAULT '0',
  `pass_marks` int(20) NULL DEFAULT '0',
  `result` varchar(500) NULL DEFAULT '',
  `status` int(2) NULL DEFAULT '0',
  `created_by` int(20) NULL,
  `created_dt_tm` DATETIME  DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) NULL,
  `updated_dt_tm` DATETIME DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1


ALTER TABLE `student_marks_entry_history` ADD `id_student_remarks_entry` INT(20) NULL DEFAULT '0' AFTER `id_student_marks_entry`;


ALTER TABLE `student_remarks_entry` ADD `updated` INT(2) NULL DEFAULT '0' AFTER `announced`;



ALTER TABLE `applicant` ADD `submitted_date` DATETIME NULL DEFAULT NULL AFTER `is_submitted`;



ALTER TABLE `applicant` ADD `id_program_scheme` INT(20) NULL DEFAULT '0' AFTER `program_scheme`;



ALTER TABLE `student` ADD `id_program_scheme` INT(20) NULL DEFAULT '0' AFTER `program_scheme`;




CREATE TABLE `applicant_has_document` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_applicant` int(20) DEFAULT NULL,
  `id_document` varchar(520) DEFAULT '',
  `file_name` varchar(520) DEFAULT '',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `fee_structure` ADD `id_program_scheme` INT(20) NULL DEFAULT '0' AFTER `id_intake`;


ALTER TABLE `apply_change_programme` ADD `id_new_program_scheme` INT(20) NULL DEFAULT '0' AFTER `id_new_semester`;


CREATE TABLE `apply_change_scheme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `id_new_semester` int(20) DEFAULT 0,
  `id_new_program_scheme` int(20) DEFAULT 0,
  `fee` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `organisation_has_training_center` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT '0',
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `complete_code` varchar(500) DEFAULT '',
  `id_contact_person` int(20) DEFAULT '0',
  `contact_number` int(20) DEFAULT '0',
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT '0',
  `id_state` int(20) DEFAULT '0',
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `organisation_training_center_has_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT '0',
  `id_training_center` int(20) DEFAULT '0',
  `id_program` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `applicant` ADD `id_branch` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;



ALTER TABLE `student` ADD `id_branch` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;

ALTER TABLE `programme` ADD `is_apel` INT(2) NULL DEFAULT '0' AFTER `certificate`;

DROP TABLE applicant_status;
DROP TABLE apex_status;


CREATE TABLE `applicant_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `apex_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `applicant_status` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES 
(NULL, 'NEW/LEAD', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'SUBMITTED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'INCOMPLETE', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'NOTQUALIFIED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'OFFERED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'OFFERED CONDITIONALLY', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'REGISTERED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'REGISTERED CONDITIONALLY', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'COMPLETED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'GRADUATED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'DESEASED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'WITHDRAW', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'DISMISSED/SUSPENDED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);





INSERT INTO `apex_status` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES 
(NULL, 'REGISTERED OAT', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'COMPLETED OAT', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'PASS OAT', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'FAILED OAT', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'COMPLETED FORTPOLIO', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'COMPLETED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'RELEASED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, 'CERTIFIED', '', '', '0', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);



ALTER TABLE `applicant` ADD `id_program_requirement` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;

ALTER TABLE `student` ADD `id_program_requirement` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;


ALTER TABLE `applicant` ADD `is_apeal_applied` INT(2) NULL DEFAULT '3' AFTER `is_alumni_discount`;


ALTER TABLE `applicant` ADD `pathway` VARCHAR(200) NULL DEFAULT '' AFTER `is_apeal_applied`;


ALTER TABLE `student` ADD `pathway` VARCHAR(200) NULL DEFAULT '' AFTER `id_applicant`;

ALTER TABLE `applicant` ADD `apel_reject_reason` VARCHAR(2048) NULL DEFAULT '' AFTER `is_apeal_applied`;

ALTER TABLE `applicant` ADD `id_apeal_status` INT(20) NULL DEFAULT '0' AFTER `is_apeal_applied`;





CREATE TABLE `change_branch` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `id_new_semester` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `id_new_branch` int(20) DEFAULT 0,
  `fee` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `course_registration` ADD `id_semester` INT(20) NULL DEFAULT '0' AFTER `id_course_registered_landscape`;



CREATE TABLE `change_branch_history` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT NULL,
  `id_change_branch` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `id_new_branch` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `fee_structure` ADD `id_program_landscape` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;

ALTER TABLE `fee_structure` ADD `currency` VARCHAR(200) NULL DEFAULT '' AFTER `id_program_landscape`;



ALTER TABLE `main_invoice` ADD `currency` VARCHAR(200) NULL DEFAULT '' AFTER `id_student`;


ALTER TABLE `alumni_discount` ADD `currency` VARCHAR(200) NULL DEFAULT '' AFTER `name`;

ALTER TABLE `employee_discount` ADD `currency` VARCHAR(200) NULL DEFAULT '' AFTER `amount`;


ALTER TABLE `sibbling_discount` ADD `currency` VARCHAR(200) NULL DEFAULT '' AFTER `amount`;

ALTER TABLE `main_invoice_details` ADD `quantity` INT(20) NULL DEFAULT '0' AFTER `amount`, ADD `price` INT(20) NULL DEFAULT '0' AFTER `quantity`, ADD `id_reference` INT(20) NULL DEFAULT '0' AFTER `price`;


ALTER TABLE `main_invoice_details` ADD `description` VARCHAR(1024) NULL DEFAULT '' AFTER `id_reference`;


CREATE TABLE `course_register` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_intake` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp,
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `course_registration` ADD `id_course_register` INT(20) NULL DEFAULT '0' AFTER `id_student`


ALTER TABLE `fee_structure_activity` CHANGE `amount` `amount_local` FLOAT(20,2) NULL DEFAULT '0.00';

ALTER TABLE `fee_structure_activity` ADD `amount_international` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount_local`;

ALTER TABLE `apply_change_status` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `id_student`;


ALTER TABLE `fee_structure` ADD `id_training_center` INT(20) NULL AFTER `id_program_landscape`, ADD `is_installment` INT(20) NULL AFTER `id_training_center`;

ALTER TABLE `fee_structure` CHANGE `id_training_center` `id_training_center` INT(20) NULL DEFAULT '0';

UPDATE `fee_structure` set id_training_center = 0

ALTER TABLE `fee_structure` CHANGE `is_installment` `is_installment` INT(20) NULL DEFAULT '0';

ALTER TABLE `fee_structure` ADD `installments` INT(20) NULL DEFAULT '0' AFTER `is_installment`;


CREATE TABLE `fee_structure_has_training_center` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_fee_structure` int(20) DEFAULT NULL,
  `id_program_landscape` int(20) DEFAULT NULL,
  `id_training_center` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `gpa_cgpa_setup` ADD `id_intake` INT(20) NULL DEFAULT '0' AFTER `id_award`;

ALTER TABLE `award_level` ADD `from_cgpa` INT(20) NULL DEFAULT '0' AFTER `code`, ADD `to_cgpa` INT(20) NULL DEFAULT '0' AFTER `from_cgpa`;


ALTER TABLE `payment_type` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `name`, ADD `description_optional_language` VARCHAR(2048) NULL DEFAULT '' AFTER `description`;

ALTER TABLE `payment_type` ADD `payment_group` VARCHAR(512) NULL DEFAULT '' AFTER `description_optional_language`;



ALTER TABLE `bulk_withdraw` ADD `id_course_registered_landscape` INT(20) NULL DEFAULT '0' AFTER `id_course`;





CREATE TABLE `bulk_withdraw_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `bulk_withdraw` ADD `id_bulk_withdraw_master` INT(20) NULL DEFAULT '0' AFTER `id`;


ALTER TABLE `course_register` ADD `id_course_registered_landscape` INT(20) NULL DEFAULT '0' AFTER `id_student`;

ALTER TABLE `course_register` ADD `id_course` INT(20) NULL DEFAULT '0' AFTER `id_course_registered_landscape`;


ALTER TABLE `bulk_withdraw_master` ADD `id_course_registered_landscape` INT(20) NULL DEFAULT '0' AFTER `id_programme`;

ALTER TABLE `bulk_withdraw_master` ADD `id_course` INT(20) NULL DEFAULT '0' AFTER `id_programme`;

ALTER TABLE `bank_registration` ADD `account_no` VARCHAR(512) NULL DEFAULT '' AFTER `code`;


ALTER TABLE `organisation` ADD `image` VARCHAR(1024) NULL DEFAULT '' AFTER `zipcode`;


ALTER TABLE `add_course_to_program_landscape` ADD `id_program_scheme` INT(20) NULL DEFAULT '0' AFTER `id_program`;


update `add_course_to_program_landscape` set id_program_scheme = 8;




CREATE TABLE `intake_has_branch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_intake` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `registration_date` datetime DEFAULT NULL,
  `registration_time` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `grade_setup_details` CHANGE `point` `point` FLOAT(20,2) NULL DEFAULT '0';

ALTER TABLE `temp_grade_setup_details` CHANGE `point` `point` FLOAT(20,2) NULL DEFAULT '0';

ALTER TABLE `examination_assesment_type` ADD `id_intake` INT(20) NULL DEFAULT '0' AFTER `exam`;

ALTER TABLE `academic_year` ADD `start_date` DATETIME NULL DEFAULT NULL AFTER `name`, ADD `end_date` DATETIME NULL DEFAULT NULL AFTER `start_date`;


CREATE TABLE `scheme_has_nationality` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_scheme` int(20) DEFAULT 0,
  `id_nationality` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `semester_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_semester` int(20) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `intake_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_intake` int(20) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `program_landscape_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `program_landscape_type` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Block', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP), (NULL, 'Semester', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP), (NULL, 'Level', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

ALTER TABLE `programme_landscape` ADD `program_landscape_type` VARCHAR(500) NULL DEFAULT '' AFTER `id_programme`;



CREATE TABLE `program_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `fee_structure_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `name` varchar(520) DEFAULT '',
  `code` varchar(520) DEFAULT '',
  `currency` varchar(520) DEFAULT '',
  `debt_account_code` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `fee_structure_has_program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_fee_structure` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `fee_structure_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_fee_structure` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_fee_item` int(20) DEFAULT NULL,
  `id_frequency_mode` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `fee_structure_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_fee_structure` int(20) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `temp_semester_has_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(500) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `fee_structure_has_training_center` ADD `id_fee_item` INT(20) NULL DEFAULT '0' AFTER `id_training_center`;



CREATE TABLE `course_description_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `code` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `course` ADD `id_course_description` INT(20) NULL DEFAULT '0' AFTER `id_course_type`;


ALTER TABLE `programme` ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_award`;


CREATE TABLE `research_topic_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `code` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `research_topic_category` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Organization Behaviour', '', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP), (NULL, 'Islamic Banking and Cooperatives', '', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

CREATE TABLE `research_topic` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `id_topic_category` int(20) DEFAULT 0,
  `max_candidates` int(20) DEFAULT 0,
  `assignment_requirement` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_topic_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `topic_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_topic` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(1024) DEFAULT '',
  `description` varchar(10240) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







CREATE TABLE `supervisor_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(512) DEFAULT '',
  `description` varchar(10240) DEFAULT '',
  `max_candidates` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `research_interest` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_reason_applying` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `research_colloquium` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(10240) DEFAULT '',
  `url` varchar(1024) DEFAULT '',
  `date_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `research_topic_category` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `name`, ADD `max_candidates` INT(20) NULL DEFAULT '0' AFTER `id_program`;




CREATE TABLE `temp_research_category_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_category_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_category` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `research_colloquium` ADD `to_date` DATETIME NULL AFTER `date_time`;


CREATE TABLE `examiner_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(512) DEFAULT '',
  `description` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `research_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` int(2) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `id_satff` int(20) DEFAULT 0,
  `full_name` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `contact_no` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `research_examiner` CHANGE `id_satff` `id_staff` INT(20) NULL DEFAULT '0';


CREATE TABLE `research_proposal` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(2) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `id_research_category` int(20) DEFAULT NULL,
  `id_research_topic` int(20) DEFAULT 0,
  `synopsis` varchar(10240) DEFAULT '',
  `meeting_date` varchar(200) DEFAULT '',
  `completion_date` varchar(200) DEFAULT '',
  `semester_start_date` varchar(200) DEFAULT '',
  `semester_end_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_research_proposal_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_staff_role` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_proposal_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_proposal` int(20) DEFAULT 0,
  `id_staff_role` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_research_proposal_has_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_examiner_role` int(20) DEFAULT 0,
  `id_examiner` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_proposal_has_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_proposal` int(20) DEFAULT 0,
  `id_examiner_role` int(20) DEFAULT 0,
  `id_examiner` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_articleship` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(2) DEFAULT 0,
  `id_semester` int(20) DEFAULT NULL,
  `start_date` varchar(200) DEFAULT '',
  `end_date` varchar(200) DEFAULT '',
  `assistance` int(2) DEFAULT 0,
  `company_name` varchar(1024) DEFAULT '',
  `designation` varchar(512) DEFAULT '',
  `contact_person` varchar(1024) DEFAULT '',
  `address` varchar(1024) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(1024) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `email` varchar(1024) DEFAULT '',
  `phone_number` int(20) DEFAULT 0,
  `fax` int(20) DEFAULT 0,
  `is_employee` int(2) DEFAULT 0,
  `year_of_service` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_research_articleship_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_staff_role` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_articleship_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_articleship` int(20) DEFAULT 0,
  `id_staff_role` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_research_articleship_has_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_examiner_role` int(20) DEFAULT 0,
  `id_examiner` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_articleship_has_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_articleship` int(20) DEFAULT 0,
  `id_examiner_role` int(20) DEFAULT 0,
  `id_examiner` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `research_professionalpracricepaper` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(2) DEFAULT 0,
  `skill_enhancement` int(2) DEFAULT 0,
  `other_details` varchar(1024) DEFAULT '',
  `proposed_area_ppp` varchar(1024) DEFAULT '',
  `start_date` varchar(200) DEFAULT '',
  `end_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_research_professionalpracricepaper_has_employment` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `company_name` varchar(1024) DEFAULT '',
  `address` varchar(1024) DEFAULT '',
  `position` varchar(1024) DEFAULT '',
  `job_function` varchar(1024) DEFAULT '',
  `from_year`int(20) DEFAULT 0,
  `to_year`int(20) DEFAULT 0,
  `reference_number` varchar(1024) DEFAULT '',
  `reference_address` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_professionalpracricepaper_has_employment` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_professionalpracricepaper` int(20) DEFAULT 0,
  `company_name` varchar(1024) DEFAULT '',
  `address` varchar(1024) DEFAULT '',
  `position` varchar(1024) DEFAULT '',
  `job_function` varchar(1024) DEFAULT '',
  `from_year`int(20) DEFAULT 0,
  `to_year`int(20) DEFAULT 0,
  `reference_number` varchar(1024) DEFAULT '',
  `reference_address` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_research_professionalpracricepaper_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_staff_role` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_professionalpracricepaper_has_supervisors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_professionalpracricepaper` int(20) DEFAULT 0,
  `id_staff_role` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `temp_research_professionalpracricepaper_has_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_examiner_role` int(20) DEFAULT 0,
  `id_examiner` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `research_professionalpracricepaper_has_examiner` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_professionalpracricepaper` int(20) DEFAULT 0,
  `id_examiner_role` int(20) DEFAULT 0,
  `id_examiner` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `research_proposal` ADD `reason` VARCHAR(10240) NULL DEFAULT '' AFTER `status`;

ALTER TABLE `research_articleship` ADD `reason` VARCHAR(10240) NULL DEFAULT '' AFTER `status`;

ALTER TABLE `research_professionalpracricepaper` ADD `reason` VARCHAR(10240) NULL DEFAULT '' AFTER `status`;

CREATE TABLE `course_offered` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_semester` int(20) DEFAULT 0,
  `id_faculty_program` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `minimum_quota` varchar(512) DEFAULT '',
  `maximum_quota` varchar(512) DEFAULT '',
  `reservation` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `organisation_has_training_center` CHANGE `id_organisation` `id_organisation` INT(20) NULL DEFAULT '0' COMMENT 'Traeted as id_partner_university in Case Of Partner_university_has_training_center';

ALTER TABLE `applicant` ADD `id_university` INT(20) NULL DEFAULT '0' AFTER `id_program_requirement`;

ALTER TABLE `student` ADD `id_university` INT(20) NULL DEFAULT '0' AFTER `id_program_requirement`;

ALTER TABLE `applicant` CHANGE `id_program_scheme` `id_program_scheme` INT(20) NULL DEFAULT '0' COMMENT 'it\'s id_learning_mode after flow change';

ALTER TABLE `student` CHANGE `id_program_scheme` `id_program_scheme` INT(20) NULL DEFAULT '0' COMMENT 'it\'s id_learning_mode after flow change'

ALTER TABLE `programme_landscape` CHANGE `program_scheme` `program_scheme` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' COMMENT 'it\'s id_program_scheme (Exactly learning mode after flow change)';

ALTER TABLE `applicant` ADD `id_program_landscape` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;

ALTER TABLE `student` ADD `id_program_landscape` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;

ALTER TABLE `course_registration` CHANGE `id_course_registered_landscape` `id_course_registered_landscape` INT(20) NULL DEFAULT '0' COMMENT 'add_course_to_program_landscape primery_key landscape';

ALTER TABLE `partner_university` ADD `login_id` VARCHAR(500) NULL DEFAULT '' AFTER `zipcode`, ADD `password` VARCHAR(500) NULL DEFAULT '' AFTER `login_id`, ADD `billing_to` VARCHAR(200) NULL DEFAULT '' AFTER `password`;

ALTER TABLE `scholarship_partner_university_has_aggrement` ADD `name` VARCHAR(1024) NULL DEFAULT '' AFTER `id_partner_university`, ADD `id_currency` INT(20) NULL DEFAULT '0' AFTER `name`, ADD `reminder_months` INT(20) NULL DEFAULT '0' AFTER `id_currency`;

CREATE TABLE `partner_university_fee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `fee_type` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `partner_university_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_partner_university` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `student_has_document` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT NULL,
  `id_document` varchar(520) DEFAULT '',
  `file_name` varchar(520) DEFAULT '',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `applicant_has_document` ADD `file` VARCHAR(500) NULL DEFAULT '' AFTER `file_name`;

ALTER TABLE `student_has_document` ADD `file` VARCHAR(500) NULL DEFAULT '' AFTER `file_name`;

ALTER TABLE `add_course_to_program_landscape` CHANGE `id_program_scheme` `id_program_scheme` INT(20) NULL DEFAULT '0' COMMENT 'Treate it as id_learning_mode'

ALTER TABLE `student` ADD `added_by_partner_university` INT(2) NULL DEFAULT '0' AFTER `pathway`;

ALTER TABLE `programme_landscape` CHANGE `program_scheme` `program_scheme` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' COMMENT 'it\'s id_program_scheme ( after flow change)';

ALTER TABLE `programme_landscape` CHANGE `learning_mode` `learning_mode` INT(20) NULL DEFAULT NULL COMMENT 'it\'s id_learning_mode';

UPDATE add_course_to_program_landscape set id_program_scheme = '15' where id_program_landscape = '17'



ALTER TABLE `applicant` ADD `id_program_has_scheme` INT(20) NULL DEFAULT '0' COMMENT 'it\'s the actual id_scheme for the program' AFTER `id_program_scheme`;

ALTER TABLE `student` ADD `id_program_has_scheme` INT(20) NULL DEFAULT '0' COMMENT 'it\'s the actual id_scheme for the program' AFTER `id_program_scheme`;

ALTER TABLE `course_register` ADD `id_program_landscape` INT(20) NULL DEFAULT '0' AFTER `id_programme`;


CREATE TABLE `partner_university_invoice_student_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(20) DEFAULT 0,
  `id_partner_university` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `main_invoice` ADD `no_count` INT(20) NULL DEFAULT '0' AFTER `id_student`;


CREATE TABLE `graduation_stage` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `mile_stone` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `order` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `stage_semester` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_stage` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `type` varchar(2048) DEFAULT '',
  `from` int(20) DEFAULT 0,
  `to` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `award_level` ADD `type` VARCHAR(200) NULL DEFAULT '' AFTER `id_intake`;


CREATE TABLE `mile_stone_tag_semester` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_stage` int(20) DEFAULT 0,
  `type` varchar(2048) DEFAULT '',
  `id_semester` int(20) DEFAULT 0,
  `id_mile_stone` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `research_supervisor` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` int(2) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `id_staff` int(20) DEFAULT 0,
  `full_name` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `contact_no` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `research_supervisor` ADD `salutation` VARCHAR(500) NULL DEFAULT '' AFTER `type`, ADD `id_specialisation` INT(20) NULL DEFAULT '0' AFTER `salutation`, ADD `first_name` VARCHAR(1024) NULL DEFAULT '' AFTER `id_specialisation`, ADD `last_name` VARCHAR(1024) NULL DEFAULT '' AFTER `first_name`;


ALTER TABLE `research_supervisor` ADD `address` VARCHAR(1024) NULL DEFAULT '' AFTER `contact_no`;




CREATE TABLE `supervisor_tagging` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_supervisor` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `student` ADD `id_supervisor` INT(20) NULL DEFAULT '0' AFTER `id_advisor`;

ALTER TABLE `research_supervisor` CHANGE `end_date` `end_date` DATETIME NULL DEFAULT NULL;

ALTER TABLE `research_supervisor` CHANGE `start_date` `start_date` VARCHAR(500) NULL DEFAULT '';


ALTER TABLE `research_supervisor` CHANGE `end_date` `end_date` VARCHAR(500) NULL DEFAULT '';


CREATE TABLE `temp_supervisor_has_specialization` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_specialisation` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `supervisor_has_specialization` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_supervisor` int(20) DEFAULT 0,
  `id_specialisation` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `research_advisory` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `email` varchar(2048) DEFAULT '',
  `course` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `research_readers` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `email` varchar(2048) DEFAULT '',
  `course` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `proposal_defense_comitee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `chairman` varchar(2048) DEFAULT '',
  `id_supervisor` int(20) DEFAULT 0,
  `reader_one` int(20) DEFAULT 0,
  `reader_two` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `research_phd_duration` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `research_chapter` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `research_deliverables` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_phd_duration` int(20) DEFAULT 0,
  `id_chapter` int(20) DEFAULT 0,
  `topic` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `supervisor_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_supervisor` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `research_deliverables_student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `application_number` varchar(200) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `id_phd_duration` int(20) DEFAULT 0,
  `id_chapter` int(20) DEFAULT 0,
  `id_topic` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `add_learning_mode_to_program_landscape` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEy,
  `id_intake` int(10) DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_program_landscape` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_learning_mode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `applicant` ADD `phd_duration` INT(20) NULL DEFAULT '0' AFTER `id_apeal_status`;

ALTER TABLE `student` ADD `phd_duration` INT(20) NULL DEFAULT '0' AFTER `id_branch`;


CREATE TABLE `student_duration_history` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEy,
  `id_student` int(10) DEFAULT NULL,
  `old_phd_duration` int(20) DEFAULT 0,
  `new_phd_duration` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `student` ADD `current_deliverable` VARCHAR(200) NULL DEFAULT '' AFTER `phd_duration`;


CREATE TABLE `student_deliverable_history` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEy,
  `id_student` int(10) DEFAULT NULL,
  `old_deliverable_term` varchar(200) DEFAULT '',
  `new_deliverable_term` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `student_duration_history` ADD `old_deliverable` VARCHAR(200) NULL DEFAULT '' AFTER `id_student`, ADD `new_deliverable` VARCHAR(200) NULL DEFAULT '' AFTER `old_deliverable`;

INSERT INTO `student_deliverable_history` (`id`, `id_student`, `old_deliverable_term`, `new_deliverable_term`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '28', '', 'Dec-2020', '1', '1', CURRENT_TIMESTAMP, '0', CURRENT_TIMESTAMP);





CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_template` int(20) NULL DEFAULT 0,
  `name` varchar(1024) NULL DEFAULT '',
  `type` varchar(200) NULL DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_group` int(20) NULL DEFAULT 0,
  `id_recepient` int(20) NULL DEFAULT 0,
  `type` varchar(200) NULL DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `scholar_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_scholar` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE `communication_template_message` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `communication_group_message` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `communication_group_message_recepients` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_menu` int(20) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `menu` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--- Menu Data For Admission
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Group Setup', 'Admission'),(NULL, 'Specialization', 'Admission'),(NULL, 'Application', 'Admission'),(NULL, 'APEL Approval', 'Admission'),(NULL, 'Sibbling Discount Approval', 'Admission'),(NULL, 'Employee Discount Approval', 'Admission'),(NULL, 'Alumni Discount Approval', 'Admission'),(NULL, 'Application Approval', 'Admission')


--- Menu Data For Registration
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Student Registration', 'Registration'),(NULL, 'Academic Advisor Tagging', 'Registration'),(NULL, 'Course Registration', 'Registration'),(NULL, 'Bulk Withdraw', 'Registration'),(NULL, 'Credit Transfer / Excemption Application', 'Registration'),(NULL, 'Credit Transfer / Excemption Approval', 'Registration'),(NULL, 'Attendence Setup', 'Registration'),(NULL, 'Report Course Count', 'Registration')


--- Menu Data For Records
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Barring Type', 'Records'),(NULL, 'Barring', 'Records'),(NULL, 'Release', 'Records'),(NULL, 'Student Profile', 'Records'),(NULL, 'Student Records', 'Records'),(NULL, 'Change Status', 'Records'),(NULL, 'Apply Change Status', 'Records'),(NULL, 'Apply Change Status Approval', 'Records'),(NULL, 'Visa Details', 'Records'),(NULL, 'Apply Change Program', 'Records'),(NULL, 'Apply Change Program Approval', 'Records'),(NULL, 'Apply Change Scheme', 'Records'),(NULL, 'Apply Change Scheme Approval', 'Records'),(NULL, 'Change Branch', 'Records'),(NULL, 'Change Branch Approval', 'Records')


--- Menu Data For Examination
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Exam Configuration', 'Examination'),(NULL, 'Exam Calender', 'Examination'),(NULL, 'GPA/CGPA Setup', 'Examination'),(NULL, 'Assesment Type', 'Examination'),(NULL, 'Grade Setup', 'Examination'),(NULL, 'Course Grade', 'Examination'),(NULL, 'Publish Exam Result', 'Examination'),(NULL, 'Exam Locaction Setup', 'Examination'),(NULL, 'Exam Center Setup', 'Examination'),(NULL, 'Exam Event', 'Examination'),(NULL, 'Exam Registration', 'Examination'),(NULL, 'Tag Student To Exam Registration', 'Examination'),(NULL, 'Release Exam Slip', 'Examination'),(NULL, 'Publish Exam Result Schedule', 'Examination'),(NULL, 'Publish Assesment Result Schedule', 'Examination'),(NULL, 'Mark Distribution', 'Examination'),(NULL, 'Mark Distribution Approval', 'Examination'),(NULL, 'Student Marks Entry Approval', 'Examination'),(NULL, 'Student Marks Entry Summary', 'Examination'),(NULL, 'Add Remarks', 'Examination'),(NULL, 'Approve Remarks', 'Examination'),(NULL, 'Remarks Summary', 'Examination'),(NULL, 'Transcript', 'Examination'),(NULL, 'Result Slip', 'Examination')



--- Menu Data For Graduation
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Award Level', 'Graduation'),(NULL, 'Convocation Setup', 'Graduation'),(NULL, 'Guest', 'Graduation'),(NULL, 'Graduation List', 'Graduation'),(NULL, 'Approve Graduation Students', 'Graduation')


--- Menu Data For Finance
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Currency Setup', 'Finance'),(NULL, 'Currency Rate Setup', 'Finance'),(NULL, 'Credit Note Type', 'Finance'),(NULL, 'Fee Structure Activity', 'Finance'),(NULL, 'Account Code', 'Finance'),(NULL, 'Bank Registration', 'Finance'),(NULL, 'Amount Calculation Type', 'Finance'),(NULL, 'Frequency Mode', 'Finance'),(NULL, 'Payment Type', 'Finance'),(NULL, 'Fee Category', 'Finance'),(NULL, 'Fee Setup', 'Finance'),(NULL, 'Fee Structure', 'Finance'),(NULL, 'Partner University Fee', 'Finance'),(NULL, 'Sibbling Discount', 'Finance'),(NULL, 'Employee Discount', 'Finance'),(NULL, 'Alumni Discount', 'Finance'),(NULL, 'Statement Of Accounts', 'Finance'),(NULL, 'Invoice Generation', 'Finance'),(NULL, 'Wrong Invoice Generated', 'Finance'),(NULL, 'Invoice Generation', 'Finance'),(NULL, 'Receipt', 'Finance'),(NULL, 'Receipt Approval', 'Finance'),(NULL, 'Sponsor Invoice Generation', 'Finance'),(NULL, 'Sponsor Invoice Cancellation', 'Finance'),(NULL, 'Advance Payment', 'Finance'),(NULL, 'Credit Note Entry', 'Finance'),(NULL, 'Debit Note Entry', 'Finance'),(NULL, 'Refund Entry', 'Finance')



--- Menu Data For Sponsor
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Sponsor', 'Sponsor'),(NULL, 'Sponsor Has Students', 'Sponsor')


--- Menu Data For Hostel
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Room Type', 'Hostel'),(NULL, 'Inventory', 'Hostel'),(NULL, 'Hostel Registration', 'Hostel'),(NULL, 'Apartment Setup', 'Hostel'),(NULL, 'Unit Setup', 'Hostel'),(NULL, 'Room Setup', 'Hostel'),(NULL, 'Room Allotment', 'Hostel'),(NULL, 'Room Allotment Summary', 'Hostel'),(NULL, 'Room Vacancy', 'Hostel')


--- Menu Data For Facility
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Equipments', 'Facility'),(NULL, 'Room Type', 'Facility'),(NULL, 'Building', 'Facility'),(NULL, 'Room Setup', 'Facility'),(NULL, 'Apply For Booking', 'Facility'),(NULL, 'Booking Summary', 'Facility')


--- Menu Data For Internship
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Max. Internship Limit', 'Internship'),(NULL, 'Company Type', 'Internship'),(NULL, 'Company Registration', 'Internship'),(NULL, 'Application', 'Internship'),(NULL, 'Application Approval', 'Internship'),(NULL, 'Reporting', 'Internship'),(NULL, 'Placement Form', 'Internship')


--- Menu Data For ALUMNI
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'ALUMNI List', 'Alumni')


--- Menu Data For Communication
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'Templates', 'Communication'),(NULL, 'Group', 'Communication'),(NULL, 'Group Recepients', 'Communication'),(NULL, 'Messaging Templates', 'Communication'),(NULL, 'Messaging Group', 'Communication'),(NULL, 'Group Recepients (Messaging)', 'Communication')


--- Menu Data For IPS
INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES (NULL, 'IPS Topics', 'IPS'),(NULL, 'Research Status', 'IPS'),(NULL, 'Field Of Interest', 'IPS'),(NULL, 'Reason For Applying', 'IPS'),(NULL, 'Research Category', 'IPS'),(NULL, 'Colloquium Setup', 'IPS'),(NULL, 'Advisory', 'IPS'),(NULL, 'Readers', 'IPS'),(NULL, 'Proposal Defense Comitee', 'IPS'),(NULL, 'Chapter', 'IPS'),(NULL, 'Deliverables', 'IPS'),(NULL, 'Submitted Deliverables', 'IPS'),(NULL, 'Phd Duration Tagging', 'IPS'),(NULL, 'Stage', 'IPS'),(NULL, 'Mile Stone', 'IPS'),(NULL, 'Tag Mile Stone To Semester', 'IPS'),(NULL, 'Overview', 'IPS'),(NULL, 'Supervisor Role', 'IPS'),(NULL, 'Supervisor', 'IPS'),(NULL, 'Supervisor Tagging', 'IPS'),(NULL, 'Examiner Role', 'IPS'),(NULL, 'Examiner', 'IPS'),(NULL, 'Proposal', 'IPS'),(NULL, 'Articleship', 'IPS'),(NULL, 'Professional Practice Paper', 'IPS'),(NULL, 'Proposal Approval', 'IPS'),(NULL, 'Articleship Approval', 'IPS'),(NULL, 'PPP Approval', 'IPS')


ALTER TABLE `permissions` ADD `module` VARCHAR(512) NULL DEFAULT '' AFTER `id_menu`;


ALTER TABLE `programme` ADD `mode` VARCHAR(10) NULL DEFAULT '' AFTER `is_apel`;


CREATE TABLE `supervisor_change_application` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `id_supervisor_old` int(20) DEFAULT 0,
  `id_supervisor_new` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `supervisor_tagging` ADD `is_change_application` INT(20) NULL DEFAULT '0' AFTER `id_student`;


ALTER TABLE `supervisor_tagging` CHANGE `is_change_application` `is_change_application` INT(20) NULL DEFAULT '0' COMMENT 'its a id to get that this change is requested by student for supervisor change';


CREATE TABLE `student_progress` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `phd_duration` varchar(200) DEFAULT '',
  `current_deliverable` varchar(200) DEFAULT '',
  `education_level` varchar(200) DEFAULT '',
  `status` varchar(200) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `applicant` ADD `present_address_same_as_mailing_address` INT(20) NULL DEFAULT '0' AFTER `mailing_zipcode`;

ALTER TABLE `student` ADD `present_address_same_as_mailing_address` INT(20) NULL DEFAULT '0' AFTER `mailing_zipcode`;

UPDATE `examination_components` SET `name` = 'Exam', `code` = 'E' WHERE `examination_components`.`id` = 3;

UPDATE `examination_components` SET `name` = 'Workshop', `code` = 'W' WHERE `examination_components`.`id` = 2;

UPDATE `examination_components` SET `name` = 'Assignment', `code` = 'A' WHERE `examination_components`.`id` = 1;

ALTER TABLE `mark_distribution` ADD `id_programme_landscape` INT(20) NULL DEFAULT '0' AFTER `id_program`;

ALTER TABLE `examination` ADD `id_programme_landscape` INT(20) NULL DEFAULT '0' AFTER `id_programme`;



556 -> Role Permission End id Before Creating AEU Admin


ALTER TABLE `student` ADD `current_semester` INT(20) NULL DEFAULT '0' AFTER `phd_duration`;

ALTER TABLE `apply_change_programme` ADD `id_new_program_has_scheme` INT(20) NULL DEFAULT '0' AFTER `id_new_program_scheme`;


ALTER TABLE `student` ADD `matrix_number` VARCHAR(200) NULL DEFAULT '' AFTER `last_name`;

ALTER TABLE `apply_change_programme` ADD `reject_reason` VARCHAR(2048) NULL DEFAULT '' AFTER `fee`;

CREATE TABLE `research_advisor` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` int(2) DEFAULT 0,
  `salutation` varchar(500) DEFAULT '',
  `id_specialisation` int(20) DEFAULT 0,
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `start_date` varchar(500) DEFAULT '',
  `end_date` varchar(500) DEFAULT '',
  `id_staff` int(20) DEFAULT 0,
  `full_name` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `contact_no` int(20) DEFAULT 0,
  `address` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_advisor_has_specialization` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_specialisation` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `advisor_has_specialization` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_advisor` int(20) DEFAULT 0,
  `id_specialisation` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `student` ADD `id_phd_advisor` INT(20) NULL DEFAULT '0' AFTER `id_advisor`;

ALTER TABLE `research_advisor` CHANGE `contact_no` `contact_no` VARCHAR(50) NULL DEFAULT '';

ALTER TABLE `research_supervisor` CHANGE `contact_no` `contact_no` VARCHAR(50) NULL DEFAULT '';

ALTER TABLE `research_examiner` CHANGE `contact_no` `contact_no` VARCHAR(50) NULL DEFAULT '';

ALTER TABLE `applicant` ADD `accept_date` DATE NULL AFTER `status`;

ALTER TABLE `student` ADD `accept_date` DATE NULL AFTER `status`;

ALTER TABLE `course_register` ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_intake`;

ALTER TABLE `research_colloquium` ADD `mode` INT(20) NULL DEFAULT '0' AFTER `to_date`, ADD `attendance` INT(20) NULL DEFAULT '0' AFTER `mode`, ADD `registration_start_date` DATETIME NULL DEFAULT NULL AFTER `attendance`, ADD `registration_end_date` DATETIME NULL DEFAULT NULL AFTER `registration_start_date`;

CREATE TABLE `temp_research_colloquium_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(520) DEFAULT '',
  `id_programme` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `research_colloquium_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_colloquium` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `research_colloquium_student_interest` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_colloquium` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `by_student` int(20) DEFAULT 0,
  `date_time` DATETIME DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `semester` ADD `year` VARCHAR(20) NULL DEFAULT '' AFTER `semester_sequence`;


ALTER TABLE `intake` ADD `code` VARCHAR(200) NULL DEFAULT '' AFTER `year`;


ALTER TABLE `research_colloquium_student_interest` ADD `attenence_type` VARCHAR(512) NULL DEFAULT '' AFTER `date_time`, ADD `is_attended` INT(20) NULL DEFAULT '0' AFTER `attenence_type`, ADD `comments` VARCHAR(10240) NULL DEFAULT '' AFTER `is_attended`;


ALTER TABLE `research_colloquium` ADD `is_attendance` INT(20) NULL DEFAULT '0' AFTER `attendance`;


ALTER TABLE `research_status` ADD `is_pass` INT(20) NULL DEFAULT '0' AFTER `id_course`;


ALTER TABLE `applicant` ADD `id_program_structure_type` INT(20) NULL DEFAULT '0' AFTER `is_updated`;


ALTER TABLE `student` ADD `id_program_structure_type` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`;

ALTER TABLE `proposal_defense_comitee` ADD `name` VARCHAR(1024) NULL DEFAULT '' AFTER `chairman`, ADD `year` INT(20) NULL DEFAULT '0' AFTER `name`, ADD `date_time` DATETIME NULL DEFAULT NULL AFTER `year`;


CREATE TABLE `research_commitee_student_interest` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_comitee` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `by_student` int(20) DEFAULT 0,
  `date_time` DATETIME DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `research_commitee_student_interest` ADD `id_research_status` INT(20) NULL DEFAULT '0' AFTER `id_student`;


ALTER TABLE `course_type` ADD `credit_hours_mandatory` INT(20) NULL DEFAULT '0' AFTER `code`, ADD `ppp` INT(20) NULL DEFAULT '0' AFTER `credit_hours_mandatory`, ADD `project_paper` INT(20) NULL DEFAULT '0' AFTER `ppp`, ADD `class_time_table` INT(20) NULL DEFAULT '0' AFTER `project_paper`, ADD `exam_time_table` INT(20) NULL DEFAULT '0' AFTER `class_time_table`, ADD `articleship` INT(20) NULL DEFAULT '0' AFTER `exam_time_table`;

CREATE TABLE `program_objective` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `programme_landscape` ADD `code` VARCHAR(2048) NULL DEFAULT '' AFTER `name`, ADD `id_intake_to` INT(20) NULL DEFAULT '0' AFTER `code`;

CREATE TABLE `add_program_objective_to_program_landscape` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEy,
  `id_intake` int(10) DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_objective` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_course_registration_type` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `temp_credit_hour_program_landscape_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(2048) DEFAULT '',
  `id_landscape_course_type` int(20) DEFAULT 0,
  `hours` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `credit_hour_program_landscape_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_landscape_course_type` int(20) DEFAULT 0,
  `hours` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `fee_structure_master` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `id_education_level` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_intake_to` int(20) DEFAULT 0,
  `id_learning_mode` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `applicant` ADD `id_fee_structure` INT(20) NULL DEFAULT '0' AFTER `id_program_has_scheme`;


ALTER TABLE `student` ADD `id_fee_structure` INT(20) NULL DEFAULT '0' AFTER `id_program_has_scheme`;

ALTER TABLE `fee_structure` CHANGE `id_program_landscape` `id_program_landscape` INT(20) NULL DEFAULT '0' COMMENT 'its a id_fee_structure_master After Flow Change';

ALTER TABLE `main_invoice` ADD `is_migrate_applicant` INT(20) NULL DEFAULT '0' AFTER `paid_amount`;

INSERT INTO `applicant` (`id`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `contact_email`, `password`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `program_scheme`, `id_program_scheme`, `id_program_has_scheme`, `id_fee_structure`, `id_program_landscape`, `id_program_requirement`, `id_university`, `id_branch`, `mailing_zipcode`, `present_address_same_as_mailing_address`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `alumni_discount`, `applicant_status`, `is_sibbling_discount`, `is_employee_discount`, `is_alumni_discount`, `is_apeal_applied`, `id_apeal_status`, `phd_duration`, `apel_reject_reason`, `pathway`, `email_verified`, `is_submitted`, `submitted_date`, `is_updated`, `id_program_structure_type`, `reason`, `status`, `accept_date`, `approved_by`, `approved_dt_tm`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '1', '6', 'DATUKSERIDR. Developper Testing2', '15', 'Developper', 'Testing2', 'DT!NRIC', '', '88888001', 'dt2@cms.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2020-12-18', 'Single', '3', 'Malaysian', '1', '1', '2', 'KL, Dala Street', 'KL, Dala Street', '110', '1', 'KL', 'Face to Face - Full Time', '21', '2', '1', '21', '1', '1', '3', '123345', '1', 'KL KL, Dala Street', 'KL, Dala Street', '110', '1', 'KL', '123', '', 'Yes', '', 'Migrated', '3', '1', '3', '1', '8', '1', '', 'APEL', '1', '1', '2020-12-08 00:56:18', '1', '1', '', NULL, NULL, '1', NULL, NULL, '2020-12-07 11:22:14', NULL, '2020-12-07 11:22:14');

ALTER TABLE `staff` ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_faculty_program`;

CREATE TABLE `staff_teaching_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_mode_of_study` int(20) DEFAULT 0,
  `id_learning_center` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `staff_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES 
(NULL, '', 'Active', '', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, '', 'Inactive', '', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, '', 'Terminated', '', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, '', 'Suspended', '', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, '', 'Deceased', '', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),
(NULL, '', 'Quit', '', '1', '1', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);


CREATE TABLE `staff_change_status_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_change_status` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `staff_leave_records` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `from_dt` varchar(200) DEFAULT '',
  `to_dt` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `payment_rate` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_award` int(20) DEFAULT 0,
  `learning_mode` varchar(200) DEFAULT '',
  `teaching_component` varchar(200) DEFAULT '',
  `id_currency` int(20) DEFAULT 0,
  `calculation_type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `payment_rate` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `calculation_type`;


ALTER TABLE `staff_change_status_details` ADD `from_dt` VARCHAR(200) NULL DEFAULT '0' AFTER `id_change_status`, ADD `to_dt` VARCHAR(200) NULL DEFAULT '0' AFTER `from_dt`;


INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '', 'Sabbatical Leave', '', '1', '1', '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');

INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '', 'Long MC', '', '1', '1', '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');

INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '', 'Maternity', '', '1', '1', '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');

ALTER TABLE `staff_change_status_details` ADD `reason` VARCHAR(2048) NULL DEFAULT '' AFTER `to_dt`;


CREATE TABLE `staff_bank_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `bank_account_name` varchar(500) DEFAULT '',
  `bank_code` varchar(500) DEFAULT '',
  `bank_address` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `staff_bank_details` ADD `bank_account_number` VARCHAR(500) NULL DEFAULT '' AFTER `bank_code`;

ALTER TABLE `research_deliverables_student` ADD `stage` INT(20) NULL DEFAULT '0' AFTER `application_number`;


CREATE TABLE `research_proposal_reporting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `application_number` varchar(200) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `id_student` bigint(20) NOT NULL,
  `id_supervisor` bigint(20) NOT NULL,
  `phd_duration` int(20) DEFAULT 0,
  `id_deliverable` int(20) DEFAULT 0,
  `id_chapter` int(20) DEFAULT 0,
  `from_dt` varchar(200) DEFAULT '',
  `to_dt` varchar(200) DEFAULT '',
  `target_dt` varchar(200) DEFAULT '',
  `stage` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `proposal_reporting_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_proposal_reporting` bigint(20) NOT NULL,
  `comments` varchar(200) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(2048) DEFAULT '',
  `stage` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `research_deliverables_student` ADD `upload_file` VARCHAR(512) NULL DEFAULT '' AFTER `id_topic`;


CREATE TABLE `research_toc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` bigint(20) NOT NULL,
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(512) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `toc_reporting_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_toc` bigint(20) NOT NULL,
  `comments` varchar(200) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `research_abstract` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` bigint(20) NOT NULL,
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(512) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `abstract_reporting_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_abstract` bigint(20) NOT NULL,
  `comments` varchar(200) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- Soft Copy Of Files


CREATE TABLE `research_sco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` bigint(20) NOT NULL,
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(512) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `sco_reporting_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sco` bigint(20) NOT NULL,
  `comments` varchar(200) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





-- PPT

CREATE TABLE `research_ppt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` bigint(20) NOT NULL,
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(512) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `ppt_reporting_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_ppt` bigint(20) NOT NULL,
  `comments` varchar(200) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





--  % Copy Of Research Bound

CREATE TABLE `research_bound` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` bigint(20) NOT NULL,
  `id_supervisor` bigint(20) NOT NULL,
  `description` varchar(2048) DEFAULT '',
  `upload_file` varchar(512) DEFAULT '',
  `upload_file2` varchar(512) DEFAULT '',
  `upload_file3` varchar(512) DEFAULT '',
  `upload_file4` varchar(512) DEFAULT '',
  `upload_file5` varchar(512) DEFAULT '',
  `approved_on` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `bound_reporting_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_bound` bigint(20) NOT NULL,
  `comments` varchar(200) DEFAULT '',
  `id_supervisor` bigint(20) NOT NULL,
  `upload_file` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `online_claim` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(256) NOT NULL,
  `id_staff` bigint(20) NOT NULL,
  `id_university` bigint(20) NOT NULL,
  `id_branch` bigint(20) NOT NULL,
  `id_programme` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `total_amount` float(20,2) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `research_proposal_reporting` CHANGE `status` `status` INT(20) NULL DEFAULT '1';

CREATE TABLE `temp_online_claim_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(256) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `online_claim_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_online_claim` int(20) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `student_semester_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `old_semester` int(20) NOT NULL,
  `new_semester` int(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- Need TO remove THese Fields From The Fee Structure Master Table

ALTER TABLE `course_registration` ADD `student_current_semester` INT(20) NULL DEFAULT '0' AFTER `id_student`;

ALTER TABLE `fee_structure_master` ADD `id_aggrement` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`, ADD `id_partner_university` INT(20) NULL DEFAULT '0' AFTER `id_aggrement`;

ALTER TABLE `fee_structure_master` ADD `is_installment` INT(20) NULL DEFAULT '0' AFTER `id_program_scheme`, ADD `id_fee_item` INT(20) NULL DEFAULT '0' AFTER `is_installment`, ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_fee_item`

ALTER TABLE `fee_structure_master` ADD `id_currency` INT(20) NULL DEFAULT '0' AFTER `is_installment`, ADD `installments` INT(20) NULL DEFAULT '0' AFTER `id_currency`;

ALTER TABLE `fee_structure_has_training_center` ADD `id_fee_structure_master` INT(20) NULL DEFAULT '0' AFTER `id_fee_structure`;

ALTER TABLE `fee_structure` ADD `id_aggrement` INT(20) NULL DEFAULT '0' AFTER `id_programme`;

ALTER TABLE `fee_structure` ADD `id_partner_university` INT(20) NULL DEFAULT '0' AFTER `id_aggrement`;

CREATE TABLE `fee_structure_triggering_point` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) NOT NULL,
  `name_optional_language` varchar(2048) NOT NULL,
  `code` varchar(2048) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `fee_structure_triggering_point` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'APPLICATION REGISTRATION', '', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

INSERT INTO `fee_structure_triggering_point` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'COURSE REGISTRATION', '', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'SEMESTER PROMOTION', '', '', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

ALTER TABLE `fee_structure` ADD `id_fee_structure_trigger` INT(20) NULL DEFAULT '0' AFTER `id_fee_item`;

ALTER TABLE `fee_structure_has_training_center` ADD `id_fee_structure_trigger` INT(20) NULL DEFAULT '0' AFTER `id_fee_item`;


ALTER TABLE `partner_university` ADD `pay_as_agent` INT(20) NULL DEFAULT '0' AFTER `id_registrar`;

ALTER TABLE `partner_university` ADD `id_bank` INT(20) NULL DEFAULT '0' AFTER `billing_to`, ADD `account_number` VARCHAR(512) NULL DEFAULT '' AFTER `id_bank`, ADD `swift_code` VARCHAR(512) NULL DEFAULT '' AFTER `account_number`, ADD `bank_address` VARCHAR(2048) NULL DEFAULT '' AFTER `swift_code`;

ALTER TABLE `applicant` ADD `is_invoice_generated` INT(20) NULL DEFAULT '0' AFTER `is_apeal_applied`;

-- For Student Individual Invoices
CREATE TABLE `partner_university_student_invoice_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student_invoice_details` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `quantity` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `main_invoice` ADD `applicant_partner_fee` INT(20) NULL DEFAULT '0' AFTER `no_count`;

-- Not Req


ALTER TABLE `menu` ADD `parent_order` VARCHAR(200) NULL DEFAULT '' AFTER `module_name`;

ALTER TABLE `menu` ADD `order` INT(20) NULL DEFAULT '0' AFTER `parent_order`, ADD `controller` VARCHAR(512) NULL DEFAULT '' AFTER `order`, ADD `action` VARCHAR(512) NULL DEFAULT '' AFTER `controller`, ADD `parent_name` INT(20) NULL DEFAULT '0' AFTER `action`;

ALTER TABLE `student` DROP `is_invoice_generated`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Payment Rate', 'af', 'Setup', '1', 'paymentRate', 'list', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Student Semester Promotion', 'Registration', 'Registration', '5', 'studentSemester', 'add', '1');

ALTER TABLE `receipt` ADD `currency` VARCHAR(200) NULL DEFAULT '' AFTER `id_sponser`;

drop TABLE nationality;

CREATE TABLE `nationality` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) NOT NULL,
  `code` varchar(2048) NOT NULL,
  `country` varchar(2048) NOT NULL,
  `person` varchar(2048) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `nationality` (`id`, `name`, `code`, `country`, `person`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Malaysian', 'MY', 'Malaysia', 'A Malaysian', '1', '1', current_timestamp(), NULL, current_timestamp());

INSERT INTO `nationality` (`id`, `name`, `code`, `country`, `person`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Other', 'OTH', 'Other', 'Other', '1', '1', current_timestamp(), NULL, current_timestamp());

------ Update CMS From Here ------












































